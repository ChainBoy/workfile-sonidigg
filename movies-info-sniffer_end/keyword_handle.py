#!/bin/python
# -*- coding: utf-8 -*- 

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

import jieba.analyse

from model import Movie_Keyword, Movie

class Keyword_Handler(object):
    """关键词词频处理"""
    def __init__(self, db_path):
        super(Keyword_Handler, self).__init__()
        engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine)) 

    def handle(self, key, movie):
        movie_keyword = self.db.query(Movie_Keyword).filter(Movie_Keyword.movie == movie, Movie_Keyword.key == key).first()
        if movie_keyword:
            movie_keyword.frequency += 1
        else:
            self.db.add(Movie_Keyword(key, 1, movie))
        self.db.commit()

if __name__ == '__main__':
    pass
