from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, BigInteger, Text
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
Base = declarative_base()
db_path = 'mysql://root:123@localhost:3306/newyear2?charset=utf8'

class Movie(Base):
    __tablename__ = 'movies'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<movies(%s')>" % (self.name)

class Detail(Base):
    __tablename__ = 'details'
    # id -pk 
    # movie_id - fk
    #address string
    #gender 
    #follows Fomr sina
    id = Column(BigInteger, primary_key=True)
    movie = Column(Integer, ForeignKey('movies.id'))
    address = Column(String(15))
    gender = Column(Integer)
    focus_num = Column(Integer)
    fans_num = Column(Integer)
    wb_num = Column(Integer)
    nick_name = Column(String(30))
    daren = Column(Integer)
    vip = Column(Integer)
    face = Column(String(100))
    follows = Column(String(20))
    time = Column(DateTime)
    ping_num = Column(Integer)
    zan_num = Column(Integer)
    zhuan_num = Column(Integer)
    content = Column(String(300))
    # song_times = relationship("Song", backref=backref("listen_times", uselist=False))

class Movie_Keyword(Base):
    __tablename__='Movie_Keyword'
    id = Column(Integer,primary_key=True)
    key = Column(String(200))
    frequency = Column(Integer(11))
    movie = Column(Integer(11), ForeignKey('movies.id'))

    def __init__(self, key, frequency, movie):
        self.key = key
        self.frequency = frequency
        self.movie = movie

def init_db(engine):
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
    init_db(engine)
