#!/bin/python
# -*- coding: utf-8 -*- 
import requests
import json
import re
import base64
import urllib, urllib2
import rsa, binascii

USER_NAME = "b1n2h3"
PASSWORD = "nihao1234"

URL_PRELOGIN = 'http://login.sina.com.cn/sso/prelogin.php?entry=weibo&callback=sinaSSOController.preloginCallBack&su=&rsakt=mod&client=ssologin.js(v1.4.5)&_=1364875106625'
URL_LOGIN = 'http://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.4.5)'

def login():
    session = requests.Session()
    ret = session.get(URL_PRELOGIN)
    data = json.loads(re.findall('({.*?})', ret.content)[0])

    servertime = data['servertime']
    nonce = data['nonce']
    pubkey = data['pubkey']
    rsakv = data['rsakv']

    su = base64.b64encode(urllib.quote(USER_NAME))
    rsaPublickey = int(pubkey, 16)
    key = rsa.PublicKey(rsaPublickey, 65537)
    message = str(servertime) + '\t' + str(nonce) + '\n' + str(PASSWORD)
    sp = binascii.b2a_hex(rsa.encrypt(message, key))

    postdata = {
                    'entry': 'weibo',
                    'gateway': '1',
                    'from': '',
                    'savestate': '7',
                    'userticket': '1',
                    'ssosimplelogin': '1',
                    'vsnf': '1',
                    'vsnval': '',
                    'su': su,
                    'service': 'miniblog',
                    'servertime': servertime,
                    'nonce': nonce,
                    'pwencode': 'rsa2',
                    'sp': sp,
                    'encoding': 'UTF-8',
                    'url': 'http://weibo.com/ajaxlogin.php?framelogin=1&callback=parent.sinaSSOController.feedBackUrlCallBack',
                    'returntype': 'META',
                    'rsakv' : rsakv,
                    }

    r = session.post(URL_LOGIN, data=postdata)
    login_url = re.findall("replace\('(.*)'\)", r.content)
    if login_url and len(login_url) > 0:
        r = session.get(login_url[0])
        with file('login.html','w')as f:
            f.write(r.content.decode('gbk').encode('utf8'))
        print 'login.html save ok'
    login_re = re.findall(r"result*\"\:true",r.content)
    if login_re:
        print USER_NAME,'Login OK.'
        return session
    else:
        print USER_NAME,'Login ERROR.'
        if len(re.findall(r"result*\"\:false",r.content))>0:
            return False

if __name__ == '__main__':
	login()
