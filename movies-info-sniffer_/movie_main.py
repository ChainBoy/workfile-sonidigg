#!/bin/python
# -*- coding: utf-8 -*- 

import urllib, urllib2, re, time
import datetime
import requests
from urllib  import quote  
from movie import Movie
import codecs
import time
from weibo_tools import login

MOVIE_NAME = '2013怒放'
START_TIME = '2013-11-16-22'
END_TIME = '2013-11-16-23'

class Controller(object):
    """docstring for Controller"""
    def __init__(self, movie_name):
        super(Controller, self).__init__()
        self.session = login()
        self.movie_name = movie_name
        self.movie = Movie(self.movie_name, self.session)

    def start(self):
        start_time = datetime.datetime.strptime(START_TIME, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(END_TIME, '%Y-%m-%d-%H')
        if start_time < end_time:
            while True:
                time.sleep(3)
                cursor_time = start_time + datetime.timedelta(hours=1)
                timescope = datetime.datetime.strftime(start_time, '%Y-%m-%d-%H') +':' + datetime.datetime.strftime(cursor_time, '%Y-%m-%d-%H')
                url = 'http://s.weibo.com/weibo/'+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)
                response = self.session.get(url)
                count = self.get_page_count(response.content)
                self.handle_one_page(url, count, self.movie.id)
                start_time = cursor_time
                if start_time > end_time:
                    break
        else:
            print 'ERROR:Start time must early than end time!'

    def get_page_count(self, content):
        max_count = 0
        result = re.findall(r'&page=\d+', content)
        for index in result:
            info = index.split('=')
            if int(info[1]) > max_count:
                max_count = int(info[1])
        print 'max_page:', max_count
        return max_count

    def handle_one_page(self, url_head, page_count, movie_id):
        for i in xrange(1, page_count + 1):
            page = str(i)
            url = url_head +"&page=" + page
            response = self.session.get(url)
            self.movie.decode_content(response.content, movie_id)
            print "现在是第%s页" % page
            time.sleep(3)

if __name__ == '__main__':
    Controller(MOVIE_NAME).start()
