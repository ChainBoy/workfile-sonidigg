#!/usr/bin/env python
# -*- coding: utf-8 -*-

#                                                                                       /*一辈子相守不离*/
#                                                                             By: 张志鹏yi     Email: qq1126918258@gmail.com
#                                                                                    Date: 2014-07-29 18:17:34
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import re
import hashlib
import os
import pickle
from random import choice
import datetime
import time
from bs4 import BeautifulSoup
from mdb import douban_db, csdn

class douban(object):
    def __init__(self, user="", pwd=""):
        self.user = user.encode('utf8')
        self.pwd = pwd.encode('utf8')
        self.logined = False
        self.sess = requests.session()
        self.setting()
        #self.check403()
        self.__set_cookie()

    def rw_says(self, tid, says="", mode='w'):
        checkdir(self.saydir)
        filepath = '%s/%s' % (self.saydir, tid)
        if mode == "w":
            with file(filepath, mode)as f:
                f.write(says)
            return True
        elif mode == "r":
            if not os.path.isfile(filepath):
                return ""
            with file(filepath, mode)as f:
                return f.read()
        else:
            return None

    def setting(self):
        self.mainpage = 'http://www.douban.com/group/explore'
        self.mine = 'http://www.douban.com/mine/'
        self.sorryurl = 'http://www.douban.com/misc/sorry'
        self.loginpage = 'http://www.douban.com/accounts/login?source=group'
        self.loginurl = 'https://www.douban.com/accounts/login'
        self.vcodepage = 'http://www.douban.com/misc/captcha?id='
        self.post_vcode_url = 'http://localhost:9000/'
        self.sess.headers.update({'User-Agent':'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36'})
        self.sfile = '.setting'
        self.action_group = '%s/action_group' % self.sfile
        self.saydir = '%s/says' % self.sfile
        self.codedir = 'code'
        checkdir(self.sfile)
        checkdir(self.saydir)
        checkdir(self.codedir)
        self.say_topic = '%s/says/template' % self.sfile
        self.cfile = '.cookies'
        self.cpath = '%s/%s' % (self.cfile, hashlib.md5('%s\n%s' % (self.user, self.pwd)).hexdigest())
        self.tuser = True
        
    def check403(self):
        try:
            con = self.sess.get(self.mainpage)
            con, code = con.content, con.status_code
            if code == 403 or code == 404:
                if con.find('403 Forbidden') > 5:
                    self.__set_cookie(code)
            return
        except requests.ConnectionError, e:
            print "连接错误.."

    def __set_cookie(self, code=403):
        #print '%s -- 载入历史 cookie.' % code
        #print '设置Cookie：403'
        st = str(time.time())[:5]
        self.sess.cookies.update({'bid':'"NcNST5pns3Q"'})
        self.sess.cookies.update({'ll':'"108288"'})
        self.sess.cookies.update({'_pk_id.100001.8cb4':'e69e3a7d1e1ab473.%s04217.2.%s13759.%s04266.' % (st, st, st)})
        self.sess.cookies.update({'__utma':'30149280.923523361.%s04281.%s04281.%s13760.2' % (st, st, st)})
        self.sess.cookies.update({'__utmc':'30149280'})
        self.sess.cookies.update({'__utmz':'30149280.%s04281.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)' % st})
        self.sess.cookies.update({'_pk_ses.100001.8cb4':'*'})
        self.sess.cookies.update({'__utmb':'30149280.1.10.%s13760' % st})

    def clearsorry(self, con):
        if con.find('你访问豆瓣的方式有点像机器人程序。为了保护用户的数据，请向我们证明你是人类') > 200:
            print '机器人\验证码：（你访问豆瓣的方式有点像机器人程序。为了保护用户的数据，请向我们证明你是人类）'
            data = self.getfrom(con)
            data['remember'] = ''
            data = self.getvcode(data)
            if not data:return False
            con = self.sess.post(self.sorryurl, data=data).content
            self.clearsorry(con)
        else:
            return con
    
    def req(self, url, method='GET', data={}, headers={}, timeout=20, trynum=5, files={}):# decode='utf8'):
        e_num = 0
        while True:
            try:
                r = self.sess.request(method, url, data=data, headers=headers, timeout=timeout, files=files)
                if r.status_code == 403:
                    if r.content.find('403 Forbidden') > 1:
                        self.__set_cookie(403)
                    elif r.content.find('呃... 这是一个非公开小组的讨论。') > 200:
                        return 403
                    elif self.clearsorry(r.content):
                        return con
                    e_num += 1
                    if e_num >= trynum:
                        return 403
                    continue
                elif r.status_code == 404:
                    if r.content.find('呃... 这是一个非公开小组的讨论。') > 200:
                        return 404
                    #self.__set_cookie(404)
                    e_num += 1
                    if e_num >= trynum:
                        return 404
                    continue
                return r.content
            except Exception, e:
                #raise e
                e_num += 1
                print 'ERROR(%s): %s ( %s )' % (e_num, e, url)
                if e_num >= trynum:
                    return ""

    def getfrom(self, con, index=0):
        if not con : return {}
        #提交帖子
        data = {}
        if index == 1:
            for i in [i for i in BeautifulSoup(con).find_all('form')[1].find_all('input') if i.get('name')]:
                data[i.get('name')] = i.get('value')
            return data
        for i in BeautifulSoup(con).find_all('input'):
            name = i.get('name')
            if name:
                data[name] = i.get('value')
        return data

    def getvcode(self, data):
        #captcha-solution:exmall
        #captcha-id:HNAuN6TclUvUronT1F1eG78E:en
        #http://www.douban.com/misc/captcha?id=Ikhh9Ok851CbNNovRabqL6Sk:en&size=s
        code_id = data.get('captcha-id')
        code_str = None
        if code_id:
            print '------验证码------ '
            img = self.req('%s%s&size=s' % (self.vcodepage, code_id))
            with file('code/%s.jpg' % str(time.time())[:10], 'wb')as f:
                f.write(img)
            code_str = self.req(self.post_vcode_url + 'baike_code?type=1008', 'POST', files={'code.png': img}, trynum=20, timeout=50)
            result = code_str.split('|')
            if len(result) != 2: 
                print result
                print '------------------'
                return None
            code_str, img_id = result
            print '|打码结果：%s|' % code_str
            print '------------------'
            data['captcha-solution'] = code_str
            data['img_id'] = img_id
        return data

    def post_error_code(self, img_id):
        try:
            requests.post(self.post_vcode_url + "baike_code_err", data={'err_id': img_id})
            return True
        except:
            return False
        

    def __login(self):
        while True:
            con = str(self.req(self.loginpage))
            data = self.getfrom(con)
            data['remember'] = ''
            data = self.getvcode(data)
            if not data:continue
            data.update({'form_email': self.user, 'form_password': self.pwd})
            con = str(self.req(self.loginurl, 'POST', data=data))
            rf = re.findall('依据用户管理细则，此帐号已被永久停用。|邮箱格式不正确|该用户不存在|帐号和密码不匹配|帐号不能为空|密码不能为空|验证码不能为空', con, re.S)
            if len(re.findall('验证码不正确', str(con), re.S)) > 0:
                print '验证码不正确!'
                self.post_error_code(data.get('img_id'))
                continue
            elif len(rf) > 0:
                print "  ".join(rf)
                return False
            elif len(re.findall('我的豆瓣', con, re.S)) > 0:
                self.doubanname = re.findall('<span>(.+?)的帐号</span>', con, re.S)
                if len(self.doubanname) > 0:
                    self.doubanname = self.doubanname[0]

                print '%s:登录成功! 豆瓣昵称:%s' % (self.user, self.doubanname.decode('utf8').encode('utf8'))
                con = str(self.req(self.mine))
                rf = re.findall('帐号被暂时锁定', con, re.S)
                if len(rf) > 0:
                    print ' '.join(rf)
                    self.alt_user(self.user)
                    return False
                return True
            elif len(re.findall('需要输入验证码', con, re.S)) >0 :
                print "需要滑动打码！！"
                return False
            elif len(re.findall('该用户已主动注销', con, re.S)) > 0:
                print '该用户已主动注销:%s' % self.user
                #self.alt_user(self.user)
                self.del_user(self.user)
                return False
            else:
                print '验证码---其他错误，请检查 log.html'
                with file('log.html', 'w')as f:
                    f.write(con)

    def alt_user(self, user):
        if self.tuser: return None
        print "%s 被禁用. 删除该帐号" % user
        return douban_db().alt_user(user)

    def del_user(self, user):
        if self.tuser: return None
        print "删除帐号:%s" % user
        return douban_db().del_user(user)

    def save_cookie(self):
        print '写入 Cookie: %s' % self.user
        checkdir(self.cfile)
        with file(self.cpath, 'w')as f:
            pickle.dump(self.sess.cookies, f)
        self.logined = True

    def rm_cookie(self):
        if os.path.isfile(self.cpath):
            print '删除 Cookie: %s' % self.user
            os.remove(self.cpath)

    def load_cookie(self):
        if os.path.isfile(self.cpath):
            print '载入 Cookie: %s' % self.user
            with file(self.cpath, 'r')as f:
                self.sess.cookies = pickle.load(f)
            return True
        else:
            return False
    
    def __check_cookie(self):
        if self.load_cookie():
            con = str(self.req(self.mainpage))
            if len(re.findall('我的豆瓣', con, re.S)) > 0:
                self.doubanname = re.findall('<span>(.+?)的帐号</span>', con, re.S)
                if len(self.doubanname) > 0:
                    self.doubanname = self.doubanname[0]
                print '%s:登录成功! 豆瓣昵称:%s Cookie状态:可用.' % (self.user, self.doubanname)
                return True
            else:
                return False
        else:
            return False

    def login(self, user="", pwd="", tuser=False):
        print ""
        self.tuser = tuser
        self.logined = False
        if user and pwd:
            self.user = user.encode('utf8')
            self.pwd = pwd.encode('utf8')
            self.doubanname = ""
            self.cpath = '%s/%s' % (self.cfile, hashlib.md5('%s\n%s' % (user.encode('utf8'), pwd.encode('utf8'))).hexdigest())
        if self.__check_cookie():
            self.save_cookie()
            return True
        login = self.__login()
        if login:
            self.save_cookie()
            return True
        else:
            self.del_user(self.user)
            self.rm_cookie()
            self.logined = False
            self.user = ""
            self.pwd = ""
            self.doubanname = ""
            return False

    def logout(self):
        if self.doubanname and self.logined and self.user and self.pwd:
            con = str(self.req(self.mine))
            logout_url = re.findall('<a href="(http://www.douban.com/accounts/logout\?.+?ck=.+?)">退出</a>', con, re.S)
            if len(logout_url) > 0:
                self.req(logout_url)
        del self.cpath
        self.sess = requests.session()

    def query_user(self, user=""):
        self.users = douban_db().query_user(user)
        return self.users
        
    def get_group_name_ck(self, con="", ftype=2, url=""):
        if url:
            con = str(self.req(url))
        if ftype == 2:
            g = [i.strip() for i in re.findall('<h1>(.+?)</h1>', con, re.S)]
            ck = [i.strip() for i in re.findall('logout.+?ck=(....)"', con, re.S)]
            g = g[0] if len(g) > 0 else ''
            ck = ck[0] if len(ck) > 0 else ''
            return g, ck

        elif ftype == 1:
            l = [i.strip() for i in re.findall('<h1>(.+?)</h1>', con, re.S)]
            if len(l) > 0:
                return l[0]
            else:
                return ""
        else:
            return None

    def get_tid(self, url):
        tid = re.findall('topic/(\d+)', url)
        tid = tid[0] if len(tid)>0 else 0
        if not tid: 
            return 302, None
        con = self.req(url)
        if type(con) == int:
            return con, None
        else:
            return 200, tid


    def get_say_num(self, tid="", url=""):
        if tid:
            url = "http://www.douban.com/group/topic/%s/" % tid
        elif not url:
            return False
        print url
        con = self.req(url)
        if type(con) == int:
            return 0, con
        count = 0
        try:
            count = max([int(i) for i in re.findall('http://www.douban.com/group/topic/%s/\?start=(\d+)' % tid, con, re.S)])
            url = 'http://www.douban.com/group/topic/%s/?start=%s' % (tid, count)
            con = str(self.req(url))
            count += self.get_now_says(con)
        except ValueError, e:
            count += len(self.get_now_says(con))
        finally:
            return count, 200

    def join_group(self, group_name='douban_wow', action='join'):
        #http://www.douban.com/group/douban_wow/?action=join&ck=YPBB
        #http://www.douban.com/group/douban_wow/?action=quit&ck=YPBB
        print '需要加入小组 %s' % group_name

        url = 'http://www.douban.com/group/%s/' % group_name
        con = str(self.req(url))
        group_name, ck = self.get_group_name_ck(con)
        if con.find('我是这个小组的成员') > 200:
            print '%s 您已加入小组：%s' % (self.doubanname, group_name)
            return True
        elif con.find('你已经申请加入小组，请等待小组管理员审核') > 200:
            print '%s 您已经申请加入小组：%s，等待审核中' % (self.doubanname, group_name)
            return False
        elif con.find('<span>申请加入小组</span>') > 200:
            print '需要申请加入小组 %s' % group_name
            default_msg = '申请加入，求通过～～'
            checkdir(self.sfile)
            rl = self.choice_say(0)
            data = {'ck':ck, 'action':'request_join', 'message':choice(rl), 'send':'发送'}
            con = str(self.req(url, 'POST', data=data))
            if con.find('你已经申请加入小组，请等待小组管理员审核') > 200:
                print '%s 您已经申请加入小组：%s，等待审核中' % (self.doubanname, group_name)
            return False
        elif con.find('<span>加入小组</span>') > 200:
            con = str(self.req(re.findall('group-misc.+?<a href="(.+?)" class="bn-join-group">', con, re.S)[0]))
            if con.find('我是这个小组的成员') > 200:
                print '%s 您已加入小组：%s' % (self.doubanname, group_name)
                return True
            else:
                print '%s 加入小组：%s 失败，原因未知。' % (self.doubanname, group_name)
                return False

    def query_tie_first(self, tid, gname=''):
        tid = str(tid)
        if not gname:
            url = 'http://www.douban.com/group/topic/%s' % tid
            con = self.req(url)
            if con == 403:
                print '非公开小组：http://www.douban.com/group/topic/%s/' % tid
                return True
            if con == 404:
                print '帖子不存在：http://www.douban.com/group/topic/%s/' % tid
                return True
            gname = re.findall('group-item.+?<a href=".+?group/(.+?)/\?.+?".+?>', con, re.S)[0].decode('utf8')
        url = 'http://www.douban.com/group/%s/' % gname
        con = str(self.req(url))
        ts=[]
        try:
            ts = re.findall('href="http://www.douban.com/group/topic/(.+?)/?"', re.findall('<table.+?</table>', con, re.S)[1], re.S)
        except IndexError, e:
            print e
        finally:
            return tid in ts
                #return self.say(tid)

    def say(self, tid='54885639'):
        # http://www.douban.com/group/topic/54885639/?start=99999#last
        if not tid :
            print '帖子ID不正确.'
            return False
        url = 'http://www.douban.com/group/topic/%s/?start=99999999999#last' % tid
        con = str(self.req(url))
        if con == '403':
            print '非公开小组：http://www.douban.com/group/topic/%s/' % tid
            return False
        if con == '404':
            print '帖子不存在：http://www.douban.com/group/topic/%s/' % tid
            return False
        if con.find('只有小组成员才能发言') > 200:
            #group_name = re.findall('group-item.+?<a href="(.+?)".+?>', con, re.S)
            group_name = re.findall('group-item.+?<a href=".+?group/(.+?)/\?.+?".+?>', con, re.S)
            if len(group_name) > 0:
                group_name = group_name[0]
                if not self.join_group(group_name):
                    return False
                con = str(self.req(url))
            else:
                print '帖子找不到小组：http://www.douban.com/group/topic/%s/' % tid
        elif con.find('你已经申请加入小组，请等待小组管理员审核') > 200:
            print '你已经申请加入小组，请等待小组管理员审核'
            return False
        data = self.getfrom(con, 1)
        del data['sync_to_mb']
        list_say = self.choice_say(1, tid=tid)
        data['rv_comment'] = choice(list_say)
        url = 'http://www.douban.com/group/topic/%s/add_comment#last' % tid
        con = str(self.req(url, 'POST', data = data))
        i_say = self.get_i_say(con)
        data = {}
        #{'user_id': '1', 'cid':'say id', 'tid':'topic id', 'con': 'say what', 'rtime': '2014-07-31 18:33:00', 'state':1}
        data['user_id'] = douban_db().query_user(self.user)[0].get('id')
        data['cid'] = i_say[1]
        data['tid'] = tid
        data['con'] = i_say[0]
        data['rtime'] = i_say[3]
        data['state'] = '1'
        if not i_say:
            print '评论失败：[http://www.douban.com/group/topic/%s/]' % tid
            data['state'] = '0'
            douban_db().add_say(data)
            return False
        print '%s>评论内容保存在数据库，%s 行受影响' % ('-' * 150, douban_db().add_say(data))
        return True

    def add_say(self, tid=""):
        if not tid:
            return False
        if not self.logined:
            self.login()
        if self.logined:
            return self.say(tid)
        else:
            return False

    def get_now_says(self, con):
        return [i for i in BeautifulSoup(con).find_all('li') if i.get('class') == ['clearfix','comment-item']]

    def get_topic(self):
        return douban_db().get_topic()

    def add_topic(self, tid):
        """{'user':'user', 'tid':'57533625', 'title':'标题1', 'con':'内容', 'group':'buybook', 'ctime':'2014-07-30 10:42:52', 'atime':'2014-07-31 16:29:05' }"""
        if not tid:
            return None
        con = str(self.req('http://www.douban.com/group/topic/%s/' % tid))
        data = {'tid': tid}
        data['group'] = re.findall('group-item.+?<a href=".+?group/(.+?)/\?.+?".+?>', con, re.S)[0].decode('utf8')
        data['title'] = [i.strip() for i in re.findall('<h1>(.+?)</h1>', con, re.S)][0].decode('utf8')
        data['user'], data['ctime'] = [i.decode('utf8') for i in re.findall('topic-doc.+?来自: .+?>(.+?)</a.+?color-green">(.+?)</span', con, re.S)[0]]
        data['con'] = BeautifulSoup(re.findall('(<div id="link-report".+?)<div class="sns-bar"', con, re.S)[0]).text.strip().encode('utf8').decode('utf8')
        data['atime'] = str(datetime.datetime.now())
        return douban_db().add_topic(data)

    def get_new_says(self, sid):
        info = douban_db().get_new_say(sid)
        if len(info) > 0:
            return info[0]
        else:return {}

    def update_topic(self, tid, says):
        if not tid:
            return False
        self.tf_say = '%s/%s' % (self.saydir, tid)
        print "update:", self.tf_say
        checkdir(self.saydir)
        with file(self.tf_say, 'w')as f:
            f.write(says)
        return True


    def get_i_say(self, con):
        """[content, cid, h4, time, user]"""
        try:
            now = self.get_now_says(con)[-1]
            content = BeautifulSoup(str(now.findChild('p'))).text
            cid = now.attrs.get('data-cid')
            h4 = BeautifulSoup(str(now.findChild('h4')))
            time = BeautifulSoup(str(h4.find_all('span'))).text[1:-1]
            user = BeautifulSoup(str(h4.find_all('a'))).text[1:-1]

            isay = [content, cid, h4, time, user]
            return isay
        except Exception,e:
            print e
            return False

    def choice_say(self, ctype=0, tid=0):
        if ctype == 0:
            #添加小组
            default_msg = '申请加入，求通过～～'
            say = '申请加入小组,验证消息.'
            set_file = self.action_group
        elif ctype == 1:
            default_msg = '赞一个～.～'
            self.tf_say = '%s/%s' % (self.saydir, tid)
            if os.path.isfile(self.tf_say):
                set_file = self.tf_say
            else:
                set_file = self.say_topic
            say = '回帖'
        else:
            print '赞无此类型'
            return False
        checkdir(self.sfile)
        checkdir(self.saydir)
        rl = []
        if os.path.isfile(set_file):
            with file(set_file, 'r')as f:
                rl = f.read().splitlines()
                rl = [i.strip() for i in rl]
                rl = [i for i in rl if i.find('#') != 0]
        else:
            with file(set_file, 'w')as f:
                f.write('#%s。行首可用#注释。\n%s' % (say, default_msg))
        rl.append(default_msg)
        return rl

def checkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

if __name__ == '__main__':
    main = douban('1126918258@qq.com', 'zhang?2011')
    main.login()
