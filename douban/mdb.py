#!/usr/bin/env python
# -*- coding: utf-8 -*-

#                                                                               /*一辈子相守不离*/
#                                                                     By: 张志鹏yi     Email: qq1126918258@gmail.com
#                                                                            Date: 2014-07-31 15:34:28
#                                                                            End:  2014-07-31 19:18:12
import MySQLdb as mysql

class csdn():
    """db -- msdn"""
    def __init__(self):
        try:
            self.conn = mysql.connect(user='root', passwd='654321', host='localhost', charset='utf8', db = 'csdn')
            self.conn.autocommit(True)
        except mysql.Error, e:
            print e
            self.conn = False

    def query_user(self, s=0, e=10000):
        sql = "select id, username as user,passwd as pwd, email from users where id > %s and email like '%%sina.com' limit %s;" % (s, e)
        print sql
        cur = self.conn.cursor(mysql.cursors.DictCursor)
        info = []
        try:
            line = cur.execute(sql)
            info = list(cur.fetchall())
            cur.close()
        except mysql.Error, e:
            print e
        finally:
            return info

class douban_db():
    """db -- douban"""
    def __init__(self):
        try:
            self.conn = mysql.connect(user='root', passwd='654321', host='localhost', charset='utf8', db = 'douban')
            self.conn.autocommit(True)
        except mysql.Error, e:
            print e
            self.conn = False

    def add_user(self, user):
        """新增用户.
        {'user':'user', 'pwd':'password', 'doubanname':'douban name'}"""
        if len(self.query_user(user.get('user', ''))) > 0:
            return 2
        sql = "insert users values (0, '%(user)s', '%(pwd)s', '%(doubanname)s', 1);" % user
        print sql
        line = self.query(sql)
        return line

    def del_user(self, user):
        """删除用户.
        user_name """
        sql = "delete from users where user='%s' and not state=0;" % user
        line = self.query(sql)
        return line

    def alt_user(self, user):
        """删除用户.
        user_name """
        sql = "update users set state = 0 where user='%s';" % user
        line = self.query(sql)
        return line

    def query_user(self, user=""):
        """查询用户 .
        user_name """
        if user:
            sql = "select * from users where user='%s';" % user
        else:
            sql = "select * from users where state=1;"
        return self.query(sql, 2)

    def add_topic(self, topic):
        """添加帖子.
        {'user':'user', 'tid':'57533625', 'title':'标题1', 'con':'内容', 'group':'buybook', 'ctime':'2014-07-30 10:42:52', 'atime':'2014-07-31 16:29:05' }"""
        sql = "insert topics values (0,'%(user)s', %(tid)s, '%(title)s', '%(con)s', '%(group)s', '%(ctime)s', '%(atime)s', NULL, 200);" % topic
        info = self.query(sql, 1)
        return info

    def get_topic(self, tid=0):
        #sql = "select topics.*, s.scount, ntime, concat('http://www.douban.com/group/topic/', d_id)as url from topics left join (select count(*) as scount, max(rtime)as ntime,tid from says group by tid) as s on s.tid=topics.id;"
        sql = "select * from topics where d_id=%s;" % tid
        if tid == 0:
            sql = "select t.*, s.scount, ntime, mid from (select * from topics where topics.state > 0)as t left join (select count(*) as scount, max(rtime)as ntime,tid, max(id)as mid from says group by tid) as s on s.tid=t.d_id;"
        return  self.query(sql, 2)

    def get_new_say(self, sid):
        if not sid:
            return []
        sql = "select cid, rtime, user, dname from says, users where says.id=%s and user_id=users.id" % sid
        return self.query(sql, 2)

    def del_topic(self, topicid=2):
        """修改帖子状态
        1 默认可用
        2 被删除
        3 .."""
        sql = "update topics set state = 0 where d_id = %s;" % topicid
        return self.query(sql)

    def add_say(self, say):
        """发表评论
        {'user_id': '1', 'cid':'say id', 'tid':'topic id', 'con': 'say what', 'rtime': '2014-07-31 18:33:00', 'state':1}
        """
        sql = "insert says values(0, %(user_id)s, %(cid)s, %(tid)s, '%(con)s', '%(rtime)s', %(state)s);" % say
        print sql
        line = self.query(sql)
        return line

    def query(self, sql="show tables;", dtype=1):
        """数据库查询
        1: 返回几行受影响，
        2：返回查询结果，
        3：返回几行受影响，和查询结果
        """
        line = 0
        info = []
        cur = self.conn.cursor(mysql.cursors.DictCursor)
        try:
            line = cur.execute(sql)
            info = list(cur.fetchall())
            cur.close()
        except mysql.Error, e:
            print e
        finally:
            if dtype == 1: return line
            elif dtype == 2: return info
            else: return line, info

