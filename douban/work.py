#!/usr/bin/env python
# -*- coding: utf-8 -*-

#                                                                                         /*一辈子相守不离*/
#                                                                               By: 张志鹏yi     Email: qq1126918258@gmail.com
#                                                                                      Date: 2014-08-06 17:54:10

import threading
import time
from douban import douban
import random
import os
from mdb import douban_db, csdn

class works():
    def __init__(self, wtime=300):
        self.wtime = wtime
        self.thread = []
        self.user_line = '.setting/user_line'

    def add_user(self, num = 10000):
        s = 0
        if os.path.isfile(self.user_line):
            with file(self.user_line, 'r')as f:
                r = f.read().strip()
                try:
                    s = int(r)
                except ValueError:
                    s = 0
        users = csdn().query_user(s=s, e=num)
        print '一共 %s 个21cn.com 帐号' % len(users)
        #{'user':'user', 'pwd':'password', 'doubanname':'douban name'}
        for index, i in enumerate(users):
            dban = douban()
            print "尝试帐号：[ %s/%s | %s ]  %s  --  %s" %(index, len(users), i.get('id'), i.get('email').encode('utf8'), i.get('pwd').encode('utf8'))
            if dban.login(i.get('email').encode('utf8'), i.get('pwd').encode('utf8'), tuser=True):
                user = {'user':dban.user, 'pwd':dban.pwd, 'doubanname':dban.doubanname.decode('utf8').encode('utf8')}
                douban_db().add_user(user)
                dban.logout()
            with file(self.user_line, 'w')as f:
                f.write(str(int(i.get('id'))))
            #self.check403()
            print ''
            print "==" * 40
            print ''

    def start(self):
        print 'start main'
        timer = threading.Timer(2, self.work)
        self.thread.append(timer)
        timer.setDaemon(True)
        timer.start()

    def cancel(self):
        for i in self.thread:
            i.cancel()

    def getuser(self):
        d = douban()
        d.query_user()
        users = []
        for i in d.users:
            user = i.get('user', '').encode('utf8')
            pwd = i.get('pwd', '').encode('utf8')
            users.append([user, pwd])
        return users

    def work(self):
        print 'start work.'
        timer = threading.Timer(self.wtime, self.work)
        self.thread.append(timer)
        timer.setDaemon(True)
        timer.start()

        print '**' * 50
        #return

        users = self.getuser()
        if len(users) < 1:
            return False
        ties = douban().get_topic()
        if len(ties) < 1:
            return False

        for i in ties:
            gurl = "http://www.douban.com/group/%s/" % i.get('gname')
            print '-' * 190
            print '开始发帖:%s      %s' % (i.get('d_id'), i.get('gname'))
            while True:
                user = random.choice(users)
                d = douban()
                if not d.login(user[0], user[1]):
                    del users[users.index(user)]
                    continue
                break
            #if not d.query_tie_first(i.get('d_id', ''), i.get('gname', '')):
            if douban().query_tie_first(i.get('d_id', '')):
                print 'http://www.douban.com/group/topic/%s 在小组：%s 首页，不回帖...' % (i.get('d_id', ''), i.get('gname', ''))
            else:
                print 'http://www.douban.com/group/topic/%s 不在小组：%s 首页，回帖...' % (i.get('d_id', ''), i.get('gname', ''))
                d.add_say(i.get('d_id', ''))
        print 'end work.'

if __name__ == '__main__':
    w = works()
    w.add_user(num = 5000)
