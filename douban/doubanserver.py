#!/usr/bin/env python
# -*- coding: utf-8 -*-

#                                                                                         /*一辈子相守不离*/
#                                                                               By: 张志鹏yi     Email: qq1126918258@gmail.com
#                                                                                      Date: 2014-08-02 00:09:15

import os
import time
import re
import requests

import tornado
import tornado.ioloop
import tornado.httpserver
import tornado.web
from tornado.escape import json_encode

import mdb
from douban import douban
from work import works
from tornado.options import define, options

define("port", default=1314, help="run on the given port", type=int)
WORK = works(300)
work = False

class Application(tornado.web.Application):
    def __init__(self):
        static_path = os.path.join(os.path.dirname(__file__), "static")
        settings = {
            "cookie_secret": 'luojingjingwoainiyishengyishi.forever.',
            "debug": True,
            }
        handlers = [
            (r"/?$", MainHandler),
            (r"/del?$", DelHandler),
            (r"/tie?$", TieHandler),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path}),
            (r'/(favicon.ico)', tornado.web.StaticFileHandler, {"path": ""}),
            ]
        tornado.web.Application.__init__(self, handlers, **settings)

class BaseHandler(tornado.web.RequestHandler):
    def db(self):
        return mdb.douban_db()

class MainHandler(BaseHandler):
    def get(self):
        say = self.get_argument('s', default="OK")
        r_url = self.get_argument('u', default="")
        if r_url:
            l = {}
            l['title'] = say
            l['url'] = r_url
            self.render('./template/main.tmp', l=l)
        else:
            self.write(say)

class DelHandler(BaseHandler):
    def get(self):
        tieid = self.get_argument("tid", default=None, strip=False)
        if tieid:
            self.db().del_topic(tieid)
            s = '删除成功'
        else:
            s = "please input tie id."
        self.redirect("/?s=%s&u=/tie" % s)

class TieHandler(BaseHandler):
    def get(self):
        self.get_argu()
        if self.cancel:
            self.cancel_work()
        if not self.tid and not self.add:
            self.ties()
        elif self.add or self.tid:
            self.tie()

    def cancel_work(self):
        global  work
        if self.cancel == 'True':
            if work:
                print '停止任务'
                workstatus(False)
                s = '任务已停止.'
        elif self.cancel == 'False':
            if not work:
                print '开启任务'
                workstatus(True)
                s = '任务已开始.'
        self.redirect("/?s=%s&u=/tie" % s)

    def post(self):
        self.get_argu()
        if self.tid:
            s = self.ch_tid(ct='upd')
            self.redirect("/?s=%s&u=/tie" % s)
        elif self.add:
            s = self.ch_tid(ct='add')
            self.redirect("/?s=%s&u=/tie" % s)
        else:
            self.redirect('/tie')

    def ch_tid(self, ct='add'):
        m = douban()
        code, self.tid = m.get_tid(self.url)
        if code == 200 and self.tid:
            m.rw_says(self.tid, says=self.says)
            "添加数据库"
            if ct == 'add':
                if m.add_topic(self.tid) > 0:
                    return '添加成功'
                else:
                    return '添加失败, 请确认是否已添加。'
            elif ct == 'upd':
                if m.update_topic(self.tid, self.says):
                    return '修改成功'
                else:
                    return '修改失败?'
        elif code == 302:
            return '网址输入错误，请检查网址是否输入正确'
        elif code == 403:
            return '没有权限，请检查帖子是否存在?'
        elif code == 404:
            return '网址不存在，请检查帖子是否存在'

    def get_argu(self):
        self.tid = int(self.get_argument('tid', default=0))
        self.add = int(self.get_argument('add', default=0))
        self.says = self.get_argument('says', default="").encode('utf8')
        self.url = self.get_argument('url', default="")
        self.cancel = self.get_argument('cancel', default="")

    def tie(self):
        global WORK, work
        state = [work, '暂停任务'] if work else [work, '开启任务']
        says="Hi,up this~~"
        turl=""
        dele = "hidden"
        add = "hidden"
        title = "任务管理"
        if self.tid > 0:
            says = douban().rw_says(self.tid, mode='r')
            turl = 'http://www.douban.com/group/topic/%s/' % self.tid
            dele = ""
            title = '修改任务'

        elif self.add:
            title = '新增任务'
            add = ""
        tie = {'tid':self.tid, 'add':self.add, 'action':'/tie?tid=%s&add=%s' % (self.tid, self.add), 'says':says, 'turl':turl, 'add': add, 'del': dele, 'title': title}
        self.render('./template/tie.tmp', tie=tie, work=state)

    def ties(self):
        global WORK, work
        m = douban()
        ties = m.get_topic()
        dcode = {200: '正常', 403:'无权限', 404:'不存在'}
        for i in ties:
            i['Gname'] = m.get_group_name_ck(url="http://www.douban.com/group/%s/" % i.get('gname'), ftype=1)
            i['t_num'], code = m.get_say_num(tid=i.get('d_id'))
            new_say = m.get_new_says(i['mid'])
            i['cid'] = new_say.get('cid', "")
            i['user'] = new_say.get('user', "")
            i['dname'] = new_say.get('dname', "")
            ntime = str(new_say.get('rtime', ""))
            i['rtime'] = '---' if not len(ntime) else ntime
            i['url'] = 'http://www.douban.com/group/topic/%s' % i.get('d_id')
            i['scount'] = 0 if i['scount'] == None else i['scount']
            i['state'] = dcode.get(code)
            i['class'] = 'ok'
            if code != 200:
                i['class'] = 'error'
        #print ties
        state = [work, '暂停任务'] if work else [work, '开启任务']
        self.render('./template/ties.tmp', ties=ties, work=state)

def end():
    global WORK, work
    WORK.cancel()
    work = False
    print 'Please wating 2s ...'
    time.sleep(2)
    print ''
    print '-' * 50
    print ''
    exit(-9)

def workstatus(state = None):
    global WORK, work
    sfile = '.setting/status'
    if state == None:
        if not os.path.isfile(sfile):
            state =  False
        else:
            with file(sfile, 'r')as f:
                r = f.read().strip()
                if r == 'True':
                    state =  True
                else:
                    state =  False
        if state:
            WORK.start()
            work = True
    else:
        work =  state
        if work:
            WORK.start()
        else:
            WORK.cancel()
        with file(sfile, 'w')as f:
            print '任务状态：' , state
            f.write(str(state))

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port, address='0.0.0.0')
    try:
        print 'Start:http://127.0.0.1:%s' % options.port
        print 'Start:douban work...'
        workstatus()
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        end()
    #except Exception, e:
    #    raise e

if __name__ == "__main__":
    main()    
