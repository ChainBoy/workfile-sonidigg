
from multiprocessing import Process, Pipe, Pool, Manager
import threading

def f(message):
    myClass = MyClass(message)
    return myClass.getMessage()
    # return message


class MyThread1(threading.Thread):
    def __init__(self,value):
        super(MyThread1, self).__init__()
        self.value = value
    def run(self):
        print self.value.recv()

class MyThread2(threading.Thread):
    def __init__(self,value):
        super(MyThread2, self).__init__()
        self.value = value
        self.pool = Pool(processes=5)
    def send_message(self, message):
        self.value.send([message,message])

    def run(self):
        self.pool.apply_async(f, args=("haha",), callback = self.send_message)

class MyClass(object):
    def __init__(self, message):
        super(MyClass, self).__init__()
        self.message = message

    def getMessage(self):
        return "hello " + self.message


if __name__ == '__main__':
    print "start"
    a, b=Pipe()
    # pipe_collection = manager.dict()
    # pipe_collection["in_put"] = in_put
    # pipe_collection["out_put"] = out_put
    myThread1 = MyThread1(a)
    myThread2 = MyThread2(b)
    myThread1.start()
    myThread2.start()
    myThread1.join()
    myThread2.join()
    # print pipe_collection["out_put"].recv()
    print "end"
