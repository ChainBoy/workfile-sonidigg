#!/usr/bin/python
# -*- coding: utf-8 -*- 

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import os
import simplejson as json
import models
from sqlalchemy import Column, Integer, String,Time,DateTime
engine = create_engine('mysql://root:654321@localhost:3306/dataservice?charset=utf8', convert_unicode=True, echo=True, pool_recycle=7200)
engine.connect()
models.init_db(engine)
db = scoped_session(sessionmaker(bind=engine))
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Station(Base):
    __tablename__ = 'station'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    assigned = Column(Integer)
    assignedTo = Column(String)
    failTime = Column(Integer)
    createdTime = Column(DateTime)
    def __init__(self, name, address):
        self.name = name
        self.address = address
    def __repr__(self):
        return "<User('%d','%s','%s')>" % (self.id,self.name, self.address)

def search_ID(updateContent):
    return db.query(Station).filter_by(id = updateContent).all()
    
def search_name(updateContent):
    return db.query(Station).filter_by(name = updateContent).all()
    
def search_address(updateContent):
    return db.query(Station).filter_by(address = updateContent).all()
    
def search_all(updateContent):
    return db.query(Station).all()
    
def insert(updateContent):
    a = models.Station(name = updateContent["name"],address = updateContent["address"])
    db.add(a)
    db.commit()
def getDispatcher(jsonStr):
    data = json.loads(jsonStr)
    method = data['method']
    updateContent = data['content']
    v = []
    i = 0
    if method == 'search_name':
        utf_8 =  search_name(updateContent)
        for u in utf_8:
            v.append( {
                "Name" : u.name,
                "ID":u.id,
                "Url":u.address
                })
            i = i + 1
        js = json.dumps(v)
        return js
    elif method == "search_ID" :
        utf_8 = search_ID(updateContent)
        for u in utf_8:
            v.append(  {
                "Name" : u.name,
                "ID":u.id,
                "Url":u.address
            })
            i = i + 1
        js = json.dumps(v)
        return js
            #print 'hello'#function call yyq.func1(updateContent)
    elif method == 'search_address':
        utf_8 = (search_address(updateContent))
        for u in utf_8:
            v.append( {
                "Name" : u.name,
                "ID":u.id,
                "Url":u.address
            })
            i = i + 1
        js = json.dumps(v)
        return js
    elif method == 'search_all':
        utf_8 = search_all(updateContent)
        index  = 0
        for u in utf_8:
            if (index == 85):
                print '85 radio url send to client!'
                break
            v.append( {
                "Name" : u.name,
                "ID":u.id,
                "Url":u.address
            })
            i = i + 1
            index += 1
        js = json.dumps(v)
        return js
    else:
        print 'error'
        
def postDispatcher(jsonStr):
    return getDispatcher(jsonStr)
