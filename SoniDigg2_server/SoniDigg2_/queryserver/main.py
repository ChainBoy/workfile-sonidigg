#!/usr/bin/python
# -*- coding: gb2312 -*-
# Python imports
import os
import simplejson as json
import json
import time
import random

# Tornado imports
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.web import url

# Sqlalchemy imports
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import  multiprocessing

# App imports
import models
import resolver
import StationManagement
import CodeManager
import StateManager

# Options
define("port", default=8002, help="run on the given port", type=int)
define("debug", default=False, type=bool)
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/', IndexHandler),
            url(r'/stationmanager', StationManagerHandler),
            url(r'/qixiangzhan', QiXiangZhanHandler),
            url(r'/statemanager', StateHandler),
        ]
        settings = dict(
            debug=options.debug,
            xsrf_cookies=False,
            # TODO Change this to a random string
            cookie_secret="nzjxcjasduuqwheazmu293nsadhaslzkci9023nsadnua9sdads/Vo=",
        )
        tornado.web.Application.__init__(self, handlers, **settings)
        manager = multiprocessing.Manager()
        self.queue = manager.Queue()
        self.balance = manager.list()
        self.balance = [0, 0]
        self.logs = manager.Queue()
        codeManager = CodeManager.CodesProcess(self.queue, self.balance, self.logs)
        codeManager.start()
        resolver_instance = resolver.Resolver(db_path, self.balance, self.logs)
        resolver_instance.start()
   
class BaseHandler(tornado.web.RequestHandler):
    def queue(self):
        return self.application.queue

class IndexHandler(BaseHandler):
    def get(self):
        pass
    def post(self):
        pass

class StationManagerHandler(BaseHandler):
    def get(self):
        pass
    def post(self):
        radioInfo = StationManagement.StationManager().postDispatcher(self.request.body) + '\n'
        self.write(radioInfo)
        self.flush()

class QiXiangZhanHandler(BaseHandler):
    def get(self):
        pass
    def post(self):
        self.queue().put(self.request.body)

class StateHandler(BaseHandler):
    def get(self):
        pass
    def post(self):
        StateManager.handleStates(self.request.body)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
