#!/usr/bin/python
# -*- coding: utf-8 -*- 

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import os
import simplejson as json
import models
from sqlalchemy import Column, Integer, String,Time,DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Station(Base):
    __tablename__ = 'station'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    assigned = Column(Integer)
    assignedTo = Column(String)
    failTime = Column(Integer)
    createdTime = Column(DateTime)
    def __init__(self, name, address):
        self.name = name
        self.address = address
    def __repr__(self):
        return "<User('%d','%s','%s')>" % (self.id,self.name, self.address)

class StationManager:
    def __init__(self):
        self.engine = create_engine('mysql://root:654321@localhost:3306/dataservice?charset=utf8', convert_unicode=True, echo=True, pool_recycle=7200)
        self.engine.connect()
        models.init_db(self.engine)
    
    def getDB(self):    
                self.engine.connect()
                return scoped_session(sessionmaker(bind=self.engine))       

    def getDispatcher(self, jsonStr):
        data = json.loads(jsonStr)
        method = data['method']
        updateContent = data['content']
        result = []
        if method == 'search_all':
            allStation = self.getDB().query(Station).all()
            index  = 0
            for station in allStation:
                if (index == 85):
                    print '85 radio url send to client!'
                    break
                result.append( {
                "Name" : station.name,
                "ID":station.id,
                "Url":station.address
                })
                index += 1
            js = json.dumps(result)
            return js
        else:
            print 'error'
        
    def postDispatcher(self, jsonStr):
        return self.getDispatcher(jsonStr)
