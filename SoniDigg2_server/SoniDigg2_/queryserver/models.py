from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, BigInteger, Text
from sqlalchemy import create_engine
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'
Base = declarative_base()

class Station(Base):
    __tablename__ = 'station'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    address = Column(String(500))
    assigned = Column(Integer)
    assignedTo = Column(Integer)
    failTime = Column(Integer)
    createdTime = Column(DateTime)

    def __init__(self, name, address):
        self.name = name
        self.address = address

    def __repr__(self):
        return "<Station('%s','%s')>" % (self.name, self.address)

class Worker(Base):
    __tablename__ = 'worker'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Worker('%s')>" % (self.name)

class WorkerAssignLog(Base):
    __tablename__ = 'worker_assign_log'
    id = Column(Integer, primary_key=True)
    assignedWorker = Column(Integer)
    assignedStation = Column(Integer)
    assignedTime = Column(DateTime)

    def __init__(self, assignedWorker, assignedStation, assignedTime):
        self.assignedWorker = assignedWorker
        self.assignedStation = assignedStation
        self.assignedTime = assignedTime


class WorkerDownLog(Base):
    __tablename__ = 'worker_down_log'
    id = Column(Integer, primary_key=True)
    downWorker = Column(Integer)
    downTime = Column(DateTime)

    def __init(self,downWorker, downTime):
        self.downWorker = downWorker
        self.downTime = downTime

class ListenLog(Base):
    __tablename__ = 'listen_log'
    id = Column(BigInteger, primary_key=True)
    stationId = Column(Integer)
    time = Column(DateTime)
    fingurePrint = Column(Text)
    song = Column(String(50),nullable=True) #to get song id from tokyo tyrant

class Song(Base):
    __tablename__ = 'song'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    artist = Column(String(100))
    album = Column(String(100))

    def __init__(self, name, artist):
        self.name = name
        self.artist = artist

    def __repr__(self):
        return "<Song('%s')>" % (self.name)

class Artist(Base):
    __tablename__ = 'artist'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    photo = Column(String(1000))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<artist('%s')>" % (self.name)

class State(Base):
    __tablename__ = 'state'
    id = Column(Integer, primary_key=True)
    time = Column(DateTime)
    stationId = Column(Integer)
    stateCode = Column(Integer)

    def __init__(self, time, stationId, state):
        self.time = time
        self.stationId =  stationId
        self.stateCode = state

    def __repr__(self):
        return "<station ('%s')>" % (self.stationId)

class Listenlog_null(Base):
    __tablename__= 'listen_log_null'
    id = Column(Integer, primary_key=True)
    time = Column(DateTime)
    stationId = Column(Integer)
    fingerPrint = Column(Text)
    def __init__(self,time, stationId,fingerPrint):
        self.time = time
        self.stationId = stationId
        self.fingerPrint = fingerPrint

class Listenlog_once(Base):
    __tablename__ = 'listen_log_once'
    id  = Column(Integer, primary_key = True)
    time = Column(DateTime)
    stationId = Column(Integer)
    fingerPrint = Column(Text)
    song = Column(Integer, ForeignKey('song.id'))
    # song_once = relationship("Song", backref=backref("listen_once", uselist=False))

class Listenlog_times(Base):
    __tablename__= 'listen_log_times'
    id = Column(BigInteger, primary_key=True)
    time = Column(DateTime)
    stationId = Column(Integer)
    fingerPrint = Column(Text)
    song = Column(Integer, ForeignKey('song.id'))
    # song_times = relationship("Song", backref=backref("listen_times", uselist=False))

def init_db(engine):
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
    init_db(engine)
