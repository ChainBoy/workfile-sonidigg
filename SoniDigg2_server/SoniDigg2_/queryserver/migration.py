from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import models
import time
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'
engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
models.init_db(engine)
engine.connect()
db = scoped_session(sessionmaker(bind=engine))

def transfer_data(db,num):
    query = db.query(models.ListenLog)
    results = []
    query_results = query.filter(models.ListenLog.song == None)
    for item in query_results[:num]:
        results.append(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
    db.add_all(results)
    db.commit()

def transfer_data_op(db,num):
    query = db.query(models.ListenLog)
    query_results = query.filter(models.ListenLog.song == None)
    count = 1

    while True:
        t = time.time()
        results = []
        for item in query_results[num*(count-1):num*count]:
            results.append(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
        db.add_all(results)
        db.commit()
        if len(results) < num:
            break
        else:
            count += 1
        print "loop time: %s" % time.time()-t
    print "transfer_data end!"

def transfer_data_op1(db, num):
    query_results = db.query(models.ListenLog)
    count = 1
    while True:
        t = time.time()
        query = query_results.filter(models.ListenLog.id > num*(count-1), models.ListenLog.id <= num*count, models.ListenLog.song == None)
        for item in query:
            db.add(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
        db.commit()
        if :
            break
        else:
            count += 1
        print "loop time: %s" % time.time()-t
    print "transfer_data end!"

def transfer_data_times(db, num):
    query = db.query(models.ListenLog)
    results = []
    query_results = query.filter(models.ListenLog.song != None,models.ListenLog.id > )
    for item in query_results[:num]:
        results.append(models.Listenlog_times(time = item.time, stationId = item.stationId, fingurePrint = item.fingurePrint, song = item.song))
    db.add_all(results)
    db.commit()

def transfer_data_times_op(db, num):
    query = db.query(models.ListenLog)
    query_results = query.filter(models.ListenLog.song != None)
    maxsize = query_results.count()
    count = 1
    while True:
        results = []
        for item in query_results[num*(count-1):num*count]:
            results.append(models.Listenlog_times(time = item.time, stationId = item.stationId, fingurePrint = item.fingurePrint, song = item.song))
        db.add_all(results)
        db.commit()
        if len(results) < num:
            break
        else:
            count += 1
    print "transfer_data end!"
    
def transfer_data_once(db, num):
    query = db.query(models.Listenlog_times)
    results = []
    stationMap = {}
    query_results = query.all()
    for item in query_results:
        if stationMap.has_key(item.stationId):
            cache = stationMap[item.stationId]
            for i in cache:
                if i = item.song:
                    break
            else:
                results.append(models.Listenlog_once(time = item.time, stationId = item.stationId, fingerPrint = item.fingerPrint, song = item.song))
                cache.append(item.song)
                if len(cache) == 4:
                    cache.pop(0)
        else:
            stationMap[item.stationId] = [item.song]
            results.append(models.Listenlog_once(time = item.time, stationId = item.stationId, fingerPrint = item.fingerPrint, song = item.song))
    db.add_all(results)
    db.commit()
  
def main(db, num):
    transfer_data_op1(db, num)

if __name__ == '__main__':
    main(db, 100)