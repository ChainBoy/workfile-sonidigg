{
    "code":200,
    "info":[
        {
            "artist":"\u9ece\u660e",
            "artist_id":28,
            "play_num":18,
            "play_num_add":null,
            "play_time":0,
            "rank":20.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u9f50\u79e6",
            "artist_id":103,
            "play_num":16,
            "play_num_add":null,
            "play_time":4480,
            "rank":21.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5468\u7b14\u7545",
            "artist_id":144,
            "play_num":16,
            "play_num_add":null,
            "play_time":0,
            "rank":22.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5f20\u97f6\u6db5",
            "artist_id":226,
            "play_num":16,
            "play_num_add":null,
            "play_time":3840,
            "rank":23.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5f20\u9753\u9896",
            "artist_id":166,
            "play_num":15,
            "play_num_add":null,
            "play_time":900,
            "rank":24.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"BEYOND",
            "artist_id":225,
            "play_num":15,
            "play_num_add":null,
            "play_time":900,
            "rank":25.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u6c5f\u7f8e\u742a",
            "artist_id":827,
            "play_num":14,
            "play_num_add":null,
            "play_time":0,
            "rank":26.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u8303\u6653\u8431",
            "artist_id":127,
            "play_num":13,
            "play_num_add":null,
            "play_time":0,
            "rank":27.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u6e29\u5c9a",
            "artist_id":110,
            "play_num":12,
            "play_num_add":null,
            "play_time":0,
            "rank":28.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u59dc\u80b2\u6052",
            "artist_id":783,
            "play_num":12,
            "play_num_add":null,
            "play_time":2160,
            "rank":29.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u6768\u4e1e\u7433",
            "artist_id":303,
            "play_num":11,
            "play_num_add":null,
            "play_time":1980,
            "rank":30.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u9ad8\u6167\u541b",
            "artist_id":52,
            "play_num":10,
            "play_num_add":null,
            "play_time":100,
            "rank":31.0,
            "time":"2013-11-12 12:50:04"
        },
        {
            "artist":"\u5f20\u6770",
            "artist_id":289,
            "play_num":10,
            "play_num_add":null,
            "play_time":200,
            "rank":32.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u8fea\u514b\u725b\u4ed4",
            "artist_id":710,
            "play_num":9,
            "play_num_add":null,
            "play_time":0,
            "rank":33.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u82cf\u6709\u670b",
            "artist_id":486,
            "play_num":9,
            "play_num_add":null,
            "play_time":0,
            "rank":34.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u674e\u7fca\u541b",
            "artist_id":815,
            "play_num":8,
            "play_num_add":null,
            "play_time":480,
            "rank":35.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u8c2d\u6676",
            "artist_id":845,
            "play_num":7,
            "play_num_add":null,
            "play_time":0,
            "rank":36.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5f6d\u7f9a",
            "artist_id":423,
            "play_num":7,
            "play_num_add":null,
            "play_time":0,
            "rank":37.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"B.A.D",
            "artist_id":632,
            "play_num":6,
            "play_num_add":null,
            "play_time":0,
            "rank":38.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u6768\u5343\u5b05",
            "artist_id":205,
            "play_num":6,
            "play_num_add":null,
            "play_time":0,
            "rank":39.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u9752\u6625\u7f8e\u5c11\u5973",
            "artist_id":1381,
            "play_num":5,
            "play_num_add":null,
            "play_time":0,
            "rank":40.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5434\u5b97\u5baa",
            "artist_id":1151,
            "play_num":5,
            "play_num_add":null,
            "play_time":600,
            "rank":41.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"EMINEM",
            "artist_id":1031,
            "play_num":4,
            "play_num_add":null,
            "play_time":240,
            "rank":42.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u8001\u72fc",
            "artist_id":1212,
            "play_num":4,
            "play_num_add":null,
            "play_time":320,
            "rank":43.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5f20\u9550\u54f2",
            "artist_id":506,
            "play_num":4,
            "play_num_add":null,
            "play_time":0,
            "rank":44.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u5e94\u8c6a",
            "artist_id":542,
            "play_num":4,
            "play_num_add":null,
            "play_time":0,
            "rank":45.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"Freezepop",
            "artist_id":272,
            "play_num":3,
            "play_num_add":null,
            "play_time":0,
            "rank":46.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u6d2a\u542f",
            "artist_id":785,
            "play_num":2,
            "play_num_add":null,
            "play_time":0,
            "rank":47.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u95ea\u4eae\u4e09\u59d0\u59b9",
            "artist_id":991,
            "play_num":2,
            "play_num_add":null,
            "play_time":0,
            "rank":48.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u674e\u5065",
            "artist_id":1590,
            "play_num":2,
            "play_num_add":null,
            "play_time":366,
            "rank":49.0,
            "time":"2013-11-13 00:00:00"
        },
        {
            "artist":"\u82cf\u9192",
            "artist_id":8,
            "play_num":2,
            "play_num_add":null,
            "play_time":120,
            "rank":50.0,
            "time":"2013-11-13 00:00:00"
        }
    ],
    "list_state":"0",
    "result_count":31
}shazam-result/top-1000_20140515-180404_0.xlsxshazam-result/yashen_20140515-180739_0.xlsxshazam-result/yashen_20140515-181109_0.xlsxshazam-result/yashen_20140515-181158_0.xlsxshazam-result/yashen_20140515-181241_0.xlsxshazam-result/yashen_20140515-181327_0.xlsxshazam-result/yashen_20140515-181501_0.xlsxshazam-result/yashen_20140515-181702_0.xlsxshazam-result/yashen_20140515-181827_0.xlsxshazam-result/yashen_20140515-182025_0.xlsxshazam-result/num_yashen_20140515-182025_0.xls
shazam-result/yashen_20140515-182132_0.xlsxshazam-result/num_yashen_20140515-182135_0.xls
shazam-result/yashen_20140515-182237_0.xlsxshazam-result/num_yashen_20140515-182240_0.xls
shazam-result/yashen_20140515-182339_0.xlsxshazam-result/num_yashen_20140515-182342_0.xls
shazam-result/yashen02_20140515-182647_1110.xlsxshazam-result/num_yashen02_20140515-182702_1110.xls
shazam-result/yashen02_20140515-183015_1110.xlsxshazam-result/num_yashen02_20140515-183030_1110.xls
shazam-result/yashen02_20140515-183131_1110.xlsxshazam-result/yashen02_20140515-183352_1110.xlsxshazam-result/yashen02_20140515-183634_1110.xlsxshazam-result/yashen02_20140515-184204_1110.xlsxshazam-result/yashen02_20140515-184226_1110.xlsxshazam-result/yashen_20140515-184342_54.xlsxshazam-result/yashen_20140515-184342_54.xlsxshazam-result/yashen_20140515-184408_0.xlsxshazam-result/yashen_20140515-190015_0.xlsxshazam-result/num_yashen_20140515-190015_0.xls
shazam-result/yashen_20140515-190110_0.xlsxshazam-result/yashen_20140515-190139_0.xlsxshazam-result/yashen02_20140515-191337_1113.xlsxshazam-result/num_yashen02_20140515-191348_1113.xls
