#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as Mysqldb
import sys



def getNew_Info(list_state,num_start = 0, num_end = 10,id=0):
  
  info={}
  result_count = 0
  code = 500
  try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='654321',
      port=3306,
      db='dataservice',
      charset='utf8'
    )
    #conn.set_character_set('utf8')
    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    sql_ = """call sp_listen_info(%s,%s,%s,%s);""" % (list_state, num_start, num_end,id)
    ex = cur.execute(sql_)
    info = list(cur.fetchall()) #get sql by state,num_start,num_end,time_start,time_end.
    result_count = len(info)
    cur.close()
    conn.close()
    code = 200
  except Mysqldb.Error,e:
    print e
    code = 501
  finally:
    return info, result_count, code

class getNewInfo():
  def __init__(self,jsonContent):
    self.json_content=jsonContent
    self.result_json={}
  def CheckJson(self):
    """
    check the request json data.
    """
    result_json=self.result_json    
    list_state = self.json_content.get("list_state",0)
    post_id = self.json_content.get("post_id",0)
    num_start=self.json_content.get("num_start",0) #defult rank start num is 0
    num_end=self.json_content.get("num_end",10)#defult rank end num is 20
    print 
    #check post_id
    try:
      post_id = post_id if int(post_id) >= 0 else 0
    except Exception:
      post_id = 0

    #check list_state .
    try:
      list_state = list_state if int(list_state) >= 0 else 0
    except Exception:
      list_state = 0
    result_json["list_state"] = list_state

    #check num_start and num_end.
    try:
      num_start = int(num_start)
      num_end = int(num_end)
      if num_start > num_end:
        num_start, num_end = num_end, num_start
      elif num_start == num_end:
        num_start, num_end = 1,10
    except Exception:
      num_start,num_end = 1,10

    #Get data:song rank(0); artist rank(1)
    if post_id >= 0 and list_state>=0:
      result_json["New_info_%s" % post_id],result_json["result_count"],result_json["code"]=getNew_Info(list_state, num_start, num_end, post_id)
    else:
      result_json["code"]=404

    return result_json

if __name__ == '__main__':
  time_start = '2011-11-11'
  time_end = '2014-11-18'