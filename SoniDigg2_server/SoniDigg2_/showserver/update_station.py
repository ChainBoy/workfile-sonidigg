#!/usr//bin/python
#-*- coding:utf-8 -*-
import time
import urllib2
import codecs
import MySQLdb as Mysqldb
from get_station import main as get_station
from get_station import station_file
import urlChecked as urlcheck





class MainClass(object):
    """docstring for MainClass"""
    def __init__(self):
        super(MainClass, self).__init__()
        conn=Mysqldb.connect(
            host='localhost',
            user='root',
            passwd='123',
            port=3306,
            db='dataservice',
            charset='utf8'
            )
        conn.set_character_set("utf8")
        self.conn = conn
        self.url_head = 'http://110.76.47.134/live/{0}.m3u8'

    def check_all_station_state(self):
        result = self.get_all_station_code()
        url_true=[]
        url_false=[]
        for i in result:
            id = int(i.get('id'))
            url = i.get('url')
            server_status,m3u8_status = url_status(url)
            if server_status and m3u8_status:
                url_true.append(id)
            else:
                url_false.append(id)
        print '\nURL False:',url_false
        print '\nURL True:',url_true
        print 'update table[station_code]:',
        print self.update_station_code_state(1,url_true),
        print self.update_station_code_state(0,url_false),' OK!'
        print 'update table[station]:',
        self.update_station(),' OK!'



    def up_station_code(self):
        print 'Start Of Update Table \'station_code\''
        with codecs.open(station_file, 'r', 'utf-8') as f:
            all_station=[]
            while True:
                line = f.readline()
                if line:
                    station_info = line.split(':')
                    new_code = station_info[0]
                    city = station_info[1]
                    station_info = station_info[2].split('|')
                    city_id = station_info[0]
                    code = station_info[1]
                    name = station_info[2]
                    #print new_code,city,city_id,code,name
                    url = self.url_head.format(new_code)
                    server_status,protocal_status = url_status(url)
                    if server_status:
                        print 'Server:200',
                    else:
                        print 'Server:404'
                        continue
                    if protocal_status:
                        print 'TS:200',
                    else:
                        print 'TS:404'
                        continue
                    station_ord = self.check_station_code(name)
                    if station_ord == 0:
                        print 'insert.'
                        all_station.append((code, new_code, name, url, city_id))
                    else:
                        print 'Update:',
                        if str(station_ord.get('new_code')) == new_code and str(station_ord.get('code')) == code and station_ord.get('url') == url and int(station_ord.get('city_id')) == int(city_id):
                            print 'NO'
                            continue
                        else:
                            print 'YES:',
                            if self.update_station_code(code, new_code, name, url, city_id):
                                print 'True'
                            else:
                                print 'False'

                else:
                    break
        print 'Insere New station:',
        insert_re = self.insert_station_code(all_station)
        if insert_re:
            print insert_re,'True'
        else:
            print 'False'
        print 'End Of Update Table \'station_code\''





#{'code': None, 'id': 2L, 'new_code': None, 'name': u'\u5317\u4eac\u6587\u827a\u5e7f\u64ad'}
    def check_station_code(self, name):
        print 'check',
        cur=self.conn.cursor(Mysqldb.cursors.DictCursor)
        a = cur.execute("select * from station_code where name='%s'" % name)
        r = cur.fetchall()
        cur.close()
        if a and r:
            return r[0]
        else:
            return 0
    def update_station_code(self, code, new_code, name, url, city_id):
        cur=self.conn.cursor()
        a = cur.execute("update station_code set code='%s', new_code='%s',url='%s',city_id='%s' where name='%s'" % (code, new_code, url, name,city_id))
        self.conn.commit()
        cur.close()
        return a

    def get_all_station_code(self):
        print 'check all station state'
        cur=self.conn.cursor(Mysqldb.cursors.DictCursor)
        a = cur.execute("select id,url from station_code")
        r = cur.fetchall()
        cur.close()
        if a and r:
            return r
        else:
            return 0
    def update_station_code_state(self,state,ids):
        #state 1/0. 1 is ok,0 is error.
        #ids List,Ex: [1, 2, 3, 5]
        ids = str(tuple(ids))
        cur=self.conn.cursor()
        a = cur.execute("update station_code set state=%s where id in %s" % (state, ids))
        self.conn.commit()
        cur.close()
        return a

    def insert_station_code(self, all_station):
        cur=self.conn.cursor()
        a =  cur.executemany("insert into station_code(code, new_code, name, url, city_id) values(%s, %s, %s, %s, %s)", all_station)
        self.conn.commit()
        cur.close()
        return a
    def update_station(self):
        cur = self.conn.cursor()
        a = cur.execute("replace into station(id,name,address,state,city_id) select id, name,url,state,city_id from station_code;")
        self.conn.commit()
        cur.close()
        return a


def main(read_file = False):
    """
    read_file = True, 直接读取缓存station_test.txt中的电台，不会从网络更新，默认True从网络更新
    read_file = False, 从网络更新,更新缓存station_test.txt中的电台
    """
    if not read_file:
        try_num = 0
        while True:
            # pass
            try_num += 1
            n = get_station()
            if n:
                print 'station update ok\nfile save OK.\nsleep 10s...'
                time.sleep(10)
                start_check_update_DB()
            else:
                if try_num <= 2:
                    print 'tation update Error ? \ntry again.'
                else:
                    print 'try num',try_num,'Error.'
                    exit('END ALL!')
    else:
        start_check_update_DB()

def start_check_update_DB():
    mainclass = MainClass()
    mainclass.check_all_station_state()
    mainclass.up_station_code()
    print 'Start Update Table station.'
    result = mainclass.update_station()
    print 'End Update Table station.',result
    exit('END ALL!')

def url_status(url):
    try:
        uc = urlcheck.UrlChecker(url, check())
        print '\n',url
        server_status,protocal_status = uc.check_url()
        return server_status,protocal_status
    except Exception,e:
        return False,False
class check(urlcheck.CheckObserver):
    def on_server_check_end(self, status):
        print 'server status is:' + str(status)
        return status
    def on_protocol_check_end(self, status):
        print 'protocol status is:' + str(status)
        return status

if __name__ == '__main__':
    main(read_file = True)
    """
    read_file = True, 直接读取缓存station_test.txt中的电台，不会从网络更新.
    read_file = False, 从网络更新,更新缓存station_test.txt中的电台
    """
