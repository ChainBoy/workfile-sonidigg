#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as Mysqldb
#import sys
import datetime

def check_time(time):
    if len(time.split(' ')) == 1:
        r = time.split('-')
        if len(r) == 3:
            return datetime.timedelta(1)+datetime.datetime(int(r[0]),int(r[1]),int(r[2]))
        else :
            return False

def mkresult(info):
    head = u"""<head><style type="text/css">
      td {
        padding-left: 20;
        white-space:nowrap;
        font-size:16},
      table{
        TABLE-LAYOUT:fixed;
        border:1;
        cellspacing:0;
        cellpadding:0;
        width:200}
        </style></head>
        """
    if info and len(info)>0:
        result = '</td><td>'.join(info[0].keys())
        result = '<tr><td>%s</td></tr>' % '</td><td>'.join(info[0].keys())
        result = result.decode('utf8')
        for i in info:
            tmp = []
            for j in i:
                s = ('%s' % i[j]).replace('&','&amp;').replace('\"','&quot;').replace('<','&lt;').replace('>','&gt;').replace(' ','&nbsp;') 
                tmp.append(s)
            tmp = '<tr><td>%s</td></tr>' % '</td><td>'.join(tmp)
            result += tmp
        result = """%s<center><table><tbody>%s</tbody></table></center>""" % (head,result)
        return result

def get_info_by_station(time_start, time_end, station_id = 0, station = ''):
  station = station.replace('\'','\\\'').replace('"', '\\"')
  time_end = check_time(time_end)
  if not time_end:
    return 'please input and check end time!',[]
  if not len(time_start.split('-')) == 3:
    return 'please input and check start time!',[]
  if station_id:
    sql_ = u"select stations.name as '电台',stamap.id as '电台id',t1.pad_id as '平板id',t1.time as '播放时间',songs.title as '歌名',artists.name as '歌手' from (select onces.* from onces,stations,stamap where stamap.id = %s and onces.station_id = stations.id and stations.name = stamap.name and onces.time between '%s' and '%s')as t1,songs,artists,stations,stamap where t1.song_id = songs.id and t1.artist_id = artists.id and t1.station_id=stations.id and stations.name=stamap.name;" % (station_id, time_start, time_end)
  if station:
    sql_ = u"select stations.name as '电台',stamap.id as '电台id',t1.pad_id as '平板id',t1.time as '播放时间',songs.title as '歌名',artists.name as '歌手' from (select onces.* from onces,stations where stations.name = '%s' and onces.station_id = stations.id and onces.time between '%s' and '%s')as t1,songs,artists,stations,stamap where t1.song_id = songs.id and t1.artist_id = artists.id and t1.station_id=stations.id and stations.name=stamap.name;" % (station, time_start, time_end)
  try:
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='654321',
      port=3306,
      db='shazam',
      charset='utf8'
    )
    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    
    ex = cur.execute(sql_)
    ex = ex if ex else 0
    info = list(cur.fetchall())
    cur.close()
    conn.close()
  except Exception,e:
    return e,[]
  return ex,mkresult(info)

if __name__ == '__main__':
  print '\n'
  station_id = 2
  station = '北京音乐广播'
  time_start = '2014-04-18'
  time_end = '2014-04-18'
  print get_info_by_station(time_start,time_end,station_id = station_id)
  print get_info_by_station(time_start,time_end,station = station)
