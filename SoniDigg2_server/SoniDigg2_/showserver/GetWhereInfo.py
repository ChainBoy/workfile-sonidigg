#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
#!/usr/bin/python
#-*- coding: utf-8 -*-
import datetime
import time
import MySQLdb as Mysqldb
import sys
import json
import decimal

#get all data count by check.no page
def getWhere(list_state, time_start, time_end, num_start = 0, num_end = 10,sql_where = ''):
  info={}
  result_count = 0
  result_num = 0
  code = 500
  percentage = 0
  try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='654321',
      port=3306,
      db='dataservice',
      charset='utf8'
    )
    #conn.set_character_set('utf8')
    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    sql_ = """
    call sp_where_info(%s,%s,%s,'%s','%s',"%s")
    """ % (list_state,num_start, num_end, time_start, time_end,sql_where)
    print sql_
    ex = cur.execute(sql_)
    info = list(cur.fetchall()) #get sql by state,num_start,num_end,time_start,time_end.
    cur.close()

    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    sql_ = "select count(*)as count from listen_log_once where time between '%s' and '%s';" % (time_start, time_end)
    print sql_
    ex = cur.execute(sql_)
    percentage = cur.fetchall()[0].get('count',0)
    if list_state != 0:
      if list_state != 1:
        for i in info:
          i['percentage'] = '%.2f' % float(int(i.get('count')) / percentage * 100)
      else:
        for i in info:
          result_num += i.get('count',0)
        for i in info:
          i['percentage'] = '%.2f' % float(int(i.get('count')) / result_num * 100)
    result_count = len(info)
    if list_state == 0:
      num_start = num_start-1 if num_start > 0 else 0
      info = info[num_start:num_end]
    cur.close()

    conn.close()
    code = 200
  except Mysqldb.Error,e:
    print e
    code = 501
  finally:
    return info, result_count, code, percentage

class getWhereInfo(object):
  def __init__(self,jsonContent):
    self.json_content=jsonContent
    self.result_json={}

  def sql_manual_Reference(self, sql,list_char = [",","'",'"','_','%']):
    for i in list_char:
      sql = sql.replace(i,'\\%s'%i)
    return sql
  def CheckJson(self):
    """
    check the request json data.
    """
    result_json=self.result_json
    #type(self.json_content)-->  dict
    #print 'test:json_content:',self.json_content,'\n'
    time_start=self.json_content.get("time_start")#data start time.
    time_end=self.json_content.get("time_end")#data end time.
    list_state=self.json_content.get("list_state",1)#all(0) or english(1) rank 
    num_start=self.json_content.get("num_start",0) #defult rank start num is 0
    num_end=self.json_content.get("num_end",10)#defult rank end num is 20
    song=self.json_content.get("song","")#defult rank end num is 20
    artist=self.json_content.get("artist","")#defult rank end num is 20

    #check rank artist or song .
    try:
      list_state = list_state if int(list_state) >= 0 else 0
    except Exception:
      list_state = 1
    result_json["list_state"] = list_state

    artist = self.sql_manual_Reference(artist)
    song  = self.sql_manual_Reference(song)
    if song and artist:
      sql_where = "s.artist ='%s' and s.name='%s' " % (artist,song)
    elif song :
      sql_where = "s.name='%s' " % song
    elif artist:
      sql_where = "s.artist='%s' " % artist
    else:
      # no parm
      result_json["code"]=302
    #check num_start and num_end.
    try:
      num_start = int(num_start)
      num_end = int(num_end)
      if num_start > num_end:
        num_start, num_end = num_end, num_start
      elif num_start == num_end:
        num_start, num_end = 1,10
    except Exception:
      num_start,num_end = 1,10
    #check time_start and time_end.
    try:
      time_start = time.mktime(datetime.datetime.strptime(time_start, "%Y-%m-%d").timetuple())
      time_end = time.mktime(datetime.datetime.strptime(time_end, "%Y-%m-%d").timetuple())
    except Exception:
      time_start,time_end = time.time()-7*24*3600,time.time()
    try:
      time_start = datetime.datetime.fromtimestamp(float(time_start)).strftime("%Y-%m-%d")
      try:
        time_end = datetime.datetime.fromtimestamp(float(time_end)).strftime("%Y-%m-%d")
      except Exception:
        time_end = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d")
      time_min = datetime.datetime.fromtimestamp(time.time()-5*365*24*3600).strftime("%Y-%m-%d")
      if time_start > time_end:
        time_start,time_end=time_end,time_start
      elif time_start < time_min:
        time_start = datetime.datetime.fromtimestamp(time.time()-7*24*3600).strftime("%Y-%m-%d")
    except Exception:
      time_start = datetime.datetime.fromtimestamp(time.time()-7*24*3600).strftime("%Y-%m-%d")
    #Get data:song rank(0); artist rank(1)
    if list_state >= 0 :
      result_json["where_info_%s" % list_state],result_json["result_count"],result_json["code"],result_json["percentage"] = getWhere(list_state, time_start,time_end, num_start, num_end,sql_where)
    else:
      result_json["code"]=404

    return result_json

if __name__ == '__main__':
  time_start = '2013-11-11'
  time_end = '2014-11-18'
  print getWhere(0,time_start,time_end,sql_where = "s.artist='周杰伦'")
  print getWhere(1,time_start,time_end,sql_where = "s.artist='周杰伦'")
  print getWhere(2,time_start,time_end,sql_where = "s.artist='周杰伦'")
