#!/bin/python
# -*- coding: utf-8 -*-

import urllib, urllib2, re, time
import datetime
import requests
from urllib  import quote
from movie import Movie
import codecs
import time
from weibo_tools import login
from send_mail import send_mail

START_DAY = '2013-11-01'
END_DAY = '2013-11-24'

REBOT_SLEEP_TIME = 120
SEARCH_SLEEP_TIME = 30

class Controller(object):
    """docstring for Controller"""
    def __init__(self, movie_name):
        super(Controller, self).__init__()
        self.session = login()
        self.movie_name = movie_name
        self.movie = Movie(self.movie_name, self.session)

    def start(self):
        start_time = datetime.datetime.strptime(START_DAY + '-0', '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(END_DAY + '-23', '%Y-%m-%d-%H')
        if start_time < end_time:
            earliest_time = self.search_nomarl(start_time, end_time)
            print '普通搜索获取到的最早时间:',earliest_time
            if earliest_time and earliest_time > start_time:
                print '普通搜索没有搜索到足够数据，继续往前爬取!'
                self.search_by_day(start_time, earliest_time)
        else:
            print 'ERROR:起始时间必须比结束时间早!'

    def search_nomarl(self, start_time, end_time):
        '''普通搜索，不设置时间'''
        url = 'http://s.weibo.com/weibo/'+ quote(self.movie_name)
        print url
        while True:
            print 'url -------- true:'
            page_content = self.session.get(url).content
            with file('tttttt','w') as f:
                f.write(page_content)
            print 'file tttttt save ok..~!'
            count = self.get_page_count(page_content)

            if count == 0:
                if self.is_rebot(page_content):
                    print '变机器人了,需要帮助!!'
                    time.sleep(REBOT_SLEEP_TIME)
                    continue

            return self.handle_pages(url, count, self.movie.id, start_time, end_time)

    def search_by_day(self, start_time, end_time):
        '''根据天做分割，如果在普通搜索不能完成时间段所有信息的时候使用'''
        end_time = datetime.datetime.strftime(end_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d') +':' + datetime.datetime.strftime(end_time, '%Y-%m-%d')
            url = 'http://s.weibo.com/weibo/'+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)
            print url
            while True:
                content = self.session.get(url).content
                count = self.get_page_count(content)

                if count == 0:
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助!!'
                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        print '数据0页～～！'
                        break
                #如果页数低于50,直接爬取
                elif count < 50:
                    print '数据小于50页，直接爬取'
                    break
                #如果页数超过50,分小时爬取
                elif count == 50:
                    print '数据等于50页，分片爬取'
                    self.search_by_hour(end_time)
                    continue

            day = datetime.datetime.strftime(end_time, '%Y-%m-%d')
            search_start_time = datetime.datetime.strptime(day + '-0', '%Y-%m-%d-%H')
            search_end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')

            self.handle_pages(url, count, self.movie.id, search_start_time, search_end_time)
            end_time = end_time - datetime.timedelta(days=1)
            if start_time > end_time:
                break
            time.sleep(SEARCH_SLEEP_TIME)

    def search_by_hour(self, day):
        '''针对一天按小时分割搜索'''
        day = datetime.datetime.strftime(day, '%Y-%m-%d')
        start_time = datetime.datetime.strptime(day + '-0', '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')

        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d-%H') +':' + datetime.datetime.strftime(end_time, '%Y-%m-%d-%H')
            url = 'http://s.weibo.com/weibo/'+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)
            print url
            while True:
                content = self.session.get(url).content
                count = self.get_page_count(content)

                if count == 0:
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助!!'
                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        break
                else:
                    break

            hour = datetime.datetime.strftime(end_time, '%Y-%m-%d-%H')
            search_start_time = datetime.datetime.strptime(hour + '-0', '%Y-%m-%d-%H-%M')
            search_end_time = datetime.datetime.strptime(hour + '-59', '%Y-%m-%d-%H-%M')

            self.handle_pages(url, count, self.movie.id, search_start_time, search_end_time)
            end_time = end_time - datetime.timedelta(hours=1)
            if start_time > end_time:
                break
            time.sleep(SEARCH_SLEEP_TIME)

    def get_page_count(self, content):
        max_count = 0
        result = re.findall(r'&page=\d+', content)
        with file('ssssssss','w') as f:
            f.write(content)
        print 'file ssssssss save ok..~!'
        print 'page ----------------------->:',result
        for index in result:
            info = index.split('=')
            if int(info[1]) > max_count:
                max_count = int(info[1])
        print '当前搜索结果页数:', max_count
        return max_count

    def is_rebot(self, content):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            send_mail('机器人', '快填验证码！')
            return True
        else:
            return False

    def handle_pages(self, url_head, page_count, movie_id, start_time, end_time):
        earliest_time = False
        for i in xrange(1, page_count + 1):
            page = str(i)
            url = url_head +"&page=" + page
            print url
            isreboot = False
            isear = False
            while True:
                response = self.session.get(url)
                content = response.content
                isreboot = self.is_rebot(content)
                if isreboot:
                    print '变机器人了,需要帮助!!'
                    time.sleep(REBOT_SLEEP_TIME)
                    continue
                earliest_time = self.movie.decode_content(content, movie_id, start_time, end_time)
                if not earliest_time:
                    isear = True
                    break
                print '现在是第',page,'/',page_count,'页'
                time.sleep(5)
                break
            if isear:
                break
        return earliest_time

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

if __name__ == '__main__':
    movies = ["怒放2013"]#,"森林战士","扫毒","郑和1405：魔海寻踪","飘落的羽毛","野草莓","幸福快递","意外的恋爱时光","我爱的是你爱我","偷书贼",          "清须会议","同屋/室友", "饥饿游戏2:星火燎原","一切都从和你相逢开始","最高通缉犯", "辉夜姬物语", "博士之日", "至尊寿喜烧"]
    for movie in movies:
        Controller(movie).start()
        send_mail('数据爬虫', '"' + movie + '"数据已经爬取完成。')
