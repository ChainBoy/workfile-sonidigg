#!/bin/python
# -*- coding: utf-8 -*- 

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import jieba.analyse
from model #import Movie_Keyword, Movie, Detail
from movie import Movie

class New_Keyword(object):
	"""docstring for ClassName"""
	def __init__(self, movie_name):
		super(ClassName, self).__init__()
		engine = create_engine(model.db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine))
        self.name = movie_name
        self.id = self.init_movie()

    def init_movie(self):
        movie = self.db.query(model.Movie).filter(model.Movie.name == self.name).first()
        if not movie:
            print "数据库电影表中没有这部电影，插值!"
            movie = model.Movie(name=self.name)
            self.db.add(movie)
            self.db.commit()
        return movie.id
		