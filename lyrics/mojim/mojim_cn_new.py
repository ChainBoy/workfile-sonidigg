#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import subprocess

def main():
    print '-'*50, 'Start Work.', '-'*50
    type_list = get_all_type_list()
    for ty in type_list:
        get_singers_list_by_type(ty)

def get_singers_list_by_type(ty):
    print 'Start:', ty
    url = ty
    singer_type = url[22]
    del ty
    try:
        con = requests.get(url, timeout=30).content.decode('utf8')
        url_name = re.findall('href="/cnh(\d{2,}).htm".+?>(.+?)</a', con)
        for i in url_name:
            if len(i) > 1:
                get_lyric_by_singer(i, singer_type)
    except Exception, e:
        print e

def get_lyric_by_singer(singer_num, singer_type):
    null_num = 0
    for i in xrange(1, 10000):
        if null_num > 1:
            break
        try:
            url = 'http://mojim.com/cn%sx%s.htm' % (singer_num[0], i)
            print url
            con = requests.get(url, timeout=30).content.decode('utf8')
            singer = re.findall(u'<!--歌手姓名-->.+?">(.+?)</a', con, re.S)[0].strip()
            con = re.findall(u'<!--专辑歌词-->.+?<!--专辑列表-->', con, re.S)[0]
            con = re.sub(u'<br />更多更详尽歌词 在  <a href="http://mojim.com" >※ Mojim.com　魔镜歌词网 </a><br />', '',con, re.S)
            if len(con) < 1:
                null_num += 1
                continue
            get_lyric_by_content(con, singer, singer_type)
        except Exception, e:
            print 'Exception:', e
            null_num += 1
            continue

def get_lyric_by_content(con, singer, singer_type):
    print '='*20, 'Start Spider Singer:', singer, '='*20
    song_lyrics = re.findall(u'<dt>.+?title="(.+?)".+?<dd>(.+?)</dd>', con, re.S)
    for song_lyric in song_lyrics:
        get_lyric_by_link(song_lyric, singer, singer_type)

def get_lyric_by_link(song_lyric, singer, singer_type):
    song = song_lyric[0].replace(u'歌词','').strip()
    lyric = song_lyric[1]
    lyric = re.sub('(<.+?>)|document.write\(.+?\);', '\n', lyric).replace(u'更多更详尽歌词 在 \n※ Mojim.com　魔镜歌词网', '').replace('\r', '').strip()
    try:
        lyric = '%s\t%s\t%s\t%s\n%s' % ('-'*20, song, singer, '-'*20, lyric)
        path = 'result/%s/%s/' % (singer_type, singer)
        filename = '%s==>%s.lyric' % (song, singer)
        save_lyric(path, filename, lyric)
    except Exception,e:
        print e
        raise e

def save_lyric(path, filename, text):
    subprocess.Popen('mkdir -p "%s"' % path, shell=True)
    import time
    time.sleep(1)
    all_path = '%s%s' % (path, filename)
    with file(all_path, 'w')as f:
        f.write(text.encode('utf8'))
    print '=' * 150 + '\b> 【', filename,'】OK!'

def get_all_type_list():
    all_list = [
            'http://mojim.com/cnzlha_all.htm',
        ]
    return all_list

if __name__ == '__main__':
    main()
