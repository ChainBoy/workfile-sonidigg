#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import subprocess

def main():
    print '-'*50, 'Start Work.', '-'*50
    type_list = get_all_type_list()
    for ty in type_list:
        get_singers_list_by_type(ty)

def get_singers_list_by_type(ty):
    print 'Start:', ty
    url = ty
    del ty
    try:
        con = requests.get(url, timeout=30).content.decode('utf8')
        url_name = re.findall('href="/twh(\d{2,}).htm".+?>(.+?)</a', con)
        for i in url_name:
            if len(i) > 1:
                get_lyric_by_singer(i)
    except Exception, e:
        raise
        print e

def get_lyric_by_singer(singer_num):
    singer = singer_num[1]
    print '='*20, 'Start Spider Singer:', singer, '='*20
    null_num = 0
    for i in xrange(1, 10000):
        if null_num > 5:
            break
        try:
            url = 'http://mojim.com/tw%sx%s.htm' % (singer_num[0], i)
            print url
            con = requests.get(url, timeout=30).content.decode('utf8')
            con = re.findall(u'<!--專輯歌詞-->.+?<!--專輯列表-->', con, re.S)[0]
            con = re.sub(u'<br />更多更詳盡歌詞 在 <a href="http://mojim.com" >※ Mojim.com　魔鏡歌詞網 </a><br />', '',con, re.S)
            if len(con) < 1:
                null_num += 1
                continue
            get_lyric_by_content(con, singer)
        except Exception, e:
            null_num += 1
            continue

def get_lyric_by_content(con, singer):
    link_songs = re.findall(u'_blank.+?href="/twthx(.+?)".+?title="修改【(.+?)】歌詞', con, re.S)
    for link_song in link_songs:
        get_lyric_by_link(link_song, singer)

def get_lyric_by_link(link_song, singer):
    url = 'http://mojim.com/twthx%s'% link_song[0]
    song = link_song[1]
    print 'DN', '='*20, 'Spider Singer:', singer, '===>', 'Song:', song
    try:
        con = requests.get(url, timeout=30).content.decode('utf8')
        print url
        con = re.findall(u'textarea.+?>修改 【(.+?)】【(.+?)】歌詞(.+?)</textarea', con, re.S)[0]
        if len(con) > 2:
            singer = con[0].strip()
            song = con[1].strip()
            lyric = con[2].strip()
            lyric = '%s\t%s\t%s\t%s\n%s' % ('-'*20, song, singer, '-'*20, lyric)
            path = 'result/%s/' % singer
            filename = '%s==>%s.lyric' % (song, singer)
            save_lyric(path, filename, lyric)
    except Exception,e:
        raise e

def save_lyric(path, filename, text):
    subprocess.Popen('mkdir -p "%s"' % path, shell=True)
    all_path = '%s%s' % (path, filename)
    with file(all_path, 'w')as f:
        f.write(text.encode('utf8'))
    print '=' * 150, '【', filename,'】OK!'

def get_all_type_list():
    all_list = [
            'http://mojim.com/twzlha_all.htm',
            'http://mojim.com/twzlhb_all.htm',
            'http://mojim.com/twzlhc_all.htm',
            'http://mojim.com/twzlhe_all.htm',
            'http://mojim.com/twzlhf_all.htm',
            'http://mojim.com/twzlhz_all.htm'
        ]
    return all_list

if __name__ == '__main__':
    main()
