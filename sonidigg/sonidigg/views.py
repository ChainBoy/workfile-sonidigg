# coding:utf-8
# django imports
from django.shortcuts import render_to_response, redirect
from django.template import Template, Context
from django.template import RequestContext
import os, datetime
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as user_login, logout as user_logout
from django.contrib.auth.views import login 
# project imports
# from admin import *
from sonidigg.forms import *
from sonidigg.models import *
from django.utils.safestring import SafeString  # 安全解析json数据的方式
# def view(request):
#    return render(request, 'template.html', {'upload_params': SafeString(json_string)})
# JSON
import simplejson
import json
import urllib2,urllib
import requests
from django.contrib.sessions.models import Session

# l1.append(a(name,times,count))定义一个累
def get_strjson():  # 获取json值的方法
	fileopen = open('artistrank.txt', 'r+')
	lines = fileopen.read().strip()
	lines = simplejson.loads(lines)
	return lines

def getserver():#请求服务器
# curl -H "Content-Type:application/json" -sd "{\"id"\":\"2\""}" http://sonidigg.f3322.org:8003/API/
#curl -H "Content-Type:application/json" -sd "{\"id"\":\"1\",\"list_state\":\"0\",\"time_start\":\"2013-11-15\",\"time_end\":\"2013-11-25\",\"num_start\":\"1\",\"num_end\":\"40\""}" http://sonidigg.f3322.org:8003/API/

	url="http://sonidigg.f3322.org:8003/API/"
	urllib2.urlopen(url)
	
	
def get_navactive(active=None):  # 设置导航栏点击效果
	navactive = {'mainpage':'', 'artistdetails':'', 'chart':'', 'price':'', 'reports':'', 'otherpage':''}
	if navactive:
		navactive[active] = 'active'
	return navactive

def requestserver():#curl链接数据库的方法
	weekmainpage=open('weekmainpage.txt','w+')
	request_data={'id':2}
	req = urllib2.Request('http://sonidigg.f3322.org:8003/API/',data=simplejson.dumps(request_data),headers={"Content-Type": "application/json"})
	response =urllib2.urlopen(req)
	weekmainpage.writelines(response)
# 	response_data=simplejson.loads(response)
def requestartistserver():
	#{\"id"\":\"1\",\"list_state\":\"0\",\"time_start\":\"2013-11-15\",
# 	\"time_end\":\"2013-11-25\",\"num_start\":\"1\",\"num_end\":\"40\""}
	weekartistrank=open('weekartistrank.txt','w+')
	request_data={"id":1,"list_state":0,"time_start":"2013-11-28","time_end":"2013-12-3","num_start":"1","num_end":"100"}
	req = urllib2.Request('http://sonidigg.f3322.org:8003/API/',data=simplejson.dumps(request_data),headers={"Content-Type": "application/json"})
	response =urllib2.urlopen(req)
	print response
	weekartistrank.writelines(response)
# 网站页面
def mainpage(request):  # 网站主页
	
# 	requestserver()
# 	requestartistserver()
	
	fileopen1 = open('weekartistrank.txt', 'r+')
	artistlines = fileopen1.read().strip()
	artistlines = simplejson.loads(artistlines)
	artistlines = artistlines['rank_info_0'][:7]
	
	
	fileopen2 = open('weekmainpage.txt', 'r+')
	maincounts = fileopen2.read().strip()
	maincounts = simplejson.loads(maincounts)
	maincounts=maincounts['preview_info']
	moneycount=(int(maincounts['play_time'])/60)*0.3
	
	fileopen3=open('weeksongrank.txt','r+')
	songlistlines=fileopen3.read().strip()
	songlistlines=simplejson.loads(songlistlines)
	songlistlines = songlistlines['rank_info_1'][:7]
# 
# 					{
# 					    "code":200,
# 					    "preview_info":{
# 					        "artist_count":10077,
# 					        "play_num":124018,
# 					        "play_time":7681014,
# 					        "song_count":41681
# 					    }
# 					}
# 	navactive = get_navactive('mainpage')
# 	navactive['username'] = request.user.username
# 	navactive['id'] = request.user.id
# 	navactive['maincounts']=maincounts['preview_info']
	username=request.user.username
	userid=request.user.id
	print maincounts
	return render_to_response('mainpage.html', {'mainpage':'active','username':username,'userid':userid,'maincounts':maincounts,'artistlines':artistlines,'moneycount':moneycount,'songlistlines':songlistlines}, context_instance=RequestContext(request))

def artistdetails(request):  # 歌手详情
	navactive = get_navactive('artistdetails')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('artistdetails.html', navactive, context_instance=RequestContext(request))

def chart(request):  # 电台详情
	navactive = get_navactive('chart')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('chart.html', navactive, context_instance=RequestContext(request))

def reports(request):  # 数据中心
	navactive = get_navactive('reports')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('reports.html', navactive, context_instance=RequestContext(request))

def price(request):  # 版权费
	navactive = get_navactive('price')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('price.html', navactive, context_instance=RequestContext(request))

def otherpage(request):  # 其他页面
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('otherpage.html', navactive, context_instance=RequestContext(request))
	
def setting(request):  # 用户设置
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('setting.html', context_instance=RequestContext(request))

# #预留模版和页面
# from django.conf.urls.defaults import *
# from django.views.generic import list_detail
# After importing django.views.generic.list.ListView
# You just need to change list_detail.object_detail to ListView.as_view()
from django.views.generic.list import *
							
def search(request):  # 搜索框方法
	name = request.REQUEST['search']
	if name:
		pass
# 		extra_lookup_kwargs={'name__icontains':name}
# 		extra_context={'searchvalue':name}
# 		return ListView(request,Address,paginate_by=10,
# 					extra_context=extra_context,extra_lookup_kwargs=extra_lookup_keargs)
# 		return ListView(request,Address.objects.filter(name__icontains=name),
# 					paginate_by=10,extra_context=extra_contex
def jsjson(request):	
	return render_to_response("jsjson.html", context_instance=RequestContext(request))

def test(request):  # 测试页面
	lines = get_strjson()
	print lines
	print lines['list_state']
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	infolist=lines['info']
	request.session['username']=request.user.username
	request.session['id']=request.user.username
	return render_to_response("test.html", {"infolist":infolist}, context_instance=RequestContext(request))

def sonidigg(request):  # 本项目下主网站地址
	return render_to_response('sonidigg.html', context_instance=RequestContext(request))

def mainpagelayout(request):  # 主页模版
	return render_to_response('mainpagelayout.html', context_instance=RequestContext(request))

def page1(request):  # page1
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('page1.html', context_instance=RequestContext(request))

def page2(request):  # page2
	fileopen1 = open('weekartistrank.txt', 'r+')
	artistlines = fileopen1.read().strip()
	artistlines = simplejson.loads(artistlines)
	artistlines = artistlines['rank_info_0']
	
	fileopen2=open('weeksongrank.txt','r+')
	songlistlines=fileopen2.read().strip()
	songlistlines=simplejson.loads(songlistlines)
	songlistlines = songlistlines['rank_info_1']
	
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
# 	print artistlines
	return render_to_response('page2.html', {"artistlines":artistlines, 'songlistlines':songlistlines}, context_instance=RequestContext(request))
	
def page3(request):  # page3
	lines = get_strjson()
	artistlines = lines['artistList']
	navactive = get_navactive('otherpage')
	navactive['username'] = request.user.username
	navactive['id'] = request.user.id
	return render_to_response('page3.html', {"artistlines":artistlines}, context_instance=RequestContext(request))


####用户登陆注册部分
def userlogin(request):  # userlogin用户登陆
	error = {}
	if request.method == 'POST':
		new_user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if new_user:
			user_login(request, new_user)
			return redirect("/")
		error = {'error':"用户名或密码错误，重新登陆"}
	return render_to_response('userlogin.html', error, context_instance=RequestContext(request))

def usersignup(request):  # usersignup用户注册
	error = {}
	form = UserForm()
	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			new_user = form.save()
			new_user = authenticate(username=request.POST['username'], password=request.POST['password1'])
			user_login(request, new_user)
			info = Userinfo(user=request.user)
			info.save()
			return redirect("/")
		error = {'error':"用户名已被注册或用户名/密码不匹配，请重新选择其他用户名进行注册"}
	return render_to_response('usersignup.html', error, context_instance=RequestContext(request))

def userlogout(request):  # user logout
	user_logout(request)
	return redirect('/')
