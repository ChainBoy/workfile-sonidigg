# coding:utf-8
"""
#用户验证USERFORM，调用DJANGO的META元方法
"""
from django import forms
from django.contrib.auth.forms import *
from django.forms.models import ModelForm
from sonidigg.models import *

class UserForm(UserCreationForm):
#     error_messages = {
#         'duplicated_username':('用户名已经存在,请选择其他名称'),
#          'password_mismatch':('密码不匹配'),
#     }
#     username = forms.RegexField(label=("用户名"), max_length=30,
#                  help_text=("小于30个字符的字母数字@/./+/-/_"),error_messages={'invalid':("用户名不符合规范@/./+/-/_")})
    username = forms.CharField(label=("用户名"), max_length=30)
    email = forms.EmailField(label=("email"))
    password1 = forms.CharField(label=("密码"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=("确认密码"), widget=forms.PasswordInput, help_text=("输入确认密码"))
    
#     class Meta(UserCreationForm.Meta):
#         model = User

class PlayedArtistListForm(forms.Form):  # 歌手点播次数排名
    artistandk=forms.CharField()
    artistname=forms.CharField()
    artistplayedtimes=forms.CharField()
    

class PlayedSongListForm(forms.Form):# 歌曲播放次数排名
    songrank=forms.CharField()
    songplayedname=forms.CharField()
    songplayedtimes=forms.CharField()
