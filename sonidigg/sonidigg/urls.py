# coding:utf-8
# django imports
from django.conf.urls import patterns, include, url
# project import
from sonidigg.views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.auth.decorators import login_required
admin.autodiscover()

def url_login(regex, views, *args, **kwargs):  # 添加页面需要登陆才能察看语句
    return url(regex, login_required(views, login_url="userlogin"), *args, **kwargs)
'''
#增加只有登陆才能察看的设置Url由url(r'^test/$',test),>>>>该写成url_login(r'^test/$',test),
'''
urlpatterns = patterns('',
    # Examples:
    url(r'^$', mainpage),
# 网站模版和其他页面预留
    url(r'^mainpagelayout/$', mainpagelayout),
    url(r'^page1/$', page1),
    url(r'^page2/$', page2),
    url(r'^page3/$', page3),
    url(r'^sonidigg/$', sonidigg),
    url(r'^test/$', test),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/$', search),
    url(r'^jsjson/$', jsjson),
# 网站页面  
    url(r'^mainpage/$', mainpage),
    url(r'^artistdetails$', artistdetails),
    url(r'^chart/$', chart),
    url(r'^reports/$', reports),
    url(r'^price/$', price),
    url(r'^setting/$', setting),
    url(r'^otherpage/$', otherpage),
# 用户登陆注册页面
    url(r'^usersignup/$', usersignup),
    url(r'^userlogin/$', userlogin, name="userlogin"),
    url(r'^userlogout/$', userlogout),
    
    # url(r'^sonidigg/', include('sonidigg.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
