# coding:utf-8
"""
 这应用models
"""
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class Userinfo(models.Model):  # views中显示用户信息
    intro = models.TextField()
    photo = models.ImageField(upload_to="photo")
    user = models.ForeignKey(User)
    role = models.CharField(max_length=64)
    
    def __unicode__(self):
        return self.intro
        return self.user
        return self.role
    
    def get_absolute_url(self):
        return reverse('set_profile_detail', keargs={'pk':self.pk})
    
class PlayedArtistList(models.Model):  # 按播放次数歌手排名
    artistname = models.CharField(max_length=64)
    artistplayedtimes = models.CharField(max_length=64)
    
    def __unicode__(self):
        return self.artistname
        return self.artistplayedtimes

class PlayedSongList(models.Model):  # 按播放次数歌曲排名 
    songplayedname = models.CharField(max_length=64)
    songplayedtimes = models.CharField(max_length=64)
    
    def __unicode__(self):
        return self.songplayedname
        return self.songplayedtimes

class BillboardTop(models.Model):  # BillboardTop歌手排行榜
    billboardname = models.CharField(max_length=64)
    billboardartist = models.CharField(max_length=64)
    
    def __unicode__(self):
        return self.billboardname
        return self.billboardartist
