$(function () {
	
	var d1 = [30, 30, 42, 44, 13, 29, 28, 19, 21, 14, 18, 11, 17, 71, 75, 95, 121, 124, 190, 170, 149, 91, 94, 85],
			d2 = [ 6, 12, 17, 7, 29, 41, 26, 37, 39, 17, 23, 30, 33, 48, 50, 48, 50, 32, 20, 24, 27, 35, 32, 28];			
				
	var dt1 = [], 
			dt2 = [], 
			st = new Date(2013, 11, 1).getTime();
	
	for( var i = 0; i < d2.length; i++ ) {
		dt1.push([st + i * 3600000, parseFloat( (d1[i]).toFixed( 3 ) )]);
		dt2.push([st + i * 3600000, parseFloat( (d2[i]).toFixed( 3 ) )]);
	}
	
	var data = [{ 
		data: dt1,
		label: '播放次数'		
	 }, { 
	 	data: dt2, 
	 	label: '播放电台', 
	 	points: { show: false }, 
	 	lines: { lineWidth: 1, fill: false }	 	
 	 }];
	
	Charts.line ( '#line-chart', data);
			
});