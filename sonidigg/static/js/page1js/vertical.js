$(function () {
	
    var ds = [];
	var data = [];
	
	ds.push ([[1,105],[2,124],[3,107],[4,125],[5,96]]);
	ds.push ([[1,13],[2,29],[3,25],[4,23],[5,31]]);
	ds.push ([[1,108],[2,113],[3,119],[4,105],[5,90]]);
						
	data.push ({
		data: ds[0], 
		label: '歌曲播放次数', 
		bars: {
		barWidth: 0.15, 
			order: 1
		}
	});
	data.push	({
		data: ds[1], 
		label: '播放电台总计', 
		bars: {
		barWidth: 0.15, 
			order: 2
		}
	});
	data.push ({
		data: ds[2], 
		label: '热度排名总计', 
		bars: {
		barWidth: 0.15, 
			order: 3
		}
	});
	
	Charts.vertical ('#vertical-chart', data);
				
});

