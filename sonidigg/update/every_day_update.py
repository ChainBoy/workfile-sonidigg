#!/usr/bin/python
#-*- coding: utf-8 -*-

# the file is to update DB:dataservice Table:song_play_log data.
#                Don't Delete !!!
#                Don't Remove !!!

#By: zhangzhipeng
#Mail:qq1126918258@gmail.com


import os, platform, logging
import time
import datetime
import urllib2
import simplejson


class Update_dataservice_song_play_log:
  """docstring for ClassName"""
  def  __init__(self):
    self.Send_Mail_to = []
    self.now = time.time()
    self.mon_Time = datetime.datetime.fromtimestamp(self.now - 31*24*3600).strftime("%Y-%m-%d")#31 Day时间 --> monce start time
    self.wee_Time = datetime.datetime.fromtimestamp(self.now - 8*24*3600).strftime("%Y-%m-%d")#8 Day时间 --> week start time
    self.yes_Time = datetime.datetime.fromtimestamp(self.now - 24*3600).strftime("%Y-%m-%d")#yestoday 时间 --> end time.
    self.tod_Time = datetime.datetime.fromtimestamp(self.now).strftime("%Y-%m-%d")#当日时间
    self.api = 'http://sonidigg.f3322.org:8003/API/'
    self.sub = ""
    self.mail_sub = ""
    self.mail_content = ""
    self.log_file = "update_ui_data.log"
    self.dict_type_name = {
      "1":"Preview",
      "2":"Rank-song-week",
      "3":"Rank-song-monce",
      "4":"Rank-artist-week",
      "5":"Rank-artist-monce",
      "6":"24-hour-timeline",
    }
    #self.logging_file
    self.content = ""
    #check log is True.
    self.re_log_file = ""
    if not os.path.isfile(self.log_file):
      self.re_log_file = "%s don't find,New Log file." % self.log_file
      self.sub += "[Warning]:No File log."
    if platform.platform().startswith('Windows'): #判断操作系统类型，获取当前用户的根目录
      self.logging_file = os.path.join(os.getenv('HOMEDRIVE'), os.getenv('HOMEPATH'), self.log_file)
    else:
      self.logging_file = os.path.join(os.getcwd(),self.log_file)
    #satrt save log ~
    s = "THE START.".center(30,'-')
    self.save_log(0, s)
    del s
    self.save_log(0,self.flush_content("Start update UI data log."))
    if self.re_log_file:
      self.save_log(2,self.flush_content(self.re_log_file))
    self.start()
    s = "THE END.".center(30,'-')
    self.save_log(0, s)
    del s
  def start(self):
    try:
      self.save_log(1,self.flush_content("Start update UI Buffer Data."))
      self.save_file(1)
      self.save_file(2)
      self.save_file(3)
      self.save_file(4)
      self.save_file(5)
      self.save_file(6)
      self.mail_sub += "UI Buffer Query OK."

    except Exception,e:
      print e
      self.save_log(2,self.flush_content(e))
      self.mail_sub += "[ERROR]:UPDATE UI Buffer Filed."
    finally:
      self.mail_content += "\n-".center(30,'-')
      self.save_log(1,self.flush_content("End update UI Buffer Data."))
      self.save_log(0,self.flush_content("End update UI data log.All time :%0.3f" % (time.time()-self.now)))
      mail_state, mail_result = self.send_mail(self.mail_sub, self.mail_content)
      self.save_log(1,mail_result + ",To:".join(self.Send_Mail_to))

  def save_fun(save_file):
    def _save_fun(self,type_):
      name_ = self.dict_type_name.get(str(type_))
      self.mail_content += name_.center(30,'-')
      if not os.path.isfile(name_+".txt"):
        self.save_log(2,self.flush_content("File[%s.txt] Not find,New data file." % name_))
        self.mail_sub += "[Waring:Not Find File:%s.txt]" % name_
      s_time = time.time()
      self.save_log(1, self.flush_content("Start Read data [%s] from server,type:%s." % (name_, type_)))
      save_file(self, type_)
      self.save_log(1, self.flush_content("End Save data [%s],type:%s,time:%0.3f." % (name_, type_, time.time()-s_time)))
    return _save_fun

  @save_fun
  def save_file(self,type_):
    """
    type_ 1 , Get Preview. -- > Preview.txt
    type_ 2 , Get Rank-song-100 a week. -- > Rank-song-week.txt
    type_ 3 , Get Rank-song-100 a monce. -- > Rank-song-monce.txt
    type_ 4 , Get Rank-artist-100 a week. -- > Rank-artist-week.txt
    type_ 5 , Get Rank-artist-100 a monce. -- > Rank-artist-monce.txt
    type_ 6 , Get 24-hour-timeline a day. -- > 24-hour-timeline.txt
    """
    request_data = {}
    data_name = ""
    if type_ == 1:
      request_data={'id':2}
    elif type_ == 2:
      request_data={"id":1,"list_state":1,"time_start":self.wee_Time,"time_end":self.yes_Time,"num_start":"1","num_end":"100"}
    elif type_ == 3:
      request_data={"id":1,"list_state":1,"time_start":self.mon_Time,"time_end":self.yes_Time,"num_start":"1","num_end":"100"}
    elif type_ == 4:
      request_data={"id":1,"list_state":0,"time_start":self.wee_Time,"time_end":self.yes_Time,"num_start":"1","num_end":"100"}
    elif type_ == 5:
      request_data={"id":1,"list_state":0,"time_start":self.mon_Time,"time_end":self.yes_Time,"num_start":"1","num_end":"100"}
    elif type_ == 6:
      request_data={'id':3}
    request=urllib2.Request(self.api,data=simplejson.dumps(request_data),headers={"Content-Type": "application/json"})
    response =urllib2.urlopen(request)
    # print simplejson.loads(response.read())
    # return
    with file(self.dict_type_name.get(str(type_)) + '.txt', 'w') as f:
      f.write(response.read())

  def flush_content(self, content):
    self.content = content
    self.mail_content += "\n%s" % self.content
    return content
  #配置调试参数  
  def  save_log(self, cate, ex):
    logging.basicConfig(
      level=logging.DEBUG,
      format='%(asctime)s : %(levelname)s : %(message)s',
      filename = self.logging_file,
      filemode = 'a',
      )
    if cate == 0:
      logging.debug(ex) #debug开始调试
    if cate == 1 :
      logging.info(ex) #测试阶段
    if cate == 2:
      logging.warning(ex)#捕获警告

  def send_mail(self, sub, content):
      '''
      mailto_list:发给谁
      sub:主题
      content:内容
      send_mail("aaa@126.com","主题","内容")
      '''
      #!/usr/bin/env python
      # -*- coding: gbk -*-
      #导入smtplib和MIMEText
      import smtplib
      from email.mime.text import MIMEText
      #############
      #要发给谁，这里发给2个人
      mailto_list = ["qq1126918258@gmail.com"]#,"mydreambei@gmail.com","myzlkun@163.com","zhangtianyi1234@126.com","baonanhai@gmail.com"]#, "1126918258@qq.com", "peng2011@vip.qq.com"]
      self.Send_Mail_to = mailto_list
      #####################
      #设置服务器，用户名、口令以及邮箱的后缀
      #mail_host="smtp.136.com"
      mail_host = "smtp.gmail.com"
      mail_user = "qq1126918258"
      mail_pass = "zhang?2011"
      mail_postfix="gmail.com"
      ######################
      me=mail_user+"<"+mail_user+"@"+mail_postfix+">"
      msg = MIMEText(content, 'base64', 'utf-8')
      msg['Subject'] = sub
      msg['From'] = me
      msg['To'] = ";".join(mailto_list)
      try:
          s = smtplib.SMTP(mail_host)
          s.docmd("EHLO server")#对于使用gmail的情况无本行则会出现 "SMTP AUTH extension not supported by server."
          s.starttls()#使用ssl加密
          #s.connect(mail_host)
          s.login(mail_user,mail_pass)
          s.sendmail(me, mailto_list, msg.as_string())
          s.close()
          return True,"Send Email OK."
      except Exception, e:
          print str(e)
          return False,"Send Email Error."


if __name__ == '__main__':
  new_update_class = Update_dataservice_song_play_log()
