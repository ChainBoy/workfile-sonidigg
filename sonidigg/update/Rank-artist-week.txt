{
    "code":200,
    "list_state":0,
    "rank_info_0":[
        {
            "artist":"\u4f5a\u540d",
            "artist_id":20,
            "play_num":559405,
            "play_num_add":558796,
            "play_time":523080,
            "rank":1,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5218\u5fb7\u534e",
            "artist_id":84,
            "play_num":5400,
            "play_num_add":5334,
            "play_time":327600,
            "rank":2,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u7fa4\u661f",
            "artist_id":402,
            "play_num":5382,
            "play_num_add":5319,
            "play_time":71955,
            "rank":3,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5353\u4f9d\u5a77",
            "artist_id":156,
            "play_num":3990,
            "play_num_add":3949,
            "play_time":44100,
            "rank":4,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u5b66\u53cb",
            "artist_id":49,
            "play_num":3496,
            "play_num_add":3443,
            "play_time":218500,
            "rank":5,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"DJ",
            "artist_id":251,
            "play_num":2185,
            "play_num_add":2144,
            "play_time":0,
            "rank":6,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u90ed\u5bcc\u57ce",
            "artist_id":167,
            "play_num":2142,
            "play_num_add":2111,
            "play_time":88200,
            "rank":7,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9648\u5955\u8fc5",
            "artist_id":416,
            "play_num":1924,
            "play_num_add":1886,
            "play_time":52910,
            "rank":8,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u90d1\u79c0\u6587",
            "artist_id":89,
            "play_num":1650,
            "play_num_add":1615,
            "play_time":80520,
            "rank":9,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9093\u4e3d\u541b",
            "artist_id":7,
            "play_num":1586,
            "play_num_add":1560,
            "play_time":4880,
            "rank":10,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9ece\u660e",
            "artist_id":28,
            "play_num":1584,
            "play_num_add":1556,
            "play_time":58080,
            "rank":11,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5e84\u5b66\u5fe0",
            "artist_id":863,
            "play_num":1536,
            "play_num_add":1510,
            "play_time":0,
            "rank":12,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"Beethoven",
            "artist_id":571,
            "play_num":1343,
            "play_num_add":1323,
            "play_time":0,
            "rank":13,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9648\u6167\u7433",
            "artist_id":424,
            "play_num":1260,
            "play_num_add":1227,
            "play_time":51660,
            "rank":14,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u53e4\u5de8\u57fa",
            "artist_id":1110,
            "play_num":1134,
            "play_num_add":1106,
            "play_time":424440,
            "rank":15,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"TWINS",
            "artist_id":75,
            "play_num":1121,
            "play_num_add":1093,
            "play_time":8260,
            "rank":16,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5bb9\u7956\u513f",
            "artist_id":115,
            "play_num":1080,
            "play_num_add":1045,
            "play_time":19200,
            "rank":17,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8521\u4f9d\u6797",
            "artist_id":192,
            "play_num":850,
            "play_num_add":827,
            "play_time":67830,
            "rank":18,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8bb8\u5fd7\u5b89",
            "artist_id":576,
            "play_num":810,
            "play_num_add":796,
            "play_time":10800,
            "rank":19,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u738b\u83f2",
            "artist_id":47,
            "play_num":777,
            "play_num_add":752,
            "play_time":35446,
            "rank":20,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u82cf\u6c38\u5eb7",
            "artist_id":376,
            "play_num":756,
            "play_num_add":741,
            "play_time":11520,
            "rank":21,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u83ab\u6587\u851a",
            "artist_id":69,
            "play_num":756,
            "play_num_add":737,
            "play_time":49680,
            "rank":22,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8c2d\u548f\u9e9f",
            "artist_id":829,
            "play_num":735,
            "play_num_add":709,
            "play_time":0,
            "rank":23,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6768\u5343\u5b05",
            "artist_id":205,
            "play_num":728,
            "play_num_add":701,
            "play_time":6240,
            "rank":24,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u4fe1\u54f2",
            "artist_id":17,
            "play_num":714,
            "play_num_add":684,
            "play_time":289464,
            "rank":25,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8c22\u9706\u950b",
            "artist_id":384,
            "play_num":602,
            "play_num_add":574,
            "play_time":18920,
            "rank":26,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u674e\u514b\u52e4",
            "artist_id":435,
            "play_num":585,
            "play_num_add":575,
            "play_time":20280,
            "rank":27,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u56fd\u8363",
            "artist_id":108,
            "play_num":578,
            "play_num_add":554,
            "play_time":22440,
            "rank":28,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u60e0\u59b9",
            "artist_id":270,
            "play_num":561,
            "play_num_add":536,
            "play_time":59235,
            "rank":29,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5468\u6770\u4f26",
            "artist_id":210,
            "play_num":550,
            "play_num_add":515,
            "play_time":63250,
            "rank":30,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6797\u5fc6\u83b2",
            "artist_id":42,
            "play_num":525,
            "play_num_add":511,
            "play_time":48125,
            "rank":31,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5b59\u71d5\u59ff",
            "artist_id":77,
            "play_num":510,
            "play_num_add":483,
            "play_time":25200,
            "rank":32,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6885\u8273\u82b3",
            "artist_id":169,
            "play_num":476,
            "play_num_add":460,
            "play_time":3400,
            "rank":33,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6768\u94b0\u83b9",
            "artist_id":1187,
            "play_num":476,
            "play_num_add":437,
            "play_time":9520,
            "rank":34,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u738b\u529b\u5b8f",
            "artist_id":41,
            "play_num":451,
            "play_num_add":426,
            "play_time":21320,
            "rank":35,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u4efb\u8d24\u9f50",
            "artist_id":44,
            "play_num":429,
            "play_num_add":412,
            "play_time":17745,
            "rank":36,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6881\u548f\u742a",
            "artist_id":411,
            "play_num":414,
            "play_num_add":391,
            "play_time":33120,
            "rank":37,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f6d\u7f9a",
            "artist_id":423,
            "play_num":390,
            "play_num_add":378,
            "play_time":18000,
            "rank":38,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9f99\u5343\u7389",
            "artist_id":339,
            "play_num":385,
            "play_num_add":368,
            "play_time":700,
            "rank":39,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u5b87",
            "artist_id":71,
            "play_num":374,
            "play_num_add":354,
            "play_time":6120,
            "rank":40,
            "time":"2013-12-02 00:00:00"
        },
        {
            "artist":"\u6797\u4fca\u6770",
            "artist_id":66,
            "play_num":374,
            "play_num_add":351,
            "play_time":39512,
            "rank":41,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u738b\u6770",
            "artist_id":91,
            "play_num":370,
            "play_num_add":339,
            "play_time":16280,
            "rank":42,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u52a8\u529b\u706b\u8f66",
            "artist_id":241,
            "play_num":364,
            "play_num_add":347,
            "play_time":24640,
            "rank":43,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9648\u5c0f\u6625",
            "artist_id":101,
            "play_num":364,
            "play_num_add":352,
            "play_time":15680,
            "rank":44,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"BEYOND",
            "artist_id":225,
            "play_num":348,
            "play_num_add":320,
            "play_time":83404,
            "rank":45,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u4f55\u97f5\u8bd7",
            "artist_id":429,
            "play_num":341,
            "play_num_add":329,
            "play_time":13020,
            "rank":46,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u960e\u7ef4\u6587",
            "artist_id":358,
            "play_num":320,
            "play_num_add":304,
            "play_time":0,
            "rank":47,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5b8b\u7956\u82f1",
            "artist_id":496,
            "play_num":319,
            "play_num_add":310,
            "play_time":0,
            "rank":48,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u53f6\u5029\u6587",
            "artist_id":491,
            "play_num":310,
            "play_num_add":294,
            "play_time":11160,
            "rank":49,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u80e1\u5f66\u658c",
            "artist_id":100,
            "play_num":304,
            "play_num_add":283,
            "play_time":31730,
            "rank":50,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6c5f\u8559",
            "artist_id":68,
            "play_num":304,
            "play_num_add":282,
            "play_time":0,
            "rank":51,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5173\u6dd1\u6021",
            "artist_id":1043,
            "play_num":294,
            "play_num_add":279,
            "play_time":9240,
            "rank":52,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8d39\u7389\u6e05",
            "artist_id":1173,
            "play_num":286,
            "play_num_add":277,
            "play_time":9880,
            "rank":53,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6e38\u9e3f\u660e",
            "artist_id":279,
            "play_num":280,
            "play_num_add":265,
            "play_time":24080,
            "rank":54,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"S.H.E",
            "artist_id":78,
            "play_num":264,
            "play_num_add":241,
            "play_time":15180,
            "rank":55,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u4f0d\u4f70",
            "artist_id":698,
            "play_num":261,
            "play_num_add":250,
            "play_time":0,
            "rank":56,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8d75\u8587",
            "artist_id":817,
            "play_num":260,
            "play_num_add":240,
            "play_time":15080,
            "rank":57,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6881\u9759\u8339",
            "artist_id":1,
            "play_num":250,
            "play_num_add":228,
            "play_time":27000,
            "rank":58,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9648\u6653\u4e1c",
            "artist_id":58,
            "play_num":242,
            "play_num_add":228,
            "play_time":21450,
            "rank":59,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u90a3\u82f1",
            "artist_id":10,
            "play_num":242,
            "play_num_add":227,
            "play_time":17930,
            "rank":60,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u680b\u6881",
            "artist_id":1506,
            "play_num":234,
            "play_num_add":226,
            "play_time":10800,
            "rank":61,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u674e\u8c37\u4e00",
            "artist_id":1420,
            "play_num":232,
            "play_num_add":198,
            "play_time":53160,
            "rank":62,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u7530\u9707",
            "artist_id":46,
            "play_num":231,
            "play_num_add":217,
            "play_time":12600,
            "rank":63,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u4e94\u6708\u5929",
            "artist_id":392,
            "play_num":230,
            "play_num_add":207,
            "play_time":12420,
            "rank":64,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6c5f\u7f8e\u742a",
            "artist_id":827,
            "play_num":221,
            "play_num_add":212,
            "play_time":25840,
            "rank":65,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u656c\u8f69",
            "artist_id":290,
            "play_num":216,
            "play_num_add":203,
            "play_time":11040,
            "rank":66,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9676\u5586",
            "artist_id":247,
            "play_num":210,
            "play_num_add":204,
            "play_time":162840,
            "rank":67,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5468\u8559",
            "artist_id":23,
            "play_num":210,
            "play_num_add":195,
            "play_time":12915,
            "rank":68,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8bb8\u5d69",
            "artist_id":482,
            "play_num":207,
            "play_num_add":193,
            "play_time":6440,
            "rank":69,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8c22\u5b89\u742a",
            "artist_id":2686,
            "play_num":207,
            "play_num_add":192,
            "play_time":4140,
            "rank":70,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u90d1\u4f0a\u5065",
            "artist_id":405,
            "play_num":207,
            "play_num_add":197,
            "play_time":1380,
            "rank":71,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6797\u5b50\u7965",
            "artist_id":968,
            "play_num":207,
            "play_num_add":202,
            "play_time":4140,
            "rank":72,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9ec4\u54c1\u6e90",
            "artist_id":138,
            "play_num":200,
            "play_num_add":192,
            "play_time":12500,
            "rank":73,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u675c\u5fb7\u4f1f",
            "artist_id":544,
            "play_num":198,
            "play_num_add":184,
            "play_time":13200,
            "rank":74,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8521\u5065\u96c5",
            "artist_id":359,
            "play_num":198,
            "play_num_add":171,
            "play_time":12760,
            "rank":75,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5deb\u542f\u8d24",
            "artist_id":61,
            "play_num":180,
            "play_num_add":170,
            "play_time":1200,
            "rank":76,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5468\u534e\u5065",
            "artist_id":54,
            "play_num":180,
            "play_num_add":161,
            "play_time":11400,
            "rank":77,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9648\u96f7",
            "artist_id":837,
            "play_num":168,
            "play_num_add":158,
            "play_time":0,
            "rank":78,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u674e\u739f",
            "artist_id":382,
            "play_num":162,
            "play_num_add":143,
            "play_time":2160,
            "rank":79,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8bb8\u8339\u82b8",
            "artist_id":53,
            "play_num":154,
            "play_num_add":134,
            "play_time":15840,
            "rank":80,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u97f6\u6db5",
            "artist_id":226,
            "play_num":154,
            "play_num_add":142,
            "play_time":3080,
            "rank":81,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8c22\u91d1\u71d5",
            "artist_id":954,
            "play_num":152,
            "play_num_add":148,
            "play_time":0,
            "rank":82,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9ec4\u4e59\u73b2",
            "artist_id":33,
            "play_num":152,
            "play_num_add":147,
            "play_time":0,
            "rank":83,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5b59\u6960",
            "artist_id":25,
            "play_num":152,
            "play_num_add":141,
            "play_time":9424,
            "rank":84,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6f58\u73ae\u67cf",
            "artist_id":204,
            "play_num":152,
            "play_num_add":137,
            "play_time":10640,
            "rank":85,
            "time":"2013-12-02 00:00:00"
        },
        {
            "artist":"\u5b59\u6dd1\u5a9a",
            "artist_id":257,
            "play_num":150,
            "play_num_add":139,
            "play_time":0,
            "rank":86,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u76d6\u9e23\u6656_\u5434\u7f8e\u82f1",
            "artist_id":741,
            "play_num":144,
            "play_num_add":138,
            "play_time":0,
            "rank":87,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"F.I.R",
            "artist_id":85,
            "play_num":144,
            "play_num_add":132,
            "play_time":9280,
            "rank":88,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u7f57\u5fd7\u7965",
            "artist_id":6,
            "play_num":144,
            "play_num_add":134,
            "play_time":11776,
            "rank":89,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u8303\u6653\u8431",
            "artist_id":127,
            "play_num":144,
            "play_num_add":136,
            "play_time":8320,
            "rank":90,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u9ad8\u80dc\u7f8e",
            "artist_id":719,
            "play_num":140,
            "play_num_add":130,
            "play_time":2800,
            "rank":91,
            "time":"2013-12-02 00:00:00"
        },
        {
            "artist":"\u6234\u4f69\u59ae",
            "artist_id":681,
            "play_num":140,
            "play_num_add":129,
            "play_time":20300,
            "rank":92,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u82ad\u6bd4",
            "artist_id":312,
            "play_num":140,
            "play_num_add":133,
            "play_time":0,
            "rank":93,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u59dc\u80b2\u6052",
            "artist_id":783,
            "play_num":140,
            "play_num_add":132,
            "play_time":2400,
            "rank":94,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f20\u667a\u6210",
            "artist_id":1035,
            "play_num":135,
            "play_num_add":128,
            "play_time":14040,
            "rank":95,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u4fe1\u4e50\u56e2",
            "artist_id":198,
            "play_num":130,
            "play_num_add":125,
            "play_time":8840,
            "rank":96,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5468\u7487",
            "artist_id":130,
            "play_num":128,
            "play_num_add":123,
            "play_time":0,
            "rank":97,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u7f57\u5bb6\u5b9d",
            "artist_id":1196,
            "play_num":126,
            "play_num_add":124,
            "play_time":0,
            "rank":98,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u5f90\u5c0f\u51e4",
            "artist_id":938,
            "play_num":126,
            "play_num_add":116,
            "play_time":2520,
            "rank":99,
            "time":"2013-12-01 00:00:00"
        },
        {
            "artist":"\u6881\u6c49\u6587",
            "artist_id":952,
            "play_num":125,
            "play_num_add":115,
            "play_time":0,
            "rank":100,
            "time":"2013-12-01 00:00:00"
        }
    ],
    "result_count":100
}