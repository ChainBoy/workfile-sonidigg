#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
#import os
import subprocess

def filename(i):
    con = requests.get('http://www.midishow.com/midi/%s.html' % i, timeout=30).content
    if len(re.findall('下载到电脑', con)) == 1:
        return '%s.mid' % re.findall(r'page_h1.+?>(.+?)</h1', con)[0] 

def get_new_id(default_id = 29370):
    try:
        req = requests.get('http://www.midishow.com/', timeout=30)
    except Exception, e:
        print e
        return default_id
    try:
        i = int(max(re.findall(r'value="(\d.+?)"',req.content)))
        print i
        return i
    except ValueError:
        return default_id

def write_file():
    for i in xrange(16107, get_new_id()):
        mid = 'http://www.midishow.com/midi/file/%s.mid' % i
        try:
            comm = 'wget -t 5 -T 30 -nc -O "result/%s" "%s"' % (filename(i), mid)
            down(comm)
        except Exception:
            continue

def down(comm):
    subprocess.Popen(comm, shell=True)

write_file()

#for i in xrange(29370):
#    if os.path.isfile('%s.mid' % i) and os.path.isfile('%s.html' % i):
#        with file('%s.html' % i, 'r')as f:
#            filename = '%s.mid' % re.findall(r'page_h1".+?>(.+?)</h1', f.read())[0]
#            subprocess.Popen('cp "%s.mid" "result/%s.mid"' % (i, filename), shell=True)
