#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import requests
import re

def down(comm):
    subprocess.Popen(comm, shell=True)

def de(content):
    return re.findall('charset=(.+?)"', content)[0]

def main():
    down('mkdir result')
    ls = get_index()
    for i in ls:
        url_head = 'http://sql.jaes.ntpc.edu.tw/javaroom/midi/alas/%s' % i[0]
        req = requests.get('%s/%s.htm' % (url_head , i[0]), timeout=30)
        if req.ok:
            con=req.content.decode('big5')
            pages = re.findall(r'href="(.+?)"', con)
            for page in pages:
                con = requests.get('%s/%s' % (url_head, page), timeout=30).content
                con = con.decode(de(con))
                results = re.findall('href="(.+?)".+?>(.+?)</a', con)
                for du in results:
                    if du[0][-3:] == 'htm':
                        con = requests.get('%s/%s' % (url_head, du[0]), timeout=30).content
                        try:
                            content = con.decode(de(con))
                        except Exception, e:
                            print e
                            print '%s/%s' % (url_head, du[0])
                            print con
                        result = re.findall('href="(.+?mid)"', content)[0]
                        comm = 'wget -t 5 -T 30 -nc -O "result/%s.mid" "%s"' % (du[1], url_head + '/' + result)
                        down(comm)
                    elif du[0][-3:] == 'mid':
                        comm = 'wget -t 5 -T 30 -nc -O "result/%s.mid" "%s"' % (du[1], url_head + '/' + du[0])
                        down(comm)
        else:
            con = requests.get(url_head + i[1], timeout=30).content
            con = con.decode(de(con))
            results = re.findall('href="(.+?)".+?>(.+?)</a', con)
            for result in results:
                comm = 'wget -t 5 -T 30 -nc -O "result/%s.mid" "%s"' % (result[1], url_head + '/' + result[0])
                down(comm)

def get_index():
    url = "http://sql.jaes.ntpc.edu.tw/javaroom/midi/alas/left1.htm"
    con = requests.get(url, timeout=30).content
    content = con.decode(de(con))
    if content:
        try:
            return [i.split('/') for i in re.findall('href="(.+?/.+?.htm)"', content) if not i[:4]=='http']
        except Exception, e:
            print e
            return None

main()
