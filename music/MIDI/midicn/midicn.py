#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
#import os
import subprocess

def down():
    down_file('mkdir result')
    req = requests.get('http://midi.midicn.com/', timeout=30)
    res = re.findall('entry.+?div', req.content, re.S)[0]
    res = list(set([i[0] for i in  re.findall('href="(/{1}.+?)">(.+?)</a', res, re.S) if i[1].find('下载') == -1 ]))
    for i in res:
        con = requests.get('http://midi.midicn.com/%s' % i, timeout=30).content
        r = re.findall('href="(.+?mid)">(.+?)</a', con)
        for i in r:
            comm = 'wget -t 5 -T 30 -nc -O "result/%s.mid" "%s"' % (i[1].strip(), i[0])
            down_file(comm)
    
def down_file(comm):
    subprocess.Popen(comm, shell=True)

down()
