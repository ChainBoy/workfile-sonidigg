#-*- coding:utf8 -*-
import urllib2
import re
import time
import datetime
import simplejson


def save_to_file(file_name,string_line):
    with file(file_name, 'a') as f:
        f.write(string_line.decode('utf8').encode('utf8') + '\n')
class GetSongList():
    def __init__(self,song = "好了伤疤忘了痛",artist = "孙子涵"):
        self.song = '青藏高原'
        self.artist = '韩红'
        self.start()

    def start(self):
        self.Yymp3()
        self.Sinamp3()
        self.mp3_365()

    def get_key(self,ty=3):
        if ty == 1: key = "%s %s" % (self.artist, self.song)
        elif ty == 2: key = "%s %s" % (self.song,self.artist)
        elif ty == 4: key = "%s" % self.artist
        else: key = "%s" % self.song
        return key
    def Yymp3(self,url="http://www.yymp3.com/search.aspx?page=0&key=&tp=1",ty=3):
        """ty是标识，搜索方式
        1 -- > 歌手 歌曲
        2 -- > 歌曲 歌手
        3 -- > 歌曲
        4 -- > 歌手
        """
        print 'start yymp3 search'
        url_head = "http://www.yymp3.com/"
        key = self.get_key(ty)
        u = urllib2.urlopen("http://www.yymp3.com/search.aspx?page=0&key=%s&tp=1" % key)
        result = u.read()
        r = re.findall(r'<b>(\d+)</b>条记录',result)
        if r: data_count = int(r[0])
        else: data_count = 0
        if data_count >= 0 :
            r = re.findall(r'页次.+?color=".+?">(\d+).+?(\d+)',result)
            if r and r[0] and r[0][1]:
                all_page = int(r[0][1])
            if all_page:
                is_normal = True
                for i in xrange(1, all_page+1):
                    u = urllib2.urlopen("http://www.yymp3.com/search.aspx?page=%s&key=%s&tp=1" % (i, key))
                    result = u.read()
                    r = re.findall(r'searchResult.+?ConterTools',result,re.S)
                    if r:
                        if r[0]:
                            rs = re.findall(r'<li>.+?</li>',r[0],re.S)
                            for j in rs:
                                song_info = re.findall(r'class="p3".+?href="(.+?)".+?red>(.+?)</font>',j,re.S)
                                artist_info = re.findall(r'class="p2".+?href="(.+?)".+?_blank?>(.+?)</a>',j,re.S)
                                if song_info:
                                    song_url, song_name = song_info[0]
                                    artist_url,artist_name = artist_info[0]
                                    if song_name == self.song:
                                        if artist_name == self.artist:
                                            print 'YES', artist_name, song_name, url_head +song_url
                                            save_to_file(self.artist + '_'+ self.song + '.txt', "%s %s %s %s" % (datetime.datetime.now(), artist_name, song_name, url_head + song_url))
                                        else:
                                            print 'No', artist_name, song_name, url_head + song_url
                                    else:
                                        is_normal = False
                                        break
                                else:continue
                        else:continue
                    else:
                        if not is_normal:break
                        continue
                    print 'sleep 2s...'
                    time.sleep(2)
            else:return
        else:
            return
            print 'not find.'
    def Sinamp3(self,url="http://music.sina.com.cn/yueku/search_new.php?type=song&key=我&nocacheset=1&page=1",ty=3):
        """ty是标识，搜索方式
        1 -- > 歌手 歌曲
        2 -- > 歌曲 歌手
        3 -- > 歌曲
        4 -- > 歌手
        """
        print 'start sina mp3 search'
        key = self.get_key(ty)
        u = urllib2.urlopen("http://music.sina.com.cn/yueku/search_new.php?type=song&key=%s" % key)
        result = u.read()
        r = re.findall(r'找到<i>(\d+)</i>条结果', result)
        if r: data_count = int(r[0])
        else: data_count = 0
        all_page = 1
        if data_count >= 0 :
            r = re.findall(r'show_page="(\d+)"', result, re.S)
            if r and r[0]:
                all_page = int(max(r))
            if all_page:
                is_normal = True
                for i in xrange(1, all_page+1):
                    u = urllib2.urlopen("http://music.sina.com.cn/yueku/search_new.php?type=song&key=%s&nocacheset=1&page=%s" % (key, i))
                    result = u.read()
                    r = re.findall(r'ul id="songs" class="list_X.+?</ul>',result,re.S)
                    if r:
                        if r[0]:
                            rs = re.findall(r'<li.+?</li>',r[0],re.S)
                            for j in rs:
                                song_info = re.findall(r'class="w190".+?href="(.+?)".+?title="(.+?)"',j,re.S)
                                artist_info = re.findall(r'class="w170".+?href="(.+?)".+?title="(.+?)"',j,re.S)
                                if song_info:
                                    song_url, song_name = song_info[0]
                                    artist_url,artist_name = artist_info[0]
                                    if song_name == self.song:
                                        if artist_name == self.artist:
                                            print 'YES', artist_name, song_name, song_url
                                            save_to_file(self.artist + '_'+ self.song + '.txt', "%s %s %s %s" % (datetime.datetime.now(), artist_name, song_name, song_url))
                                        else:
                                            print 'No', artist_name, song_name, song_url
                                    else:
                                        is_normal = False
                                        break
                                else:continue
                        else:continue
                    else:
                        if not is_normal:break
                        continue
                    print 'sleep 2s...'
                    time.sleep(2)
            else:return
        else:
            return
            print 'not find.'

    def mp3_365(self,url="http://my.yue365.com/ajax/Search.ashx?keyword=%u9752%u85CF%u9AD8%u539F&Page=1&type=song",ty=3):
        """ty是标识，搜索方式
        1 -- > 歌手 歌曲
        2 -- > 歌曲 歌手
        3 -- > 歌曲
        4 -- > 歌手
        """
        "http://www.yue365.com/play/gid/mid.shtml"
        print 'start yue365 mp3 search.'
        url_head = "http://www.yue365.com/play"
        key = self.get_key(ty).decode('utf8').encode('raw_unicode_escape').replace('\\','%')
        u = urllib2.urlopen("http://my.yue365.com/ajax/Search.ashx?keyword=%s&Page=1&type=song" % key)
        result = u.read()
        s = simplejson.loads(result[1:-1].decode('gbk'))
        r = s.get('count', 0)
        if r: data_count = int(r)
        else: data_count = 0
        if data_count > 0:
            r = s.get('pagecount', 0)
            if r:
                all_page = int(r)
            if all_page:
                for i in xrange(1, all_page+1):
                    u = urllib2.urlopen("http://my.yue365.com/ajax/Search.ashx?keyword=%s&Page=%s&type=song" % (key, i))
                    result = u.read()
                    s = simplejson.loads(result[1:-1].decode('gbk'))
                    r = s.get('data', {})
                    if r:
                        for j in r:
                            song = j.get('mname').encode('utf8')
                            mid = j.get('mid')
                            album = j.get('zname').encode('utf8')
                            zid = j.get('zid')
                            artist = j.get('gname').encode('utf8')
                            gid = j.get('gid')
                            if song and artist:
                                song_url = '%s/%s/%s.shtml' % (url_head, gid, mid)
                                if song == self.song:
                                    print song,artist,self.song,self.artist
                                    if artist == self.artist:
                                        print 'YES', str(artist), str(song), type(song_url)
                                        content = "%s %s %s %s" % (datetime.datetime.now(), str(artist), str(song), str(song_url))
                                        save_to_file(self.artist + '_'+ self.song + '.txt', content)
                                    else:
                                        print 'No', artist, song, song_url
                                else:
                                    print 'dump.'
                                    continue
                            else:continue
                    else:
                        continue
                    print 'sleep 2s...'
                    time.sleep(2)
            else:return
        else:
            return
            print 'not find.'
if __name__ == "__main__":
    GetSongList()
