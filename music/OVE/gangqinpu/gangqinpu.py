#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import subprocess
import re


def init():
    s = requests.session()
    get_new_id(s)
    return s

def get_rar(num, s):
    url = 'http://www.gangqinpu.com/ovedown_%s.htm' % num
    s.cookies.set('pureadcookie_%s' % num,'pu_%s' % num)
    try:
        r = s.get(url).content.decode('gbk')
        s.cookies.set('pureadcookie_%s' % num,'')
        url = re.findall('href=\'(.+?)\'.+?download', r, re.S)[0]
        url = 'http://www.gangqinpu.com/%s' % url
        return url
    except Exception,e:
        print url, "No Ove"
        return None

def get_new_id(s, default_id = 24130):
    try:
        req = s.get('http://www.gangqinpu.com/band.aspx?abcd=a', timeout=30).content.decode('gbk')
    except Exception, e:
        print e
        return default_id
    try:
        i = int(max(re.findall(u'::最新钢琴谱::.+?href=\'\/.+?(\d{1,}).+?htm\'', req, re.S)))
        print i
        return i
    except ValueError:
        return default_id

def down(comm):
    subprocess.Popen(comm, shell=True)

def main():
    s = requests.session()
    down('mkdir .tmp')
    down('mkdir result')
    for i in xrange(1743, get_new_id(s)):
        if i%500 == 0:
            s = init()
        url = get_rar(i, s)
        if url:
            filename = url.split('/')[-1]
            tp = filename[-3:]
            if tp == "rar":
                comm = 'wget -t 5 -T 30 -nc -O ".tmp/%s" "%s" && unrar e -or ".tmp/%s" result && rm ".tmp/%s" ' % (filename, url, filename, filename)
            elif tp == "zip":
                comm = 'wget -t 5 -T 30 -nc -O ".tmp/%s" "%s" && unzip -n -O gbk ".tmp/%s" -d result && rm ".tmp/%s" ' % (filename, url, filename, filename)
            elif tp== "ove":
                comm = 'wget -t 5 -T 30 -nc -O "result/%s" "%s"' % (filename, url)
            else:
                comm = ""
                print tp
            if comm:
                down(comm)
                st = 'http://www.gangqinpu.com/ovedown_%s.htm\t%s\n' % (i, url.encode('utf8'))
            else:
                st = 'http://www.gangqinpu.com/ovedown_%s.htm\t%s ---- ERROR \n' % (i, url.encode('utf8'))
            with file('.log', 'a')as f:
                f.write(st)
        else:
            continue

if __name__ == "__main__":
    main()
