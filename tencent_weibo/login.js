var $ = window.Simple = function(A) {
    return typeof(A) == "string" ? document.getElementById(A) : A
};
$.cookie = {
    get: function(B) {
        var A = document.cookie.match(new RegExp("(^| )" + B + "=([^;]*)(;|$)"));
        return ! A ? "": decodeURIComponent(A[2])
    },
    getOrigin: function(B) {
        var A = document.cookie.match(new RegExp("(^| )" + B + "=([^;]*)(;|$)"));
        return ! A ? "": (A[2])
    },
    set: function(C, E, D, F, A) {
        var B = new Date();
        if (A) {
            B.setTime(B.getTime() + 3600000 * A);
            document.cookie = C + "=" + E + "; expires=" + B.toGMTString() + "; path=" + (F ? F: "/") + "; " + (D ? ("domain=" + D + ";") : "")
        } else {
            document.cookie = C + "=" + E + "; path=" + (F ? F: "/") + "; " + (D ? ("domain=" + D + ";") : "")
        }
    },
    del: function(A, B, C) {
        document.cookie = A + "=; expires=Mon, 26 Jul 1997 05:00:00 GMT; path=" + (C ? C: "/") + "; " + (B ? ("domain=" + B + ";") : "")
    },
    uin: function() {
        var A = $.cookie.get("uin");
        return ! A ? null: parseInt(A.substring(1, A.length), 10)
    }
};
$.http = {
    getXHR: function() {
        return window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest()
    },
    ajax: function(url, para, cb, method, type) {
        var xhr = $.http.getXHR();
        xhr.open(method, url);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304 || xhr.status === 1223 || xhr.status === 0) {
                    if (typeof(type) == "undefined" && xhr.responseText) {
                        cb(eval("(" + xhr.responseText + ")"))
                    } else {
                        cb(xhr.responseText);
                        if ((!xhr.responseText) && $.badjs._smid) {
                            $.badjs("HTTP Empty[xhr.status]:" + xhr.status, url, 0, $.badjs._smid)
                        }
                    }
                } else {
                    if ($.badjs._smid) {
                        $.badjs("HTTP Error[xhr.status]:" + xhr.status, url, 0, $.badjs._smid)
                    }
                }
                xhr = null
            }
        };
        xhr.send(para);
        return xhr
    },
    post: function(C, B, A, F) {
        var E = "";
        for (var D in B) {
            E += "&" + D + "=" + B[D]
        }
        return $.http.ajax(C, E, A, "POST", F)
    },
    get: function(C, B, A, E) {
        var F = [];
        for (var D in B) {
            F.push(D + "=" + B[D])
        }
        if (C.indexOf("?") == -1) {
            C += "?"
        }
        C += F.join("&");
        return $.http.ajax(C, null, A, "GET", E)
    },
    jsonp: function(A) {
        var B = document.createElement("script");
        B.src = A;
        document.getElementsByTagName("head")[0].appendChild(B)
    },
    loadScript: function(C, D, B) {
        var A = document.createElement("script");
        A.onload = A.onreadystatechange = function() {
            if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                if (typeof D == "function") {
                    D()
                }
                A.onload = A.onreadystatechange = null;
                if (A.parentNode) {
                    A.parentNode.removeChild(A)
                }
            }
        };
        A.src = C;
        document.getElementsByTagName("head")[0].appendChild(A)
    },
    preload: function(A) {
        var B = document.createElement("img");
        B.src = A;
        B = null
    }
};
$.get = $.http.get;
$.post = $.http.post;
$.jsonp = $.http.jsonp;
$.browser = function(B) {
    if (typeof $.browser.info == "undefined") {
        var A = {
            type: ""
        };
        var C = navigator.userAgent.toLowerCase();
        if (/webkit/.test(C)) {
            A = {
                type: "webkit",
                version: /webkit[\/ ]([\w.]+)/
            }
        } else {
            if (/opera/.test(C)) {
                A = {
                    type: "opera",
                    version: /version/.test(C) ? /version[\/ ]([\w.]+)/: /opera[\/ ]([\w.]+)/
                }
            } else {
                if (/msie/.test(C)) {
                    A = {
                        type: "msie",
                        version: /msie ([\w.]+)/
                    }
                } else {
                    if (/mozilla/.test(C) && !/compatible/.test(C)) {
                        A = {
                            type: "ff",
                            version: /rv:([\w.]+)/
                        }
                    }
                }
            }
        }
        A.version = (A.version && A.version.exec(C) || [0, "0"])[1];
        $.browser.info = A
    }
    return $.browser.info[B]
};
$.e = {
    _counter: 0,
    _uid: function() {
        return "h" + $.e._counter++
    },
    add: function(C, B, E) {
        if (typeof C != "object") {
            C = $(C)
        }
        if (document.addEventListener) {
            C.addEventListener(B, E, false)
        } else {
            if (document.attachEvent) {
                if ($.e._find(C, B, E) != -1) {
                    return
                }
                var F = function(J) {
                    if (!J) {
                        J = window.event
                    }
                    var I = {
                        _event: J,
                        type: J.type,
                        target: J.srcElement,
                        currentTarget: C,
                        relatedTarget: J.fromElement ? J.fromElement: J.toElement,
                        eventPhase: (J.srcElement == C) ? 2 : 3,
                        clientX: J.clientX,
                        clientY: J.clientY,
                        screenX: J.screenX,
                        screenY: J.screenY,
                        altKey: J.altKey,
                        ctrlKey: J.ctrlKey,
                        shiftKey: J.shiftKey,
                        keyCode: J.keyCode,
                        data: J.data,
                        origin: J.origin,
                        stopPropagation: function() {
                            this._event.cancelBubble = true
                        },
                        preventDefault: function() {
                            this._event.returnValue = false
                        }
                    };
                    if (Function.prototype.call) {
                        E.call(C, I)
                    } else {
                        C._currentHandler = E;
                        C._currentHandler(I);
                        C._currentHandler = null
                    }
                };
                C.attachEvent("on" + B, F);
                var D = {
                    element: C,
                    eventType: B,
                    handler: E,
                    wrappedHandler: F
                };
                var G = C.document || C;
                var A = G.parentWindow;
                var H = $.e._uid();
                if (!A._allHandlers) {
                    A._allHandlers = {}
                }
                A._allHandlers[H] = D;
                if (!C._handlers) {
                    C._handlers = []
                }
                C._handlers.push(H);
                if (!A._onunloadHandlerRegistered) {
                    A._onunloadHandlerRegistered = true;
                    A.attachEvent("onunload", $.e._removeAllHandlers)
                }
            }
        }
    },
    remove: function(D, C, F) {
        if (document.addEventListener) {
            D.removeEventListener(C, F, false)
        } else {
            if (document.attachEvent) {
                var B = $.e._find(D, C, F);
                if (B == -1) {
                    return
                }
                var H = D.document || D;
                var A = H.parentWindow;
                var G = D._handlers[B];
                var E = A._allHandlers[G];
                D.detachEvent("on" + C, E.wrappedHandler);
                D._handlers.splice(B, 1);
                delete A._allHandlers[G]
            }
        }
    },
    _find: function(D, A, I) {
        var B = D._handlers;
        if (!B) {
            return - 1
        }
        var G = D.document || D;
        var H = G.parentWindow;
        for (var E = B.length - 1; E >= 0; E--) {
            var C = B[E];
            var F = H._allHandlers[C];
            if (F.eventType == A && F.handler == I) {
                return E
            }
        }
        return - 1
    },
    _removeAllHandlers: function() {
        var A = this;
        for (id in A._allHandlers) {
            var B = A._allHandlers[id];
            B.element.detachEvent("on" + B.eventType, B.wrappedHandler);
            delete A._allHandlers[id]
        }
    },
    src: function(A) {
        return A ? A.target: event.srcElement
    },
    stopPropagation: function(A) {
        A ? A.stopPropagation() : event.cancelBubble = true
    },
    trigger: function(C, B) {
        var E = {
            HTMLEvents: "abort,blur,change,error,focus,load,reset,resize,scroll,select,submit,unload",
            UIEevents: "keydown,keypress,keyup",
            MouseEvents: "click,mousedown,mousemove,mouseout,mouseover,mouseup"
        };
        if (document.createEvent) {
            var D = ""; (B == "mouseleave") && (B = "mouseout"); (B == "mouseenter") && (B = "mouseover");
            for (var F in E) {
                if (E[F].indexOf(B)) {
                    D = F;
                    break
                }
            }
            var A = document.createEvent(D);
            A.initEvent(B, true, false);
            C.dispatchEvent(A)
        } else {
            if (document.createEventObject) {
                C.fireEvent("on" + B)
            }
        }
    }
};
$.bom = {
    query: function(B) {
        var A = window.location.search.match(new RegExp("(\\?|&)" + B + "=([^&]*)(&|$)"));
        return ! A ? "": decodeURIComponent(A[2])
    },
    getHash: function(B) {
        var A = window.location.hash.match(new RegExp("(#|&)" + B + "=([^&]*)(&|$)"));
        return ! A ? "": decodeURIComponent(A[2])
    }
};
$.winName = {
    set: function(C, A) {
        var B = window.name || "";
        if (B.match(new RegExp(";" + C + "=([^;]*)(;|$)"))) {
            window.name = B.replace(new RegExp(";" + C + "=([^;]*)"), ";" + C + "=" + A)
        } else {
            window.name = B + ";" + C + "=" + A
        }
    },
    get: function(C) {
        var B = window.name || "";
        var A = B.match(new RegExp(";" + C + "=([^;]*)(;|$)"));
        return A ? A[1] : ""
    },
    clear: function(B) {
        var A = window.name || "";
        window.name = A.replace(new RegExp(";" + B + "=([^;]*)"), "")
    }
};
$.localData = function() {
    var A = "ptlogin2.qq.com";
    var D = /^[0-9A-Za-z_-]*$/;
    var B;
    function C() {
        var G = document.createElement("link");
        G.style.display = "none";
        G.id = A;
        document.getElementsByTagName("head")[0].appendChild(G);
        G.addBehavior("#default#userdata");
        return G
    }
    function E() {
        if (typeof B == "undefined") {
            if (window.localStorage) {
                B = localStorage
            } else {
                try {
                    B = C();
                    B.load(A)
                } catch(G) {
                    B = false;
                    return false
                }
            }
        }
        return true
    }
    function F(G) {
        if (typeof G != "string") {
            return false
        }
        return D.test(G)
    }
    return {
        set: function(G, H) {
            var J = false;
            if (F(G) && E()) {
                try {
                    H += "";
                    if (window.localStorage) {
                        B.setItem(G, H);
                        J = true
                    } else {
                        B.setAttribute(G, H);
                        B.save(A);
                        J = B.getAttribute(G) === H
                    }
                } catch(I) {}
            }
            return J
        },
        get: function(G) {
            if (F(G) && E()) {
                try {
                    return window.localStorage ? B.getItem(G) : B.getAttribute(G)
                } catch(H) {}
            }
            return null
        },
        remove: function(G) {
            if (F(G) && E()) {
                try {
                    window.localStorage ? B.removeItem(G) : B.removeAttribute(G);
                    return true
                } catch(H) {}
            }
            return false
        }
    }
} ();
$.str = (function() {
    var htmlDecodeDict = {
        quot: '"',
        lt: "<",
        gt: ">",
        amp: "&",
        nbsp: " ",
        "#34": '"',
        "#60": "<",
        "#62": ">",
        "#38": "&",
        "#160": " "
    };
    var htmlEncodeDict = {
        '"': "#34",
        "<": "#60",
        ">": "#62",
        "&": "#38",
        " ": "#160"
    };
    return {
        decodeHtml: function(s) {
            s += "";
            return s.replace(/&(quot|lt|gt|amp|nbsp);/ig,
            function(all, key) {
                return htmlDecodeDict[key]
            }).replace(/&#u([a-f\d]{4});/ig,
            function(all, hex) {
                return String.fromCharCode(parseInt("0x" + hex))
            }).replace(/&#(\d+);/ig,
            function(all, number) {
                return String.fromCharCode( + number)
            })
        },
        encodeHtml: function(s) {
            s += "";
            return s.replace(/["<>& ]/g,
            function(all) {
                return "&" + htmlEncodeDict[all] + ";"
            })
        },
        trim: function(str) {
            str += "";
            var str = str.replace(/^\s+/, ""),
            ws = /\s/,
            end = str.length;
            while (ws.test(str.charAt(--end))) {}
            return str.slice(0, end + 1)
        },
        uin2hex: function(str) {
            var maxLength = 16;
            str = parseInt(str);
            var hex = str.toString(16);
            var len = hex.length;
            for (var i = len; i < maxLength; i++) {
                hex = "0" + hex
            }
            var arr = [];
            for (var j = 0; j < maxLength; j += 2) {
                arr.push("\\x" + hex.substr(j, 2))
            }
            var result = arr.join("");
            eval('result="' + result + '"');
            return result
        },
        bin2String: function(a) {
            var arr = [];
            for (var i = 0,
            len = a.length; i < len; i++) {
                var temp = a.charCodeAt(i).toString(16);
                if (temp.length == 1) {
                    temp = "0" + temp
                }
                arr.push(temp)
            }
            arr = "0x" + arr.join("");
            arr = parseInt(arr, 16);
            return arr
        },
        utf8ToUincode: function(s) {
            var result = "";
            try {
                var length = s.length;
                var arr = [];
                for (i = 0; i < length; i += 2) {
                    arr.push("%" + s.substr(i, 2))
                }
                result = decodeURIComponent(arr.join(""));
                result = $.str.decodeHtml(result)
            } catch(e) {
                result = ""
            }
            return result
        },
        json2str: function(obj) {
            var result = "";
            if (typeof JSON != "undefined") {
                result = JSON.stringify(obj)
            } else {
                var arr = [];
                for (var i in obj) {
                    arr.push("'" + i + "':'" + obj[i] + "'")
                }
                result = "{" + arr.join(",") + "}"
            }
            return result
        },
        time33: function(str) {
            var hash = 0;
            for (var i = 0,
            length = str.length; i < length; i++) {
                hash = hash * 33 + str.charCodeAt(i)
            }
            return hash % 4294967296
        }
    }
})();
$.css = function() {
    return {
        getWidth: function(A) {
            return $(A).offsetWidth
        },
        getHeight: function(A) {
            return $(A).offsetHeight
        },
        show: function(A) {
            A.style.display = "block"
        },
        hide: function(A) {
            A.style.display = "none"
        },
        hasClass: function(D, E) {
            if (!D.className) {
                return false
            }
            var B = D.className.split(" ");
            for (var C = 0,
            A = B.length; C < A; C++) {
                if (E == B[C]) {
                    return true
                }
            }
            return false
        },
        addClass: function(A, B) {
            $.css.updateClass(A, B, false)
        },
        removeClass: function(A, B) {
            $.css.updateClass(A, false, B)
        },
        updateClass: function(D, I, K) {
            var A = D.className.split(" ");
            var G = {},
            E = 0,
            H = A.length;
            for (; E < H; E++) {
                A[E] && (G[A[E]] = true)
            }
            if (I) {
                var F = I.split(" ");
                for (E = 0, H = F.length; E < H; E++) {
                    F[E] && (G[F[E]] = true)
                }
            }
            if (K) {
                var B = K.split(" ");
                for (E = 0, H = B.length; E < H; E++) {
                    B[E] && (delete G[B[E]])
                }
            }
            var J = [];
            for (var C in G) {
                J.push(C)
            }
            D.className = J.join(" ")
        },
        setClass: function(B, A) {
            B.className = A
        }
    }
} ();
$.animate = {
    fade: function(D, H, B, E, L) {
        D = $(D);
        if (!D) {
            return
        }
        if (!D.effect) {
            D.effect = {}
        }
        var F = Object.prototype.toString.call(H);
        var C = 100;
        if (!isNaN(H)) {
            C = H
        } else {
            if (F == "[object Object]") {
                if (H) {
                    if (H.to) {
                        if (!isNaN(H.to)) {
                            C = H.to
                        }
                        if (!isNaN(H.from)) {
                            D.style.opacity = H.from / 100;
                            D.style.filter = "alpha(opacity=" + H.from + ")"
                        }
                    }
                }
            }
        }
        if (typeof(D.effect.fade) == "undefined") {
            D.effect.fade = 0
        }
        window.clearInterval(D.effect.fade);
        var B = B || 1,
        E = E || 20,
        G = window.navigator.userAgent.toLowerCase(),
        K = function(M) {
            var O;
            if (G.indexOf("msie") != -1) {
                var N = (M.currentStyle || {}).filter || "";
                O = N.indexOf("opacity") >= 0 ? (parseFloat(N.match(/opacity=([^)]*)/)[1])) + "": "100"
            } else {
                var P = M.ownerDocument.defaultView;
                P = P && P.getComputedStyle;
                O = 100 * (P && P(M, null)["opacity"] || 1)
            }
            return parseFloat(O)
        },
        A = K(D),
        I = A < C ? 1 : -1;
        if (G.indexOf("msie") != -1) {
            if (E < 15) {
                B = Math.floor((B * 15) / E);
                E = 15
            }
        }
        var J = function() {
            A = A + B * I;
            if ((Math.round(A) - C) * I >= 0) {
                D.style.opacity = C / 100;
                D.style.filter = "alpha(opacity=" + C + ")";
                window.clearInterval(D.effect.fade);
                if (typeof(L) == "function") {
                    L(D)
                }
            } else {
                D.style.opacity = A / 100;
                D.style.filter = "alpha(opacity=" + A + ")"
            }
        };
        D.effect.fade = window.setInterval(J, E)
    },
    animate: function(B, C, H, R, G) {
        B = $(B);
        if (!B) {
            return
        }
        if (!B.effect) {
            B.effect = {}
        }
        if (typeof(B.effect.animate) == "undefined") {
            B.effect.animate = 0
        }
        for (var M in C) {
            C[M] = parseInt(C[M]) || 0
        }
        window.clearInterval(B.effect.animate);
        var H = H || 10,
        R = R || 20,
        I = function(V) {
            var U = {
                left: V.offsetLeft,
                top: V.offsetTop
            };
            return U
        },
        T = I(B),
        F = {
            width: B.clientWidth,
            height: B.clientHeight,
            left: T.left,
            top: T.top
        },
        D = [],
        Q = window.navigator.userAgent.toLowerCase();
        if (! (Q.indexOf("msie") != -1 && document.compatMode == "BackCompat")) {
            var K = document.defaultView ? document.defaultView.getComputedStyle(B, null) : B.currentStyle;
            var E = C.width || C.width == 0 ? parseInt(C.width) : null,
            S = C.height || C.height == 0 ? parseInt(C.height) : null;
            if (typeof(E) == "number") {
                D.push("width");
                C.width = E - K.paddingLeft.replace(/\D/g, "") - K.paddingRight.replace(/\D/g, "")
            }
            if (typeof(S) == "number") {
                D.push("height");
                C.height = S - K.paddingTop.replace(/\D/g, "") - K.paddingBottom.replace(/\D/g, "")
            }
            if (R < 15) {
                H = Math.floor((H * 15) / R);
                R = 15
            }
        }
        var P = C.left || C.left == 0 ? parseInt(C.left) : null,
        L = C.top || C.top == 0 ? parseInt(C.top) : null;
        if (typeof(P) == "number") {
            D.push("left");
            B.style.position = "absolute"
        }
        if (typeof(L) == "number") {
            D.push("top");
            B.style.position = "absolute"
        }
        var J = [],
        O = D.length;
        for (var M = 0; M < O; M++) {
            J[D[M]] = F[D[M]] < C[D[M]] ? 1 : -1
        }
        var N = B.style;
        var A = function() {
            var U = true;
            for (var V = 0; V < O; V++) {
                F[D[V]] = F[D[V]] + J[D[V]] * Math.abs(C[D[V]] - F[D[V]]) * H / 100;
                if ((Math.round(F[D[V]]) - C[D[V]]) * J[D[V]] >= 0) {
                    U = U && true;
                    N[D[V]] = C[D[V]] + "px"
                } else {
                    U = U && false;
                    N[D[V]] = F[D[V]] + "px"
                }
            }
            if (U) {
                window.clearInterval(B.effect.animate);
                if (typeof(G) == "function") {
                    G(B)
                }
            }
        };
        B.effect.animate = window.setInterval(A, R)
    }
};
$.check = {
    isHttps: function() {
        return document.location.protocol == "https:"
    },
    isSsl: function() {
        var A = document.location.host;
        return /^ssl./i.test(A)
    },
    isIpad: function() {
        var A = navigator.userAgent.toLowerCase();
        return /ipad/i.test(A)
    },
    isQQ: function(A) {
        return /^[1-9]{1}\d{4,9}$/.test(A)
    },
    isNullQQ: function(A) {
        return /^\d{1,4}$/.test(A)
    },
    isNick: function(A) {
        return /^[a-zA-Z]{1}([a-zA-Z0-9]|[-_]){0,19}$/.test(A)
    },
    isName: function(A) {
        if (A == "<请输入帐号>") {
            return false
        }
        return /[\u4E00-\u9FA5]{1,8}/.test(A)
    },
    isPhone: function(A) {
        return /^(?:86|886|)1\d{10}\s*$/.test(A)
    },
    isDXPhone: function(A) {
        return /^(?:86|886|)1(?:33|53|80|81|89)\d{8}$/.test(A)
    },
    isSeaPhone: function(A) {
        return /^(00)?(?:852|853|886(0)?\d{1})\d{8}$/.test(A)
    },
    isMail: function(A) {
        return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(A)
    },
    isPassword: function(A) {
        return A && A.length >= 16
    },
    isForeignPhone: function(A) {
        return /^00\d{7,}/.test(A)
    },
    needVip: function(E) {
        var A = ["21001601", "21000110", "21000121", "46000101", "716027609", "716027610", "549000912"];
        var B = true;
        for (var C = 0,
        D = A.length; C < D; C++) {
            if (A[C] == E) {
                B = false;
                break
            }
        }
        return B
    },
    isPaipai: function() {
        return /paipai.com$/.test(window.location.hostname)
    },
    is_weibo_appid: function(A) {
        if (A == 46000101 || A == 607000101 || A == 558032501) {
            return true
        }
        return false
    }
};
$.report = {
    monitor: function(C, B) {
        if (Math.random() > (B || 1)) {
            return
        }
        var A = location.protocol + "//ui.ptlogin2.qq.com/cgi-bin/report?id=" + C;
        $.http.preload(A)
    },
    nlog: function(E, B) {
        var A = "http://badjs.qq.com/cgi-bin/js_report?";
        if ($.check.isHttps()) {
            A = "https://ssl.qq.com//badjs/cgi-bin/js_report?"
        }
        var C = location.href;
        var D = encodeURIComponent(E + "|_|" + C + "|_|" + window.navigator.userAgent);
        A += ("bid=110&level=2&mid=" + B + "&msg=" + D + "&v=" + Math.random());
        $.http.preload(A)
    },
    isdSpeed: function(A, F) {
        var B = false;
        var D = "http://isdspeed.qq.com/cgi-bin/r.cgi?";
        if ($.check.isHttps()) {
            D = "https://login.qq.com/cgi-bin/r.cgi?"
        }
        D += A;
        if (Math.random() < (F || 1)) {
            var C = $.report.getSpeedPoints(A);
            for (var E in C) {
                if (C[E] && C[E] < 30000) {
                    D += ("&" + E + "=" + C[E]);
                    B = true
                }
            }
            D += "&v=" + Math.random();
            if (B) {
                $.http.preload(D)
            }
        }
        $.report.setSpeedPoint(A)
    },
    speedPoints: {},
    basePoint: {},
    setBasePoint: function(A, B) {
        $.report.basePoint[A] = B
    },
    setSpeedPoint: function(A, B, C) {
        if (!B) {
            $.report.speedPoints[A] = {}
        } else {
            if (!$.report.speedPoints[A]) {
                $.report.speedPoints[A] = {}
            }
            $.report.speedPoints[A][B] = C - $.report.basePoint[A]
        }
    },
    setSpeedPoints: function(A, B) {
        $.report.speedPoints[A] = B
    },
    getSpeedPoints: function(A) {
        return $.report.speedPoints[A]
    }
};
$.sso_ver = 0;
$.sso_state = 0;
$.plugin_isd_flag = "";
$.nptxsso = null;
$.sso_loadComplete = true;
$.np_clock = 0;
$.loginQQnum = 0;
$.suportActive = function() {
    var A = true;
    try {
        if (window.ActiveXObject || window.ActiveXObject.prototype) {
            A = true;
            if (window.ActiveXObject.prototype && !window.ActiveXObject) {
                $.report.nlog("activeobject 判断有问题")
            }
        } else {
            A = false
        }
    } catch(B) {
        A = false
    }
    return A
};
$.getLoginQQNum = function() {
    try {
        var F = 0;
        if ($.suportActive()) {
            $.plugin_isd_flag = "flag1=7808&flag2=1&flag3=20";
            $.report.setBasePoint($.plugin_isd_flag, new Date());
            var K = new ActiveXObject("SSOAxCtrlForPTLogin.SSOForPTLogin2");
            var E = K.CreateTXSSOData();
            K.InitSSOFPTCtrl(0, E);
            var B = K.CreateTXSSOData();
            var A = K.DoOperation(2, B);
            var D = A.GetArray("PTALIST");
            F = D.GetSize();
            try {
                var C = K.QuerySSOInfo(1);
                $.sso_ver = C.GetInt("nSSOVersion")
            } catch(G) {
                $.sso_ver = 0
            }
        } else {
            if (navigator.mimeTypes["application/nptxsso"]) {
                $.plugin_isd_flag = "flag1=7808&flag2=1&flag3=21";
                $.report.setBasePoint($.plugin_isd_flag, (new Date()).getTime());
                if (!$.nptxsso) {
                    $.nptxsso = document.createElement("embed");
                    $.nptxsso.type = "application/nptxsso";
                    $.nptxsso.style.width = "0px";
                    $.nptxsso.style.height = "0px";
                    document.body.appendChild($.nptxsso)
                }
                if (typeof $.nptxsso.InitPVANoST != "function") {
                    $.sso_loadComplete = false;
                    $.report.nlog("没有找到插件的InitPVANoST方法", 269929)
                } else {
                    var I = $.nptxsso.InitPVANoST();
                    if (I) {
                        F = $.nptxsso.GetPVACount();
                        $.sso_loadComplete = true
                    }
                    try {
                        $.sso_ver = $.nptxsso.GetSSOVersion()
                    } catch(G) {
                        $.sso_ver = 0
                    }
                }
            } else {
                $.report.nlog("插件没有注册成功", 263744);
                $.sso_state = 2
            }
        }
    } catch(G) {
        var J = null;
        try {
            J = $.http.getXHR()
        } catch(G) {
            return 0
        }
        var H = G.message || G;
        if (/^pt_windows_sso/.test(H)) {
            if (/^pt_windows_sso_\d+_3/.test(H)) {
                $.report.nlog("QQ插件不支持该url" + G.message, 326044)
            } else {
                $.report.nlog("QQ插件抛出内部错误" + G.message, 325361)
            }
            $.sso_state = 1
        } else {
            if (J && ($.browser("type") == "msie")) {
                if (window.navigator.platform != "Win64") {
                    $.report.nlog("可能没有安装QQ" + G.message, 322340);
                    $.sso_state = 2
                } else {
                    $.report.nlog("使用64位IE" + G.message, 343958)
                }
            } else {
                $.report.nlog("获取登录QQ号码出错" + G.message, 263745);
                if (window.ActiveXObject && window.navigator.platform == "Win32") {
                    $.sso_state = 1
                }
            }
        }
        return 0
    }
    $.loginQQnum = F;
    return F
};
$.checkNPPlugin = function() {
    var A = 10;
    window.clearInterval($.np_clock);
    $.np_clock = window.setInterval(function() {
        if (typeof $.nptxsso.InitPVANoST == "function" || A == 0) {
            window.clearInterval($.np_clock);
            if (typeof $.nptxsso.InitPVANoST == "function") {
                pt.preload.auth()
            }
        } else {
            A--;
            if (window.console) {
                console.log(A)
            }
        }
    },
    200)
};
$.guanjiaPlugin = null;
$.initGuanjiaPlugin = function() {
    try {
        if (window.ActiveXObject) {
            $.guanjiaPlugin = new ActiveXObject("npQMExtensionsIE.Basic")
        } else {
            if (navigator.mimeTypes["application/qqpcmgr-extensions-mozilla"]) {
                $.guanjiaPlugin = document.createElement("embed");
                $.guanjiaPlugin.type = "application/qqpcmgr-extensions-mozilla";
                $.guanjiaPlugin.style.width = "0px";
                $.guanjiaPlugin.style.height = "0px";
                document.body.appendChild($.guanjiaPlugin)
            }
        }
        var A = $.guanjiaPlugin.QMGetVersion().split(".");
        if (A.length == 4 && A[2] >= 9319) {} else {
            $.guanjiaPlugin = null
        }
    } catch(B) {
        $.guanjiaPlugin = null
    }
};
function pluginBegin() {
    if (!$.sso_loadComplete) {
        try {
            $.checkNPPlugin()
        } catch(A) {}
    }
    $.sso_loadComplete = true;
    $.report.setSpeedPoint($.plugin_isd_flag, 1, (new Date()).getTime());
    window.setTimeout(function(B) {
        $.report.isdSpeed($.plugin_isd_flag, 0.01)
    },
    2000);
    if (window.console) {}
} (function() {
    var A = "nohost_guid";
    var B = "/nohost_htdocs/js/SwitchHost.js";
    if ($.cookie.get(A) != "") {
        $.http.loadScript(B,
        function() {
            var C = window.SwitchHost && window.SwitchHost.init;
            C && C()
        })
    }
})();
var g_connectTime = 0;
var g_responseStartTime = 0;
var g_responseEndTime = 0; (function() {
    try {
        if (performance.timing.connectStart && performance.timing.connectStart != 0) {
            g_connectTime = performance.timing.connectStart
        }
        if (performance.timing.responseStart && performance.timing.responseStart != 0) {
            g_responseStartTime = performance.timing.responseStart
        }
        if (performance.timing.responseEnd && performance.timing.responseEnd != 0) {
            g_responseEndTime = performance.timing.responseEnd
        }
    } catch(A) {
        g_connectTime = 0;
        g_responseStartTime = 0;
        g_responseEndTime = 0
    }
})();
g_time.time0 = g_connectTime;
g_time.time1 = g_responseStartTime;
g_time.time2 = g_responseEndTime;
pt.setHeader = function(A) {
    for (var B in A) {
        if (B != "") {
            if ($("auth_face")) {
                if (A[B] && A[B].indexOf("sys.getface.qq.com") > -1) {
                    $("auth_face").src = pt.login.dftImg
                } else {
                    $("auth_face").src = A[B]
                }
            }
        }
    }
};
pt.crossMessage = function(C) {
    if (typeof window.postMessage != "undefined") {
        var B = $.str.json2str(C);
        window.parent.postMessage(B, "*")
    } else {
        if (!pt.ptui.proxy_url) {
            return
        }
        var D = pt.ptui.proxy_url + "#";
        for (var A in C) {
            D += (A + "=" + C[A] + "&")
        }
        $("proxy") && ($("proxy").innerHTML = '<iframe src="' + encodeURI(D) + '"></iframe>')
    }
},
pt.preload = function() {
    var V = "";
    var c = false;
    var U = 0;
    var H = 0;
    var O = 1;
    var M = 0;
    var P = 0;
    var I = false;
    var R = "";
    var C = false;
    var K = "";
    var F = false;
    var d = function() {
        if (pt.ptui.jumpname != "") {
            if (pt.ptui.qtarget != -1) {
                pt.ptui.qtarget = parseInt(pt.ptui.qtarget)
            }
        } else {
            switch (pt.ptui.target) {
            case "_self":
                pt.ptui.qtarget = 0;
                break;
            case "_top":
                pt.ptui.qtarget = 1;
                break;
            case "_parent":
                pt.ptui.qtarget = 2;
                break;
            default:
                pt.ptui.qtarget = 1
            }
        }
        pt.ptui.isHttps = $.check.isHttps();
        pt.ptui.enable_qlogin = pt.ptui.enable_qlogin != "0" && ($.cookie.get("pt_qlogincode") != 5);
        pt.ptui.needVip = $.check.needVip(pt.ptui.appid) ? 1 : 0
    };
    var T = function(f) {
        try {
            document.execCommand("BackgroundImageCache", false, true)
        } catch(g) {}
        $.cookie.del("ptui_qstatus", "ptlogin2." + pt.ptui.domain, "/");
        Z("login");
        if ( !! f && pt.ptui.auth_mode == 0) {
            C = true;
            K = encodeURIComponent(f)
        }
        V = pt.ptui.pt_size;
        if (pt.ptui.enable_qlogin) {
            M = $.getLoginQQNum();
            M += C ? 1 : 0
        }
        if (pt.ptui.auth_mode == 2) {
            M = 0
        }
        if (M > 0) {
            I = true;
            W(M);
            $("login").className = "login";
            $("switch_login").innerHTML = pt.str.h_pt_login;
            S(false)
        } else {
            $("login").className = "login_no_qlogin";
            $("switch_login").innerHTML = pt.str.h_other_login;
            S(true, true);
            Z("login");
            if ($("u").value && typeof pt.login.check == "function" && pt.ptui.auth_mode == 0) {
                pt.login.check()
            }
        }
        if (pt.ptui.auth_mode != 0 && f) {
            b(pt.ptui.auth_mode, f)
        }
    };
    var L = function() {
        R = (pt.ptui.isHttps ? "https://ssl.": "http://") + "ptlogin2." + pt.ptui.domain + "/pt4_auth?daid=" + pt.ptui.daid + "&appid=" + pt.ptui.appid + "&auth_token=" + $.str.time33($.cookie.get("supertoken"));
        var e = document.forms[0].u1.value;
        if (/^https/.test(e)) {
            R += "&pt4_shttps=1"
        }
        if (pt.ptui.pt_qzone_sig == "1") {
            R += "&pt_qzone_sig=1"
        }
    };
    var N = function() {
        d();
        L();
        var e = $.cookie.get("superuin");
        if (pt.ptui.daid && pt.ptui.noAuth != "1" && e != "") {
            $.http.loadScript(R)
        } else {
            pt.preload.init()
        }
    };
    var Q = function(e) {
        var g = "flag1=7808&flag2=1&flag3=11";
        if (pt.ptui.isHttps) {
            if ($.check.isSsl()) {
                g = "flag1=7808&flag2=1&flag3=8"
            } else {
                g = "flag1=7808&flag2=1&flag3=12"
            }
        }
        $.report.setBasePoint(g, (new Date()).getTime());
        var f = document.createElement("link");
        f.setAttribute("type", "text/css");
        f.setAttribute("rel", "stylesheet");
        f.setAttribute("href", e);
        document.getElementsByTagName("head")[0].appendChild(f);
        if ($.browser("type") == "msie") {
            $.e.add(f, "load", X)
        }
    };
    var B = function() {
        if (!F && $("qrswitch")) {
            if (pt.ptui.isCdn == "1") {
                Q(pt.ptui.cdnCssPath + "/h_cdn_qr_login.css?max_age=86400")
            } else {
                var f = pt.ptui.cssPath + "/h_qr_login_1.css";
                try {
                    var h = $.localData.get("cssCacheFlag");
                    if (!h) {
                        h = Math.floor(Math.random() * 100);
                        $.localData.set("cssCacheFlag", h)
                    }
                    switch (h + "") {
                    case "1":
                        f += ("?v=" + Math.random());
                        break;
                    case "2":
                        f += ("?max_age=600");
                        break;
                    case "3":
                        f += ("?max_age=86400");
                        break;
                    case "4":
                        f += ("?max_age=604800");
                        break;
                    case "5":
                        f += ("?max_age=2592000");
                        break;
                    default:
                        f += ("?max_age=604800");
                        break
                    }
                } catch(g) {}
                if (pt.ptui.isHttps) {
                    if ($.check.isSsl()) {
                        f = pt.ptui.cssPath.replace("https://", "https://ssl.") + "/h_qr_login_1.css"
                    } else {
                        f = pt.ptui.cssPath + "/h_qr_login_1.css"
                    }
                }
                Q(f)
            }
            F = true
        }
    };
    var X = function() {
        try {
            var h = $.localData.get("cssCacheFlag");
            var g = "flag1=7808&flag2=1&flag3=11";
            if (pt.ptui.isHttps) {
                if ($.check.isSsl()) {
                    g = "flag1=7808&flag2=1&flag3=8"
                } else {
                    g = "flag1=7808&flag2=1&flag3=12"
                }
                $.report.setSpeedPoint(g, 1, (new Date()).getTime());
                $.report.isdSpeed(g)
            } else {
                if (pt.ptui.style == "11") {
                    switch (h + "") {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                        $.report.setSpeedPoint(g, h, (new Date()).getTime());
                        $.report.isdSpeed(g);
                        break
                    }
                }
            }
        } catch(f) {}
    };
    var Z = function(m) {
        try {
            if (D() == 1) {
                $("bottom_web") && $.css.hide($("bottom_web"));
                pt.login && pt.login.adjustLoginsize();
                $("bottom_web") && $.css.show($("bottom_web"))
            }
        } catch(k) {}
        if (pt.ptui.no_drop_domain == "1") {
            var j = $(m);
            var l = {};
            l.action = "resize";
            l.width = j.offsetWidth || 1;
            l.height = j.offsetHeight || 1;
            pt.crossMessage(l)
        } else {
            try {
                var j = $(m);
                if (j) {
                    width = 1;
                    height = 1;
                    if (j.offsetWidth > 0) {
                        width = j.offsetWidth
                    }
                    if (j.offsetHeight > 0) {
                        height = j.offsetHeight
                    }
                    var h = window.location.hostname.replace(/ui\.ptlogin2\./i, "");
                    var g = new Date();
                    g.setTime(g.getTime() + 5 * 1000);
                    if (V == "1") {
                        document.cookie = "pt_size=" + width + "-" + height + ";domain=" + h + ";path=/;expires=" + g.toGMTString()
                    }
                    try {
                        if (typeof window.postMessage != "undefined") {
                            window.parent.postMessage("pt_size=" + width + "-" + height, "*")
                        }
                    } catch(k) {}
                    var f = $.bom.query("_crossDomain");
                    if (f == "1") {
                        return
                    }
                    if (parent.ptlogin2_onResize) {
                        parent.ptlogin2_onResize(width, height);
                        window.scroll(0, 10)
                    } else {
                        frameElement.width = width;
                        frameElement.style.width = width + "px";
                        frameElement.height = height;
                        frameElement.style.height = height + "px";
                        frameElement.style.visibility = "hidden";
                        frameElement.style.visibility = "visible"
                    }
                }
            } catch(k) {}
        }
    };
    var J = function() {
        U = window.setInterval(f, 200);
        var e = 50;
        function f() {
            var g = $.cookie.get("ptui_qstatus");
            if (e == 0) {
                I = true;
                S(true)
            }
            if (g == 2) {
                Y();
                I = true
            }
            if (g == 3) {
                Y();
                I = false;
                S(true)
            }
            e--
        }
    };
    var Y = function() {
        clearInterval(U)
    };
    var A = function() {
        if (pt.ptui.isCdn == "1" && $.sso_ver >= 1024) {
            var g = location.href.replace("ui", "xui").replace("login.html", "qlogin.html");
            return g
        }
        var j = "qlogin";
        var f = encodeURIComponent(encodeURIComponent(document.forms[0].u1.value));
        var h = (pt.ptui.jumpname == "" || pt.ptui.jumpname == "jump") ? encodeURIComponent("u1=" + encodeURIComponent(document.forms[0].u1.value)) : "";
        var k = $.check.isHttps() && $.check.isSsl();
        var e = k ? "https://ssl.": "https://";
        var g = (pt.ptui.isHttps ? e: "http://") + "xui.ptlogin2." + pt.ptui.domain + "/cgi-bin/" + j + "?domain=" + pt.ptui.domain + "&lang=" + pt.ptui.lang + "&qtarget=" + pt.ptui.qtarget + "&jumpname=" + pt.ptui.jumpname + "&appid=" + pt.ptui.appid + "&param=" + encodeURIComponent((pt.ptui.qlogin_param ? encodeURIComponent(pt.ptui.qlogin_param) : h)) + "&mibao_css=" + pt.ptui.mibao_css + "&s_url=" + f + "&low_login=" + pt.ptui.low_login + (pt.ptui.daid ? "&daid=" + pt.ptui.daid: "") + (pt.ptui.regmaster ? "&regmaster=" + pt.ptui.regmaster: "") + "&style=" + pt.ptui.style + "&authParamUrl=" + K + "&needVip=" + pt.ptui.needVip + "&ptui_version=" + pt.ptui.ptui_version;
        if (pt.ptui.csimc != "0") {
            g += "&csimc=" + pt.ptui.csimc + "&csnum=" + pt.ptui.csnum + "&authid=" + pt.ptui.authid
        }
        if (pt.ptui.pt_qzone_sig == "1") {
            g += "&pt_qzone_sig=1"
        }
        return g
    };
    var W = function(l) {
        if (c) {
            $("qlogin").style.display = "block";
            return
        } else {
            var f = A();
            var j = $("qlogin");
            var g = 265;
            if (pt.ptui.style == 12) {}
            j.innerHTML = '<iframe  id="xui" name="xui" allowtransparency="true" scrolling="no" frameborder="0" width="100%" height="' + g + '"px src="' + f + '">';
            c = true;
            S(false);
            try {
                frames.xui.focus()
            } catch(k) {}
        }
    };
    var E = function(g) {
        try {
            var f = g.u;
            var j = g.p;
            var k = g.verifycode;
            if (f.value == "") {
                f.focus();
                return
            }
            if (j.value == "") {
                j.focus();
                return
            }
            if (k.value == "") {
                k.focus()
            }
        } catch(h) {}
    };
    var S = function(h, f) {
        var k, g;
        if (h) {
            $.css.hide($("qlogin"));
            $.css.show($("web_qr_login"));
            $("qrswitch") && $.css.show($("qrswitch"));
            O = 1;
            if (!I) {
                $("login").className = "login_no_qlogin"
            }
            try {
                $("xui").blur()
            } catch(j) {}
            $("switch_login").className = "switch_btn_focus";
            $("switch_qlogin").className = "switch_btn";
            g = $("switch_login").offsetWidth;
            k = $("switch_login").parentNode.offsetWidth - g;
            if (!f) {
                E(document.loginform)
            }
            Z("login");
            if (pt.login && pt.login.isQrLogin) {
                pt.login.begin_qrlogin()
            }
        } else {
            $.css.hide($("web_qr_login"));
            $.css.show($("qlogin"));
            $("qrswitch") && $.css.hide($("qrswitch"));
            O = 2;
            try {
                frames.xui.focus()
            } catch(j) {}
            $("switch_qlogin").className = "switch_btn_focus";
            $("switch_login").className = "switch_btn";
            g = $("switch_qlogin").offsetWidth;
            k = 0;
            Z("login");
            if (pt.login && pt.login.isQrLogin) {
                pt.login.cancle_qrlogin()
            }
        }
        window.setTimeout(function() {
            try {
                $.animate.animate("switch_bottom", {
                    left: k,
                    width: g
                },
                80, 20)
            } catch(l) {
                $("switch_bottom").style.left = k + "px";
                $("switch_bottom").style.width = g + "px"
            }
        },
        100);
        pt.login && pt.login.changeBottom()
    };
    var G = function(e) {
        var f = $.cookie.get("uin").replace(/^o0*/, "");
        var g = $.str.utf8ToUincode($.cookie.get("ptuserinfo")) || f;
        $("auth_uin").innerHTML = $.str.encodeHtml(f);
        $("auth_nick").innerHTML = $.str.encodeHtml(g);
        $("auth_area").setAttribute("authUrl", $.str.encodeHtml(e));
        $.http.loadScript((pt.ptui.ishttps ? "https://ssl.ptlogin2.": "http://ptlogin2.") + pt.ptui.domain + "/getface?appid=" + pt.ptui.appid + "&imgtype=3&encrytype=0&devtype=0&keytpye=0&uin=" + f + "&r=" + Math.random())
    };
    var b = function(g, f) {
        if (g == 2) {
            $.css.hide($("cancleAuthOuter"))
        }
        G(f);
        var e = pt.ptui.style;
        if (e == 12 || e == 13) {
            $.css.hide($("header"));
            $.css.hide($("authHeader"))
        }
        $("authLogin").style.height = $("login").offsetHeight - (e == 11 ? 2 : 4) + "px";
        $.css.show($("authLogin"));
        Z("login")
    };
    var a = function() {
        var e = pt.ptui.style;
        if (e == 12 || e == 13) {
            $.css.show($("header"));
            $.css.show($("authHeader"))
        }
        $.css.hide($("authLogin"));
        Z("login")
    };
    var D = function() {
        return O
    };
    return {
        init: T,
        auth: N,
        initFocus: E,
        cancleAuth: a,
        authParamUrl: K,
        switchpage: S,
        getLoginStatus: D,
        loadQrCss: B,
        ptui_notifySize: Z
    }
} ();
pt.preload.auth();
function ptui_auth_CB(C, B) {
    switch (parseInt(C)) {
    case 0:
        pt.preload.init(B);
        break;
    case 1:
        pt.preload.init();
        break;
    case 2:
        pt.ptui.s_url = encodeURIComponent(document.forms[0].u1.value);
        var A = B + "&regmaster=" + pt.ptui.regmaster + "&aid=" + pt.ptui.appid + "&s_url=" + pt.ptui.s_url;
        switch (pt.ptui.target) {
        case "_self":
            location.href = A;
            break;
        case "_top":
            top.location.href = A;
            break;
        case "_parent":
            parent.location.href = A;
            break;
        default:
            top.location.href = A
        }
        break;
    default:
        pt.preload.init()
    }
}
$.RSA = function() {
    function G(z, t) {
        return new AS(z, t)
    }
    function AI(Aa, Ab) {
        var t = "";
        var z = 0;
        while (z + Ab < Aa.length) {
            t += Aa.substring(z, z + Ab) + "\n";
            z += Ab
        }
        return t + Aa.substring(z, Aa.length)
    }
    function R(t) {
        if (t < 16) {
            return "0" + t.toString(16)
        } else {
            return t.toString(16)
        }
    }
    function AG(Ab, Ae) {
        if (Ae < Ab.length + 11) {
            uv_alert("Message too long for RSA");
            return null
        }
        var Ad = new Array();
        var Aa = Ab.length - 1;
        while (Aa >= 0 && Ae > 0) {
            var Ac = Ab.charCodeAt(Aa--);
            if (Ac < 128) {
                Ad[--Ae] = Ac
            } else {
                if ((Ac > 127) && (Ac < 2048)) {
                    Ad[--Ae] = (Ac & 63) | 128;
                    Ad[--Ae] = (Ac >> 6) | 192
                } else {
                    Ad[--Ae] = (Ac & 63) | 128;
                    Ad[--Ae] = ((Ac >> 6) & 63) | 128;
                    Ad[--Ae] = (Ac >> 12) | 224
                }
            }
        }
        Ad[--Ae] = 0;
        var z = new AE();
        var t = new Array();
        while (Ae > 2) {
            t[0] = 0;
            while (t[0] == 0) {
                z.nextBytes(t)
            }
            Ad[--Ae] = t[0]
        }
        Ad[--Ae] = 2;
        Ad[--Ae] = 0;
        return new AS(Ad)
    }
    function k() {
        this.n = null;
        this.e = 0;
        this.d = null;
        this.p = null;
        this.q = null;
        this.dmp1 = null;
        this.dmq1 = null;
        this.coeff = null
    }
    function O(z, t) {
        if (z != null && t != null && z.length > 0 && t.length > 0) {
            this.n = G(z, 16);
            this.e = parseInt(t, 16)
        } else {
            uv_alert("Invalid RSA public key")
        }
    }
    function w(t) {
        return t.modPowInt(this.e, this.n)
    }
    function P(Aa) {
        var t = AG(Aa, (this.n.bitLength() + 7) >> 3);
        if (t == null) {
            return null
        }
        var Ab = this.doPublic(t);
        if (Ab == null) {
            return null
        }
        var z = Ab.toString(16);
        if ((z.length & 1) == 0) {
            return z
        } else {
            return "0" + z
        }
    }
    k.prototype.doPublic = w;
    k.prototype.setPublic = O;
    k.prototype.encrypt = P;
    var AW;
    var AJ = 244837814094590;
    var AA = ((AJ & 16777215) == 15715070);
    function AS(z, t, Aa) {
        if (z != null) {
            if ("number" == typeof z) {
                this.fromNumber(z, t, Aa)
            } else {
                if (t == null && "string" != typeof z) {
                    this.fromString(z, 256)
                } else {
                    this.fromString(z, t)
                }
            }
        }
    }
    function H() {
        return new AS(null)
    }
    function B(Ac, t, z, Ab, Ae, Ad) {
        while (--Ad >= 0) {
            var Aa = t * this[Ac++] + z[Ab] + Ae;
            Ae = Math.floor(Aa / 67108864);
            z[Ab++] = Aa & 67108863
        }
        return Ae
    }
    function AY(Ac, Ah, Ai, Ab, Af, t) {
        var Ae = Ah & 32767,
        Ag = Ah >> 15;
        while (--t >= 0) {
            var Aa = this[Ac] & 32767;
            var Ad = this[Ac++] >> 15;
            var z = Ag * Aa + Ad * Ae;
            Aa = Ae * Aa + ((z & 32767) << 15) + Ai[Ab] + (Af & 1073741823);
            Af = (Aa >>> 30) + (z >>> 15) + Ag * Ad + (Af >>> 30);
            Ai[Ab++] = Aa & 1073741823
        }
        return Af
    }
    function AX(Ac, Ah, Ai, Ab, Af, t) {
        var Ae = Ah & 16383,
        Ag = Ah >> 14;
        while (--t >= 0) {
            var Aa = this[Ac] & 16383;
            var Ad = this[Ac++] >> 14;
            var z = Ag * Aa + Ad * Ae;
            Aa = Ae * Aa + ((z & 16383) << 14) + Ai[Ab] + Af;
            Af = (Aa >> 28) + (z >> 14) + Ag * Ad;
            Ai[Ab++] = Aa & 268435455
        }
        return Af
    }
    if (AA && (navigator.appName == "Microsoft Internet Explorer")) {
        AS.prototype.am = AY;
        AW = 30
    } else {
        if (AA && (navigator.appName != "Netscape")) {
            AS.prototype.am = B;
            AW = 26
        } else {
            AS.prototype.am = AX;
            AW = 28
        }
    }
    AS.prototype.DB = AW;
    AS.prototype.DM = ((1 << AW) - 1);
    AS.prototype.DV = (1 << AW);
    var AB = 52;
    AS.prototype.FV = Math.pow(2, AB);
    AS.prototype.F1 = AB - AW;
    AS.prototype.F2 = 2 * AW - AB;
    var AF = "0123456789abcdefghijklmnopqrstuvwxyz";
    var AH = new Array();
    var AQ, U;
    AQ = "0".charCodeAt(0);
    for (U = 0; U <= 9; ++U) {
        AH[AQ++] = U
    }
    AQ = "a".charCodeAt(0);
    for (U = 10; U < 36; ++U) {
        AH[AQ++] = U
    }
    AQ = "A".charCodeAt(0);
    for (U = 10; U < 36; ++U) {
        AH[AQ++] = U
    }
    function AZ(t) {
        return AF.charAt(t)
    }
    function Y(z, t) {
        var Aa = AH[z.charCodeAt(t)];
        return (Aa == null) ? -1 : Aa
    }
    function y(z) {
        for (var t = this.t - 1; t >= 0; --t) {
            z[t] = this[t]
        }
        z.t = this.t;
        z.s = this.s
    }
    function N(t) {
        this.t = 1;
        this.s = (t < 0) ? -1 : 0;
        if (t > 0) {
            this[0] = t
        } else {
            if (t < -1) {
                this[0] = t + DV
            } else {
                this.t = 0
            }
        }
    }
    function C(t) {
        var z = H();
        z.fromInt(t);
        return z
    }
    function V(Ae, z) {
        var Ab;
        if (z == 16) {
            Ab = 4
        } else {
            if (z == 8) {
                Ab = 3
            } else {
                if (z == 256) {
                    Ab = 8
                } else {
                    if (z == 2) {
                        Ab = 1
                    } else {
                        if (z == 32) {
                            Ab = 5
                        } else {
                            if (z == 4) {
                                Ab = 2
                            } else {
                                this.fromRadix(Ae, z);
                                return
                            }
                        }
                    }
                }
            }
        }
        this.t = 0;
        this.s = 0;
        var Ad = Ae.length,
        Aa = false,
        Ac = 0;
        while (--Ad >= 0) {
            var t = (Ab == 8) ? Ae[Ad] & 255 : Y(Ae, Ad);
            if (t < 0) {
                if (Ae.charAt(Ad) == "-") {
                    Aa = true
                }
                continue
            }
            Aa = false;
            if (Ac == 0) {
                this[this.t++] = t
            } else {
                if (Ac + Ab > this.DB) {
                    this[this.t - 1] |= (t & ((1 << (this.DB - Ac)) - 1)) << Ac;
                    this[this.t++] = (t >> (this.DB - Ac))
                } else {
                    this[this.t - 1] |= t << Ac
                }
            }
            Ac += Ab;
            if (Ac >= this.DB) {
                Ac -= this.DB
            }
        }
        if (Ab == 8 && (Ae[0] & 128) != 0) {
            this.s = -1;
            if (Ac > 0) {
                this[this.t - 1] |= ((1 << (this.DB - Ac)) - 1) << Ac
            }
        }
        this.clamp();
        if (Aa) {
            AS.ZERO.subTo(this, this)
        }
    }
    function n() {
        var t = this.s & this.DM;
        while (this.t > 0 && this[this.t - 1] == t) {--this.t
        }
    }
    function Q(z) {
        if (this.s < 0) {
            return "-" + this.negate().toString(z)
        }
        var Aa;
        if (z == 16) {
            Aa = 4
        } else {
            if (z == 8) {
                Aa = 3
            } else {
                if (z == 2) {
                    Aa = 1
                } else {
                    if (z == 32) {
                        Aa = 5
                    } else {
                        if (z == 4) {
                            Aa = 2
                        } else {
                            return this.toRadix(z)
                        }
                    }
                }
            }
        }
        var Ac = (1 << Aa) - 1,
        Af,
        t = false,
        Ad = "",
        Ab = this.t;
        var Ae = this.DB - (Ab * this.DB) % Aa;
        if (Ab-->0) {
            if (Ae < this.DB && (Af = this[Ab] >> Ae) > 0) {
                t = true;
                Ad = AZ(Af)
            }
            while (Ab >= 0) {
                if (Ae < Aa) {
                    Af = (this[Ab] & ((1 << Ae) - 1)) << (Aa - Ae);
                    Af |= this[--Ab] >> (Ae += this.DB - Aa)
                } else {
                    Af = (this[Ab] >> (Ae -= Aa)) & Ac;
                    if (Ae <= 0) {
                        Ae += this.DB; --Ab
                    }
                }
                if (Af > 0) {
                    t = true
                }
                if (t) {
                    Ad += AZ(Af)
                }
            }
        }
        return t ? Ad: "0"
    }
    function q() {
        var t = H();
        AS.ZERO.subTo(this, t);
        return t
    }
    function AM() {
        return (this.s < 0) ? this.negate() : this
    }
    function e(t) {
        var Aa = this.s - t.s;
        if (Aa != 0) {
            return Aa
        }
        var z = this.t;
        Aa = z - t.t;
        if (Aa != 0) {
            return Aa
        }
        while (--z >= 0) {
            if ((Aa = this[z] - t[z]) != 0) {
                return Aa
            }
        }
        return 0
    }
    function J(z) {
        var Ab = 1,
        Aa;
        if ((Aa = z >>> 16) != 0) {
            z = Aa;
            Ab += 16
        }
        if ((Aa = z >> 8) != 0) {
            z = Aa;
            Ab += 8
        }
        if ((Aa = z >> 4) != 0) {
            z = Aa;
            Ab += 4
        }
        if ((Aa = z >> 2) != 0) {
            z = Aa;
            Ab += 2
        }
        if ((Aa = z >> 1) != 0) {
            z = Aa;
            Ab += 1
        }
        return Ab
    }
    function T() {
        if (this.t <= 0) {
            return 0
        }
        return this.DB * (this.t - 1) + J(this[this.t - 1] ^ (this.s & this.DM))
    }
    function AR(Aa, z) {
        var t;
        for (t = this.t - 1; t >= 0; --t) {
            z[t + Aa] = this[t]
        }
        for (t = Aa - 1; t >= 0; --t) {
            z[t] = 0
        }
        z.t = this.t + Aa;
        z.s = this.s
    }
    function x(Aa, z) {
        for (var t = Aa; t < this.t; ++t) {
            z[t - Aa] = this[t]
        }
        z.t = Math.max(this.t - Aa, 0);
        z.s = this.s
    }
    function S(Af, Ab) {
        var z = Af % this.DB;
        var t = this.DB - z;
        var Ad = (1 << t) - 1;
        var Ac = Math.floor(Af / this.DB),
        Ae = (this.s << z) & this.DM,
        Aa;
        for (Aa = this.t - 1; Aa >= 0; --Aa) {
            Ab[Aa + Ac + 1] = (this[Aa] >> t) | Ae;
            Ae = (this[Aa] & Ad) << z
        }
        for (Aa = Ac - 1; Aa >= 0; --Aa) {
            Ab[Aa] = 0
        }
        Ab[Ac] = Ae;
        Ab.t = this.t + Ac + 1;
        Ab.s = this.s;
        Ab.clamp()
    }
    function L(Ae, Ab) {
        Ab.s = this.s;
        var Ac = Math.floor(Ae / this.DB);
        if (Ac >= this.t) {
            Ab.t = 0;
            return
        }
        var z = Ae % this.DB;
        var t = this.DB - z;
        var Ad = (1 << z) - 1;
        Ab[0] = this[Ac] >> z;
        for (var Aa = Ac + 1; Aa < this.t; ++Aa) {
            Ab[Aa - Ac - 1] |= (this[Aa] & Ad) << t;
            Ab[Aa - Ac] = this[Aa] >> z
        }
        if (z > 0) {
            Ab[this.t - Ac - 1] |= (this.s & Ad) << t
        }
        Ab.t = this.t - Ac;
        Ab.clamp()
    }
    function AC(z, Ab) {
        var Aa = 0,
        Ac = 0,
        t = Math.min(z.t, this.t);
        while (Aa < t) {
            Ac += this[Aa] - z[Aa];
            Ab[Aa++] = Ac & this.DM;
            Ac >>= this.DB
        }
        if (z.t < this.t) {
            Ac -= z.s;
            while (Aa < this.t) {
                Ac += this[Aa];
                Ab[Aa++] = Ac & this.DM;
                Ac >>= this.DB
            }
            Ac += this.s
        } else {
            Ac += this.s;
            while (Aa < z.t) {
                Ac -= z[Aa];
                Ab[Aa++] = Ac & this.DM;
                Ac >>= this.DB
            }
            Ac -= z.s
        }
        Ab.s = (Ac < 0) ? -1 : 0;
        if (Ac < -1) {
            Ab[Aa++] = this.DV + Ac
        } else {
            if (Ac > 0) {
                Ab[Aa++] = Ac
            }
        }
        Ab.t = Aa;
        Ab.clamp()
    }
    function b(z, Ab) {
        var t = this.abs(),
        Ac = z.abs();
        var Aa = t.t;
        Ab.t = Aa + Ac.t;
        while (--Aa >= 0) {
            Ab[Aa] = 0
        }
        for (Aa = 0; Aa < Ac.t; ++Aa) {
            Ab[Aa + t.t] = t.am(0, Ac[Aa], Ab, Aa, 0, t.t)
        }
        Ab.s = 0;
        Ab.clamp();
        if (this.s != z.s) {
            AS.ZERO.subTo(Ab, Ab)
        }
    }
    function p(Aa) {
        var t = this.abs();
        var z = Aa.t = 2 * t.t;
        while (--z >= 0) {
            Aa[z] = 0
        }
        for (z = 0; z < t.t - 1; ++z) {
            var Ab = t.am(z, t[z], Aa, 2 * z, 0, 1);
            if ((Aa[z + t.t] += t.am(z + 1, 2 * t[z], Aa, 2 * z + 1, Ab, t.t - z - 1)) >= t.DV) {
                Aa[z + t.t] -= t.DV;
                Aa[z + t.t + 1] = 1
            }
        }
        if (Aa.t > 0) {
            Aa[Aa.t - 1] += t.am(z, t[z], Aa, 2 * z, 0, 1)
        }
        Aa.s = 0;
        Aa.clamp()
    }
    function c(Ai, Af, Ae) {
        var Ao = Ai.abs();
        if (Ao.t <= 0) {
            return
        }
        var Ag = this.abs();
        if (Ag.t < Ao.t) {
            if (Af != null) {
                Af.fromInt(0)
            }
            if (Ae != null) {
                this.copyTo(Ae)
            }
            return
        }
        if (Ae == null) {
            Ae = H()
        }
        var Ac = H(),
        z = this.s,
        Ah = Ai.s;
        var An = this.DB - J(Ao[Ao.t - 1]);
        if (An > 0) {
            Ao.lShiftTo(An, Ac);
            Ag.lShiftTo(An, Ae)
        } else {
            Ao.copyTo(Ac);
            Ag.copyTo(Ae)
        }
        var Ak = Ac.t;
        var Aa = Ac[Ak - 1];
        if (Aa == 0) {
            return
        }
        var Aj = Aa * (1 << this.F1) + ((Ak > 1) ? Ac[Ak - 2] >> this.F2: 0);
        var Ar = this.FV / Aj,
        Aq = (1 << this.F1) / Aj,
        Ap = 1 << this.F2;
        var Am = Ae.t,
        Al = Am - Ak,
        Ad = (Af == null) ? H() : Af;
        Ac.dlShiftTo(Al, Ad);
        if (Ae.compareTo(Ad) >= 0) {
            Ae[Ae.t++] = 1;
            Ae.subTo(Ad, Ae)
        }
        AS.ONE.dlShiftTo(Ak, Ad);
        Ad.subTo(Ac, Ac);
        while (Ac.t < Ak) {
            Ac[Ac.t++] = 0
        }
        while (--Al >= 0) {
            var Ab = (Ae[--Am] == Aa) ? this.DM: Math.floor(Ae[Am] * Ar + (Ae[Am - 1] + Ap) * Aq);
            if ((Ae[Am] += Ac.am(0, Ab, Ae, Al, 0, Ak)) < Ab) {
                Ac.dlShiftTo(Al, Ad);
                Ae.subTo(Ad, Ae);
                while (Ae[Am] < --Ab) {
                    Ae.subTo(Ad, Ae)
                }
            }
        }
        if (Af != null) {
            Ae.drShiftTo(Ak, Af);
            if (z != Ah) {
                AS.ZERO.subTo(Af, Af)
            }
        }
        Ae.t = Ak;
        Ae.clamp();
        if (An > 0) {
            Ae.rShiftTo(An, Ae)
        }
        if (z < 0) {
            AS.ZERO.subTo(Ae, Ae)
        }
    }
    function m(t) {
        var z = H();
        this.abs().divRemTo(t, null, z);
        if (this.s < 0 && z.compareTo(AS.ZERO) > 0) {
            t.subTo(z, z)
        }
        return z
    }
    function j(t) {
        this.m = t
    }
    function v(t) {
        if (t.s < 0 || t.compareTo(this.m) >= 0) {
            return t.mod(this.m)
        } else {
            return t
        }
    }
    function AL(t) {
        return t
    }
    function h(t) {
        t.divRemTo(this.m, null, t)
    }
    function f(t, Aa, z) {
        t.multiplyTo(Aa, z);
        this.reduce(z)
    }
    function AU(t, z) {
        t.squareTo(z);
        this.reduce(z)
    }
    j.prototype.convert = v;
    j.prototype.revert = AL;
    j.prototype.reduce = h;
    j.prototype.mulTo = f;
    j.prototype.sqrTo = AU;
    function Z() {
        if (this.t < 1) {
            return 0
        }
        var t = this[0];
        if ((t & 1) == 0) {
            return 0
        }
        var z = t & 3;
        z = (z * (2 - (t & 15) * z)) & 15;
        z = (z * (2 - (t & 255) * z)) & 255;
        z = (z * (2 - (((t & 65535) * z) & 65535))) & 65535;
        z = (z * (2 - t * z % this.DV)) % this.DV;
        return (z > 0) ? this.DV - z: -z
    }
    function F(t) {
        this.m = t;
        this.mp = t.invDigit();
        this.mpl = this.mp & 32767;
        this.mph = this.mp >> 15;
        this.um = (1 << (t.DB - 15)) - 1;
        this.mt2 = 2 * t.t
    }
    function AK(t) {
        var z = H();
        t.abs().dlShiftTo(this.m.t, z);
        z.divRemTo(this.m, null, z);
        if (t.s < 0 && z.compareTo(AS.ZERO) > 0) {
            this.m.subTo(z, z)
        }
        return z
    }
    function AT(t) {
        var z = H();
        t.copyTo(z);
        this.reduce(z);
        return z
    }
    function o(t) {
        while (t.t <= this.mt2) {
            t[t.t++] = 0
        }
        for (var Aa = 0; Aa < this.m.t; ++Aa) {
            var z = t[Aa] & 32767;
            var Ab = (z * this.mpl + (((z * this.mph + (t[Aa] >> 15) * this.mpl) & this.um) << 15)) & t.DM;
            z = Aa + this.m.t;
            t[z] += this.m.am(0, Ab, t, Aa, 0, this.m.t);
            while (t[z] >= t.DV) {
                t[z] -= t.DV;
                t[++z]++
            }
        }
        t.clamp();
        t.drShiftTo(this.m.t, t);
        if (t.compareTo(this.m) >= 0) {
            t.subTo(this.m, t)
        }
    }
    function AN(t, z) {
        t.squareTo(z);
        this.reduce(z)
    }
    function X(t, Aa, z) {
        t.multiplyTo(Aa, z);
        this.reduce(z)
    }
    F.prototype.convert = AK;
    F.prototype.revert = AT;
    F.prototype.reduce = o;
    F.prototype.mulTo = X;
    F.prototype.sqrTo = AN;
    function I() {
        return ((this.t > 0) ? (this[0] & 1) : this.s) == 0
    }
    function W(Af, Ag) {
        if (Af > 4294967295 || Af < 1) {
            return AS.ONE
        }
        var Ae = H(),
        Aa = H(),
        Ad = Ag.convert(this),
        Ac = J(Af) - 1;
        Ad.copyTo(Ae);
        while (--Ac >= 0) {
            Ag.sqrTo(Ae, Aa);
            if ((Af & (1 << Ac)) > 0) {
                Ag.mulTo(Aa, Ad, Ae)
            } else {
                var Ab = Ae;
                Ae = Aa;
                Aa = Ab
            }
        }
        return Ag.revert(Ae)
    }
    function AO(Aa, t) {
        var Ab;
        if (Aa < 256 || t.isEven()) {
            Ab = new j(t)
        } else {
            Ab = new F(t)
        }
        return this.exp(Aa, Ab)
    }
    AS.prototype.copyTo = y;
    AS.prototype.fromInt = N;
    AS.prototype.fromString = V;
    AS.prototype.clamp = n;
    AS.prototype.dlShiftTo = AR;
    AS.prototype.drShiftTo = x;
    AS.prototype.lShiftTo = S;
    AS.prototype.rShiftTo = L;
    AS.prototype.subTo = AC;
    AS.prototype.multiplyTo = b;
    AS.prototype.squareTo = p;
    AS.prototype.divRemTo = c;
    AS.prototype.invDigit = Z;
    AS.prototype.isEven = I;
    AS.prototype.exp = W;
    AS.prototype.toString = Q;
    AS.prototype.negate = q;
    AS.prototype.abs = AM;
    AS.prototype.compareTo = e;
    AS.prototype.bitLength = T;
    AS.prototype.mod = m;
    AS.prototype.modPowInt = AO;
    AS.ZERO = C(0);
    AS.ONE = C(1);
    var M;
    var u;
    var AD;
    function D(t) {
        u[AD++] ^= t & 255;
        u[AD++] ^= (t >> 8) & 255;
        u[AD++] ^= (t >> 16) & 255;
        u[AD++] ^= (t >> 24) & 255;
        if (AD >= l) {
            AD -= l
        }
    }
    function s() {
        D(new Date().getTime())
    }
    if (u == null) {
        u = new Array();
        AD = 0;
        var g;
        if (navigator.appName == "Netscape" && navigator.appVersion < "5" && window.crypto && window.crypto.random) {
            var d = window.crypto.random(32);
            for (g = 0; g < d.length; ++g) {
                u[AD++] = d.charCodeAt(g) & 255
            }
        }
        while (AD < l) {
            g = Math.floor(65536 * Math.random());
            u[AD++] = g >>> 8;
            u[AD++] = g & 255
        }
        AD = 0;
        s()
    }
    function a() {
        if (M == null) {
            s();
            M = AP();
            M.init(u);
            for (AD = 0; AD < u.length; ++AD) {
                u[AD] = 0
            }
            AD = 0
        }
        return M.next()
    }
    function AV(z) {
        var t;
        for (t = 0; t < z.length; ++t) {
            z[t] = a()
        }
    }
    function AE() {}
    AE.prototype.nextBytes = AV;
    function K() {
        this.i = 0;
        this.j = 0;
        this.S = new Array()
    }
    function E(Ac) {
        var Ab, z, Aa;
        for (Ab = 0; Ab < 256; ++Ab) {
            this.S[Ab] = Ab
        }
        z = 0;
        for (Ab = 0; Ab < 256; ++Ab) {
            z = (z + this.S[Ab] + Ac[Ab % Ac.length]) & 255;
            Aa = this.S[Ab];
            this.S[Ab] = this.S[z];
            this.S[z] = Aa
        }
        this.i = 0;
        this.j = 0
    }
    function A() {
        var z;
        this.i = (this.i + 1) & 255;
        this.j = (this.j + this.S[this.i]) & 255;
        z = this.S[this.i];
        this.S[this.i] = this.S[this.j];
        this.S[this.j] = z;
        return this.S[(z + this.S[this.i]) & 255]
    }
    K.prototype.init = E;
    K.prototype.next = A;
    function AP() {
        return new K()
    }
    var l = 256;
    function r(Ab, Aa, z) {
        Aa = "DF29C573C20C0B3D46F7C214B6ADB6DF55326ABFD6B4C182462446A2F6C103B80568B50019F0998D4680B0ADCA51FF916DBA64ED1004FCAE5B05A1D2EA8E986A6E0E4A153D4E0F231D9672407DC859AF8C403B938077AA736E115C2D5D7282FBC2D15CA6CE2EBE2B20EA44B45BCDA05B37D0A41EE590C0F17936E02235B8DB31";
        z = "3";
        var t = new k();
        t.setPublic(Aa, z);
        return t.encrypt(Ab)
    }
    return {
        rsa_encrypt: r
    }
} ();
$.Encryption = function() {
    var hexcase = 1;
    var b64pad = "";
    var chrsz = 8;
    var mode = 32;
    function md5(s) {
        return hex_md5(s)
    }
    function hex_md5(s) {
        return binl2hex(core_md5(str2binl(s), s.length * chrsz))
    }
    function str_md5(s) {
        return binl2str(core_md5(str2binl(s), s.length * chrsz))
    }
    function hex_hmac_md5(key, data) {
        return binl2hex(core_hmac_md5(key, data))
    }
    function b64_hmac_md5(key, data) {
        return binl2b64(core_hmac_md5(key, data))
    }
    function str_hmac_md5(key, data) {
        return binl2str(core_hmac_md5(key, data))
    }
    function core_md5(x, len) {
        x[len >> 5] |= 128 << ((len) % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;
        var a = 1732584193;
        var b = -271733879;
        var c = -1732584194;
        var d = 271733878;
        for (var i = 0; i < x.length; i += 16) {
            var olda = a;
            var oldb = b;
            var oldc = c;
            var oldd = d;
            a = md5_ff(a, b, c, d, x[i + 0], 7, -680876936);
            d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
            b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
            d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
            c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
            d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);
            a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
            d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
            b = md5_gg(b, c, d, a, x[i + 0], 20, -373897302);
            a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
            d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
            c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
            a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
            d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
            c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);
            a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
            d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
            d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
            c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
            d = md5_hh(d, a, b, c, x[i + 0], 11, -358537222);
            c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
            a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
            b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);
            a = md5_ii(a, b, c, d, x[i + 0], 6, -198630844);
            d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
            d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
            a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
            b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);
            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd)
        }
        if (mode == 16) {
            return Array(b, c)
        } else {
            return Array(a, b, c, d)
        }
    }
    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b)
    }
    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t)
    }
    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t)
    }
    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t)
    }
    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t)
    }
    function core_hmac_md5(key, data) {
        var bkey = str2binl(key);
        if (bkey.length > 16) {
            bkey = core_md5(bkey, key.length * chrsz)
        }
        var ipad = Array(16),
        opad = Array(16);
        for (var i = 0; i < 16; i++) {
            ipad[i] = bkey[i] ^ 909522486;
            opad[i] = bkey[i] ^ 1549556828
        }
        var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
        return core_md5(opad.concat(hash), 512 + 128)
    }
    function safe_add(x, y) {
        var lsw = (x & 65535) + (y & 65535);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 65535)
    }
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt))
    }
    function str2binl(str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for (var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (i % 32)
        }
        return bin
    }
    function binl2str(bin) {
        var str = "";
        var mask = (1 << chrsz) - 1;
        for (var i = 0; i < bin.length * 32; i += chrsz) {
            str += String.fromCharCode((bin[i >> 5] >>> (i % 32)) & mask)
        }
        return str
    }
    function binl2hex(binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF": "0123456789abcdef";
        var str = "";
        for (var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8 + 4)) & 15) + hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8)) & 15)
        }
        return str
    }
    function binl2b64(binarray) {
        var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var str = "";
        for (var i = 0; i < binarray.length * 4; i += 3) {
            var triplet = (((binarray[i >> 2] >> 8 * (i % 4)) & 255) << 16) | (((binarray[i + 1 >> 2] >> 8 * ((i + 1) % 4)) & 255) << 8) | ((binarray[i + 2 >> 2] >> 8 * ((i + 2) % 4)) & 255);
            for (var j = 0; j < 4; j++) {
                if (i * 8 + j * 6 > binarray.length * 32) {
                    str += b64pad
                } else {
                    str += tab.charAt((triplet >> 6 * (3 - j)) & 63)
                }
            }
        }
        return str
    }
    function hexchar2bin(str) {
        var arr = [];
        for (var i = 0; i < str.length; i = i + 2) {
            arr.push("\\x" + str.substr(i, 2))
        }
        arr = arr.join("");
        eval("var temp = '" + arr + "'");
        return temp
    }
    function getEncryption(password, uin, vcode) {
        var str1 = hexchar2bin(md5(password));
        var str2 = md5(str1 + uin);
        var str3 = md5(str2 + vcode.toUpperCase());
        return str3
    }
    function getRSAEncryption(password, vcode) {
        var str1 = md5(password);
        var str2 = str1 + vcode.toUpperCase();
        var str3 = $.RSA.rsa_encrypt(str2);
        return str3
    }
    return {
        getEncryption: getEncryption,
        getRSAEncryption: getRSAEncryption
    }
} ();
pt.login = {
    accout: "",
    at_accout: "",
    uin: "",
    saltUin: "",
    hasCheck: false,
    lastCheckAccout: "",
    needVc: false,
    vcFlag: false,
    ckNum: {},
    action: [0, 0],
    passwordErrorNum: 1,
    isIpad: false,
    t_appid: 46000101,
    seller_id: 703010802,
    checkUrl: "",
    loginUrl: "",
    verifycodeUrl: "",
    checkClock: 0,
    isCheckTimeout: false,
    checkTime: 0,
    submitTime: 0,
    errclock: 0,
    loginClock: 0,
    login_param: pt.ptui.href.substring(pt.ptui.href.indexOf("?") + 1),
    err_m: $("err_m"),
    low_login_enable: true,
    low_login_isshow: false,
    list_index: [ - 1, 2],
    keyCode: {
        UP: 38,
        DOWN: 40,
        LEFT: 37,
        RIGHT: 39,
        ENTER: 13,
        TAB: 9,
        BACK: 8,
        DEL: 46,
        F5: 116
    },
    knownEmail: ["qq.com", "foxmail.com", "gmail.com", "hotmail.com", "yahoo.com", "sina.com", "163.com", "126.com", "vip.qq.com", "vip.sina.com", "sina.cn", "sohu.com", "yahoo.cn", "yahoo.com.cn", "139.com", "wo.com.cn", "189.cn", "live.com", "msn.com", "live.hk", "live.cn", "hotmail.com.cn", "hinet.net", "msa.hinet.net", "cm1.hinet.net", "umail.hinet.net", "xuite.net", "yam.com", "pchome.com.tw", "netvigator.com", "seed.net.tw", "anet.net.tw"],
    qrlogin_clock: 0,
    qrlogin_timeout: 0,
    qrlogin_timeout_time: 100000,
    isQrLogin: false,
    qr_uin: "",
    qr_nick: "",
    dftImg: "",
    need_hide_operate_tips: true,
    js_type: 1,
    xuiState: 1,
    delayTime: 5000,
    delayMonitorId: "294059",
    hasSubmit: false,
    RSAKey: false,
    domFocus: function(B) {
        try {
            B.focus()
        } catch(A) {}
    },
    detectCapsLock: function(C) {
        var B = C.keyCode || C.which;
        var A = C.shiftKey || (B == 16) || false;
        if (((B >= 65 && B <= 90) && !A) || ((B >= 97 && B <= 122) && A)) {
            return true
        } else {
            return false
        }
    },
    generateEmailTips: function(E) {
        var H = E.indexOf("@");
        var G = "";
        if (H == -1) {
            G = E
        } else {
            G = E.substring(0, H)
        }
        var B = [];
        for (var D = 0,
        A = pt.login.knownEmail.length; D < A; D++) {
            B.push(G + "@" + pt.login.knownEmail[D])
        }
        var F = [];
        for (var C = 0,
        A = B.length; C < A; C++) {
            if (B[C].indexOf(E) > -1) {
                F.push($.str.encodeHtml(B[C]))
            }
        }
        return F
    },
    createEmailTips: function(E) {
        var A = pt.login.generateEmailTips(E);
        var G = A.length;
        var F = [];
        var D = "";
        var C = 4;
        G = Math.min(G, C);
        if (G == 0) {
            pt.login.list_index[0] = -1;
            pt.login.hideEmailTips();
            return
        }
        for (var B = 0; B < G; B++) {
            if (E == A[B]) {
                pt.login.hideEmailTips();
                return
            }
            D = "emailTips_" + B;
            if (0 == B) {
                F.push("<li id=" + D + " class='hover' >" + A[B] + "</li>")
            } else {
                F.push("<li id=" + D + ">" + A[B] + "</li>")
            }
        }
        $("email_list").innerHTML = F.join(" ");
        pt.login.list_index[0] = 0
    },
    showEmailTips: function() {
        $.css.show($("email_list"))
    },
    hideEmailTips: function() {
        $.css.hide($("email_list"))
    },
    setUrl: function() {
        var A = pt.ptui.domain;
        var B = $.check.isHttps() && $.check.isSsl();
        pt.login.checkUrl = (pt.ptui.isHttps ? "https://ssl.": "http://check.") + "ptlogin2." + A + "/check";
        pt.login.loginUrl = (pt.ptui.isHttps ? "https://ssl.": "http://") + "ptlogin2." + A + "/";
        pt.login.verifycodeUrl = (pt.ptui.isHttps ? "https://ssl.": "http://") + "captcha." + A + "/getimage";
        if (B && A != "qq.com" && A != "tenpay.com") {
            pt.login.verifycodeUrl = "https://ssl.ptlogin2." + A + "/ptgetimage"
        }
        if (pt.ptui.regmaster == 2) {
            pt.login.checkUrl = "http://check.ptlogin2.function.qq.com/check?regmaster=2&";
            pt.login.loginUrl = "http://ptlogin2.function.qq.com/"
        } else {
            if (pt.ptui.regmaster == 3) {
                pt.login.checkUrl = (pt.ptui.isHttps ? "https://ssl.": "http://") + "check.ptlogin2.crm2.qq.com/check?regmaster=3&";
                pt.login.loginUrl = (pt.ptui.isHttps ? "https://ssl.": "http://") + "ptlogin2.crm2.qq.com/"
            }
        }
    },
    init: function() {
        $.cookie.set("ptui_version", pt.ptui.ptui_version);
        if (pt.login.isJiechi()) {
            pt.login.antiProcess()
        }
        pt.login.setUrl();
        pt.login.bindEvent();
        $("login_button") && ($("login_button").disabled = false);
        pt.login.set_default_uin(pt.ptui.defaultUin);
        if ($.check.is_weibo_appid(pt.ptui.appid)) {
            $("u") && ($("u").style.imeMode = "auto")
        }
        pt.login.changeBottom();
        pt.login.dftImg = pt.ptui.isHttps ? "https://ui.ptlogin2.qq.com/style/0/images/1.gif": "http://imgcache.qq.com/ptlogin/v4/style/0/images/1.gif";
        if (pt.ptui.isCdn == "1") {
            pt.login.dftImg = location.protocol + "//pt.url.cn/ptlogin/v4/style/0/images/1.gif"
        }
        if (pt.ptui.isHttps) {
            pt.login.delayTime = 7000;
            pt.login.delayMonitorId = "294060"
        }
        pt.login.vipTest();
        window.setTimeout(function() {
            $.report.monitor("313826&union=256042", 0.05);
            var A = "flag1=7808&flag2=1&flag3=9";
            $.report.setBasePoint(A, 0);
            if (typeof window.postMessage != "undefined") {
                $.report.setSpeedPoint(A, 1, 2000)
            } else {
                $.report.setSpeedPoint(A, 1, 1000)
            }
            $.report.isdSpeed(A, 0.01);
            if (!pt.ptui.login_sig) {
                $.report.nlog("新版登录框login_sig为空|_|" + pt.ptui.ptui_version, "291552")
            }
        },
        1000);
        if (window.g_cdn_js_fail && $.browser("type") == "msie") {
            pt.login.domLoad()
        } else {
            $.e.add(window, "load", pt.login.domLoad)
        }
    },
    isJiechi: function() {
        if ((location.href.indexOf(pt.ptui.href) == -1) && (pt.ptui.jumpname == "aqjump") && !$.check.isHttps()) {
            return true
        } else {
            return false
        }
    },
    antiProcess: function() {
        document.body.innerHTML = "";
        var G = document.createElement("div");
        G.setAttribute("id", "login");
        G.style.cssText = "margin: 0 auto;padding: 0 0 5px; width: 370px;background:#fff";
        var F = document.createElement("div");
        F.innerHTML = '<input type="button" style="background:url(http://imgcache.qq.com/ptlogin/v4/style/0/images/icons.gif) no-repeat 0 -284px;margin:-4px 4px 0 0;float:right;width:20px;height:20px;cursor:pointer;" id="close" name="close" onclick="javascript:onPageClose();" title="关闭" /><u id="label_login_title">用户登录</u>';
        var K = $.bom.query("hide_title_bar") == 1;
        if (K) {
            G.style.border = "0px"
        } else {
            G.appendChild(F);
            F.style.cssText = "background:url(http://imgcache.qq.com/ptlogin/v4/style/0/images/icons.gif) no-repeat 0 0;height:21px;font-weight:bold;font-size:12px;padding:7px 0 0 30px;border-bottom:1px solid #438ece;";
            G.style.border = "1px solid #99c2ee"
        }
        var M = document.createElement("div");
        M.style.textAlign = "center";
        M.innerHTML = '<div style="position:relative;">              <br/>              <p style="line-height:20px;text-align:left;width:220px;margin:0 auto;">您当前的网络存在链路层劫持，为了确保您的帐号安全，请使用安全登录。</p></div>              <br/>              <input id="safe_login" value="安全登录"" type="button"  style="border:0;background:url(http://imgcache.qq.com/ptlogin/v4/style/0/images/icons.gif) no-repeat -102px -130px;color:#2473A2;width:103px;height:28px;cursor:pointer;font-weight:bold;font-size:14px;"/>              </div>              <div style="margin-top:10px;margin-left:10px; height:20px;">              <span style="float:left;height:15px;width:14px; background: url(https://ui.ptlogin2.qq.com/style/14/images/help.png) no-repeat scroll right center transparent;"></span>              <a style="float:left; margin-left:5px;" href="http://kf.qq.com/info/80861.html" target="_blank" >什么是链路层劫持</a>              </div>';
        G.appendChild(M);
        document.body.appendChild(G);
        pt.preload.ptui_notifySize("login");
        var D = $.bom.query("s_url");
        var A = $.bom.query("appid");
        var L = $.bom.query("regmaster");
        var C = $.bom.query("enable_qlogin");
        var H = $.bom.query("jumpname");
        var I = $.bom.query("target");
        var B = $.bom.query("qtarget");
        var E = $.bom.query("daid");
        switch (B) {
        case "self":
            B = 0;
            break;
        case "top":
            B = 1;
            break;
        case "parent":
            B = 2;
            break;
        default:
            B = 1
        }
        var J = 1;
        if (H != "") {
            if (B != -1) {
                J = B
            }
        } else {
            switch (I) {
            case "self":
                J = 0;
                break;
            case "top":
                J = 1;
                break;
            case "parent":
                J = 2;
                break;
            default:
                J = 1
            }
        }
        $("safe_login").onclick = function() {
            $.report.monitor(247563);
            if (J != 1) {
                try {
                    D = top.location.href
                } catch(O) {}
            }
            D = encodeURIComponent(D);
            var N = "https://ui.ptlogin2.qq.com/cgi-bin/login";
            window.open(N + "?style=14&pt_safe=1&appid=" + A + "&s_url=" + D + "&regmaster=" + L + "&enable_qlogin=" + C + "&daid=" + E, "_top")
        };
        $.report.monitor(248671)
    },
    vipTest: function() {
        var B = $("vip_link2");
        var A = $("vip_dot");
        if ((B && A) && (!pt.ptui.needVip || pt.ptui.lang != "2052")) {
            $.css.addClass(B, "hide");
            $.css.addClass(A, "hide")
        }
    },
    set_default_uin: function(A) {
        if (A) {} else {
            A = unescape($.cookie.getOrigin("ptui_loginuin"));
            if (pt.ptui.appid != pt.login.t_appid && ($.check.isNick(A) || $.check.isName(A))) {
                A = $.cookie.get("pt2gguin").replace(/^o/, "") - 0;
                A = A == 0 ? "": A
            }
        }
        $("u").value = A;
        if (A) {
            $.css.hide($("uin_tips"));
            $.css.show($("uin_del"));
            pt.login.set_accout()
        }
    },
    set_accout: function() {
        var A = $.str.trim($("u").value);
        var B = pt.ptui.appid;
        pt.login.accout = A;
        pt.login.at_accout = A;
        if ($.check.is_weibo_appid(B)) {
            if ($.check.isQQ(A) || $.check.isMail(A)) {
                return true
            } else {
                if ($.check.isNick(A) || $.check.isName(A)) {
                    pt.login.at_accout = "@" + A;
                    return true
                } else {
                    if ($.check.isPhone(A)) {
                        pt.login.at_accout = "@" + A.replace(/^(86|886)/, "");
                        return true
                    } else {
                        if ($.check.isSeaPhone(A)) {
                            pt.login.at_accout = "@00" + A.replace(/^(00)/, "");
                            if (/^(@0088609)/.test(pt.login.at_accout)) {
                                pt.login.at_accout = pt.login.at_accout.replace(/^(@0088609)/, "@008869")
                            }
                            return true
                        }
                    }
                }
            }
        } else {
            if ($.check.isQQ(A) || $.check.isMail(A)) {
                return true
            }
            if ($.check.isPhone(A)) {
                pt.login.at_accout = "@" + A.replace(/^(86|886)/, "");
                return true
            }
            if ($.check.isNick(A)) {
                $("u").value = A + "@qq.com";
                pt.login.accout = A + "@qq.com";
                pt.login.at_accout = A + "@qq.com";
                return true
            }
        }
        if ($.check.isForeignPhone(A)) {
            pt.login.at_accout = "@" + A
        }
        return true
    },
    show_err: function(A) {
        pt.login.hideLoading();
        pt.login.hideOperate();
        $.css.show($("error_tips"));
        pt.login.err_m.innerHTML = A;
        clearTimeout(pt.login.errclock);
        pt.login.errclock = setTimeout("pt.login.hide_err()", 5000)
    },
    hide_err: function() {
        $.css.hide($("error_tips"));
        pt.login.err_m.innerHTML = ""
    },
    showAssistant: function(A) {
        if (pt.ptui.lang != "2052") {
            return
        }
        pt.login.hideLoading();
        pt.login.hideOperate();
        $.css.show($("error_tips"));
        switch (A) {
        case 0:
            pt.login.err_m.innerHTML = "快速登录异常，试试<a class='tips_link' style='color: #29B1F1' href='/assistant/troubleshooter.html' target='_blank'>登录助手</a>修复";
            $.report.monitor("315785");
            break;
        case 1:
            pt.login.err_m.innerHTML = "快速登录异常，试试<a class='tips_link' style='color: #29B1F1' href='/assistant/troubleshooter.html' target='_blank'>登录助手</a>修复";
            $.report.monitor("315786");
            break;
        case 2:
            pt.login.err_m.innerHTML = "登录异常，试试<a class='tips_link' style='color: #29B1F1' href='/assistant/troubleshooter.html' target='_blank'>登录助手</a>修复";
            $.report.monitor("315787");
            break;
        case 3:
            pt.login.err_m.innerHTML = "快速登录异常，试试<a class='tips_link' style='color: #29B1F1' href='http://im.qq.com/qq/2013/' target='_blank' onclick='$.report.monitor(326049);'>升级QQ</a>修复";
            $.report.monitor("326046");
            break
        }
    },
    showGuanjiaTips: function() {
        $.initGuanjiaPlugin();
        if ($.guanjiaPlugin) {
            $.guanjiaPlugin.QMStartUp(16, '/traytip=3 /tipProblemid=1401 /tipSource=18 /tipType=0 /tipIdParam=0 /tipIconUrl="http://dldir2.qq.com/invc/xfspeed/qqpcmgr/clinic/image/tipsicon_qq.png" /tipTitle="QQ快速登录异常?" /tipDesc="不能用已登录的QQ号快速登录，只能手动输入账号密码，建议用电脑诊所一键修复。"');
            $.report.monitor("316548")
        } else {
            $.report.monitor("316549")
        }
    },
    showLoading: function(A) {
        pt.login.hideOperate();
        pt.login.hide_err();
        $.css.show($("loading_tips"));
        $("loading_wording").innerHTML = A
    },
    hideLoading: function() {
        $.css.hide($("loading_tips"))
    },
    showOperate: function() {
        if (pt.ptui.lang == "2052") {
            pt.login.hideLoading();
            pt.login.hide_err();
            $.css.show($("operate_tips"))
        }
    },
    hideOperate: function(A) {
        if (pt.login.need_hide_operate_tips) {
            $.css.hide($("operate_tips"))
        }
    },
    showLowList: function() {
        var A = $("combox_list");
        if (A) {
            $.css.show(A);
            pt.login.low_login_isshow = true
        }
    },
    hideLowList: function() {
        var A = $("combox_list");
        if (A) {
            $.css.hide(A);
            pt.login.low_login_isshow = false
        }
    },
    u_focus: function() {
        if ($("u").value == "") {
            $.css.show($("uin_tips"));
            $("uin_tips").className = "input_tips_focus"
        }
        $("u").parentNode.className = "inputOuter_focus"
    },
    u_blur: function() {
        pt.login.hideOperate();
        if ($("u").value == "") {
            $.css.show($("uin_tips"));
            $("uin_tips").className = "input_tips"
        } else {
            pt.login.set_accout();
            pt.login.check()
        }
        $("u").parentNode.className = "inputOuter"
    },
    window_blur: function() {
        pt.login.lastCheckAccout = ""
    },
    u_change: function() {
        pt.login.set_accout();
        pt.login.passwordErrorNum = 1;
        pt.login.hasCheck = false;
        pt.login.hasSubmit = false
    },
    list_keydown: function(H, F) {
        var E = $("email_list");
        var D = $("u");
        if (F == 1) {
            var E = $("combox_list")
        }
        var G = E.getElementsByTagName("li");
        var B = G.length;
        var A = H.keyCode;
        switch (A) {
        case pt.login.keyCode.UP:
            G[pt.login.list_index[F]].className = "";
            pt.login.list_index[F] = (pt.login.list_index[F] - 1 + B) % B;
            G[pt.login.list_index[F]].className = "hover";
            break;
        case pt.login.keyCode.DOWN:
            G[pt.login.list_index[F]].className = "";
            pt.login.list_index[F] = (pt.login.list_index[F] + 1) % B;
            G[pt.login.list_index[F]].className = "hover";
            break;
        case pt.login.keyCode.ENTER:
            var C = G[pt.login.list_index[F]].innerHTML;
            if (F == 0) {
                $("u").value = $.str.decodeHtml(C)
            }
            pt.login.hideEmailTips();
            pt.login.hideLowList();
            H.preventDefault();
            break;
        case pt.login.keyCode.TAB:
            pt.login.hideEmailTips();
            pt.login.hideLowList();
            break;
        default:
            break
        }
        if (F == 1) {
            $("combox_box").innerHTML = G[pt.login.list_index[F]].innerHTML;
            $("low_login_hour").value = G[pt.login.list_index[F]].getAttribute("value")
        }
    },
    u_keydown: function(A) {
        $.css.hide($("uin_tips"));
        if (pt.login.list_index[0] == -1) {
            return
        }
        pt.login.list_keydown(A, 0)
    },
    u_keyup: function(B) {
        var C = this.value;
        if (C == "") {
            $.css.show($("uin_tips"));
            $.css.hide($("uin_del"))
        } else {
            $.css.show($("uin_del"))
        }
        var A = B.keyCode;
        if (A != pt.login.keyCode.UP && A != pt.login.keyCode.DOWN && A != pt.login.keyCode.ENTER && A != pt.login.keyCode.TAB && A != pt.login.keyCode.F5) {
            if ($("u").value.indexOf("@") > -1) {
                pt.login.showEmailTips();
                pt.login.createEmailTips($("u").value)
            } else {
                pt.login.hideEmailTips()
            }
        }
    },
    email_mousemove: function(C) {
        var B = C.target;
        if (B.tagName.toLowerCase() != "li") {
            return
        }
        var A = $("emailTips_" + pt.login.list_index[0]);
        if (A) {
            A.className = ""
        }
        B.className = "hover";
        pt.login.list_index[0] = parseInt(B.getAttribute("id").substring(10));
        C.stopPropagation()
    },
    email_click: function(C) {
        var B = C.target;
        if (B.tagName.toLowerCase() != "li") {
            return
        }
        var A = $("emailTips_" + pt.login.list_index[0]);
        if (A) {
            $("u").value = $.str.decodeHtml(A.innerHTML);
            pt.login.set_accout();
            pt.login.check()
        }
        pt.login.hideEmailTips();
        C.stopPropagation()
    },
    p_focus: function() {
        if (this.value == "") {
            $.css.show($("pwd_tips"));
            $("pwd_tips").className = "input_tips_focus"
        }
        this.parentNode.className = "inputOuter_focus";
        pt.login.check()
    },
    p_blur: function() {
        if (this.value == "") {
            $.css.show($("pwd_tips"));
            $("pwd_tips").className = "input_tips"
        }
        $.css.hide($("caps_lock_tips"));
        this.parentNode.className = "inputOuter"
    },
    p_keydown: function(A) {
        $.css.hide($("pwd_tips"))
    },
    p_keyup: function() {
        if (this.value == "") {
            $.css.show($("pwd_tips"))
        }
    },
    p_keypress: function(A) {
        if (pt.login.detectCapsLock(A)) {
            $.css.show($("caps_lock_tips"))
        } else {
            $.css.hide($("caps_lock_tips"))
        }
    },
    vc_focus: function() {
        if (this.value == "") {
            $.css.show($("vc_tips"));
            $("vc_tips").className = "input_tips_focus"
        }
        this.parentNode.className = "inputOuter_focus"
    },
    vc_blur: function() {
        if (this.value == "") {
            $.css.show($("vc_tips"));
            $("vc_tips").className = "input_tips"
        }
        this.parentNode.className = "inputOuter"
    },
    vc_keydown: function() {
        $.css.hide($("vc_tips"))
    },
    vc_keyup: function() {
        if (this.value == "") {
            $.css.show($("vc_tips"))
        }
    },
    document_click: function() {
        pt.login.action[0]++;
        pt.login.hideEmailTips();
        pt.login.hideLowList()
    },
    document_keydown: function() {
        pt.login.action[1]++
    },
    checkbox_click: function(B) {
        var A = B.target;
        if (A.id == "low_login_wording") {
            A = $("low_login_enable")
        }
        if (A.id == "auth_low_login_wording") {
            A = $("auth_low_login_enable")
        }
        if (pt.login.low_login_enable) {
            A.className = "uncheck"
        } else {
            A.className = "checked"
        }
        pt.login.low_login_enable = !pt.login.low_login_enable
    },
    feedback: function(D) {
        var C = D ? D.target: null;
        var A = C ? C.id + "-": "";
        var B = "http://support.qq.com/write.shtml?guest=1&fid=713&SSTAG=hailunna-" + A + $.str.encodeHtml(pt.login.accout);
        window.open(B)
    },
    bind_account: function() {
        $.css.hide($("operate_tips"));
        pt.login.need_hide_operate_tips = true;
        window.open("http://id.qq.com/index.html#account");
        $.report.monitor("234964")
    },
    combox_click: function(A) {
        if (pt.login.low_login_isshow) {
            pt.login.hideLowList()
        } else {
            pt.login.showLowList()
        }
        A.stopPropagation()
    },
    switchpage: function(C) {
        var A = C.origin;
        var B = new RegExp("http(s){0,1}://(ssl.){0,1}xui.ptlogin2." + pt.ptui.domain);
        if (B.test(A)) {
            pt.login.setQloginState(C.data);
            if (pt.login.getQloginState() == "3") {
                pt.preload.switchpage(true)
            }
        } else {
            if (C.data == "hidePtui") {}
        }
    },
    setQloginState: function(A) {
        pt.login.xuiState = A
    },
    getQloginState: function() {
        return pt.login.xuiState
    },
    checkQloginState: function() {
        var A = 2000;
        if ($.check.isSsl()) {
            A = 4000
        } else {
            if (window.g_cdn_js_fail && $.browser("type") == "msie") {
                A = 5000
            }
        }
        window.setTimeout(function() {
            if ($.loginQQnum > 0) {
                var B = pt.login.getQloginState();
                var C = $.cookie.get("ptui_qstatus");
                if (B != "2" && C != "2") {
                    pt.preload.switchpage(true);
                    if (B == "1" && Math.random() < 0.1) {
                        if (!C) {
                            $.report.nlog("没有收到xui跨域消息(2)", 282482)
                        } else {
                            $.report.nlog("没有收到xui跨域消息(3)", 282313)
                        }
                    }
                }
                if (B == 3) {
                    pt.login.showAssistant(1);
                    pt.login.showGuanjiaTips()
                }
            } else {
                if ($.sso_state == 1 && /windows/.test(navigator.userAgent.toLowerCase())) {
                    pt.login.showAssistant(0)
                } else {
                    if ($.sso_state == 2 && /windows/.test(navigator.userAgent.toLowerCase())) {
                        pt.login.showAssistant(3)
                    }
                }
            }
        },
        A);
        if ($("u").value && pt.ptui.auth_mode == 0) {
            pt.login.check()
        }
        if ($.cookie.get("pt_qlogincode") == 5) {
            $.report.monitor("300967")
        }
    },
    delUin: function(A) {
        A && $.css.hide(A.target);
        $("u").value = "";
        pt.login.domFocus($("u"))
    },
    check_cdn_img: function() {
        if (!window.g_cdn_js_fail || pt.ptui.isHttps) {
            return
        }
        var A = new Image();
        A.onload = function() {
            A.onload = A.onerror = null
        };
        A.onerror = function() {
            A.onload = A.onerror = null;
            var D = $("main_css").innerHTML;
            var B = "http://imgcache.qq.com/ptlogin/v4/style/11/images/";
            var C = "http://ui.ptlogin2.qq.com/style/11/images/";
            D = D.replace(new RegExp(B, "g"), C);
            pt.login.insertInlineCss(D);
            $.report.monitor(312520)
        };
        A.src = "http://imgcache.qq.com/ptlogin/v4/style/11/images/icon_3.png"
    },
    insertInlineCss: function(A) {
        if (document.createStyleSheet) {
            var C = document.createStyleSheet("");
            C.cssText = A
        } else {
            var B = document.createElement("style");
            B.type = "text/css";
            B.textContent = A;
            document.getElementsByTagName("head")[0].appendChild(B)
        }
    },
    domLoad: function(B) {
        pt.login.checkNPLoad();
        pt.preload.loadQrCss();
        var A = $("loading_img");
        if (A) {
            A.setAttribute("src", A.getAttribute("place_src"))
        }
        pt.login.check_cdn_img();
        window.setTimeout(function() {
            pt.preload.ptui_notifySize("login");
            if (pt.ptui.auth_mode == 0) {
                pt.preload.initFocus(document.loginform)
            }
        },
        0);
        pt.login.checkQloginState();
        window.setTimeout(function() {
            $.http.loadScript(pt.ptui.jsPath + "/monitor.js")
        },
        2000)
    },
    checkNPLoad: function() {
        if (navigator.mimeTypes["application/nptxsso"] && !$.sso_loadComplete) {
            $.checkNPPlugin();
            if (window.console) {
                console.log("np 回调没执行")
            }
        }
    },
    noscript_err: function() {
        $.report.nlog("noscript_err", 316648);
        $("noscript_area").style.display = "none"
    },
    bindEvent: function() {
        var M = $("u");
        var N = $("p");
        var P = $("verifycode");
        var E = $("verifyimgArea");
        var L = $("login_button");
        var U = $("low_login_enable");
        var W = $("email_list");
        var J = $("feedback_web");
        var Q = $("feedback_qr");
        var T = $("feedback_qlogin");
        var I = $("low_login_wording");
        var F = $("close");
        var O = $("switch_qlogin");
        var A = $("switch_login");
        var V = $("uin_del");
        var K = $("qrswitch_logo");
        var C = $("bind_account");
        var B = $("cancleAuth");
        var D = $("authClose");
        var H = $("auth_area");
        var G = $("auth_low_login_enable");
        if (H) {
            $.e.add(H, "click", pt.login.authLogin);
            $.e.add(H, "mousedown", pt.login.authMouseDowm);
            $.e.add(H, "mouseup", pt.login.authMouseUp)
        }
        if (B) {
            $.e.add(B, "click", pt.preload.cancleAuth)
        }
        if (D) {
            $.e.add(D, "click", pt.login.ptui_notifyClose)
        }
        if (O) {
            $.e.add(O, "click",
            function() {
                pt.preload.switchpage(false)
            })
        }
        if (K) {
            $.e.add(K, "click", pt.login.switch_qrlogin)
        }
        if (A) {
            $.e.add(A, "click",
            function(X) {
                X.preventDefault();
                pt.preload.switchpage(true)
            })
        }
        if (C) {
            $.e.add(C, "click", pt.login.bind_account);
            $.e.add(C, "mouseover",
            function(X) {
                pt.login.need_hide_operate_tips = false
            });
            $.e.add(C, "mouseout",
            function(X) {
                pt.login.need_hide_operate_tips = true
            })
        }
        if (F) {
            $.e.add(F, "click", pt.login.ptui_notifyClose)
        }
        if (pt.ptui.low_login == 1 && U) {
            $.e.add(U, "click", pt.login.checkbox_click);
            $.e.add(I, "click", pt.login.checkbox_click)
        }
        if (pt.ptui.low_login == 1 && G) {
            $.e.add(G, "click", pt.login.checkbox_click);
            $.e.add($("auth_low_login_wording"), "click", pt.login.checkbox_click)
        }
        $.e.add(M, "focus", pt.login.u_focus);
        $.e.add(M, "blur", pt.login.u_blur);
        $.e.add(M, "change", pt.login.u_change);
        $.e.add(M, "keydown", pt.login.u_keydown);
        $.e.add(M, "keyup", pt.login.u_keyup);
        $.e.add(M, "click", pt.login.showOperate);
        $.e.add(V, "click", pt.login.delUin);
        $.e.add(N, "focus", pt.login.p_focus);
        $.e.add(N, "blur", pt.login.p_blur);
        $.e.add(N, "keydown", pt.login.p_keydown);
        $.e.add(N, "keyup", pt.login.p_keyup);
        $.e.add(N, "keypress", pt.login.p_keypress);
        $.e.add(L, "click", pt.login.submit);
        $.e.add(E, "click", pt.login.changeVC);
        $.e.add(W, "mousemove", pt.login.email_mousemove);
        $.e.add(W, "click", pt.login.email_click);
        $.e.add(document, "click", pt.login.document_click);
        $.e.add(document, "keydown", pt.login.document_keydown);
        $.e.add(P, "focus", pt.login.vc_focus);
        $.e.add(P, "blur", pt.login.vc_blur);
        $.e.add(P, "keydown", pt.login.vc_keydown);
        $.e.add(P, "keyup", pt.login.vc_keyup);
        $.e.add(window, "message", pt.login.switchpage);
        $("qrlogin") && ($("qrlogin").style.visibility = "hidden");
        var R = $("vip_link2");
        if (R) {
            $.e.add(R, "click",
            function(X) {
                window.open("http://pay.qq.com/qqvip/index.shtml?aid=vip.gongneng.other.red.dengluweb_wording2_open");
                X.preventDefault();
                $.report.monitor("263482")
            })
        }
        var S = $("noscript_img");
        if (S) {
            $.e.add(S, "load", pt.login.noscript_err);
            $.e.add(S, "error", pt.login.noscript_err)
        }
    },
    showVC: function() {
        pt.login.vcFlag = true;
        $.css.show($("verifyArea"));
        $("verifycode").value = "";
        pt.preload.ptui_notifySize("login");
        var A = $("verifyimg");
        var B = pt.login.getVCUrl();
        A.src = B
    },
    hideVC: function() {
        pt.login.vcflag = false;
        $.css.hide($("verifyArea"));
        pt.preload.ptui_notifySize("login")
    },
    changeVC: function(B) {
        var A = $("verifyimg");
        var C = pt.login.getVCUrl();
        A.src = C;
        B && B.preventDefault()
    },
    getVCUrl: function() {
        var D = pt.login.at_accout;
        var C = pt.ptui.domain;
        var B = pt.ptui.appid;
        var A = pt.login.verifycodeUrl + "?uin=" + D + "&aid=" + B + "&" + Math.random();
        return A
    },
    checkValidate: function(B) {
        try {
            var A = B.u;
            var D = B.p;
            var E = B.verifycode;
            if ($.str.trim(A.value) == "") {
                pt.login.show_err(pt.str.no_uin);
                pt.login.domFocus(A);
                return false
            }
            if ($.check.isNullQQ(A.value)) {
                pt.login.show_err(pt.str.inv_uin);
                pt.login.domFocus(A);
                return false
            }
            if (D.value == "") {
                pt.login.show_err(pt.str.no_pwd);
                pt.login.domFocus(D);
                return false
            }
            if (E.value == "") {
                if (!pt.login.needVc && !pt.login.vcFlag) {
                    pt.login.checkResultReport(14);
                    clearTimeout(pt.login.checkClock);
                    pt.login.showVC()
                } else {
                    pt.login.show_err(pt.str.no_vcode);
                    pt.login.domFocus(E)
                }
                return false
            }
            if (E.value.length < 4) {
                pt.login.show_err(pt.str.inv_vcode);
                pt.login.domFocus(E);
                E.select();
                return false
            }
        } catch(C) {}
        return true
    },
    checkTimeout: function() {
        var A = $.str.trim($("u").value);
        if ($.check.isQQ(A)) {
            pt.login.saltUin = $.str.uin2hex(A);
            pt.login.showVC();
            pt.login.isCheckTimeout = true
        }
    },
    loginTimeout: function() {
        pt.login.loginResultReport(13);
        pt.login.showAssistant(2)
    },
    check: function() {
        if (!pt.login.accout) {
            pt.login.set_accout()
        }
        if ($.check.isNullQQ(pt.login.accout)) {
            pt.login.show_err(pt.str.inv_uin);
            return false
        }
        if (pt.login.accout == pt.login.lastCheckAccout || pt.login.accout == "") {
            return
        }
        pt.login.ptui_uin(pt.login.accout);
        pt.login.lastCheckAccout = pt.login.accout;
        var B = pt.ptui.appid;
        var A = pt.login.getCheckUrl(pt.login.at_accout, B);
        pt.login.isCheckTimeout = false;
        g_time.time6 = new Date();
        clearTimeout(pt.login.checkClock);
        pt.login.checkClock = setTimeout("pt.login.checkTimeout();", 5000);
        $.http.loadScript(A)
    },
    getCheckUrl: function(B, C) {
        var A = pt.login.checkUrl + "?regmaster=" + pt.ptui.regmaster + "&";
        A += "uin=" + B + "&appid=" + C + "&js_ver=" + pt.ptui.ptui_version + "&js_type=" + pt.login.js_type + "&login_sig=" + pt.ptui.login_sig + "&u1=" + encodeURIComponent(document.forms[0].u1.value) + "&r=" + Math.random();
        return A
    },
    getSubmitUrl: function(H) {
        var B = document.forms[0];
        var I = pt.login.loginUrl + H + "?";
        var A = document.getElementById("login2qq");
        for (var E = 0; E < B.length; E++) {
            if (H == "ptqrlogin" && (B[E].name == "u" || B[E].name == "p" || B[E].name == "verifycode" || B[E].name == "h")) {
                continue
            }
            if (B[E].name == "ipFlag" && !B[E].checked) {
                I += B[E].name + "=-1&";
                continue
            }
            if (B[E].name == "fp" || B[E].type == "submit") {
                continue
            }
            if (B[E].name == "ptredirect") {
                g_ptredirect = B[E].value
            }
            if (B[E].name == "low_login_hour" && (!pt.login.low_login_enable)) {
                continue
            }
            if (B[E].name == "webqq_type" && !A && (!B[E].checked)) {
                continue
            }
            I += B[E].name;
            I += "=";
            if (B[E].name == "u") {
                I += encodeURIComponent(pt.login.at_accout) + "&";
                continue
            }
            if (B[E].name == "p") {
                var K = B.p.value;
                var G = B.verifycode.value.toUpperCase();
                var F = "";
                if (pt.login.RSAKey) {
                    F = $.Encryption.getRSAEncryption(K, G)
                } else {
                    F = $.Encryption.getEncryption(K, pt.login.saltUin, G)
                }
                I += F
            } else {
                if (B[E].name == "u1" || B[E].name == "ep") {
                    var C = B[E].value;
                    var J = "";
                    if (pt.ptui.appid == "1003903" && A) {
                        J = /\?/g.test(C) ? "&": "?";
                        var D = document.getElementById("webqq_type").value;
                        J += "login2qq=" + A.value + "&webqq_type=" + D
                    }
                    I += encodeURIComponent(C + J)
                } else {
                    I += B[E].value
                }
            }
            I += "&"
        }
        I += "low_login_enable=" + (pt.ptui.low_login == 1 && pt.login.low_login_enable ? ("1&low_login_hour=720") : 0) + "&regmaster=" + pt.ptui.regmaster + "&fp=loginerroralert&action=" + pt.login.action.join("-") + "-" + (new Date() - 0) + "&mibao_css=" + pt.ptui.mibao_css + "&t=" + pt.login.passwordErrorNum + "&g=1";
        I += "&js_ver=" + pt.ptui.ptui_version + "&js_type=" + pt.login.js_type + "&login_sig=" + pt.ptui.login_sig;
        if (pt.ptui.csimc != "0") {
            I += "&csimc=" + pt.ptui.csimc + "&csnum=" + pt.ptui.csnum + "&authid=" + pt.ptui.authid
        }
        if ($.bom.query("pt_safe") == 1) {
            I += "&ptmbproto=1"
        }
        if (pt.login.RSAKey) {
            I += "&pt_rsa=1"
        } else {
            I += "&pt_rsa=0"
        }
        if (pt.ptui.pt_qzone_sig == "1") {
            I += "&pt_qzone_sig=1"
        }
        return I
    },
    submit: function(A) {
        pt.login.submitTime = new Date().getTime();
        A && A.preventDefault();
        if (!pt.login.ptui_onLogin(document.loginform)) {
            return false
        } else {
            $.cookie.set("ptui_loginuin", escape(document.loginform.u.value), pt.ptui.domain, "/", 24 * 30)
        }
        if (pt.login.isCheckTimeout) {
            pt.login.checkResultReport(14)
        }
        g_time.time12 = new Date();
        clearTimeout(pt.login.loginClock);
        pt.login.loginClock = setTimeout("pt.login.loginTimeout();", 5000);
        var B = pt.login.getSubmitUrl("login");
        $.winName.set("login_param", encodeURIComponent(pt.login.login_param));
        pt.login.showLoading(pt.str.h_loading_wording);
        if (pt.login.isVCSessionTimeOut() && !pt.login.needVc) {
            pt.login.lastCheckAccout = "";
            pt.login.check();
            window.setTimeout(function() {
                pt.login.submit()
            },
            1000)
        } else {
            $.http.loadScript(B)
        }
        return false
    },
    isVCSessionTimeOut: function() {
        if (pt.login.submitTime - pt.login.checkTime > 1200000) {
            $.report.monitor(330323, 0.05);
            return true
        } else {
            return false
        }
    },
    getAuthLoginUrl: function() {
        var A = $.str.decodeHtml($("auth_area").getAttribute("authUrl"));
        A += "&regmaster=" + pt.ptui.regmaster + "&aid=" + pt.ptui.appid + "&s_url=" + encodeURIComponent(document.forms[0].u1.value);
        if (pt.ptui.low_login == 1 && pt.login.low_login_enable) {
            A += "&low_login_enable=1&low_login_hour=720"
        }
        return A
    },
    authLogin: function() {
        var A = pt.login.getAuthLoginUrl();
        switch (pt.ptui.target) {
        case "_self":
            location.href = A;
            break;
        case "_top":
            top.location.href = A;
            break;
        case "_parent":
            parent.location.href = A;
            break;
        default:
            top.location.href = A
        }
    },
    authMouseDowm: function(A) {
        var B = $("auth_mengban");
        B && (B.className = "face_mengban")
    },
    authMouseUp: function(A) {
        var B = $("auth_mengban");
        B && (B.className = "")
    },
    resultReport: function(B, A, E) {
        var D = "http://isdspeed.qq.com/cgi-bin/v.cgi?flag1=" + B + "&flag2=" + A + "&flag3=" + E;
        var C = new Image();
        C.src = D
    },
    checkResultReport: function(C) {
        if (pt.ptui.isHttps || Math.random() > 0.1) {
            return
        }
        var B = 170025;
        var A = 0;
        var D = C;
        switch (C) {
        case 11:
        case 12:
        case 13:
            A = 1;
            break;
        case 14:
        case 15:
            A = 2;
            break;
        default:
            break
        }
        if (A != 0) {
            pt.login.resultReport(B, A, D)
        }
    },
    loginResultReport: function(C) {
        if (pt.ptui.isHttps || Math.random() > 0.1) {
            return
        }
        var B = 170026;
        var A = 0;
        var D = C;
        switch (C) {
        case 11:
        case 12:
            A = 1;
            break;
        case 13:
            A = 2;
            break;
        default:
            break
        }
        if (A != 0) {
            pt.login.resultReport(B, A, D)
        }
    },
    ptui_notifyClose: function(A) {
        window.clearInterval(pt.login.qrlogin_clock);
        if (pt.ptui.no_drop_domain == "1") {
            A && A.preventDefault();
            var B = {};
            B.action = "close";
            pt.crossMessage(B)
        } else {
            try {
                if (parent.ptlogin2_onClose) {
                    parent.ptlogin2_onClose()
                } else {
                    if (top == this) {
                        window.close()
                    }
                }
            } catch(A) {
                window.close()
            }
        }
    },
    ptui_onLogin: function(B) {
        var A = true;
        A = pt.login.checkValidate(B);
        try {
            if (typeof parent.ptlogin2_onLogin == "function") {
                if (!parent.ptlogin2_onLogin()) {
                    return false
                }
            }
            if (typeof parent.ptlogin2_onLoginEx == "function") {
                var E = B.u.value;
                var C = B.verifycode.value;
                if (!parent.ptlogin2_onLoginEx(E, C)) {
                    return false
                }
            }
        } catch(D) {}
        return A
    },
    ptui_uin: function(A) {
        try {
            if (typeof parent.ptui_uin == "function") {
                parent.ptui_uin(A)
            }
        } catch(B) {}
    },
    is_mibao: function(A) {
        return /^http(s)?:\/\/ui.ptlogin2.(\S)+\/cgi-bin\/mibao_vry/.test(A)
    },
    get_qrlogin_pic: function() {
        var B = "ptqrshow";
        var A = (pt.ptui.isHttps ? "https://ssl.": "http://") + "ptlogin2." + pt.ptui.domain + "/" + B + "?";
        if (pt.ptui.regmaster == 2) {
            A = "http://ptlogin2.function.qq.com/" + B + "?regmaster=2&"
        } else {
            if (pt.ptui.regmaster == 3) {
                A = "http://ptlogin2.crm2.qq.com/" + B + "?regmaster=3&"
            }
        }
        A += "appid=" + pt.ptui.appid + "&e=2&l=M&s=3&d=72&v=4&t=" + Math.random();
        return A
    },
    go_qrlogin_step: function(A) {
        switch (A) {
        case 1:
            $("qrlogin_step1").style.display = "block";
            $("qrlogin_step2").style.display = "none";
            break;
        case 2:
            $("qrlogin_step1").style.display = "none";
            $("qrlogin_step2").style.display = "block";
            break;
        default:
            break
        }
    },
    adjustLoginsize: function() {
        var A = pt.login.isQrLogin;
        if (A) {
            $("web_qr_login").style.height = ($("qrlogin").offsetHeight || 265) + "px"
        } else {
            $("web_qr_login").style.height = $("web_login").offsetHeight + "px"
        }
    },
    switch_qrlogin_animate: function() {
        var B = pt.login.isQrLogin;
        var A = $("web_qr_login_show");
        var C = 0;
        if (B) {
            C = -$("web_login").offsetHeight;
            $("web_qr_login").style.height = ($("qrlogin").offsetHeight || 265) + "px";
            $("qrlogin").style.visibility = "";
            $("web_login").style.visibility = "hidden"
        } else {
            C = 0;
            $("web_qr_login").style.height = $("web_login").offsetHeight + "px";
            $("web_login").style.visibility = "";
            $("qrlogin").style.visibility = "hidden"
        }
        $.animate.animate(A, {
            top: C
        },
        30, 20)
    },
    changeBottom: function() {
        if (pt.preload.getLoginStatus() == 2) {
            $("bottom_qlogin") && $.css.show($("bottom_qlogin"))
        } else {
            $("bottom_qlogin") && $.css.hide($("bottom_qlogin"))
        }
    },
    begin_qrlogin: function() {
        pt.login.cancle_qrlogin();
        $("qrlogin_img").src = pt.login.get_qrlogin_pic();
        pt.login.qrlogin_clock = window.setInterval("pt.login.qrlogin_submit();", 2000);
        pt.login.qrlogin_timeout = window.setTimeout(function() {
            pt.login.switch_qrlogin()
        },
        pt.login.qrlogin_timeout_time)
    },
    cancle_qrlogin: function() {
        window.clearInterval(pt.login.qrlogin_clock);
        window.clearTimeout(pt.login.qrlogin_timeout)
    },
    switch_qrlogin: function(A) {
        A && A.preventDefault();
        if (!pt.login.isQrLogin) {
            pt.login.go_qrlogin_step(1);
            pt.login.begin_qrlogin();
            $("qrswitch_logo").title = "返回";
            $("qrswitch_logo").className = "qrswitch_logo_qr";
            $.report.monitor("273367", 0.05)
        } else {
            pt.login.cancle_qrlogin();
            $("qrswitch_logo").title = "二维码登录";
            $("qrswitch_logo").className = "qrswitch_logo";
            $.report.monitor("273368", 0.05)
        }
        pt.login.isQrLogin = !pt.login.isQrLogin;
        pt.login.changeBottom();
        pt.login.switch_qrlogin_animate();
        pt.preload.ptui_notifySize("login")
    },
    qrlogin_submit: function() {
        var A = pt.login.getSubmitUrl("ptqrlogin");
        $.winName.set("login_param", encodeURIComponent(pt.login.login_param));
        $.http.loadScript(A);
        return
    },
    getShortWord: function(D, G, F) {
        F = D.getAttribute("w") || F;
        G = G ? G: "";
        var B = "...";
        D.innerHTML = $.str.encodeHtml(G);
        if (D.clientWidth <= F) {} else {
            var A = Math.min(G.length, 20);
            for (var C = A; C > 0; C--) {
                var E = G.substring(0, C);
                D.innerHTML = $.str.encodeHtml(E + B);
                if (D.clientWidth <= F) {
                    break
                }
            }
        }
        D.style.width = F + "px"
    }
};
pt.login.init();
function ptuiCB(H, J, B, F, C, A) {
    clearTimeout(pt.login.loginClock);
    g_time.time13 = new Date();
    var K = g_time.time13 - g_time.time12;
    var I = 0;
    if (K < 0) {} else {
        if (K <= 3000) {
            I = 11
        } else {
            if (K <= 5000) {
                I = 12
            }
        }
    }
    if (I > 0) {
        pt.login.loginResultReport(I)
    }
    pt.login.hideLoading();
    function E() {
        try {
            var M = $.cookie.get("uin");
            var L = $.localData.get("nocookieTime") ? $.localData.get("nocookieTime") : 0;
            L = parseInt(L);
            if (!M) {
                if (navigator.cookieEnabled) {
                    L += 1;
                    $.localData.set("nocookieTime", L);
                    switch (L) {
                    case 1:
                        $.report.monitor("269923");
                        break;
                    case 2:
                        $.report.monitor("269924");
                        break;
                    default:
                        $.report.monitor("269925");
                        break
                    }
                } else {
                    $.report.monitor("273080")
                }
            } else {
                $.localData.set("nocookieTime", 0);
                $.report.monitor("269926", 0.05)
            }
        } catch(N) {}
    }
    function D() {
        switch (F) {
        case "0":
            window.location.href = B;
            break;
        case "1":
            E();
            top.location.href = B;
            break;
        case "2":
            parent.location.href = B;
            break;
        default:
            top.location.href = B
        }
    }
    pt.login.lastCheckAccout = "";
    pt.login.hasSubmit = true;
    switch (H) {
    case "0":
        if (pt.login.isQrLogin && !pt.login.is_mibao(B)) {
            window.clearInterval(pt.login.qrlogin_clock);
            D()
        } else {
            D()
        }
        break;
    case "3":
        $("p").value = "";
        pt.login.domFocus($("p"));
        pt.login.passwordErrorNum++;
        if (J == "101" || J == "102" || J == "103") {
            pt.login.showVC()
        }
        pt.login.check();
        break;
    case "4":
        if (pt.login.vcFlag) {
            pt.login.changeVC()
        } else {
            pt.login.showVC()
        }
        try {
            $("verifycode").focus();
            $("verifycode").select()
        } catch(G) {}
        break;
    case "65":
        pt.login.switch_qrlogin();
        return;
    case "66":
        return;
    case "67":
        pt.login.go_qrlogin_step(2);
        return;
    default:
        if (pt.login.needVc) {
            pt.login.changeVC()
        } else {
            pt.login.check()
        }
        break
    }
    if (!pt.login.isQrLogin && H != 0) {
        pt.login.show_err(C)
    }
    if (!pt.login.hasCheck && !pt.login.isQrLogin) {
        pt.login.showVC();
        $("verifycode").focus();
        $("verifycode").select()
    }
}
function ptui_checkVC(B, E, D) {
    clearTimeout(pt.login.checkClock);
    pt.login.saltUin = D;
    if (!D) {
        pt.login.RSAKey = true
    } else {
        pt.login.RSAKey = false
    }
    if (D == "\x00\x00\x00\x00\x00\x00\x27\x10") {
        pt.login.show_err(pt.str.inv_uin)
    } else {
        if (!pt.login.hasSubmit) {
            pt.login.hide_err()
        }
    }
    if (B == "0") {
        pt.login.hideVC();
        $("verifycode").value = E;
        pt.login.needVc = false
    } else {
        if (B == "1") {
            pt.login.showVC();
            $.css.show($("vc_tips"));
            pt.login.needVc = true
        } else {}
    }
    pt.login.domFocus($("p"));
    pt.login.hasCheck = true;
    pt.login.checkTime = new Date().getTime();
    g_time.time7 = new Date();
    var A = g_time.time7 - g_time.time6;
    var C = 0;
    if (A < 0) {
        return
    } else {
        if (A <= 3000) {
            C = 11
        } else {
            if (A <= 5000) {
                C = 12
            } else {
                C = 13
            }
        }
    }
    pt.login.checkResultReport(C)
};