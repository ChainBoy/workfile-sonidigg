#!coding:utf8
import hashlib
#腾讯的密码加密策略按照fxx筒靴的话说是个极品，通过查看login.js发现fxx筒靴此话不假啊
def Md5_3(password):
    #三次密码值的md5迭代
    m1 =hashlib.md5()
    m1.update(password)
    m2 =hashlib.md5()
    m2.update(m1.digest())
    m3 = hashlib.md5()
    m3.update(m2.digest())
    re =  m3.hexdigest()
    #print "Md5_3:",re
    return re

def Md5_Final(password, verifycode):
    #三次密码值的MD5迭代与验证码值的混合hash
    m =hashlib.md5()
    strMixedTarget = Md5_3(password).upper()+str(verifycode).upper()
    byteMixedTarget = bytes(strMixedTarget)
    m.update(byteMixedTarget)
    re = m.hexdigest().upper()
    #print "Md5_Final:", re
    return re


#if '__name__'== '__main__':
# pwd=b"ChenxofHit" #<a href="mailto:9@qq""></a>
# verifycode = b"efta"
# Md5_Final(pwd, verifycode)
