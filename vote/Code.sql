/*
-- Query: 
-- Date: 2014-05-14 16:24
*/
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (100,'帐号类型',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (101,'注册用户',100);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (102,'新浪用户',100);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (200,'帐号操作',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (201,'登录',200);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (202,'退出',200);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (203,'更新资料',200);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (205,'重置密码',200);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (300,'上传Media',0);

INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1300,'喜欢否',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1301,'喜欢',1300);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1302,'取消喜欢',1300);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1400,'浏览',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1500,'感兴趣否',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1501,'感兴趣',1500);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1502,'取消感兴趣',1500);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1600,'投票类型',0);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1601,'投票',1600);
INSERT INTO `Code` (`ID`,`Name`,`PID`) VALUES (1602,'取消投票',1600);
