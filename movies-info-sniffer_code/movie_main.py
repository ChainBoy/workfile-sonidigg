#!/bin/python
# -*- coding: utf-8 -*-

import urllib, urllib2, re, time
import datetime
import requests
from urllib  import quote
from movie import Movie
import codecs
import time
from weibo_tools import login
from send_mail import send_mail

START_DAY = '2013-11-01'
END_DAY = '2013-11-21'

REBOT_SLEEP_TIME = 120
SEARCH_SLEEP_TIME = 30

class Controller(object):
    """docstring for Controller"""
    def __init__(self, movie_name):
        super(Controller, self).__init__()
        self.session = login()
        self.movie_name = movie_name
        self.movie = Movie(self.movie_name, self.session)
        #self.url = 'http://s.weibo.com/weibo/'
        self.url = 'http://s.weibo.com/wb/'
        self.page_count = 0

    def start(self):
        start_time = datetime.datetime.strptime(START_DAY, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(END_DAY, '%Y-%m-%d')
        if start_time <= end_time:
            print 'Name:', self.movie_name
            print 'Time:', START_DAY,'--',END_DAY
            self.search_nomarl(start_time, end_time)
        else:
            print 'ERROR:起始时间必须比结束时间早!'

    def search_nomarl(self, start_time, end_time):
        '''普通搜索,All时间'''
        print 'search_nomarl:::'
        url = self.url+ quote(self.movie_name)
        start_time = datetime.datetime.strftime(start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strftime(end_time, '%Y-%m-%d')
        while True:
            timescope = start_time +':' + end_time
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)+ '&xsort=time&nodup=1'
            print url
            page_content = self.session.get(url).content
            count = self.get_page_count(page_content)

            if count == 0:
                if self.is_rebot(page_content):
                    print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                    time.sleep(REBOT_SLEEP_TIME)
                    continue
                else:
                    print '数据0页～～！'
                    return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            elif count < 50:
                print '数据小于50页，直接爬取'
                return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            #如果页数超过50
            elif count == 50:
                print '数据等于50页，分片爬取'
                self.search_by_day(start_time, end_time)
                return 'End'

    def search_by_day(self, start_time, end_time):
        '''根据天做分割，如果在普通搜索不能完成时间段所有信息的时候使用'''
        print 'search_by_day:::'
        start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d') 
            timescope = timescope +':' + timescope
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)+ '&xsort=time&nodup=1'
            print url
            while True:
                content = self.session.get(url).content
                count = self.get_page_count(content)

                if count == 0:
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        print '数据0页～～！'
                #如果页数低于50,直接爬取
                elif count < 50:
                    print '数据小于50页，直接爬取'
                    self.handle_pages(url, count, self.movie.id, end_time, end_time)
                    break
                #如果页数超过50,分小时爬取
                elif count == 50:
                    print '数据等于50页，分片爬取'
                    self.search_by_hour(end_time)
                    break
            end_time = end_time - datetime.timedelta(days=1)
            if start_time > end_time:
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def search_by_hour(self, day):
        '''针对一天按小时分割搜索'''
        print "search_by_hour:::"
        day = datetime.datetime.strftime(day, '%Y-%m-%d')
        l_hour = [i for i in range(0 ,25)] #1-24
        while True:
            i_day = l_hour.pop()
            print 'i_day',i_day
            if i_day == 24:
                end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')
                # end_time = end_time - datetime.timedelta(hours = 1)
            elif i_day == 0:
                break
            else :
                end_time = datetime.datetime.strptime(day + '-' + str(i_day), '%Y-%m-%d-%H')
                end_time = end_time - datetime.timedelta(hours = 1)
            start_time = end_time #- datetime.timedelta(hours=1)

            timescope = datetime.datetime.strftime(start_time, '%Y-%m-%d-%H') 
            timescope = timescope +':' 
            timescope = timescope + datetime.datetime.strftime(end_time, '%Y-%m-%d-%H') 
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope) +'&xsort=time&nodup=1'
            print url
            while True:
                content = self.session.get(url).content
                count = self.get_page_count(content)

                if int(count) == 0 or count == '0':
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME

                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        print '数据0页～～！'
                self.handle_pages(url, count, self.movie.id, start_time, end_time)
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def get_page_count(self, content):
        max_count = 0
        result = re.findall(r'&page=(\d+)', content)
        result.append(0)
        max_count = max([int(i) for i in result])
        print 'All Page:', result, 'Max Page:-->', max_count
        return max_count

    def is_rebot(self, content):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            send_mail('机器人', '快填验证码！')
            return True
        else:
            return False

    def handle_pages(self, url_head, page_count, movie_id, start_time, end_time):
        earliest_time = False
        if page_count == 0: page_count = 1
        for i in xrange(1, page_count + 1):
            self.page_count += 1
            url = url_head +"&page=%s" % i
            print 'Start Page','-'*90, '->:%02d' % i,'/', '%02d .' % page_count, 'All Data Page:',self.page_count
            print url
            isreboot = False
            isear = False
            error_num = 0
            while True:
                response = self.session.get(url)
                content = response.content
                isreboot = self.is_rebot(content)
                if isreboot:
                    print '变机器人了,需要帮助,while sleep %ss...' % REBOT_SLEEP_TIME
                    time.sleep(REBOT_SLEEP_TIME)
                    continue
                earliest_time = self.movie.decode_content(content, movie_id, start_time, end_time)
                if not earliest_time:
                    isear = True
                    break
                if earliest_time == 'page_error' :
                    time.sleep(1)
                    error_num +=1
                    if error_num <= 1:
                        continue
                    else :
                        break
                import random
                n = random.choice([1,1,2,2,2,3,3,3,3,4,4,5,5,10,])
                print 'End  Page','-'*90, '->:%02d' % i,'/', '%02d' % page_count, 'while sleep %ss...\n' % n
                time.sleep(n)
                break
            if isear or error_num >= 3:
                break
            time.sleep(2)
        return earliest_time

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

if __name__ == '__main__':
    movies = ["怒放2013"]#,"森林战士","扫毒","郑和1405：魔海寻踪","飘落的羽毛","野草莓","幸福快递","意外的恋爱时光","我爱的是你爱我","偷书贼",          "清须会议","同屋/室友", "饥饿游戏2:星火燎原","一切都从和你相逢开始","最高通缉犯", "辉夜姬物语", "博士之日", "至尊寿喜烧"]
    #movies = ['乱七八糟的东西']
    for movie in movies:
        Controller(movie).start()
        send_mail('数据爬虫', '"' + movie + '"数据已经爬取完成。')
