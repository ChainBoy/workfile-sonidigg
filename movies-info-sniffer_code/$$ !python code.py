#!/bin/python
#!coding:utf8
import urllib2
# url  = http://s.weibo.com/ajax/pincode/pin?type=sass&ts=1387423251

from PIL import Image, ImageFilter, ImageEnhance
for i in xrange(101):
	file_name = 'img/%s.png' % i
	im = Image.open(file_name)
	im.save('result/'+str(i)+'0.png')
	im = im.convert()
	im.save('result/'+str(i)+'1.png')
	enhancer = ImageEnhance.Brightness(im)
	im = enhancer.enhance(1.1)#加亮，效果见图2 --  1  --1.1 
	im.save('result/'+str(i)+'2.png')
	enhancer = ImageEnhance.Contrast(im) 
	im = enhancer.enhance(3) #提高对比度，效果见图3   4 --3
	im.save('result/'+str(i)+'3.png')
	im = im.convert('1') #二值化，效果见图4
	im.save('result/'+str(i)+'4.png')
	im = im.filter(ImageFilter.MedianFilter) #中值去噪，效果见图4
	im.save('result/'+str(i)+'5.png')
	im.save('code/'+str(i)+'.png')
print 'END'
