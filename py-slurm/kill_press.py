#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import signal
import subprocess
from speedp import NetSpeed
import time
import os
file_name = 'kill.py'

while True:
    run = True
    result = NetSpeed().get_rx_tx()
    p = subprocess.Popen(['ps', 'a'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.splitlines():
        if file_name in line:
            run = True
            if float(result[0]) < 200:
                pid = int(line.split(None, 2)[1])
                os.system('kill %s'% pid)
                run = False
            else:
                run = True
            time.sleep(3)
        else:
            run = False
    else:
        if not run:
            output = subprocess.Popen(['python', file_name], stdout=subprocess.PIPE).communicate()[0]
