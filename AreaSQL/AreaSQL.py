#!/bin/python
# -*- coding: utf-8 -*- 
import simplejson as json
import codecs
import time
import os
import MySQLdb as Mysqldb


class MainClass(object):
    """docstring for MainClass"""
    def __init__(self):
        super(MainClass, self).__init__()
        conn=Mysqldb.connect(
            host='localhost',
            user='root',
            passwd='123',
            port=3306,
            db='DB_Music2',
            charset='utf8'
            )
        conn.set_character_set("utf8")
        self.conn = conn
        
        self.n_file = 'nation.txt'
        self.p_file = 'province.txt'
        self.c_file = 'city.txt'
        self.start()
        exit('Data collation success.')

    def save_file(self,string,file_name = 'test_file.txt'):
        with file(file_name,'a')as f:
            f.write(string.encode('utf8') + '\n')

    def del_file(self,file_name):
        if os.path.exists(file_name):
            os.remove(file_name)
            print file_name,'is deleted.'
    def del_old(self,):
        self.del_file(self.n_file)
        self.del_file(self.p_file)
        self.del_file(self.c_file)

    def insert_DB(self, ID, Title, PID, Sort, Content,Code):
        cur = self.conn.cursor()
        a = cur.execute("insert Area values('%s','%s','%s','%s','%s','%s')" % (ID, Title, PID, Sort, Content, Code))
        self.conn.commit()
        cur.close()
        return a

    def start(self,):
        self.del_old()
        time.sleep(4)
        if not os.path.exists('Area.js'):
            exit('Not find Area.js,End.')
        f = open('Area.js','rb')
        r = f.read()
        r = json.loads(r)
        f.close()
        n = 0
        p = 0
        c = 0
        nation_num = 100000000
        for i in r:
            #if i!='USA':continue
            nation_num += 1000000
            n_sort = str(nation_num / 1000000 - 100 + 1)
            n_num = nation_num
            nation = r.get(i)
            nation_name = nation.get('n')
            n +=1
            s = str(i) + ' ' + n_sort + ' ' + nation_name + ' ' + str(nation_num)
            print '1',s
            self.insert_DB(nation_num, nation_name, 0, n_sort, nation_name, i)
            self.save_file(s, self.n_file)
            del nation['n']
            for j in nation:
                if j == '0':
                    province = nation.get('0')
                    del province['n']
                    for k in province:
                        province_name = province.get(k).get('n')
                        if province_name and k:
                            n_num += 10000
                            p_sort = int(str(n_num)[3:5])
                            p += 1
                            s = k + ' ' + str(p_sort) + ' ' + province_name + ' ' + str(n_num) + ' ' + nation_name + ' ' + str(nation_num)
                            self.insert_DB(n_num, province_name, nation_num, p_sort, nation_name +' '+ province_name, k)
                            self.save_file(s, self.p_file)
                            print '2',s
                elif j == 'n':
                    continue
                else:
                    for k in nation:
                        n_num += 10000
                        p_num = n_num
                        province = nation.get(k)
                        province_name = province.get('n')
                        p += 1
                        p_sort = int(str(n_num)[3:5])
                        s = k + ' ' + str(p_sort) + ' ' + province_name + ' ' + str(n_num) + ' ' + nation_name + ' ' + str(nation_num)
                        self.insert_DB(n_num, province_name, nation_num, p_sort, nation_name +' '+ province_name, k)
                        self.save_file(s, self.p_file)
                        print '2',s
                        for v in province:
                            if v =='n':
                                continue
                            city_name = province.get(v).get('n')
                            if city_name and v:
                                p_num += 100
                                c_sort = int(str(p_num)[5:7])
                                c += 1
                                s = v + ' ' + str(c_sort) + ' ' + city_name + ' ' + str(p_num) + ' ' + province_name + ' ' + str(n_num) + ' ' + nation_name + ' ' + str(nation_num)
                                self.insert_DB(p_num, city_name, n_num, p_sort, nation_name +' '+ province_name + ' '+city_name, v)
                                self.save_file(s, self.c_file)
                                print '3',s
                    print('%s %s %s.' % (n, p ,c))
                break    
                time.sleep(1)

if __name__ == '__main__':
    MainClass()