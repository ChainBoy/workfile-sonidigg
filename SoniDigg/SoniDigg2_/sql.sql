alter view view_song_play_num #统计每首歌前一天的在所有电台的播放次数
#select date_format('2013-11-11 18:35:21','%Y-%m-%d');
#select date_sub(date_format('2013-11-12','%Y-%m-%d'),interval 1 day);
#select song,count(tb.song)as play_count from listen_log_once as tb group by tb.song order by play_count DESC
as select tb.song,count(tb.song)as play_num,now() as time from listen_log_once as tb 
       where (date_format(tb.time,'%Y-%m-%d'))=
         (date_sub(date_format('2013-11-12','%Y-%m-%d'),interval 1 day)) 
       group by tb.song order by play_num DESC


select song.id as song_id,song.artist,pn.play_num,pn.time from view_song_play_num as pn,song where pn.song=song.id
#歌曲id 歌手姓名 播放次数 统计日期 热度



#以下用于测试统计热度 hot状态
select b.id,b.song,b.num from tb_two as b left join tb_one as a on a.song=b.song;
# CASE WHEN 1>0 THEN 'true' ELSE 'false' END;

SELECT b.id,b.song,b.num,
    CASE 
        WHEN b.num>a.num THEN 0 
        WHEN b.num<a.num THEN 1
        ELSE 2 
    END as hot
from tb_two as b left join tb_one as a on a.song=b.song;

select * from tb_one;
select * from tb_two;
select * from 
        (select t.*,@rownum := @rownum + 1 AS rank from 
                (select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add 
                    from (
                            select so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num 
                                from song_play_log as lo,
                                    (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so 
                                        where lo.artist_id = so.artist_id and lo.time between '2014-01-13' and '2014-01-20' group by lo.artist_id order by lo.play_num desc)as tb1
                                        left join (
                                            select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time 
                                                from song_play_log as lo2 
                                                    where lo2.time between date_sub('2014-01-13',interval 7 day) and date_sub('2014-01-20',interval 7 day) group by lo2.artist_id order by play_num)as tb2
                                        on tb1.artist_id=tb2.artist_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) as r) as re_tb 
        where re_tb.rank between 0 and 10;

select * from 
        (select t.*,@rownum := @rownum + 1 AS rank from 
                (select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add 
                    from (
                            select so.name as artist,so.id as artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num 
                                from song_play_log as lo,artist as so 
                                        where lo.artist_id = so.id and lo.time between '2014-01-13' and '2014-01-20' group by lo.artist_id order by lo.play_num desc)as tb1
                                        left join (
                                            select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time 
                                                from song_play_log as lo2 
                                                    where lo2.time between date_sub('2014-01-13',interval 7 day) and date_sub('2014-01-20',interval 7 day) group by lo2.artist_id order by play_num)as tb2
                                        on tb1.artist_id=tb2.artist_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) as r) as re_tb 
        where re_tb.rank between 0 and 10;
call sp_get_rank(0,1,10,'2014-01-13','2014-01-20')
select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo,(select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.artist_id = so.artist_id and lo.time between '2014-01-13' and '2014-01-20' group by lo.artist_id order by lo.play_num desc)as tb1 left join (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub('2014-01-13',interval 7 day) and date_sub('2014-01-20',interval 7 day) group by lo2.artist_id order by play_num)as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc;






SELECT tb1.time,hour(tb1.time) AS h,count(DISTINCT tb1.stationId) as station_count
FROM listen_log_once as tb1
WHERE tb1.time between date_sub(now(),interval 24 hour) and now()
GROUP BY h order by tb1.time;



SELECT hour(time) AS h,time,COUNT(*)as play_count
FROM listen_log_once
WHERE time between date_sub(now(),interval 24 hour) and now()
GROUP BY h order by time;

CREATE TRIGGER before_insert_Artists
  BEFORE INSERT ON Artists
  FOR EACH ROW
  if 1 then
  select '000';
  end if;

CREATE TRIGGER before_insert_Artists
  BEFORE INSERT ON Artists
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');


/* 修改表结构*/
ALTER TABLE `dataservice`.`song_play_log` ADD COLUMN `state` INT(11) NULL DEFAULT '0' COMMENT '时间长度是否为零，如果时间为零，则状态为1；默认为0，正常状态，时间不为零 '  ;


/* 创建事务*/
DELIMITER $$
create trigger before_insert_song_play_log
before insert on song_play_log
for each row
begin
    if new.`play_time`=0 then
        set new.`state`=1, new.`play_time` = 20 * new.`play_num`;
    end if;
end$$



/*修改已存在的*/
update song_play_log set state=1, play_time = 20*play_num where play_time = 0;

/*实时修改时长为0的时长*/
update song_play_log set play_time = 20*play_num where play_time = 0 or state= 1;


select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo,(select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.artist_id = so.artist_id and lo.time between '2014-01-13' and '2014-01-20' group by lo.artist_id order by lo.play_num desc)as tb1 left join (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub('2014-01-13',interval 7 day) and date_sub('2014-01-20',interval 7 day) group by lo2.artist_id order by play_num)as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc;


