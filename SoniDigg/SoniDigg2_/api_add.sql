ALTER TABLE `dataservice`.`station_code` ADD COLUMN `city_id` INT NULL DEFAULT 0  AFTER `blanca` ;
ALTER TABLE `dataservice`.`station` ADD COLUMN `city_id` INT NULL  AFTER `createdTime` ;
 
CREATE  TABLE `dataservice`.`city` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `pid` INT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

insert into city(name,id) values('华北地区',100001);
insert into city(name,id) values('华中地区',100002);
insert into city(name,id) values('东北地区',100003);
insert into city(name,id) values('西南地区',100004);
insert into city(name,id) values('西北地区',100005);
insert into city(name,id) values('华南地区',100006);
insert into city(name,id) values('华东地区',100007);
insert into city(name,id) values('其他',100008);

insert into city(name,id) values('贵州',37);
insert into city(name,id) values('河南',25);
insert into city(name,id) values('山东',31);
insert into city(name,id) values('四川',18);
insert into city(name,id) values('香港',47);
insert into city(name,id) values('江苏',17);
insert into city(name,id) values('青海',41);
insert into city(name,id) values('新疆',45);
insert into city(name,id) values('福建',34);
insert into city(name,id) values('浙江',19);
insert into city(name,id) values('湖北',27);
insert into city(name,id) values('天津',23);
insert into city(name,id) values('江西',38);
insert into city(name,id) values('西藏',42);
insert into city(name,id) values('黑龙江',28);
insert into city(name,id) values('重庆',26);
insert into city(name,id) values('云南',46);
insert into city(name,id) values('北京',11);
insert into city(name,id) values('台湾',49);
insert into city(name,id) values('国家台',54);
insert into city(name,id) values('澳门',48);
insert into city(name,id) values('广西',36);
insert into city(name,id) values('陕西',51);
insert into city(name,id) values('甘肃',33);
insert into city(name,id) values('河北',30);
insert into city(name,id) values('吉林',39);
insert into city(name,id) values('广东',22);
insert into city(name,id) values('宁夏',40);
insert into city(name,id) values('湖南',20);
insert into city(name,id) values('安徽',32);
insert into city(name,id) values('内蒙古',44);
insert into city(name,id) values('上海',10);
insert into city(name,id) values('网络',60);
insert into city(name,id) values('山西',24);
insert into city(name,id) values('海南',43);
insert into city(name,id) values('辽宁',21);

update city set pid = 100001 where id in(30,24,44,11,23);
update city set pid = 100002 where id in(25,20,27);
update city set pid = 100003 where id in(28,39,21);
update city set pid = 100004 where id in(18,46,37,42,26);
update city set pid = 100005 where id in(45,51,33,40,41);
update city set pid = 100006 where id in(47,48,22,36,43);
update city set pid = 100007 where id in(17,19,34,31,38,32,49,10);
update city set pid = 100008 where id in(54,60);




DELIMITER $$
CREATE PROCEDURE sp_top_rank(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
    #已完善，查询本周top榜单
    #cate 0 全部，cate 1 英文，cate 2 中文
    if cate = 0 then
        set @tql=concat("select s.name,s.artist, count(*) as count from song s inner join listen_log_once o on s.id=o.song where o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc limit ",num_start,",",num_end,";");
    elseif cate = 1 then
        set @tql= concat("select s.name,s.artist, count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.name not regexp '[^ -~]' and s.artist not regexp '[^ -~]' and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc limit ",num_start,",",num_end,";");
    elseif cate = 2 then
        set @tql= concat("select s.name,s.artist, count(*) as count from song s inner join listen_log_once o on s.id=o.song where (s.name regexp '[^ -~]' and s.artist regexp '[^ -~]') and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc limit ",num_start,",",num_end,";");
    end if;
    #select @tql;
    PREPARE stmt from @tql;
    EXECUTE stmt;
end$$

DELIMITER $$
CREATE PROCEDURE sp_top_rank_info(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30),duli_tuple longtext charset 'utf8')
begin
    #已完善，查询本周独立音乐人top歌曲的播放详情
    #cate 0 全部，cate 1 英文，cate 2 中文，cate 3 音乐人
    if cate = 0 then
        set @tql= concat("select s1.name,s1.id as song_id,s1.artist,d1.name as station,d1.id as station_id, o1.time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc limit ",num_start,",",num_end,")tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id;");
    elseif cate = 1 then
        set @tql= concat("select s1.name,s1.id as song_id,s1.artist,d1.name as station,d1.id as station_id, o1.time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.name not regexp '[^ -~]' and s.artist not regexp '[^ -~]' and o.time between '",time_start,"' and '",time_end,"' and date(now()) group by s.name,s.artist order by count desc limit ",num_start,",",num_end,")tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id;");
    elseif cate = 2 then
        set @tql= concat("select s1.name,s1.id as song_id,s1.artist,d1.name as station,d1.id as station_id, o1.time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where (s.name regexp '[^ -~]' or s.artist regexp '[^ -~]') and o.time between '",time_start,"' and '",time_end,"' and date(now()) group by s.name,s.artist order by count desc limit ",num_start,",",num_end,")tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id;");
    elseif cate = 3 then
        set @tql= concat("select s1.name,s1.id as song_id,s1.artist,d1.name as station,d1.id as station_id, o1.time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.artist in ",duli_tuple," and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc limit ",num_start,",",num_end,")tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id;");
    end if;
    #select @tql;
    PREPARE stmt from @tql;
    EXECUTE stmt;
end$$

drop PROCEDURE sp_top_rank_info;

drop PROCEDURE sp_where_info;
DELIMITER $$
CREATE PROCEDURE sp_where_info(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30),sql_where longtext charset 'utf8')
begin
    #已完善，根据歌名查询电台播放记录，分省份次数/总播放次数，24小时分时，12个月分月
    if cate = 0 then
        set @tql= concat("select s1.name,s1.artist,s1.artist_id,d1.name as station,d1.id as station_id, o1.time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where ",sql_where," and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc)tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id;");
    elseif cate = 1 then
        set @tql= concat("select nt1.station,nt1.station_id,tn2.name,nt1.city_id,tn2.district,tn2.pid,nt1.count from (select d1.name as station,d1.id as station_id,d1.city_id,count(*)as count from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where ",sql_where," and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc)tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id group by d1.city_id) as nt1,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as tn2 where nt1.city_id = tn2.id;");
    elseif cate = 2 then
        set @tql= concat("select s1.name,s1.artist,o1.time,hour(o1.time)as h,count(*)as count from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where ",sql_where," and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc)tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id group by h;");
    elseif cate = 3 then
        set @tql= concat("select s1.name,s1.artist,o1.time,month(o1.time)as m,count(*)as count from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where ",sql_where," and o.time between '",time_start,"' and '",time_end,"' group by s.name,s.artist order by count desc)tb1 on s1.id = tb1.song_id where o1.time between '",time_start,"' and '",time_end,"' and s1.artist_id=tb1.artist_id group by m;");
    end if;
    #select @tql;
    PREPARE stmt from @tql;
    EXECUTE stmt;
end$$



drop procedure ff;
DELIMITER $$
CREATE PROCEDURE `ff`(s_time varchar(10),e_time varchar(10))
BEGIN
    declare time longtext;
    declare n_time varchar(25);
    declare i int;
    set time = date_sub(s_time,interval 0 hour);
    set n_time = date_sub(s_time,interval 0 hour);
    set s_time = date_sub(s_time,interval 0 hour);
    set e_time = date_sub(e_time,interval 0 hour);
    set i = 0;
    while n_time >= s_time and n_time <= e_time do
        begin
            #操作
            set i =i + 1;
            #select date_sub(s_time,interval i hour);
            set n_time = date_sub(s_time,interval -i hour);
            SET time=concat(time,'|',date_sub(s_time,interval -i hour));
            #select time,n_time;
        end;
    end while;
    select time;
END$$

drop PROCEDURE sp_listen_info;
DELIMITER $$
CREATE PROCEDURE sp_listen_info(in cate int,num_start int,num_end int, id int)
begin
    #获取最近的一些记录
    #0最近多少条 1最近几分钟的多少条 2根据电台id查询多少条 3根据省份id查询多少条 4根据歌曲id查询多少条
    if cate = 0 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select * from listen_log_once order by id desc limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    elseif cate = 1 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select * from listen_log_once where time between date_sub(now(), interval ",id," minute) and now() order by time desc limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    elseif cate = 2 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select * from listen_log_once where stationId = ",id," order by id desc limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    elseif cate = 3 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select distinct * from listen_log_once where stationId in(select id from station where city_id = ",id,") limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    elseif cate = 4 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select * from listen_log_once where song = ",id," order by id desc limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    elseif cate = 5 then
        set @tql= concat("select t1.time,t2.name as song,t1.song as song_id,t2.artist,t2.artist_id,t3.name as station,t1.stationId,t3.city_id,t4.name as province,t4.id as province_id,t4.district,t4.pid as district_id from (select * from listen_log_once where song in(select id from song where artist_id = ",id,") order by id desc limit ",num_start,",",num_end,") as t1,song as t2,station as t3,(select c1.*,c2.name as district from city as c1,city as c2 where c1.pid = c2.id) as t4 where t1.song=t2.id and t1.stationId = t3.id and t3.city_id=t4.id order by t1.time;");
    end if;
    #select @tql;
    PREPARE stmt from @tql;
    EXECUTE stmt;
end$$
