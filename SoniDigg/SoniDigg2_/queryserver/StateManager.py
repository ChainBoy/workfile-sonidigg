#!/usr/bin/python
# -*- coding: gb2312 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import simplejson as json
import models
import lookup
import time

# Options
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'

def handleStates(jsonStr):
    engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
    engine.connect()
    db = scoped_session(sessionmaker(bind=engine))
    listeningLog = json.loads(jsonStr)
    leng = str(len(listeningLog))
    for l in listeningLog:
        oldTime =  l['time']
        newTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(long(oldTime)/1000))
        state = models.State(stationId = l['radioID'],time=newTime,state=l['state'])
        db.add(state)
    db.commit()

