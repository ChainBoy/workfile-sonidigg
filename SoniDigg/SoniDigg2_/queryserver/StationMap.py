from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import models
# Options
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'

class StationMap(object):
    def __init__(self, db_path):
        engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine))
        self.cache = self.creat_map()
    def _makeMap(self, stationId, limits):
        if stationId > 95:
            return []
        else:
            query = self.db.query(models.Listenlog_times.song).filter(models.Listenlog_times.stationId == stationId).order_by(models.Listenlog_times.time.desc()).limit(limits)
            return map(lambda x: x.song, query)
    def _makeIdList(self):
        query = self.db.query(models.Station.id);
        return map(lambda x:x[0], query)

    def creat_map(self):
        idlist = self._makeIdList()
        result = {}
        for i in idlist:
            print i
            result[i] = self._makeMap(1, 5)
        return result
if __name__ == '__main__':
    station =StationMap(db_path)
    #print station._makeIdList()
    #print station.creat_map()
