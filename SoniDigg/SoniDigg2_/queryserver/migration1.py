from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import models
import time
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'
engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
models.init_db(engine)
engine.connect()
db = scoped_session(sessionmaker(bind=engine))

def transfer_data(db,num):
    query = db.query(models.ListenLog)
    results = []
    query_results = query.filter(models.ListenLog.song == None)
    for item in query_results[:num]:
        results.append(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
    db.add_all(results)
    db.commit()

def transfer_data_op(db,num):
    query = db.query(models.ListenLog)
    query_results = query.filter(models.ListenLog.song == None)
    print "transfer_data start"
    count = 1
    while True:
        print count
        t = time.time()
        results = []
        for item in query_results[num*(count-1):num*count]:
            results.append(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
        db.add_all(results)
        db.commit()
        if len(results) < num:
            break
        else:
            count += 1
        print "loop time: " , time.time()-t
    print "transfer_data end!"

def transfer_data_op1(db, num):
    query_results = db.query(models.ListenLog)
    count = 1

    while True:
        print count
        t = time.time()
        query = query_results.filter(models.ListenLog.id > 3907879 + num*(count-1), models.ListenLog.id <= 3907879 + num*count, models.ListenLog.song == None)
        for item in query:
            db.add(models.Listenlog_null(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint))
        db.commit()
        if item.id == 3988260:
            break
        else:
            count += 1
        print "loop time: ", time.time()-t
    print "transfer_data end!"

def transfer_data_times(db, num):
    query = db.query(models.ListenLog)
    results = []
    query_results = query.filter(models.ListenLog.song != None)
    for item in query_results[:num]:
        results.append(models.Listenlog_times(time = item.time, stationId = item.stationId, fingurePrint = item.fingurePrint, song = item.song))
    db.add_all(results)
    db.commit()

def transfer_data_times_op(db, num):
    query = db.query(models.ListenLog)
    query_results = query.filter(models.ListenLog.song != None, models.ListenLog.id > 3907879)
    count = 1
    while True:
        results = []
        print count
        for item in query_results[num*(count-1):num*count]:
            results.append(models.Listenlog_times(time = item.time, stationId = item.stationId, fingerPrint = item.fingurePrint, song = item.song))
        db.add_all(results)
        db.commit()
        if len(results) < num:
            break
        else:
            count += 1
    print "transfer_data end!"

def transfer_data_once(db, num):
    query_results = db.query(models.Listenlog_times).filter(models.Listenlog_times.id > 288706)
    stationMap = {}
    print "transfer_data start"
    count = 1
    while True:
        results = []
        print count
        for index, item in enumerate(query_results[num*(count-1):num*count]):
            if stationMap.has_key(item.stationId):
                cache = stationMap[item.stationId]
                for i in cache:
                    if i == item.song:
                        break
                else:
                    results.append(models.Listenlog_once(time = item.time, stationId = item.stationId, fingerPrint = item.fingerPrint, song = item.song))
                    cache.append(item.song)
                    if len(cache) == 6:
                        cache.pop(0)
            else:
                stationMap[item.stationId] = [item.song]
                results.append(models.Listenlog_once(time = item.time, stationId = item.stationId, fingerPrint = item.fingerPrint, song = item.song))
        db.add_all(results)
        db.commit()
        if index+1<num:
            break
        else:
            count += 1
    print "transfer data stop!"

def main(db, num):
    transfer_data_once(db, num)

if __name__ == '__main__':
    main(db, 1000)
