#-*- encoding: utf-8 -*-
import sys
import threading
import time
sys.path.insert(0, "/home/pogo/servers/echoprint-server/API")
import fp
import models
import ArtistPhotoScanner

class lookupThread():
    def __init__(self, songdb):
        self.db = songdb
        
    def lookup(self, code, id):
        #print id,'code list size is ' + str(len(code)) + '!'
        try:
            time1 = time.time()
            result = fp.best_match_for_query(code)
            time2 = time.time()
            print 'query time is %0.2f'%(time2- time1) + 's'
            if result.TRID:
                songName = result.metadata.get("track")
                artistName = result.metadata.get("artist")
                print "ID: %s" % (result.TRID)
                print "Artist: %s" % (artistName)
                print "Song: %s" % (songName)
                #insert data to song db
                query = self.db.query(models.Song)
                first = query.filter(models.Song.name == songName, models.Song.artist == artistName).first()
                if first is None:
                    print 'Not in db. Will insert.'
                    song = models.Song(name = songName, artist = artistName)
                    self.db.add(song)
                    self.db.commit()
                songId = query.filter(models.Song.name == songName, models.Song.artist == artistName).first().id
                #update listen log db data
                query = self.db.query(models.ListenLog)
                query.filter(models.ListenLog.id == id).update({models.ListenLog.song: songId})
                self.db.commit()
                #insert artist info to artist db
                query = self.db.query(models.Artist)
                first = query.filter(models.Artist.name == artistName).first()
                if first is None:
                    artist = models.Artist(name = artistName)
                    self.db.add(artist)
                    self.db.commit()
                first = query.filter(models.Artist.name == artistName).first()
                if first.photo is None:
                    ArtistPhotoScanner.getPhoto(self.db, artistName, first.id) 
                    self.db.commit()
            else:
                print "No match."
        except KeyError:
                    print 'response error'
                    
class lookupThread():
    def __init__(self, songdb):
        self.db = songdb
        
    def lookup(self, log):
        #print id,'code list size is ' + str(len(code)) + '!'
        code = log['code']
        try:
            time1 = time.time()
            result = fp.best_match_for_query(code)
            time2 = time.time()
            print 'query time is %0.2f'%(time2- time1) + 's'
            if result.TRID:
                songName = result.metadata.get("track")
                artistName = result.metadata.get("artist")
                print "ID: %s" % (result.TRID)
                print "Artist: %s" % (artistName)
                print "Song: %s" % (songName)

                #insert data to song db
                query = self.db.query(models.Song)
                first = query.filter(models.Song.name == songName, models.Song.artist == artistName).first()
                if first is None:
                    print 'Not in db. Will insert.'
                    song = models.Song(name = songName, artist = artistName)
                    self.db.add(song)
                    self.db.commit()
                    songId = song.id
                else:
                    songid = first.id
                # songId = query.filter(models.Song.name == songName, models.Song.artist == artistName).first().id
                listenlog_times = models.Listenlog_times(stationId = log['stationId'], time= log['time'], fingerprint = log['code'], song = songid)
                self.db.add(listenlog_times)
                self.db.commit()
                #check_insert_listenonce
                if check_insert_listenonce(3, songid, listenlog_times.stationId):
                    listenlog_once = models.Listenlog_once(stationId = log['stationId'], time= log['time'], fingerprint = log['code'], song = songid)
                    self.db.add(listenlog_once)
                    self.db.commit()       
                #insert artist info to artist db
                query = self.db.query(models.Artist)
                first = query.filter(models.Artist.name == artistName).first()
                if first is None:
                    artist = models.Artist(name = artistName)
                    self.db.add(artist)
                    self.db.commit()
                    first = artist
                # first = query.filter(models.Artist.name == artistName).first()
                if first.photo is None:
                    ArtistPhotoScanner.getPhoto(self.db, artistName, first.id) 
                    self.db.commit()
            else:
                print "No match."
                listenlog_null = models.Listenlog_null(stationId = log['stationId'], time= log['time'], fingerprint = log['code'])
                self.db.add(listenlog_null)
                self.db.commit()
        except KeyError:
                    print 'response error'

    def check_insert_listenonce(self, limits, songid, stationId):
        results = self.db.query(models.Listenlog_times).filter(models.Listenlog_times.stationId = stationId).order_by(models.Listenlog_times.time.desc())[:limits]
        results = map(lambda x: x.song, results)
        if songid not in results:
            return True
        return False