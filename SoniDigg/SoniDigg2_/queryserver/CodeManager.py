#!/usr/bin/python
# -*- coding:utf-8 -*-

############
#负载均衡#
############

import threading
import multiprocessing
import time
import CodeHandler
from multiprocessing import reduction

class CodesProcess(threading.Thread):
    def __init__(self, queue, balance, logs):
        threading.Thread.__init__(self)
        self.queue = queue
        self.pool = multiprocessing.Pool(processes=50)
        self.local = balance[0]
        self.remote = balance[1]
        self.logs = logs

    def run(self):
        while 1:
            if not self.queue.empty():
                jsonStr = self.queue.get()
                print 'local:' + str(self.local) + ' remote:' + str(self.remote)
                # self.pool.apply_async(CodeHandler.handleCodes, args=(jsonStr, 1, self.logs))
                if (self.local > self.remote):
                    Logs(jsonStr, 2, self.logs).start()
                    self.remote += 1
                    print  'add remote~~'
                else:
                    Logs(jsonStr, 1, self.logs).start()
                    self.local += 1
                    print  'add local~~'


class Logs(threading.Thread):
    def __init__(self, jsonStr, flag,logs):
        super(Logs, self).__init__()
        self.jsonStr = jsonStr
        self.logs = logs
        self.flag = flag
    def run(self):
        CodeHandler.handleCodes(self.jsonStr, self.logs)


if __name__ == '__main__':
    pipe = multiprocessing.Pipe()
    codeManager = CodesProcess(multiprocessing.Queue())
    codeManager.start()

