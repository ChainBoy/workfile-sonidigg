from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import models
import threading
import time
import ArtistPhotoScanner
from multiprocessing import Queue, Pool
# Options
db_path = 'mysql://root:654321@localhost:3306/dataservice?charset=utf8'

class StationMap(object):
    def __init__(self, db_path):
        engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine))
        self.cache = self.creat_map()
    def _makeMap(self, stationId, limits):
        if stationId > 95:
            print "stationId", stationId
            return []
        else:
            query = self.db.query(models.Listenlog_times.song).filter(models.Listenlog_times.stationId == stationId).order_by(models.Listenlog_times.time.desc()).limit(limits)
            result = []
            count = 1
            for i in query:
                result.append(i.song)
                count += 1
                print stationId, count, i.song, "end!"
            return result

    def _makeIdList(self):
        query = self.db.query(models.Station.id)
        return map(lambda x: x[0], query)

    def creat_map(self):
        idlist = self._makeIdList()
        result = {}
        for i in idlist:
            result[i] = self._makeMap(i, 5)
        return result

    def insert_table(self, log):
        if log["table"] == 0:
            self.db.add(models.Listenlog_null(stationId = log['stationId'],time= log['time'], fingerPrint = log['code']))
        else:
            query = self.db.query(models.Song)
            first = query.filter(models.Song.name == log["song"], models.Song.artist == log["artist"]).first()
            if first is None:
                print 'Not in db. Will insert.'
                song = models.Song(name = log["song"], artist = log["artist"])
                self.db.add(song)
                self.db.commit()
                log["song"] = song.id
            else:
                log["song"] = first.id

            self.db.add(models.Listenlog_times(stationId = log['stationId'], time= log['time'], fingerPrint = log['code'], song = log['song']))
            query = self.db.query(models.Artist)
            first = query.filter(models.Artist.name == log["artist"]).first()
            if first is None:
                artist = models.Artist(name = log["artist"])
                self.db.add(artist)
                self.db.commit()
                first = artist
            if first.photo is None:
                ArtistPhotoScanner.getPhoto(self.db, log["artist"], first.id)
                self.db.commit()
            if self.check_insert_once(log):
                self.db.add(models.Listenlog_once(stationId = log['stationId'], time= log['time'], fingerPrint = log['code'], song = log['song']))
        self.db.commit()

    def check_insert_once(self, log):
        stationId = log["stationId"]
        songId = log["song"]
        if self.cache.has_key(stationId):
            for i in self.cache[stationId]:
                if i == songId:
                    return False
            else:
                self.cache[stationId].append(songId)
                if len(self.cache[stationId] == 6):
                    self.cache[stationId].pop(0)
                return True
        else:
            self.cache[stationId] = [songId]
            return True

class Resolver(threading.Thread):
    def __init__(self, db_path, balance, queue):
        super(Resolver, self).__init__()
        self.stationMap = StationMap(db_path)
        self.local = balance[0]
        self.remote = balance[1]
        self.queue = queue

    def run(self):
        while 1:
            log = self.queue.get()
            if log:
                print "run"*10
                self.stationMap.insert_table(log)
                if log["balance"] == 1:
                   self.local -= 1
                else:
                   self.remote -= 1
            else:
                pass

if __name__ == '__main__':
    pass
