#!/usr/bin/python
# -*- coding:utf-8 -*-
import simplejson as json
import time
import sys
sys.path.insert(0, "/home/pogo/servers/echoprint-server/API")
import fp
import multiprocessing

def handleCodes(jsonStr, flag, logs):
    listening_logs = json.loads(jsonStr)
    length = str(len(listening_logs))
    if flag != 1:
        print "*"*20 + " remote " + "*"*20
        import fp_remote as fp
    else:
        print "*"*20 + " locate " + "*"*20
    for index, item in enumerate(listening_logs):
        easy_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item['time'])/1000))
        print '*******' + str(index) + '/' + length + '**' + easy_time +' ********'
        log = {'time': easy_time, 'stationId': item['radioID'], 'code': item['code'], 'balance': flag}
        logs.put(lookup(log))
    
def lookup(log):
    code = log['code']
    try:
        t = time.time()
        match_result = fp.best_match_for_query(code)
        print "match cost time: ", time.time()-t
        if match_result.TRID:
            print "Match"
            song = match_result.metadata.get("track")
            artist = match_result.metadata.get("artist")
            print "ID: %s" % match_result.TRID
            print "Artist: %s" % artist
            print "Song: %s" % song
            log["song"] = song
            log["artist"] = artist
            log['table'] = 1
        else:
            print "No match"
            log["table"] = 0
    except KeyError:
        print "response error"
        log["table"] = 0
    return log
