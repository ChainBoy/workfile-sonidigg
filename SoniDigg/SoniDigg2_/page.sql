/* 分页测试--- start */
DELIMITER $$
create procedure sp_test(in pagename varchar(50))
begin
set @s=concat('select * from (select artist.*,@rownum := @rownum + 1 AS Newid from ',pagename,',(SELECT @rownum := 0) r where id<50 order by id) as newtable where Newid between 5*(3-1) and 3*5',';');
PREPARE stmt from @s;
EXECUTE stmt;
end$$
DELIMITER
/* 分页测试 --- end*/
call sp_test('artist')

/* |服务器在用|      分页存储过程 proc      -start- */
DELIMITER $$
create procedure sp_GetPage(in pagename varchar(10000), columnname varchar(500), pageindex int, pagesize int,checkwhere varchar(10000) charset 'utf8')
begin
# 分页存储过程
#pagename varchar-- 表明
#columnname varchar-- 排序依据列
#pageindex int,-- 当前页码数
#pagesize int,  -- 每页显示数量
#checkwhere varchar''-- 筛选条件,需要传递sql语句,函数中不存在where，需要外部传递.e.g:where id>50
#declare tbl longtext;
set @tbl =concat('select * from (select ntb.*,@rown := @rown + 1 AS Rank from ',pagename,'as ntb,(SELECT @rown := 0) r ',checkwhere,' order by ',columnname,') as newtable where Rank between ',pagesize,'*(',pageindex,'-1) and ',pageindex,'*',pagesize,';');
#select @tbl;
#select length(@tbl);
#set @s=tb1;
PREPARE stmt from @tbl;
EXECUTE stmt;
end
/* 分页存储过程  -end- */

/* |服务器在用|      获取排行榜，歌手歌曲歌手混合 proc      -start- */
DELIMITER $$
CREATE PROCEDURE sp_get_rank(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
    #已完善，含播放次数和播放时长的同上次的比差，等以后有需求再使用，或者加参数使用
    #cate 1 歌曲，cate 0 歌手
    declare time_space int;
    set time_space = DATEDIFF(time_end,time_start);
    if cate = 1 then
        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.song_id order by lo.play_num desc)as tb1 left join (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.song_id order by play_num)as tb2 on tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);
    elseif cate = 0 then
        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from (select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select so.name as artist,so.id as artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo,artist as so where lo.artist_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.artist_id order by lo.play_num desc)as tb1 left join (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.artist_id order by play_num)as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) as r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);
    end if;
    select @tql;
    PREPARE stmt from @tql;
    EXECUTE stmt;
end$$
DELIMITER
/* 取排行榜，歌手歌曲歌手混合  -end- */


/* |服务器在用|      统计数据概要，歌手数，歌曲数，播放总时长，播放总次数。*/
DELIMITER $$
create  procedure sp_preview()
begin
#数据概要，歌手数，歌曲数，播放总时长，播放总次数.
select * from (select count(song.id)as song_count,(select count(artist.id)as artist_count from artist) as artist_count from song)as tb_s_a_count,(select sum(tb.play_time) as play_time from (select TIME_TO_SEC(TiMEDIFF(max(time),min(time))) as play_time from listen_log_times group by once_id) as tb) as tb_play_time_count,(select count(*)as play_num from listen_log_once) as tb_play_count;
end$$
DELIMITER
/* 概要统计 --- end*/

/* |服务器在用|      统计数据概要，歌手数，歌曲数，播放总次数。*/
DELIMITER $$
create  procedure sp_preview()
begin
#数据概要，歌手数，歌曲数，播放总次数.
select * from (select count(song.id)as song_count,(select count(artist.id)as artist_count from artist) as artist_count from song)as tb_s_a_count,(select count(*)as play_num from listen_log_once) as tb_play_count;
end$$
DELIMITER
/* 概要统计 --- end*/
call sp_preview()

/* |服务器在用|     查询24小时内所有歌曲的播放次数(以小时为间隔)      -start- */
delimiter $$
create procedure sp_Get_day_all_song_play_count()
begin
    #查询24小时内所有歌曲的播放次数(以小时为间隔)
    SELECT hour(time) AS h,time,COUNT(*)as play_count
    FROM listen_log_once
    WHERE time between date_sub(now(),interval 24 hour) and now()
    GROUP BY h order by time;
end $$
delimiter
/* 查询24小时内所有歌曲的播放次数  -end- */

call sp_Get_day_all_song_play_count();



/* |服务器在用|     查询24小时内播放次数TOP100的歌曲，在24小时内所有歌曲的播放次数(以小时为间隔)      -start- */
delimiter $$
create procedure sp_Get_day_top_100_song_play_count()
begin
    #查询24小时内播放次数TOP100的歌曲，在24小时内所有歌曲的播放次数(以小时为间隔)
    SELECT hour(tb1.time) AS h,tb1.time,COUNT(tb1.id)as play_count
    FROM
        listen_log_once as tb1,
        (select song,count(id)as play_count from listen_log_once where time between date_sub(now(),interval 1 day) and now() group by song order by play_count desc limit 0,100) as tb2
    WHERE tb1.time between date_sub(now(),interval 24 hour) and now() and tb1.song=tb2.song
    GROUP BY h order by tb1.time;
end $$
delimiter
/* 询24小时内播放次数TOP100的歌曲，在24小时内所有歌曲的播放次数  -end- */

call sp_Get_day_top_100_song_play_count();


/* |服务器在用|     查询24小时内所有在监听的电台(以小时为间隔)      -start- */
delimiter $$
create procedure sp_Get_day_all_listening_station_count()
begin
    #查询24小时内所有在监听的电台(以小时为间隔)
    SELECT tb1.time,hour(tb1.time) AS h,count(DISTINCT tb1.stationId) as station_count
    FROM listen_log_once as tb1
    WHERE tb1.time between date_sub(now(),interval 24 hour) and now()
    GROUP BY h order by tb1.time;
end $$
delimiter
/* 查询24小时内所有在监听的电台(以小时为间隔)  -end- */

call sp_Get_day_all_listening_station_count();


select date_sub(date(now()),interval 1 day)

call sp_priview()




/* |可用|     获取歌曲排行榜单 start -*/
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_song_rank`(in num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
#已完善，含播放次数和播放时长的同上次的比差，等以后有需求再使用，或者加参数使用
declare time_space int;
set time_space = DATEDIFF(time_end,time_start);
set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_time-tb2.play_time as play_time_add,tb1.play_num-tb2.play_num as play_num_add from (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.song_id order by lo.play_num desc)as tb1 left join (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.song_id order by play_num)as tb2 on tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);
select @tql;
#PREPARE stmt from @tql;
#EXECUTE stmt;
end
/* 获取歌曲排行榜 -end-*/

call sp_GetPage('(select * from artist)','id',5,10,' ');

#set global group_concat_max_len=1024*1024*50;
#select @@global.group_concat_max_len;

call sp_GetPage(concat('(select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between ','\'2013-11-11\'',' and ','\'2013-11-13\'',' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)'),'play_num',1,10,' ');



select concat('(select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between ','\'2013-11-11\'',' and ','\'2013-11-13\'',' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)')
(select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between '2013-11-11' and '2013-11-13' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)


select tb1.*,@rownum := @rownum + 1 AS rank from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num 
from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist 
from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between 
'2013-11-11' 
and 
'2013-11-13' 
group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r




call sp_GetPage(concat('(select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between ','\'2013-11-11\'',' and ','\'2013-11-13\'',' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)'),'play_num',
1,10,' where song_id=1 ');

#### 获取歌曲排行榜 指定排行开始和结束
select t.*,@rownum := @rownum + 1 AS rank
from (
    select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_time-tb2.play_time as play_time_add,tb1.play_num-tb2.play_num as play_num_add
    from 
    (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num
        from song_play_log as lo, 
            (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so 
            where lo.song_id = so.id and lo.time between '2013-11-11' and '2013-11-18' 
            group by lo.song_id order by lo.play_num desc
    )as tb1 left join 
    (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time 
        from song_play_log as lo2 where lo2.time between date_sub('2013-11-01',interval 7 day) and date_sub('2013-11-20',interval 7 day) 
        group by lo2.song_id order by play_num
    )as tb2 on tb1.song_id=tb2.song_id order by tb1.play_num desc
) as t,(SELECT @rownum := 0) r;

#### 获取歌手排行榜 指定排行开始和结束
select * from 
(
    select t.*,@rownum := @rownum + 1 AS rank
    from (
        select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add
        from 
        (select so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num
            from song_play_log as lo, 
                (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so 
                where lo.artist_id = so.artist_id and lo.time between '2013-11-11' and '2013-11-18' 
                group by lo.artist_id order by lo.play_num desc
        )as tb1 left join 
        (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time 
            from song_play_log as lo2 where lo2.time between date_sub('2013-11-01',interval 7 day) and date_sub('2013-11-20',interval 7 day) 
            group by lo2.artist_id order by play_num
        )as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc
    ) as t,(SELECT @rownum := 0) as r
) as re_tb where re_tb.rank between 20 and 50




########排行榜proc,不完善,
DELIMITER $$
create procedure sp_song_rank(in time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
declare time_space int;
set time_space = DATEDIFF(time_end,time_start);
set @tql=concat('select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_time-tb2.play_time as play_time_add,tb1.play_num-tb2.play_num as play_num_add from  (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ''',time_start,''' and ''',time_end,''' group by lo.song_id order by lo.play_num desc)as tb1,(select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(''',time_start,''',interval ',time_space,' day) and date_sub(''',time_end,''',interval ',time_space,' day) group by lo2.song_id order by play_num )as tb2 where tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r;');
select @tql;
#PREPARE stmt from @tbl;
#EXECUTE stmt;
end$$
DELIMITER

#################旧的备份,不完善
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_song_rank`(in time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
declare time_space int;
set time_space = DATEDIFF(time_end,time_start);
set @tql=concat('
select t.*,@rownum := @rownum + 1 AS rank
from (
    select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_time-tb2.play_time as play_time_add,tb1.play_num-tb2.play_num as play_num_add
    from 
    (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num
        from song_play_log as lo, 
            (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so 
            where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' 
            group by lo.song_id order by lo.play_num desc
    )as tb1,
    (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time 
        from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) 
        group by lo2.song_id order by play_num
    )as tb2
    where tb1.song_id = tb2.song_id order by tb1.play_num desc
) as t,(SELECT @rownum := 0) r;
');
select @tql;
PREPARE stmt from @tbl;
EXECUTE stmt;
end$$
DELIMITER




DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_song_rank`(in num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin
#已完善，含播放次数和播放时长的同上次的比差，等以后有需求再使用，或者加参数使用
declare time_space int;
set time_space = DATEDIFF(time_end,time_start);
set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_time-tb2.play_time as play_time_add,tb1.play_num-tb2.play_num as play_num_add from (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.song_id order by lo.play_num desc)as tb1 left join (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.song_id order by play_num)as tb2 on tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);
select @tql;
#PREPARE stmt from @tql;
#EXECUTE stmt;
end$$
DELIMITER
