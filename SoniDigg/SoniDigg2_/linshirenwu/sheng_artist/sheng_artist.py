import MySQLdb as Mysqldb
import json

s_id = [1,10,11,13,16,25,33,53,59,92,160,398,415,597,662,711,865,884,960,962,978,990,1012,1028,1044,1075,1097,1133,1173,1268,1346,1362]

for i in s_id:
    conn=Mysqldb.connect(host='localhost',user='root',passwd='654321',port=3306,db='dataservice',charset='utf8')
    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    sql = "select station.name as station,t.* from station,(select o.stationId,s.artist, count(*) as count from song s inner join listen_log_once o on s.id=o.song where o.time between '2010-02-01' and '2014-02-31' and o.stationId =%s group by o.stationId,s.artist order by count desc limit 110)as t where station.id = t.stationId;" % i
    ex = cur.execute(sql)
    info = list(cur.fetchall())
    cur.close()
    with file(str(i),'w')as f:
        f.write('station\tartist\tcount\n')
        if info:
            for j in info:
                a = j.get('station')
                if a : a = a.encode('utf8')
                c = j.get('artist')
                if c : c = c.encode('utf8')
                d = j.get('count')
                if not d : d = 0
                f.write('%s\t%s\t%s\n' % (a,c,d) )

