

###############   注意：因测试需要，SQL语句中计算计算时间需要手动改回 昨天-1,前天-2    ###############
#'%Y-%m-%d %H:%m:%s'
select * from view_song_play_num;
/*统计每首歌昨天的在所有电台的播放次数*/
create view view_song_play_num 
as select tb.song,count(tb.song)as play_num,(date_format(tb.time,'%Y-%m-%d')) as t from listen_log_once as tb 
       where (date_format(tb.time,'%Y-%m-%d'))=
         (date_sub(date_format(now(),'%Y-%m-%d'),interval 2 day)) 
       group by tb.song order by play_num DESC;

/*获取昨天歌曲热度以及播放次数*/
create view view_yesDay_play
as select song.id as song_id,song.artist_id,pn.play_num,pn.t as time from view_song_play_num as pn,song where pn.song=song.id;

/*获取前天歌曲热度以及播放次数*/
create view view_lastDay_all
as select lg.song_id,lg.artist_id,lg.play_num,date_format(lg.time,'%Y-%m-%d')as time,lg.play_time
    from song_play_log as lg,artist as ar where (date_format(lg.time,'%Y-%m-%d'))=
         (date_sub(date_format(now(),'%Y-%m-%d'),interval 3 day)) and ar.id=lg.artist_id;

/*统计歌曲在昨天的播放总时长,mysql 不支持视图自查询,所以建立以下两张视图*/
create view view_yesDay_play_time 
as select sum(num) as play_time,song from 
(select TIME_TO_SEC(timediff(max(time),min(time)))as num,song from listen_log_times 
where (date_format(time,'%Y-%m-%d'))=(date_sub(date_format(now(),'%Y-%m-%d'),interval 2 day)) 
group by once_id)as tb1 group by song;

/* 统计歌曲所有播放次数的时长*/
create view view_yesDay_play_time
as select TIME_TO_SEC(timediff(max(time),min(time)))as num,song from listen_log_times 
where (date_format(time,'%Y-%m-%d'))=(date_sub(date_format(now(),'%Y-%m-%d'),interval 2 day)) 
group by once_id;

/* 统计歌曲所天所有次数的所有时长*/
create view view_yesDay_play_time_all
as select sum(num) as play_time,song from view_yesDay_play_time as tb1 group by song;

#昨天所有详情
create view view_yesDay_all
as select yes.*,yl.play_time from view_yesDay_play as yes,view_yesDay_play_time_all as yl 
where yes.song_id=yl.song;

/* 通过昨天播放次数比较前天的播放次数获取热度*/
create view add_yesDay
as
SELECT b.song_id,b.artist_id,b.play_num,b.play_time,b.time,
    CASE 
        WHEN b.play_num>a.play_num THEN 0 
        WHEN b.play_num<a.play_num THEN 1
        ELSE 2 
    END as hot
from view_yesDay_all as b left join view_lastDay_all as a on a.song_id=b.song_id;

/* 脚本,用于每天0点之后更新*/
INSERT INTO song_play_log(song_id,artist_id,play_num,play_time,time,hot) SELECT * FROM add_yesDay;

#统计所有歌手数和歌曲数目
select count(song.id)as song_count,(select count(artist.id)as artist_count from artist) as artist_count from song;
#统计所有歌曲播放时长
select sum(tb.play_time) as play_time from (select TIME_TO_SEC(TiMEDIFF(max(time),min(time))) as play_time from listen_log_times group by once_id) as tb;
#统计所有歌曲播放次数
select count(*)as play_num from listen_log_once;

#查询24小时内所有歌曲的播放次数(以小时为间隔)
SELECT hour(time) AS h,time,COUNT(*)as play_count
FROM listen_log_once
WHERE time between date_sub(now(),interval 1 day) and now()
GROUP BY h order by time;

#查询24小时内播放次数TOP100的歌曲，在24小时内所有歌曲的播放次数(以小时为间隔)
SELECT hour(tb1.time) AS h,tb1.time,COUNT(tb1.id)as play_count
FROM
    listen_log_once as tb1,
    (select song,count(id)as play_count from listen_log_once where time between date_sub(now(),interval 1 day) and now() group by song order by play_count desc limit 0,100) as tb2
WHERE tb1.time between date_sub(now(),interval 1 day) and now() and tb1.song=tb2.song
GROUP BY h order by tb1.time;

#查询24小时内所有在监听的电台(以小时为间隔)
SELECT tb1.time,hour(tb1.time) AS h,count(DISTINCT tb1.stationId) as station_count
FROM listen_log_once as tb1
WHERE tb1.time between date_sub(now(),interval 21 day) and now()
GROUP BY h order by tb1.time;

#create station_code.
insert into station_code(id, name,url) select id,name,address from station;
#update station.
replace into station(id,name,address) select id, name,url from station_code;

#update station_code
truncate table station_code;







##################################################################################################
/*将artist_id 对应为artist 中name .. ()中为多余内容 time：20.24 sec)*/
update song,(select a.id,a.name from artist as a,song as b where b.artist=a.name) as ar 
set song.artist_id = ar.id 
where song.artist=ar.name;
/*将artist_id 对应为artist 中name .. 最优time：(7.17 sec*/
update song,artist as ar 
set song.artist_id = ar.id 
where song.artist=ar.name;

/* 给times once_id 建立once对应的id*/
update listen_log_times as lst,listen_log_once as lso
set lst.once_id=lso.id
where lst.stationId=lso.stationId and lst.song=lso.song;
#################################################################################################
select * from add_yesDay;
delete from song_play_log where id>80;
select * from song_play_log;
INSERT INTO tb_one(song,num) SELECT song,num FROM tb_two;
select * from tb_one;
select * from listen_log_times where song=9; #100
select hour('2013-11-12 20:26:26'); #获取小时
select TIME_TO_SEC('00:01:00');#将时间转为秒
select sec_to_time(360555555); # 将秒转为time
select TiMEDIFF('2013-11-13 10:48:20','2013-11-13 10:47:21'); #求时间差
#'100', '9', '2013-11-13 16:27:51'
select * from view_yesDay_play;
select * from view_yesDay_play_time_all;
#select date_format('2013-11-11 18:35:21','%Y-%m-%d');
#select date_sub(date_format('2013-11-12','%Y-%m-%d'),interval 1 day);
#select song,count(tb.song)as play_count from listen_log_once as tb group by tb.song order by play_count DESC
#select * from artist where id <100
insert song_play_log(song_id,artist_id,play_num,play_time,hot) values(4,5,10,100,2);
#select count(*) from song_play_log;
#alter VIEW view_show_song_play
#as select lt.id,lt.time,lt.stationId,lt.song from listen_log_times as lt,listen_log_once as lo where lt.song=lo.song and lt.stationId=lo.stationId group by lt.song;
