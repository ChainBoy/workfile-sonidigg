#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/python
# -*- coding:utf-8 -*-

# Python imports
import os
import simplejson as json
import time
import datetime
import decimal
import requests

# Tornado imports
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.web import url

# Sqlalchemy imports
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import and_

# App imports
import models
import info
import JsonHandle
import GetArtistInfo
import GetRankInfo
import GetPreview
import GetPolyline
import GetTopList
import GetTopListInfo
import GetWhereInfo
import GetHistoryBysong
import GetNewInfo
import GetInfoByPad
import GetInfoByStation
import GetGeDanInfo
import GetGeDanNum
import GetFileByStation

# Options
define("port", default=8003, help="run on the given port", type=int)
define("debug", default=False, type=bool)
#define("db_path", default='mysql://root:123@localhost:3306/dataservice?charset=utf8', type=str)

define("db_path", default='mysql://root:654321@http://sonidigg.f3322.org:3306/dataservice?charset=utf8', type=str)

#dict --> ditc_a = {"num":2.35} ---> {"num":2}
json.encoder.FLOAT_REPR = lambda o: format(o, '.0f') 



class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", '*')
    def pool(self):
        return self.application.pool

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/', ShowHandler),
            url(r'/API/$', SongInfo),
            url(r'/check_station/$', CheckStation),
            url(r'/check_pad/$', CheckPad),
            url(r'/check_sta/$', CheckSta),
            url(r'/gedan/?$', GetGedan),
            url(r'/down_ts/?$', DownTs),
        ]
        settings = dict(
            debug=options.debug,
            xsrf_cookies=False,
            cookie_secret="nzjxcjasaddsduuqwheazmu293nsadhaslzkci9023nsadnua9sdads/Vo=",
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class CheckPad(BaseHandler):
    def get(self):
        pad_id = self.get_argument('id',default = 0)
        start_time = self.get_argument('s',default = '')
        end_time = self.get_argument('e',default = '')
        if start_time and end_time and pad_id:
            count,info = GetInfoByPad.get_info_by_pad_id(pad_id, start_time, end_time)
            try:
                count = int(count)
                self.write('<center><h3>All count:%s</h3><h3></h3></center> %s<br><br>' % (count, info))
            except Exception,e:
                self.write('Wrong!<br>%s<br>%s' % (e, info))
        else:
            self.write('please input padid or start time or end time!')
        self.flush()
        self.finish()

class GetGedan(BaseHandler):
    def get(self):
        filename = self.get_argument('n',default ='')
        if filename:
            filename = self.get_argument('n',default ='')
            if os.path.isfile(filename):
                self.set_header('Content-Type', 'application/x-xls')
                self.set_header('Content-Disposition', 'attachment; filename=%s' % filename)
                with file(filename, 'rb')as f:
                    self.write(f.read())
        else:
            self.write('''
<html class=""><head><title>上传歌单</title><link type="text/css" rel="stylesheet" charset="UTF-8" href="https://translate.googleapis.com/translate_static/css/translateelement.css"><style type="text/css"></style></head>
                  <body><div style="
    margin-left: 40;
    margin-top: 30;
    /* margin: auto; */
    margin-bottom: initial;
    width: 400;
    /* border-bottom-style: groove; */
    border: ridge;
">
                    文件格式示例:<br><hr>
                    王麟 当爱情离开的时候<br>
                    许嵩 城府<br>
                    许嵩 千百度<br>
                    舒淇 一生所爱<br>
                    卢冠庭 一生所爱<br>
                    梁静茹 明明很爱你<br>
                    梁静茹 可惜不是你<br>
                    梁静茹 会呼吸的痛<br>
                    刘欢 在路上<br>
                    ...<br><hr> 注意:请用[TAB]将歌手和歌曲分割<br>
                    <form action="gedan" enctype="multipart/form-data" method="post" style="
    margin-bottom: 0px;
">
                    <input type="file" name="file"><br>
                    <input type="submit" value="开始上传">
                    </form><div id="goog-gt-tt" class="skiptranslate" dir="ltr"><div style="padding: 8px;"><div><div class="logo"><img src="https://www.google.com/images/icons/product/translate-32.png" width="20" height="20"></div></div></div><div class="top" style="padding: 8px; float: left; width: 100%;"><h1 class="title gray">原文</h1></div><div class="middle" style="padding: 8px;"><div class="original-text"></div></div><div class="bottom" style="padding: 8px;"><div class="activity-links"><span class="activity-link">提供更好的翻译建议</span><span class="activity-link"></span></div><div class="started-activity-container"><hr style="color: #CCC; background-color: #CCC; height: 1px; border: none;"><div class="activity-root"></div></div></div><div class="status-message" style="display: none;"></div></div>
                  
                
  </div>
  
 
<div style="
    margin-bottom: -;
    position: inherit;
    bottom: inherit;
    margin-top: 300px;
">
     <p style="margin: 0px; padding: 0px; text-align: center; color: rgb(64, 62, 57); font-family: 'Helvetica Neue', 微软雅黑, Tohoma; font-size: 12px; line-height: 21.600000381469727px;">Copyright © 2005 - 2014 Sonidigg.<a href="http://www.tencent.com/en-us/le/copyrightstatement.shtml" target="_blank" style="color: rgb(64, 62, 57); text-decoration: none;">All Rights Reserved.</a></p>
  <p style="margin: 0px; padding: 0px; text-align: center; color: rgb(64, 62, 57); font-family: 'Helvetica Neue', 微软雅黑, Tohoma; font-size: 12px; line-height: 21.600000381469727px;">数音堂公司&nbsp;<a href="http://www.tencent.com/zh-cn/le/copyrightstatement.shtml" target="_blank" style="color: rgb(64, 62, 57); text-decoration: none;">版权所有</a>(By:<a href="http://t.qq.com/QMusicVip" target="_blank">张志鹏yi</a>)</p>
  </div>
</body></html>
            ''')

    def post(self):
        upload_path=os.path.join(os.path.dirname(__file__),'shazam-source')  #文件的暂存路径
        file_metas=self.request.files['file']    #提取表单中‘name’为‘file’的文件元数据
        for meta in file_metas:
            filename=meta['filename']
            filepath=os.path.join(upload_path,filename)
            with file(filepath,'wb') as up:      #有些文件需要已二进制的形式存储，实际中可以更改
                up.write(meta['body'])
            self.write('upload ok!<br>')

            gedan = GetGeDanInfo.MainClass()
            num, rfilename_info = gedan.start(1, filepath)
            if not num:
                self.write('没有播放记录,请重新上传文件.')
                return
            self.write('You upload file <b>%s</b>,All play info count: <b>%s</b>.<br>will Generate results of All play info, please wating...' % (filename, num))
            self.flush()
            gedan = GetGeDanNum.MainClass()
            num, rfilename_num = gedan.start(1, filepath)
            self.write('<br>will Generate results of All song/artist play num, please wating...')
            self.flush()
            self.write('<br>Download Play Record at all stations.<b><a href="?n=%s">Download All Record</a><a></a></b><a>.' % rfilename_info)
            self.write('<br>Download Play Number of times at all stations.<b></b></a><b><a href="?n=%s">Download All Number of times</a><a></a></b><a>' % rfilename_num)
            return
            
class DownTs(BaseHandler):
    def get(self):
        station = self.get_argument('name',default = '')
        start_time = self.get_argument('s',default = '')
        end_time = self.get_argument('e',default = '')
        self.write(station)
        self.flush()
        if start_time and end_time:
            print 's:',start_time,'e:',end_time
            if station:
                info = GetFileByStation.get_file_by_station(start_time, end_time, station)
                self.write('<br>%s<br>' % (info))
            else:
                self.write('Please input station name')
        else:
            print 's:',start_time,'e:',end_time
            self.write('please input start time or end time!')
        return

    def post(self):
        filename = self.get_argument('file',default = '')
        ip = self.get_argument('ip',default = '')
        if filename and ip:
            self.set_header('Content-Type', 'audio/mp3')
            self.set_header('Content-Disposition', 'attachment; filename=%s' % filename[-28:])
            #TODO:访问下载服务端
            url =  'http://soniegg.oicp.net:8003' + self.request.uri
            print url
            result = downloadts(url)
            print url
            self.write(result)
            return

def downloadts(url):
    r = requests.get(url).content
    print 'ok'
    return r



class CheckSta(BaseHandler):
    def get(self):
        sta_id = self.get_argument('id',default = 0)
        station = self.get_argument('name',default = 0)
        start_time = self.get_argument('s',default = '')
        end_time = self.get_argument('e',default = '')
        if not sta_id and not station:
            self.write('please input station id  or station name or start time or end time!')
        if start_time and end_time:
            print 's:',start_time,'e:',end_time
            if sta_id:
                count,info = GetInfoByStation.get_info_by_station(start_time, end_time,station_id = sta_id)
            if station:
                count,info = GetInfoByStation.get_info_by_station(start_time, end_time,station = station)
            try:
                count = int(count)
                self.write('<center><h3>All count:%s</h3><h3></h3></center> %s<br><br>' % (count,info))
            except Exception,e:
                self.write('Wrong!<br>%s<br>%s' % (e, info))
        else:
            print 's:',start_time,'e:',end_time
            self.write('please input start time or end time!')
        self.flush()
        self.finish()






class CheckStation(BaseHandler):
    def get(self):
        start_time = self.get_argument('s',default = '')
        end_time = self.get_argument('e',default = '')
        # jsonStr = str(self.request.body)
        # jsonContent = json.loads(jsonStr)
        # start_time = jsonContent.get('start_time','')
        # end_time = jsonContent.get('end_time','')
        if start_time and end_time:
            self.Check_station(start_time, end_time)
        else:
            self.write('Wrong')
        self.flush()
        self.finish()

    def post(self):
        pass

    def Check_station(self,start_time,end_time):
        result  = 'result.txt'
        PWD = '654321'
        print start_time,end_time
        comm = "use shazam;call ft('%s','%s');" % (start_time, end_time)
        #comm = """mysql -u root -p -e "use shazam;call ft('2014-02-08 00:00:00','2014-03-10 00:53:30');" > result.txt"""
        os.system("mysql -u root -p" + PWD + """ -e "%s" > %s""" % (comm, result))

        try:
            with file(result,'r')as f:
                res = f.read()
        except Exception:
            res = 'Server Error'
        self.write(res)


class SongInfo(BaseHandler):
    def get(self):
        pass
    def post(self):
        jsonStr = str(self.request.body)
        jsonContent = json.loads(jsonStr)
        #jsonContent = {}
        result_json={}
        post_id = jsonContent.get("id")
        try:
            post_id = int(post_id) if int(post_id) >=0 else -1
        except ValueError:
            result_json["code"] = 404
        result_json["post_id"] = post_id
        if post_id != None and post_id >=0:
            result_class=None
            #Get artist,song data
            if post_id == 0 or post_id=='0':
                result_class = GetArtistInfo.getArtistInfo(jsonContent)
                result_json = result_class.CheckJson()   
            #Get Rank             
            elif post_id == 1 or post_id == '1':
                result_class = GetRankInfo.getRankInfo(jsonContent)
                result_json = result_class.CheckJson()
            #Get Preview Info.--->song_count,artist_count,play_num,play_time
            elif post_id == 2 or post_id == '2':
                result_class = GetPreview.getPreviewInfo()
                result_json = result_class.CheckJson()
            elif post_id == 3 or post_id == '3':
                result_class = GetPolyline.getPolylineInfo()
                result_json = result_class.CheckJson()
            elif post_id == 4 or post_id == '4':
                result_class = GetTopList.getTopInfo(jsonContent)
                result_json = result_class.CheckJson()
            elif post_id == 5 or post_id == '5':
                result_class = GetTopListInfo.getTopInfo(jsonContent)
                result_json = result_class.CheckJson()
            elif post_id == 6 or post_id == '6':
                result_class = GetWhereInfo.getWhereInfo(jsonContent)
                result_json = result_class.CheckJson()
            elif post_id == 7 or post_id == '7':
                result_class = GetHistoryBysong.getHistoryBysong(jsonContent)
                result_json = result_class.CheckJson()                
            elif post_id == 8 or post_id == '8':
                result_class = GetNewInfo.getNewInfo(jsonContent)
                result_json = result_class.CheckJson()                
        else:
            result_json["code"] = 404 #request state is error.
        if not result_json.get("code"):
            result_json["code"] = 404
        res = json.dumps(result_json, sort_keys=True, indent=4, skipkeys=True, separators=(',',':'), cls=DecimalEncoder)
        
        # Test :::  save to file.
        # f = open('result.txt','w')
        # f.write(res)
        # f.close()
        self.write(res)
        self.flush()
        self.finish()
  
class ShowHandler(BaseHandler):
    def get(self):
        self.write('OK.')
    def post(self):
        print 'OK'
        self.write(JsonHandle.handle(self.handle()))
        self.flush()
        self.finish()

    def handle(self):
        engine = create_engine(options.db_path, convert_unicode=True, echo=options.debug, pool_size = 100, pool_recycle=7200)
        engine.connect()
        db = scoped_session(sessionmaker(bind=engine))
        weekTime = datetime.datetime.fromtimestamp(time.time()-7*24*3600).strftime("%Y-%m-%d")
        #all data from table_ListenLog,while time > 7day time,and song_id != NULL 
        result = db.query(models.ListenLog).filter(and_(models.ListenLog.time > weekTime, models.ListenLog.song != 'NULL')).all()
        resultJson = []#
        songs = {}
        for log in result:
            if songs.has_key(log.song):
                isIn = False
                songInfos = songs[log.song]
                for songinfo in songInfos:
                    if songinfo.radioId == log.stationId:
                        if log.time < songinfo.time + datetime.timedelta(minutes=10) and log.time > songinfo.time - datetime.timedelta(minutes=10):
                            isIn = True
                            break
                if isIn == False:
                        songInfos.append(info.info(log.stationId, log.time))
                        songresult = db.query(models.Song).filter(models.Song.id == log.song).all()
                        resultJson.append( {
                        "time" : str(log.time),
                        "title": songresult[0].name,
                        "artist": songresult[0].artist
                        })
            else: 
                songs[log.song] = [info.info(log.stationId, log.time)]
                songresult = db.query(models.Song).filter(models.Song.id == log.song).all()
                resultJson.append( {
                "time" : str(log.time),
                "title": songresult[0].name,
                "artist": songresult[0].artist
                })
        print resultJson
        return json.dumps(resultJson)

#class DecimalEncoder(json.JSONEncoder):
#    def default(self, o):
#        if isinstance(o, decimal.Decimal):
#            return float(o)
#        if isinstance(o, datetime.datetime):
#            return str(o)
#        if isinstance(o,float):
#            return int(o)
#        super(DecimalEncoder, self).default(o)
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        if isinstance(o,float):
            return int(o)
        super(DecimalEncoder, self).default(o)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    print 'start'
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
