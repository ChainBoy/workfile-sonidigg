#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2

class UrlChecker(object):
    def __init__(self, url, check_observer):
        self.url = url
        self.observer = check_observer
    
    def check_url_type(self, content):
        info = content.split('\n')
        for line in info:
            if line.startswith('#EXT-X-STREAM-INF'):
                return True, info
        return False, info

    def handle_bandwidth(self, info):
        is_next_usefull = False
        for line in info:
            if line.startswith('#EXT-X-STREAM-INF'):
                is_next_usefull = True
            elif is_next_usefull:
                return line

    def handle_ts_url(self, url):
        is_next_usefull = False
        request = urllib2.Request(url)
        try:
            response = urllib2.urlopen(request, timeout=3)
        except Exception as e:
            print e
            return False, ""

        if response.code == 200:
            content = response.read()
            info = content.split('\n')
            return True, self.get_ts_url(url, info)
        else:
            return False, ""

    def get_ts_url(self, base_url, info):
        ts_url = ""
        is_next_usefull = False
        for line in info:
            if line.startswith('#EXTINF'):
                is_next_usefull = True
            elif is_next_usefull:
                if line.startswith('http://'): 
                    ts_url = line
                    break
                else:
                    url_info = base_url.split('/')
                    for i in xrange(0, len(url_info)):
                        if (i < len(url_info) - 1):
                            ts_url = ts_url + url_info[i]
                            ts_url = ts_url + '/'
                    ts_url = ts_url + line
                    break
        return ts_url

    def check_server_status(self):
        request = urllib2.Request(self.url)
        try:
            response = urllib2.urlopen(request, timeout=3)
        except Exception as e:
            print e
            return False, None
        if response.code == 200:
            content = response.read()
            return True, content
        else:
            return False, None

    def check_url(self):
        status, content = self.check_server_status()
        self.observer.on_server_check_end(status)
        server_status = status
        protocol_status = False
        if status:
            isBandWidthResponse, info = self.check_url_type(content)
            ts_url = ""
            if isBandWidthResponse:
                base_url = self.handle_bandwidth(info)
                is_get_ts_suc, ts_url = self.handle_ts_url(base_url)
                self.observer.on_protocol_check_end(is_get_ts_suc)
                protocol_status = is_get_ts_suc
                print 'ts_url:' + ts_url
            else:
                ts_url = self.get_ts_url(self.url, info)
                if ts_url == "":
                    self.observer.on_protocol_check_end(False)
                    protocol_status = False
                else:
                    self.observer.on_protocol_check_end(True)
                    protocol_status = True
                print 'ts_url:' + ts_url
        return server_status,protocol_status


class CheckObserver(object):
    def on_server_check_end(self, status):
        print 'server status is:' + str(status)

    def on_protocol_check_end(self, status):
        print 'protocol status is:' + str(status)

if __name__ == '__main__':
    #url = 'http://42.96.141.199/live/332.m3u8?bitrate=24'
    url = 'http://live.gslb.letv.com/gslb?stream_id=cctv1&tag=live&ext=m3u8&sign=live_tv'
    uc = UrlChecker(url, CheckObserver())
    print uc.check_url()

