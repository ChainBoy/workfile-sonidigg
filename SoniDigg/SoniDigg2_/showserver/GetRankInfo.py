#!/usr/bin/python
#-*- coding: utf-8 -*-
import datetime
import time
import MySQLdb as Mysqldb
import sys
import json
import decimal


#get all data count by check.no page
def getSongRank(list_state, time_start, time_end, num_start, num_end):
  info={}
  result_count = 0
  code = 500
  try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='654321',
      port=3306,
      db='dataservice',
      charset='utf8'
    )
    #conn.set_character_set('utf8')
    cur = conn.cursor(Mysqldb.cursors.DictCursor)
    sql_ = """
    call sp_get_rank(%s,%s,%s,'%s','%s')
    """ % (list_state,num_start, num_end, time_start, time_end)
    ex = cur.execute(sql_)
    info = list(cur.fetchall()) #get sql by state,num_start,num_end,time_start,time_end.
    cur.close()

    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    result_count = cur.execute(info[0]["@tql"])
    info = cur.fetchall()
    cur.close()
    conn.close()
    code = 200
  except Mysqldb.Error,e:
    print e
    code = 501
  finally:
    return info, result_count, code

class getRankInfo():
  def __init__(self,jsonContent):
    self.json_content=jsonContent
    self.result_json={}
  def CheckJson(self):
    """
    check the request json data.
    """
    result_json=self.result_json
    #type(self.json_content)-->  dict
    #print 'test:json_content:',self.json_content,'\n'
    time_start=self.json_content.get("time_start")#data start time.
    time_end=self.json_content.get("time_end")#data end time.
    list_state=self.json_content.get("list_state",1)#artist(0) or song(1) rank .defult is song rank(1).
    num_start=self.json_content.get("num_start",0) #defult rank start num is 0
    num_end=self.json_content.get("num_end",20)#defult rank end num is 20

    #check rank artist or song .
    try:
      list_state = list_state if int(list_state) >= 0 else 0
    except Exception:
      list_state = 1
    result_json["list_state"] = list_state

    #check num_start and num_end.
    try:
      num_start = int(num_start)
      num_end = int(num_end)
      if num_start > num_end:
        num_start, num_end = num_end, num_start
      elif num_start == num_end:
        num_start, num_end = 1,100
    except Exception:
      num_start,num_end = 1,100
    #check time_start and time_end.
    try:
      time_start = time.mktime(datetime.datetime.strptime(time_start, "%Y-%m-%d").timetuple())
      time_end = time.mktime(datetime.datetime.strptime(time_end, "%Y-%m-%d").timetuple())
    except Exception:
      time_start,time_end = time.time()-7*24*3600,time.time()
    try:
      time_start = datetime.datetime.fromtimestamp(float(time_start)).strftime("%Y-%m-%d")
      try:
        time_end = datetime.datetime.fromtimestamp(float(time_end)).strftime("%Y-%m-%d")
      except Exception:
        time_end = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d")
      time_min = datetime.datetime.fromtimestamp(time.time()-5*365*24*3600).strftime("%Y-%m-%d")
      if time_start > time_end:
        time_start,time_end=time_end,time_start
      elif time_start < time_min:
        time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    except Exception:
      time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    #Get data:song rank(0); artist rank(1)
    if list_state >= 0 :
      result_json["rank_info_%s" % list_state],result_json["result_count"],result_json["code"]=getSongRank(list_state, time_start,time_end, num_start, num_end)
    else:
      result_json["code"]=404

    return result_json

if __name__ == '__main__':
  time_start = '2013-11-11'
  time_end = '2013-11-18'
  getSongRank(time_start,time_end)
