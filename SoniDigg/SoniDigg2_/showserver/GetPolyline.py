#!/usr/bin/python
#-*- coding: utf-8 -*-
import datetime
import time
import MySQLdb as Mysqldb
import sys
import json

def song_timeline():
  """
  call sp_Get_day_all_song_play_count();
  """
  try:
    info = {}
    code = 500
    conn=Mysqldb.connect(host='localhost', user='root', passwd='654321',
                        port=3306, db='dataservice', charset='utf8')
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    ex = cur.execute("call sp_Get_day_all_song_play_count();")
    info=cur.fetchall()
    if info:
      info = list(info)
    else :
      info = {}
    code=200
    cur.close()
    conn.close()
  except Exception,e:
    code=500
  finally:
    return info, code
def song_top100_timeline():
  """
  call sp_Get_day_top_100_song_play_count();
  """
  try:
    info = {}
    code = 500
    conn=Mysqldb.connect(host='localhost', user='root', passwd='654321',
                        port=3306, db='dataservice', charset='utf8')
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    ex = cur.execute("call sp_Get_day_top_100_song_play_count();")
    info=cur.fetchall()
    if info:
      info = list(info)
    else :
      info = {}
    code=200
    cur.close()
    conn.close()
  except Exception,e:
    code=500
  finally:
    return info, code
def station_timeline():
  """
  call sp_Get_day_all_listening_station_count();
  """
  try:
    info = {}
    code = 500
    conn=Mysqldb.connect(host='localhost', user='root', passwd='654321',
                        port=3306, db='dataservice', charset='utf8')
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    # 
    ex = cur.execute("call sp_Get_day_all_listening_station_count();")
    info=cur.fetchall()
    if info:
      info = list(info)
    else :
      info = {}
    print info
    print type(info)
    code=200
    cur.close()
    conn.close()
  except Exception,e:
    code=500
    print e
  finally:
    return info, code


class getPolylineInfo():
  def __init__(self):
    self.result_json={}
  def CheckJson(self):
    """
    check the request json data.
    """
    result_json=self.result_json
    result_json["song_timeline"],result_json["code"]=song_timeline()
    result_json["song_top100_timeline"],result_json["code"]=song_top100_timeline()
    result_json["station_timeline"],result_json["code"]=station_timeline()
    return result_json

#if __name__=='__main__':
#  time_start='2013-11-11'
#  time_end='2013-11-13'
#  sql_where=''
#  getAllNum(time_start,time_end,sql_where)
