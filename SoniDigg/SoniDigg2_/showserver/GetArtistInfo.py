#!/usr/bin/python
#-*- coding: utf-8 -*-
import datetime
import time
import MySQLdb as Mysqldb
import sys
import json


#get all data count by check.no page
def getAllData(pageindex,num_per_page,time_start,time_end,sql_where):
  try:
    info = []
    result_count=0
    code = 500
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='654321',
      port=3306,
      db='dataservice',
      charset='utf8'
    )
    #conn.set_character_set('utf8')
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    sql_page = """
    call sp_GetPage(
                  '(
                    select tb1.*, @rownum := @rownum + 1 as play_index 
                    from 
                    (select l.song_id, l.artist_id, sa.song, sa.artist, sum(l.play_time) as play_time, sum(play_num)as play_num 
                        from 
                        song_play_log as l,
                        (select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa 
                        where (l.song_id=sa.song_id ) and l.time between \'\'%s\'\' and \'\'%s\'\' group by l.song_id order by play_num desc
                    ) as tb1,
                    (SELECT @rownum := 0) AS r
                  )',
                  'play_num',
                   %s,
                   %s,
                   " %s "); 
    """ % (time_start, time_end,pageindex,num_per_page,sql_where)
    #By page
    #print sql_page
    sql_where = ' and '+ sql_where[5:] if sql_where[0:5] == 'where' else ""
    #sql_all="select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between '"+time_start+"' and '"+time_end+"' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r"#all'
    sql_count = """
    select count(*)as result_count 
      from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num 
              from song_play_log as l,
              (select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist 
                  from song as s,artist as a where s.artist_id = a.id) as sa 
      where (l.song_id=sa.song_id ) %s and l.time between \'%s\' and \'%s\' group by l.song_id order by play_num desc) as tb1

    """ % (sql_where, time_start, time_end)
    #all count'
    #-----------------------------------
    ex = cur.execute(sql_page)
    info=list(cur.fetchall())
    print info
    #print info
    #for i in re:
    #  for j in i.items():
    #    for x in j:
    #      print x,
    #  print''
    cur.close()
    #print 'test---:',info[0]['@tbl'],'---'
    #-----------------------------------
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    ex = cur.execute(sql_count)
    result_count=cur.fetchall()
    if result_count:
      result_count = result_count[0].get("result_count",0)
    #-----------------------------------
    cur.close()
    conn.close()
    code=200
  except Mysqldb.Error,e:
    code=500
  finally:
    return info,result_count,code

class getArtistInfo():
  def __init__(self,jsonContent):
    self.json_content=jsonContent
    self.result_json={}
  def CheckJson(self):
    """
    check the request json data.
    """
    result_json=self.result_json
    #type(self.json_content)-->  dict
    #print 'test:json-content:',self.json_content,'\n'
    num_per_page=self.json_content.get("num_per_page",10) #data num for per page. default per page num is 10
    pageindex=self.json_content.get("pageindex",1) #data page index.default page index num is 1
    time_start=self.json_content.get("time_start")#data start time.
    time_end=self.json_content.get("time_end")#data end time.
    artist_song=self.json_content.get("artist_song")#artist and song
    sql_where=''
    #check num_per_page and pageindex
    try:
      if num_per_page <5 or int(num_per_page)<5:
        num_per_page=10
      if pageindex <=1 or int(pageindex)<=1:
        pageindex=1
    except Exception:
      num_per_page=10
      pageindex=1
    #check time_start and time_end.
    try:
      time_start = time.mktime(datetime.datetime.strptime(time_start,"%Y-%m-%d").timetuple())
      time_end = time.mktime(datetime.datetime.strptime(time_end, "%Y-%m-%d").timetuple())
    except Exception:
      time_start,time_end = time.time()-7*24*3600,time.time()
    try:
      time_start = datetime.datetime.fromtimestamp(float(time_start)).strftime("%Y-%m-%d")
      try:
        time_end = datetime.datetime.fromtimestamp(float(time_end)).strftime("%Y-%m-%d")
      except Exception:
        time_end = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d")
      time_min = datetime.datetime.fromtimestamp(time.time()-5*365*24*3600).strftime("%Y-%m-%d")
      if time_start > time_end:
        time_start,time_end=time_end,time_start
      elif time_start < time_min:
        time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    except Exception:
      time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    #check artist_song 
    #check the artist_song items(artist_name,song_name)don't is None or "" or "  ",
    is_true=False 
    for i in xrange(len(artist_song)):
      if artist_song[i].get('name'):
        is_true=True
        break
      for j in artist_song[i].get('song'):
        if j:
          is_true=True
          break
    #when artist_song not None or "" or "  ",and check is true not None or "" or "  ".get the artist and song list.
    if artist_song and is_true:
      sql_where=" where ("
      for i in xrange(len(artist_song)):
        artist=dict(artist_song[i]).get('name').strip()
        if i==0:
          sql_where+="("
          sql_where+="(artist='"+artist+"')" if artist else ""
        else:
          sql_where+=" or ((artist='"+artist+"')" if artist else " or ("
        song_list=dict(artist_song[i]).get('song')
        song_list=[m for m in song_list if m.strip()] if song_list else []#remove the song list item is None or "" or "  "
        if song_list:
          #print 'test:get song list:',song_list
          sql_where+=" and (" if artist else ""
          for j in xrange(len(song_list)):
            sql_where+="song='"+song_list[j]+"'" if j==0 else " or song='"+song_list[j]+"'"
          sql_where+="))" if artist else ")"
        else:
          sql_where+=")"
      sql_where+=")"
      #print 'test:sql_where:',sql_where
    result_json["check_info"],result_json["result_count"],result_json["code"]=getAllData(pageindex,num_per_page,time_start,time_end,sql_where.strip())
    result_json["pagecount"] = result_json["result_count"]/ num_per_page
    result_json["pagecount"] += 0 if result_json["result_count"] % num_per_page == 0 else 1
    result_json["num_per_page"] = num_per_page
    result_json["pageindex"] = pageindex
    return result_json
      #return json.dumps(result_json, sort_keys=True, indent=4, skipkeys=True, separators=(',',':'), cls=DecimalEncoder)

#if __name__=='__main__':
#  time_start='2013-11-11'
#  time_end='2013-11-13'
#  sql_where=''
#  getAllNum(time_start,time_end,sql_where)
