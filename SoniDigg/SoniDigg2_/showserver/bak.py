#!/usr/bin/python
#-*- coding: utf-8 -*-
#coding=utf-8
#encoding=utf-8
import datetime
import time
import MySQLdb as Mysqldb
import sys


#get all data count by check.no page
def getAllNum(time_start,time_end,sql_where):
  try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
    conn=Mysqldb.connect(
      host='localhost',
      user='root',
      passwd='123',
      port=3306,
      db='dataservice',
      charset='utf8'
    )
    #conn.set_character_set('utf8')
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    sql_page="call sp_GetPage1(concat('(select tb1.*,@rownum := @rownum + 1 as play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between ','''"+time_start+"''',' and ','''"+time_end+"''',' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)'),'play_num',1,100,' '); "#By page
    print sql_page
    sql_where=' and '+sql_where[5:] if sql_where[0:5]=='where' else ""
    sql_all="select tb1.*,@rownum := @rownum + 1 AS play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between '"+time_start+"' and '"+time_end+"' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r"#all'
    sql_count="select count(*)as result_count from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) "+sql_where+"and l.time between '"+time_start+"' and '"+time_end+"' group by l.song_id order by play_num desc) as tb1"#all count'
    #print sql_count
    #-----------------------------------
    sql_page=sql_page.decode('utf-8')
    print '-:::-\n',sql_page
    test_sql="call sp_GetPage1(concat('(select tb1.*,@rownum := @rownum + 1 as play_index from (select l.song_id,l.artist_id,sa.song,sa.artist,sum(l.play_time) as play_time,sum(play_num)as play_num from song_play_log as l,(select s.id as song_id,s.name as song,a.id as artist_id,a.name as artist from song as s,artist as a where s.artist_id = a.id) as sa where (l.song_id=sa.song_id ) and l.time between ','''2013-10-21''',' and ','''2013-11-13''',' group by l.song_id order by play_num desc) as tb1,(SELECT @rownum := 0) r)'),'play_num',1,100,'where (((artist=''品冠'') and (song=''陪你一起老''))or ((artist=''梁静茹'')))');"
    print '-:::-\n',test_sql
    ex = cur.execute(sql_page)
    print ex
    re=cur.fetchall()
    #for i in re:
    #  for j in i.items():
    #    for x in j:
    #      print x,
    #  print''
    cur.close()
    #-----------------------------------
    cur=conn.cursor(Mysqldb.cursors.DictCursor)
    ex = cur.execute(sql_count)
    re=cur.fetchall()
    #print re
    result_count = re[0].items()[0][1]
    print result_count
    #-----------------------------------
    cur.close()
    conn.close()
  except Mysqldb.Error,e:
    print e

class getArtistInfo():
  def __init__(self,jsonContent):
    print 'getArtistInfo class .. __init__'
    self.json_content=jsonContent
    self.result_json={}
  def CheckJson(self):
    # get song or arists info
    result_json=self.result_json
    print type(self.json_content)
    print 'json-content:',self.json_content,'\n'
    num_per_page=self.json_content.get("num_per_page") #data num for per page.
    pageindex=self.json_content.get("pageindex") #data page index.
    time_start=self.json_content.get("time_start")#data start time.
    time_end=self.json_content.get("time_end")#data end time.
    artist_song=self.json_content.get("artist_song")#artist and song
    sql_where=''
    try:
      if num_per_page <5 or int(num_per_page)<5:
        num_per_page=10
      if pageindex <=1 or int(pageindex)<=1:
        pageindex=1
    except ValueError:
      result_json["code"]=403 #par is error.
    try:
      time_start = datetime.datetime.fromtimestamp(float(time_start)).strftime("%Y-%m-%d")
      try:
        time_end = datetime.datetime.fromtimestamp(float(time_end)).strftime("%Y-%m-%d")
      except ValueError:
        time_end = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d")
      time_min = datetime.datetime.fromtimestamp(time.time()-5*365*24*3600).strftime("%Y-%m-%d")
      if time_start > time_end:
        time_start,time_end=time_end,time_start
      elif time_start < time_min:
        time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    except ValueError:
      time_start = datetime.datetime.fromtimestamp(time.time()-30*24*3600).strftime("%Y-%m-%d")
    is_true=False
    for i in xrange(len(artist_song)):
      if artist_song[i].get('name'):
        is_true=True
        break
      for j in artist_song[i].get('song'):
        if j:
          is_true=True
          break
    if artist_song and is_true:
      sql_where=' where ('
      for i in xrange(len(artist_song)):
        artist=dict(artist_song[i]).get('name').strip()
        if i==0:
          sql_where+='('
          sql_where+="(artist=\'\'"+artist+"\'\')" if artist else ""
        else:
          sql_where+="or ((artist=\'\'"+artist+"\'\')" if artist else "or("
        song_list=dict(artist_song[i]).get('song')
        song_list=[m for m in song_list if m.strip()]
        if song_list:
          print 'Tets..song_list::::',song_list
          sql_where+=" and (" if artist else ""
          for j in xrange(len(song_list)):
            sql_where+="song=\'\'"+song_list[j]+"\'\'" if j==0 else " or song=\'\'"+song_list[j]+"\'\'"
          sql_where+='))' if artist else ")"
        else:
          sql_where+=")"
      sql_where+=")"
      print 'where 6: ',sql_where
      getAllNum(time_start,time_end,sql_where.strip())

#if __name__=='__main__':
#  time_start='2013-11-11'
#  time_end='2013-11-13'
#  sql_where=''
#  getAllNum(time_start,time_end,sql_where)
