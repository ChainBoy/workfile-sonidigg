#!/bin/python
# -*- coding: utf-8 -*- 
import urllib2
import re
import time
import simplejson
import codecs
import MySQLdb as Mysqldb

station_file = 'station_test.txt'
def main():
    u = urllib2.urlopen('http://qingting.fm/category/domestic/54')
    r = u.read()
    l = re.findall(r'/category/domestic/(\d{1,2})(..)(.+?)(</a)',r);
    d = {}
    for i in l:
        d[i[2].decode("utf-8")] = {'id':i[0]}
    print 'Get city code OK!'
    print 'Get frequency'
    for i in d:
        print i,":",
        u = urllib2.urlopen('http://qingting.fm/category/domestic/%s' % d[i]['id'] )
        r = u.read()
        l = re.findall(r'/channels/(\d{1,10})(.+?)title=\'(.+?)\'',r)
        d_fre = {}
        for j in l:
            if j[0] and j[2]:
                d_fre[j[2].decode("utf-8")] = {'id':j[0],'name':j[2].decode("utf-8")}
            print j[2].decode("utf-8"),
        print "\n"
        d[i]['data'] = d_fre
    print 'Get frequency OK.'


    with codecs.open(station_file, 'w','utf-8') as f:
        for i in d:
            print i,':',
            dd = d[i]['data']
            city_id = d[i]['id']
            for j in dd:
                print j,
                n_city =  j
                i_city = dd[j]['id']
                r = ''
                timeout_num = 0
                while True:
                    try:
                        u= urllib2.urlopen('http://api.qingting.fm/api/qtradiov2/liveprograms?id=' + i_city + '&day=1,2,3,4,5,6,7&wt=json&_=' + str(time.time()).split('.')[0], timeout = 10)
                        r = u.read()
                        dr = simplejson.loads(r.decode("utf-8"))
                        dr = dr['data']
                        id = dr['id']
                        name = dr['name']
                        fre = dr['frequency']
                        media_id = dr['mediainfo']['id']
                        print '\b='+media_id,
                        break
                    except Exception, e:
                        timeout_num += 1
                        print e,',try again',
                        if timeout_num == 3:
                            print '<-Del.',
                            break
                t = '%s:%s:%s|%s|%s|%s\n' % (media_id, i, city_id, id, name, fre)
                update_station(True,name,city_id)
                f.write(t)
                print ' ',
            print '\nwaiting...\n'
    print 'Get station code is OK,Exit!'
    return True
def update_station(isupdate,name,city_id):
    if not isupdate:
        return
    conn=Mysqldb.connect(
        host='localhost',
        user='root',
        passwd='123',
        port=3306,
        db='dataservice',
        charset='utf8'
        )
    conn.set_character_set("utf8")
    cur=conn.cursor()
    a = cur.execute("update station_code set city_id='%s' where name='%s'" % (city_id, name))
    conn.commit()
    cur.close()
    cur=conn.cursor()
    a = cur.execute("update station set city_id='%s' where name='%s'" % (city_id, name))
    conn.commit()
    cur.close()
if __name__ == '__main__':
    main()


# http://42.96.141.199/live/2774.m3u8?bitrate=24
# http://110.76.47.134/live/1539849.m3u8?bitrate=48
