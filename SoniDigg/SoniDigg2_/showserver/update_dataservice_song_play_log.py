#!/usr/bin/python
#-*- coding: utf-8 -*-

# the file is to update DB:dataservice Table:song_play_log data.
#                Don't Delete !!!
#                Don't Remove !!!

#By: zhangzhipeng
#Mail:qq1126918258@gmail.com


import MySQLdb as Mysqldb
import os, platform, logging
import time
import datetime
import cPickle as p


class Update_dataservice_song_play_log:
  """docstring for ClassName"""

  def  __init__(self):
    self.mail_sub = ""
    self.mail_content = ""
    self.save_file = "every_day_log"
    self.log_file = "update_dataservice_song_play_log"
    #self.logging_file
    self.content = ""
    self.save_dict = {}
    self.Today_Date = datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S")
    self.Today_Date = datetime.datetime.strptime(self.Today_Date, "%Y-%m-%d %H:%M:%S").timetuple()
    self.YesDay_Date = datetime.datetime.fromtimestamp(time.time()-24*3600).strftime("%Y-%m-%d %H:%M:%S")
    self.YesDay_Date = datetime.datetime.strptime(self.YesDay_Date, "%Y-%m-%d %H:%M:%S").timetuple()
    self.default_YesDay_Date = "-".join([str(i) for i in self.YesDay_Date[0:3]])
    self.default_ToDay_Date = "-".join([str(i) for i in self.Today_Date[0:3]])+" "
    self.default_ToDay_Date += ":".join([str(i) for i in self.Today_Date[3:6]])
    self.sub = "Message:song_play_log Update Bash."
    #check All log is True.
    self.re_log_file = ""
    if not os.path.isfile(self.log_file):
      self.re_log_file = "%s is don't find,New file." % self.log_file
      self.sub += "[Warning]:No File log."
    if platform.platform().startswith('Windows'): #判断操作系统类型，获取当前用户的根目录
      self.logging_file = os.path.join(os.getenv('HOMEDRIVE'), os.getenv('HOMEPATH'), 'test.log')
    else:
      self.logging_file = os.path.join(os.getcwd(),self.log_file) #os.path.join(os.getenv('HOME'), 'test.log')
    #satrt save log ~
    self.flush_content("Start Update DB log.")
    self.save_log(0,self.content)
    if self.re_log_file:
      self.flush_content(self.re_log_file)
      self.save_log(0,self.content)
    #check every day log is True.
    if not os.path.isfile(self.save_file):
      self.flush_content("every day log is don't find!!!,update DB...")
      self.save_log(2,self.content)
      self.sub += "[Warning]:No File day_log"
      self.update_dataservice_song_play_log()
    else:
      with file(self.save_file) as f:
        self.save_dict = p.load(f)

      #get the every day log info.
      Data_Date = self.save_dict.get("Data_Date", self.default_YesDay_Date)
      Operate_Date = self.save_dict.get("Operate_Date", self.default_ToDay_Date)
      Send_Mail_to = self.save_dict.get("Send_Mail_to", "Not Have mail to ...")
      Send_Mail_state = self.save_dict.get("Send_Mail_state", False)
      DB_Qeury = self.save_dict.get("DB_Qeury", False)
      DB_Add_Row = self.save_dict.get("DB_Add_Row", 0)
      #check the every day log info and update the DB
      try:
        Operate_Date = datetime.datetime.strptime(Operate_Date, "%Y-%m-%d %H:%M:%S").timetuple()
        Data_Date = datetime.datetime.strptime(Data_Date, "%Y-%m-%d").timetuple()
        s_o = "-".join([str(i) for i in Operate_Date[0:3]])+" " + ":".join([str(i) for i in Operate_Date[3:6]])
        s_d = "-".join([str(i) for i in Data_Date[0:3]])+" " + ":".join([str(i) for i in Data_Date[3:6]])
        if Operate_Date[0:3] == self.Today_Date[0:3]:
          self.flush_content("DB has been updated! Operate_Date:%s."%s_o)
          self.save_log(2,self.content)
          self.flush_content("DB has been updated! Data_Date:%s."%s_d)
          self.save_log(2,self.content)
          self.flush_content("Stop Update DB.")
          self.save_log(1,self.content)
          self.flush_content("End Update DB log")
          self.save_log(0,self.content)
          self.send_mail("DB Update,[Query Warning],DB has been Update!!!",self.mail_content)
          return
        if Operate_Date[0:3] == self.YesDay_Date[0:3]:
          self.flush_content("DB YesDay_Date and Operate_Date,Is True.")
          self.update_dataservice_song_play_log()
        elif Operate_Date[0:3] < self.YesDay_Date[0:3]:
          self.sub += "[Warning]:%s No Update" % self.default_YesDay_Date
          self.flush_content("DB %s Not updated! Operate_Date:%s"%(self.default_YesDay_Date, s_o))
          self.save_log(2,self.content)
          self.flush_content("DB %s Not updated! Data_Date:%s"%(self.default_YesDay_Date, s_d))
          self.save_log(2,self.content)
          self.update_dataservice_song_play_log()
        elif Operate_Date[0:3] > self.Today_Date[0:3]:
          elf.sub += "[Warning/Error]:Update next Day(%s)?" % s_o
          self.flush_content("DB %s Has updated!? Operate_Date:%s"%(self.default_YesDay_Date, s_o))
          self.save_log(2,self.content)
          self.flush_content("DB %s Has updated!? Data_Date:%s"%(self.default_YesDay_Date, s_d))
          self.save_log(2,self.content)
          self.flush_content("Stop Update DB.")
          self.save_log(2,self.content)          
          self.flush_content("End Update DB log")
          self.save_log(0,elf.content)
          self.send_mail("DB Update,[Query Warning/Error]DB has been Update Befor next Day?",self.mail_content)
          return
          #datetime.datetime.fromtimestamp(time.time()-7*24*3600).strftime("%Y-%m-%d %H:%M:%S")
      except ValueError,e:
        self.flush_content("every_day_log is failed,ValueError\n"+e)
        self.save_log(2,self.content)
    self.save_log(0,"End Update DB log")
  def flush_content(self, content):
    self.content = content
    self.mail_content += "\n" + self.content
  #配置调试参数  
  def  save_log(self, cate, ex):
    logging.basicConfig(
      level=logging.DEBUG,
      format='%(asctime)s : %(levelname)s : %(message)s',
      filename = self.logging_file,
      filemode = 'a',
      )
    if cate == 0:
      logging.debug(ex) #debug开始调试
    if cate == 1 :
      logging.info(ex) #测试阶段
    if cate == 2:
      logging.warning(ex)#捕获警告

  def  update_dataservice_song_play_log(self):
    self.flush_content("Stat Update the dataservice")
    self.save_log(1, self.content)
    # self.save_dict = {    
    # "Data_Date":"2013-11-22",
    # "Operate_Date":"2013-11-23 05:20:23",
    # "Send_Mail_to":"",     #--
    # "Send_Mail_state":True, #--
    # "DB_Qeury":True, #--
    # "DB_Add_Row":20, #--
    # }


    try:
      conn=Mysqldb.connect(
            host='localhost',
            user='root',
            passwd='654321',
            port=3306,
            db='dataservice',
            charset='utf8')
      conn.autocommit(1)
      cur = conn.cursor(Mysqldb.cursors.DictCursor)
      sql_ = "INSERT INTO song_play_log(song_id,artist_id,play_num,play_time,time,hot) SELECT * FROM add_yesDay;"
      #ex = cur.execute("INSERT INTO tb_one(song,num) values(%d,%d)"%(12,101))
      #ex = cur.execute("INSERT INTO tb_one(song,num) SELECT song,num FROM tb_two;")
      ex = cur.execute(sql_)
      is_ok = 1
      self.save_dict["Data_Date"] = self.default_YesDay_Date
      self.save_dict["Operate_Date"] = self.default_ToDay_Date
      if ex:
        self.save_dict["DB_Qeury"] = True
        self.flush_content("Query OK,Update DB,Add new row %s" % ex)
        self.mail_sub += "Update DB, Query OK!"
        is_ok = 1
      else:
        self.save_dict["DB_Qeury"] = False
        self.flush_content("Query Error,Update dataservice_song_play_log,Add new row %s" % ex)
        self.mail_sub += "Update DB, [Query Error]"
        is_ok = 2
      self.save_dict["DB_Add_Row"] = ex if ex>=0 else -1
      self.save_log(2,self.content)
      cur.close()
      conn.close()
    except Mysqldb.Error,e:
      self.flush_content("Error,%s" % e)
      self.save_log(2,self.content)
    finally:
      self.flush_content("End Update the dataservice")
      self.save_log(2,self.content)
      re,send_result = self.send_mail(self.mail_sub,"%s\n%s\n%s" % (self.mail_content,"Updated Every Day Log","End Update DB"))
      if re:
        self.save_log(1,send_result)
      else:
        self.save_log(2,send_result)
      self.save_dict["Send_Mail_state"] = re
      with file(self.save_file,"w") as f:
        p.dump(self.save_dict, f)
        self.save_log(1,"Updated Every Day Log")
      # self.save_log(1,"End Update DB")

  def send_mail(self, sub, content):
      '''
      mailto_list:发给谁
      sub:主题
      content:内容
      send_mail("aaa@126.com","主题","内容")
      '''
      #!/usr/bin/env python
      # -*- coding: gbk -*-
      #导入smtplib和MIMEText
      import smtplib
      from email.mime.text import MIMEText
      #############
      #要发给谁，这里发给2个人
      mailto_list = ["qq1126918258@gmail.com","mydreambei@gmail.com","myzlkun@163.com","zhangtianyi1234@126.com","baonanhai@gmail.com"]#, "1126918258@qq.com", "peng2011@vip.qq.com"]
      self.save_dict["Send_Mail_to"] = mailto_list
      #####################
      #设置服务器，用户名、口令以及邮箱的后缀
      #mail_host="smtp.136.com"
      mail_host = "smtp.gmail.com"
      mail_user = "qq1126918258"
      mail_pass = "zhang?2011"
      mail_postfix="gmail.com"
      ######################
      me=mail_user+"<"+mail_user+"@"+mail_postfix+">"
      msg = MIMEText(content, 'base64', 'utf-8')
      msg['Subject'] = sub
      msg['From'] = me
      msg['To'] = ";".join(mailto_list)
      try:
          s = smtplib.SMTP(mail_host)
          s.docmd("EHLO server")#对于使用gmail的情况无本行则会出现 "SMTP AUTH extension not supported by server."
          s.starttls()#使用ssl加密
          #s.connect(mail_host)
          s.login(mail_user,mail_pass)
          s.sendmail(me, mailto_list, msg.as_string())
          s.close()
          return True,"Send Email OK."
      except Exception, e:
          print str(e)
          return False,"Send Email Error."


if __name__ == '__main__':
  new_update_class = Update_dataservice_song_play_log()
