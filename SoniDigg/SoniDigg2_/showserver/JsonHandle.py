#! /usr/bin/python
# -*- coding:utf-8 -*-

import json
import operator

def handle(jsonStr):
	array = json.loads(jsonStr)
	result = {}
	#最后七天匹配歌曲数量信息
	everyDayCount = {}
	#歌曲播放次数
	everySongCount = {}
	#歌手播放次数
	everyArtistCount = {}
	#最新匹配歌曲
	lastSongs = {}
	for songinfo in array:
		day = songinfo['time'].split(' ')[0]		
		if (everyDayCount.has_key(day)):
			everyDayCount[day]  = everyDayCount[day] + 1
		else:
			everyDayCount[day] = 1
		song = songinfo['title']
		if (everySongCount.has_key(song)):
			everySongCount[song] = everySongCount[song] + 1
		else:
			everySongCount[song] = 1

		lastSongs[song] = songinfo['time']

		artist = songinfo['artist']
		if (everyArtistCount.has_key(artist)):
                        everyArtistCount[artist] = everyArtistCount[artist] + 1
                else:
                        everyArtistCount[artist] = 1
	
	result['dayCount'] = everyDayCount
	
	songList = sort_by_value(everySongCount)
	result['songList'] = songList[0:100]
	
	artistList = sort_by_value(everyArtistCount)
	result['artistList'] = artistList[0:100]
	
	lastSongList = sort_by_value(lastSongs)
	result['lastSongList'] = lastSongList[0:10]
	jsonResult = json.dumps(result)
  #print jsonResult
	return jsonResult

def sort_by_value(d):
	return sorted(d.iteritems(), key=operator.itemgetter(1), reverse=True)  

if __name__ == '__main__':
	file = open('json.txt', 'r')
	line = file.read()
	handle(line)

