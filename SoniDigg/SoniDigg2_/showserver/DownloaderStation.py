#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python imports
import simplejson as json
import requests

# Tornado imports
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.web import url

# Options
define("port", default=8013, help="run on the given port", type=int)
define("debug", default=False, type=bool)

class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", '*')
    def pool(self):
        return self.application.pool

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/', MainPage),
            url(r'/down_ts/?$', DownTs),
        ]
        settings = dict(
            debug=options.debug,
            xsrf_cookies=False,
            cookie_secret="nzjxcjasaddsduuqwheazmu293nsadhaslzkci9023nsadnua9sdads/Vo=",
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class DownTs(BaseHandler):
    def get(self):
        filename = self.get_argument('file',default = '')
        ip = self.get_argument('ip',default = '')
        if filename and ip:
            data = {'file':filename}
            req = requests.post('http://%s/file/', data = json.dumps(data))
            result = req.content
            if len(result) > 500: self.write(result)
    def post(self):
        self.get()
  
class MainPage(BaseHandler):
    def get(self):
        self.write('OK.')
    def post(self):
        print 'OK'
        self.flush()
        self.finish()

if __name__ == "__main__":
    tornado.options.parse_command_line()
    print 'start'
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
