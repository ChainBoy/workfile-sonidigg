#!/usr/bin/python
#-*- coding:utf8 -*-
import sys
import os
import MySQLdb as mysqldb
import urllib2
import re
import threading
import Queue
import datetime
import time

class Get_Music(threading.Thread):
    def __init__(self,que):
        threading.Thread.__init__(self)
        self.que = que    
    def run(self):
        while True:
            if not self.que.empty():
                try:
                    music_dic = self.que.get(block=False,timeout = 1)
                    time.sleep(1)
                    # file_name = music_dic.get('file_name')
                    urls = music_dic.get('url')
                    urls = sorted(urls.iteritems(), key=lambda d:d[0])
                    for u in urls:
                        file_name = u[0]
                        url = u[1]
                        file_dir = '/'.join(file_name.split('/')[:2]) + '/'
                        file_url = file_dir + 'url.txt' #保存最近下载成功的ts
                        with file(file_url,'r+')as f:
                            file_urls = f.read()
                            f.flush()
                            f.close()
                        file_urls = file_urls.splitlines()
                        if url in file_urls:
                            print 'This file has been downloaded!!Continue!\n'
                            continue
                        url_list = self.get_ts_urls(url)
                        l = url_list
                        buffer_num = 3
                        is_down = False
                        for i,j in enumerate(l[:len(l)/buffer_num+1]):
                            url_list = l[i*buffer_num:i*buffer_num+buffer_num]
                            ts = self.download_ts_by_urls_list(url_list)
                            ts_add = 'ts'
                            if url_list and len(url_list) > 0:
                                ts_add = url_list[0]
                            ts_add = ts_add.split('.')[-1]
                            if ts:
                                is_down = True
                                self.save_ts(file_name.encode('utf8') + '.%s' % ts_add,ts,mode ='ab')
                            else:
                                self.save_ts('result/not_download_music.txt',str(datetime.datetime.now()) + (file_name[7:]).encode('utf8'),mode = 'a')
                        if len(l) >= 0 and is_down:
                            self.save_ts(file_url,url,mode = 'a')
                            print url
                        #with file(file_url,'a')as f:
                         #   f.write(str(url) + '\n')
                          #  f.flush()
                           # f.close()
                            #print 'Save OK...',url,type(url)
                        time.sleep(0.1)
                    self.queue.task_done()
                except Exception, e:
                    print e

            else:
                print 'thread is null,not Queue'
                break
        return

    def download_ts_by_urls_list(self,urls):
        r = b''
        if len(urls) == 0:
            return r
        for url in urls:
            closed = True
            while closed:
                self.test_conn()
                try:
                    try_num = 0
                    u = urllib2.urlopen(url,timeout = 20)
                    r += u.read()
                    break
                except Exception,e:
                    #print 'Time Out.Continue.'
                    self.test_conn()
                    try_num += 1
                    if try_num >= 3:
                        con = '[%s]%s :%s' % (str(datetime.datetime.now()), e, url)
                        self.save_ts('result/not_download_ts_url.txt', con,mode = 'a')
                        closed = False
                        break
                    else:
                        continue
            if closed:
                continue
        return r
    def save_ts(self,file_name,ts,mode = 'w'):
        con = ''
        if mode != 'ab':
            ts = ts + '\n'
        try:
            with file(file_name,mode)as f:
                f.write(ts)
            con = '[%s]OK :%s\n' % (str(datetime.datetime.now()), file_name)
        except Exception,e:
            con = '[%s]%s :%s\n' % (str(datetime.datetime.now()), e, file_name)

        finally:
            print self.name,con
            with file('result/SAVE_LOG','a')as f:
                f.write(con)



    def get_ts_urls(self,url):
        result = []
        try_num = 0
        while True:
            try:
                self.test_conn()
                u = urllib2.urlopen(url, timeout = 10)
                r = u.read()
                result = re.findall(r'http:.+?ts',r)
                if not result and not len(result)>0:
                    result = re.findall(r'http:.+?aac',r)
                return result
            except Exception,e:
                try:
                    self.test_conn()
                    n_url = url.replace('42.96.141.199','110.76.47.134')
                    print self.name,'Error:',self.name,url
                    print self.name,'Try:',self.name,n_url
                    u = urllib2.urlopen(n_url, timeout = 10)
                    r = u.read()
                    result = re.findall(r'http:.+?ts',r)
                    if not result and not len(result)>0:
                        result = re.findall(r'http:.+?aac',r)
                    return result
                except Exception,e:
                    self.test_conn()
                    try_num += 1
                    if try_num >= 3 :
                        con = '[%s]%s :%s' % (str(datetime.datetime.now()), e, url)
                        self.save_ts('result/not_download_ts_list_url.txt', con,mode = 'a')
                        return []
                    else:
                        continue


    def test_conn(self,url = 'http://www.baidu.com/'):
        closed = True
        while closed:
            try:
                u = urllib2.urlopen(url,timeout = 10)
                r = u.read()
                if len(r) > 20:
                    return True
                else:
                    continue
            except Exception,e:
                print self.name,'Connection closed!Try again, wait 10s...'
                time.sleep(10)
                continue



class MainClass(object):
    def __init__(self):
        super(MainClass, self).__init__()
        self.dir = 'result'
        self.check_dir()
        conn = mysqldb.connect(
                host = 'localhost',
                port = 3306,
                user = 'root',
                passwd = '123',
                db = 'dataservice',
                charset = 'utf8'
            )
        self.conn = conn
    def check_dir(self,station="",file_name = ''):
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)
            print 'make dir:',self.dir
        if not station:
            return
        n_dir = self.dir + '/' + station
        if not os.path.isdir(n_dir):
            os.mkdir(n_dir)
            print 'make dir:',n_dir
        if not file_name:
            return
        n_dir = n_dir + '/' + file_name
        if not os.path.isfile(n_dir):
            with file(n_dir,'w')as f:
                f.write('')
                print 'make file:',n_dir

    def start(self,thread_num = 5,s_time = '2014-11-11', e_time = '2014-11-12'):
        music_list = self.get_music_station_time_list(s_time,e_time)
        # print music_list
        que = Queue.Queue()
        self.Que = self.que_put_list(que,music_list)
        self.start_thread(thread_num)

    def start_thread(self,thread_num):
        #n_threads = []
        for i in xrange(1,thread_num + 1):
            print i
            n_thread = Get_Music(self.Que)
            n_thread.setDaemon(True)
            n_thread.start()
        self.Que.join()
        time.sleep(1)
        #    n_threads.append(n_thread)
        #for i in n_threads:
        #    i.join()


    def que_put_list(self,Que,que_list_or_tuple):
        que_list = que_list_or_tuple
        for i in que_list:
            Que.put(i)
        return Que

    def get_music_station_time_list(self,s_time = '2014-11-11', e_time = '2014-11-12'):
        result = self.get_station_list()
        if not result:
            print 'No Have Data,Break!'
            return
        all_time = self.get_alltime(s_time , e_time)
        list_time = all_time[0].get('time').split('|')
        for i in result:
            station = i.get('station','')
            code = i.get('code',0)
            code = int(code) if code else 0
            new_code = i.get('new_code',0)
            new_code = int(new_code) if new_code else 0
            self.check_dir(str(code),'url.txt')

            l_time = {}
            print station,code
            for k,j in enumerate(list_time[:-1]):
                start_time = j
                end_time = list_time[k+1]
            
                s_date,s_time = start_time.split(' ')
                s_Y,s_M,s_D = s_date[2:].split('-')
                s_h,s_m,s_s = s_time.split(':')
                e_date,e_time = end_time.split(' ')
                e_Y,e_M,e_D = e_date[2:].split('-')
                e_h,e_m,e_s = e_time.split(':')
                f_time = '%s%s%s%s%s%s_%s%s%s%s%s%s' % (s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
                file_name = u'%s/%s/%s' % (self.dir,code,f_time)
                url = 'http://42.96.141.199/cache?id=%s&bitrate=24&start=%sM%sD%sh%sm%ss%s&end=%sM%sD%sh%sm%ss%s' % (new_code,s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
                l_time[file_name] = url
                with file('result/url.txt','a')as f:
                    # print file_name

                    # print type(str(datetime.datetime.now()).decode('utf8').split('.')[0])
                    # print type(url)
                    # print type(file_name)
                    # print type(file_name.encode('utf8').decode('utf8'))
                    con = '%s:%s > %s\n' % (str(datetime.datetime.now()).decode('utf8').split('.')[0], url, file_name)
                    f.write(con.encode('utf8'))
            i['url'] = l_time

        return result


    def get_station_list(self):
        l_station = []
        with file('station.txt','r')as f:
            r = f.read()
        if r:
            l = r.splitlines()
            for i in l:
                d_station = {}
                l_info = i.split('\t')
                if not l_info or len(l_info) < 3:
                    continue
                code = l_info[0]
                new_code = l_info[1]
                station = l_info[2]
                d_station['code']=code
                d_station['new_code']=new_code
                d_station['station']=station
                l_station.append(d_station)
        return l_station



    def get_alltime(self,s_time = '2014-11-11', e_time = '2014-11-12'):
        cur = self.conn.cursor(mysqldb.cursors.DictCursor)
        a = cur.execute("call ff('%s','%s');" % (s_time,e_time))
        r = cur.fetchall()
        cur.close()
        if a and r and len(r)>=1:return r
        else:return 0

def _main():
    Main = MainClass()
    # artist_list=[]
    # if len(sys.argv) > 1:
    #     artist = sys.argv[1]
    #     artist_list = artist.split(',')
    # else:
    #     with file('duli.txt','r')as f:
    #         duli = f.read()
    #     artist_list = duli.splitlines()
    Main.start(10,s_time = '2014-02-01', e_time = '2014-02-08')

if __name__ == '__main__':
    #_main()
     try:
         _main()
         print 'End'
     except Exception,e:
         print e
         print 'End.'

