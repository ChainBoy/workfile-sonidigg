#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
PROXY = {'http':'http://127.0.0.1:8087'}
url = 'http://zhangzhipeng2023.cn'

urllib.socket.setdefaulttimeout(10)

def test(url,proxy = {}):
    if proxy:
        u = urllib.urlopen(url, proxies = proxy)
    return u.read()
print test(url, PROXY)