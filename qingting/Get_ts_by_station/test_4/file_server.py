#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python imports
import simplejson as json
import os

# Tornado imports
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.web import url

PORT = 9009

# Options
define("port", default=PORT, help="run", type=int)
define("debug", default=False, type=bool)


#http://127.0.0.7:9009/file/?file=/1/140208/140208000000_140208010000.ts
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/file/?$', Post_file),
        ]
        settings = dict(
            debug=options.debug,
            xsrf_cookies=False,
            cookie_secret="nzjxcjasaddsduuqwheazmu293nsadhaslzkci9023nsadnua9sdads/Vo=",
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", '*')
    def pool(self):
	    return self.application.pool


class Post_file(BaseHandler):
    def _send_file_response(self, filepath):
        d = filepath
        if not os.path.isfile(d):
            with file(d,'w')as f:
                f.write('')
            print 'make file:',d
        user_filename = '_'.join(filepath.split('/')[-2:])
        file_size = os.path.getsize(filepath)

        headers = [('Content-Disposition', 'attachment; filename=\"' + user_filename + '\"'),
                   ('Content-Type', 'text/plain'),
                   ('Content-Length', str(file_size))]

        fapp = FileApp(filepath, headers=headers)

        return fapp(self.request.environ, self.start_response)

    def get(self):
        filepath = start_time = self.get_argument('file',default = '')
        try:
            if not filepath:
                filepath = ''
            filepath = 'result' + filepath
        except Exception, e:
            filepath = ''
            r = e
        try:
            if filepath:
                if os.path.isfile(filepath):
                    with file(filepath,'rb') as f:
                        r = f.read()
                else:
                    print filepath
                    r = 'Not Find File!'
            else:
                r = r + 'Please input File!'
        except Exception, e:
            r =  e
        self.write(r)
        self.flush()
        self.finish()

    def post(self):
        jsonStr = str(self.request.body)
        jsonContent = json.loads(jsonStr)
        filepath = jsonContent.get("file",'')
        print filepath
        try:
            if not filepath:
                filepath = ''
            filepath = 'result' + filepath
        except Exception, e:
            filepath = ''
            r = e
        try:
            if filepath:
                if os.path.isfile(filepath):
                    with file(filepath,'rb') as f:
                        r = f.read()
                else:
                    print filepath
                    r = 'Not Find File!'
            else:
                r = r + 'Please input File!'
        except Exception, e:
            r =  e
        self.write(r)
        self.flush()
        self.finish()

if __name__ == "__main__":
    tornado.options.parse_command_line()
    print 'start'
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
