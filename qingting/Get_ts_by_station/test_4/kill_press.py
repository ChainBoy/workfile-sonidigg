#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
from speed import NetSpeed
import time
import os
import datetime

screen_name = 'quto'
file_name = 'get_ts_station.py'
SPEED = 800

def now():
    return str(datetime.datetime.now()).split('.')[0]

while True:
    run = True
    result = float(NetSpeed().get_rx_tx('eth0','zh')[0])
    print now(),'now speed:%skb/s' % result
    p = subprocess.Popen(['ps', 'a','u','x'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    l = []
    for line in out.splitlines():
        if screen_name in line:
            run = True
            pid = int(line.split(None, 2)[1])
            l.append(pid)
        else:
            run = False
    for pid in l:
        if result < SPEED:
            print now(),'will kill screen %s :'% screen_name, pid
            os.system('kill %s'% pid)
            run = False
        else:
            print now(),'net speed code is ok.'
            run = True
    if not run:
        print screen_name,'or',file_name,'not runing,will run..'
        # os.system('screen -S ppppp python test.py')
        l = 'screen -dmS %s python %s' % (screen_name, file_name)
        l = l.split(' ')
        p = subprocess.Popen(l, stdout=subprocess.PIPE).communicate()
        print now(),'Starting,please waiting 1200s...'
        time.sleep(1200)
    sleeptime = 0
    while True:
        if sleeptime <= 600:
            sys.stdout.write('\rWaiting %s...' % (600-sleeptime))
            sys.stdout.flush()
            time.sleep(1)
            sleeptime += 1
        else:
            break
