﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/python
#-*- coding:utf8 -*-
import sys
import os
import urllib2
import re
import threading
import Queue
import datetime
import time
import random
import simplejson
#from datetime import *

POST_URL = 'http://192.168.1.103:8008/fileinfo/'
CONN_URL = 'http://soniegg.oicp.net:10000'
def print_file(event,content):
    with file('log','a')as f:
        now_time = str(datetime.datetime.now()).split('.')[0]  
        ths = [i.name for i in threading.enumerate()]
        th = threading.currentThread().name
        s0 = '%s %s >> %s' % (th,now_time,content)
        s1 = '%s >>[%s/%s] %s' % (now_time,threading.activeCount(),len(ths), ','.join(ths))
        print  s0
        print >> f, s0
        print  s1
        print >> f,  s1
        th_n = threading.activeCount()
        if th_n < 1:
            event.clear()
            time.sleep(3)
            event.set()

def del_file(file_name,event):
    if os.path.isfile(file_name):
        print_file(event,'[%s]The file will be deleted,Re-download!' % file_name.split('/',1)[-1])
        os.remove(file_name)
        if not os.path.isfile(file_name):
            print_file(event,'[%s]Deleted Successfully!' % file_name.split('/',1)[-1])
        else:
            print_file(event,'[%s]Deleted Failed!' % file_name.split('/',1)[-1])

def post_path(data, post_url = POST_URL):
    print data
    try_num = 0
    while True:
        try_num += 1
        try:
            req = urllib2.Request(post_url)
            req.add_header('Content-Type', 'application/json')
            response = urllib2.urlopen(req, simplejson.dumps(data),timeout = 30)
            if response.read() == 'OK':
                print 'post ok',
                return True
        except Exception, e:
            if try_num > 3:
                return False
            else:
                continue

class Get_Music(threading.Thread):
    def __init__(self,que,name,event):
        threading.Thread.__init__(self)
        self.que = que
        self.name = name
        self.event = event
        self.post_url = POST_URL
    def run(self):
        while True:
            if not self.que.empty():
                try:
                    music_dic = self.que.get(block=False,timeout = 1)
                    time.sleep(1)
                    # file_name = music_dic.get('file_name')
                    urls = music_dic.get('url')
                    urls = sorted(urls.iteritems(), key=lambda d:d[0])
                    n = 0
                    for u in urls:
                        n += 1
                        file_name = u[0]
                        now_date = int(file_name.split('/')[-1][:6])
                        try:
                            next_date = int(urls[n][0].split('/')[-1][:6])
                        except Exception, e:
                            next_date = 999999
                        url = u[1]
                        file_dir = '/'.join(file_name.split('/')[:2])
                        file_url = file_dir + '/url.txt' #保存最近下载成功的ts
                        with file(file_url,'r+')as f:
                            file_urls = f.read()
                            f.flush()
                            f.close()
                        file_urls = file_urls.splitlines()
                        if url in file_urls:
                            print_file(self.event,'This file has been downloaded!!Continue!')
                            continue
                        del_file(file_name + '.ts',self.event)
                        del_file(file_name + '.aac',self.event)
                        url_list = self.get_ts_urls(url)
                        l = url_list
                        buffer_num = 3
                        is_down = False
                        for i,j in enumerate(l[:len(l)/buffer_num+1]):
                            url_list = l[i*buffer_num:i*buffer_num+buffer_num]
                            ts = self.download_ts_by_urls_list(url_list)
                            ts_add = 'ts'
                            if url_list and len(url_list) > 0:
                                ts_add = url_list[0]
                            ts_add = ts_add.split('.')[-1]
                            if ts:
                                is_down = True
                                self.save_ts(file_name.encode('utf8') + '.%s' % ts_add,ts,mode ='ab')
                            else:
                                self.save_ts('result/not_download_music.txt',self.name + str(datetime.datetime.now()) + ' ' + (file_name[7:]).encode('utf8') + ' ' + str(url_list) ,mode = 'a')
                        if len(l) >= 0 and is_down:
                            self.save_ts(file_url,url,mode = 'a')
                            print_file(self.event,url)
                            abs_path = os.path.abspath(file_name) + '.%s' % ts_add
                            rel_path = file_name[6:] + '.%s' % ts_add
                            station = file_name.split('/')[-3]
                            post_dic = {'station':station,'abs_path':abs_path,'rel_path':rel_path}
                            post_result = post_path(post_dic, self.post_url)
                            if post_result:
                                post_result = 'Success!'
                            else:
                                post_result = 'Failed!'
                                self.save_ts('result/' + 'post_failed.txt', simplejson.dumps(post_dic), mode = 'a')

                            print_file(self.event,'data:%s Post %s!' % (post_dic,post_result))
                            self.save_ts('result/' + 'successfully_hour.txt', 'result'+ rel_path, mode = 'a')
                            print_file(self.event,rel_path)
                        if next_date > now_date:
                            self.save_ts('result/' + 'dir_rel.txt','/'.join(file_name.split('/',)[1:-1]),mode = 'a')
                            print_file(self.event,url)#os.path.abspath('result/url.txt')
                            self.save_ts('result/' + 'dir_abs.txt',os.path.abspath('/'.join(file_name.split('/',)[:-1])))
                        self.event.clear()
                        time.sleep(0.1)
                        self.event.set()
                    self.que.task_done()
                except Exception, e:
                    print_file(self.event,e)
            else:
                print self.name + 'thread is null,not Queue'
                break
        return

    def download_ts_by_urls_list(self,urls):
        r = b''
        if len(urls) == 0:
            return r
        for url in urls:
            closed = True
            try_num = 0
            while closed:
                #self.test_conn()
                try:
                    u = urllib2.urlopen(url,timeout = 20)
                    r += u.read()
                    break
                except Exception,e:
                    self.test_conn()
                    print 'net stat:ok'
                    try_num += 1
                    if try_num >= 3:
                        print_file(self.event,'[%s]:%s' % (e, url))
                        self.save_ts('result/not_download_ts_url.txt', url, mode = 'a')
                        closed = False
                        print 'try:3,break!'
        return r

    def save_ts(self,file_name,ts,mode = 'w'):
        con = ''
        if mode != 'ab':
            ts = ts + '\n'
        try:
            with file(file_name,mode)as f:
                f.write(ts)
            con = '[%s]OK :%s\n' % (str(datetime.datetime.now()), file_name)
        except Exception,e:
            con = '[%s]%s :%s\n' % (str(datetime.datetime.now()), e, file_name)

        finally:
            print_file(self.event,self.name + con)
            with file('result/SAVE_LOG','a')as f:
                f.write(con)

    def get_ts_urls(self,url):
        result = []
        try_num = 0
        while True:
            try:
                #self.test_conn()
                u = urllib2.urlopen(url, timeout = 10)
                r = u.read()
                result = re.findall(r'http:.+?ts',r)
                if not result and not len(result)>0:
                    result = re.findall(r'http:.+?aac',r)
                return result
            except Exception,e:
                try:
                    #self.test_conn()
                    n_url = url.replace('42.96.141.199','110.76.47.134')
                    print self.name,'Error:',self.name,url
                    print self.name,'Try:',self.name,n_url
                    u = urllib2.urlopen(n_url, timeout = 10)
                    r = u.read()
                    result = re.findall(r'http:.+?ts',r)
                    if not result and not len(result)>0:
                        result = re.findall(r'http:.+?aac',r)
                    return result
                except Exception,e:
                    self.test_conn()
                    try_num += 1
                    if try_num >= 3 :
                        print_file(self.event,'[%s]:%s' % (e , url))
                        con = '[%s]%s :%s' % (str(datetime.datetime.now()), e, url)
                        self.save_ts('result/not_download_ts_list_url.txt', con,mode = 'a')
                        return []
                    else:
                        continue


    def test_conn(self,url = CONN_URL):
        closed = True
        while closed:
            try:
                u = urllib2.urlopen(url,timeout = 10)
                r = u.read()
                if r == 'OK':
                    return True
            except Exception,e:
                t = random.randint(1,13)
                print self.name,'Connection closed!Try again, wait %ss...[%s]' % (t,e)
                self.event.clear()
                time.sleep(t)
                self.event.set()
                continue


class MainClass(object):
    def __init__(self):
        super(MainClass, self).__init__()
        self.dir = 'result'
        self.check_dir()
        self.post_old_dir()

    def post_old_dir(self):
        try:
            with file(self.dir + '/' + 'successfully_hour.txt','r+')as f:
                r = f.readlines()
                for i in r:
                    ii = i.replace('\n','')
                    l = ii.split('/')
                    station = l[1]
                    abs_path = os.path.abspath(ii)
                    rel_path = ii[6:]
                    post_dic = {'station':station,'abs_path':abs_path,'rel_path':rel_path}
                    if not post_path(post_dic):
                        print 'post Failed!'
                        with file(self.dir + '/' +'post_failed.txt','a')as f_failed:
                            f_failed.write(simplejson.dumps(post_dic) + '\n')
        except Exception, e:
            raise e
        print 'Check and Post successfully_hour.txt data Success!'
        time.sleep(3)

        try:
            with file(self.dir + '/' +'post_failed.txt','r+')as f:
                nr = []
                r = f.readlines()
                for i,j in enumerate(r):
                    if not post_path(simplejson.loads(j)):
                        nr.append(j)
        except Exception, e:
            raise e

        try:
            with file(self.dir + '/' +'post_failed.txt','w')as f:
                f.writelines(nr)
        except Exception, e:
            raise e
        print 'Post ord data Success!'
        time.sleep(3)

    def check_dir(self,station="",file_name = ''):
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)
            print 'make dir:',self.dir
        l =['dir_rel.txt','dir_abs.txt','successfully_hour.txt','post_failed.txt','not_download_ts_list_url.txt','not_download_ts_url.txt','SAVE_LOG']
        for i in l:
            d = self.dir + '/' + i
            if not os.path.isfile(d):
                with file(d,'w')as f:
                    f.write('')
                print 'make file:',d
        if not station:
            return
        n_dir = self.dir + '/' + station
        if not os.path.isdir(n_dir):
            os.mkdir(n_dir)
            print 'make dir:',n_dir
        if not file_name:
            return
        n_dir = n_dir + '/' + file_name
        if not os.path.isfile(n_dir):
            with file(n_dir,'w')as f:
                f.write('')
                print 'make file:',n_dir

    def start(self,thread_num = 5,s_time = '2014-11-11', e_time = '2014-11-12'):
        music_list = self.get_music_station_time_list(s_time,e_time)
        # print music_list
        que = Queue.Queue()
        self.Que = self.que_put_list(que,music_list)
        self.start_thread(thread_num)

    def start_thread(self,thread_num):
        #n_threads = []
        event = threading.Event()
        threads = []
        for i in xrange(1,thread_num + 1):
            print i,
            threads.append(Get_Music(self.Que, '%s/[%s]' % (self.Que.qsize(), i), event))
        event.clear()
        for thread in threads:
            thread.start()
            time.sleep(random.randint(1,3))
        event.set()

    def que_put_list(self,Que,que_list_or_tuple):
        que_list = que_list_or_tuple
        for i in que_list:
            Que.put(i)
        return Que

    def get_alltime(self,s_time = '2014-11-11', e_time = '2014-11-12'):
        s_time_info = s_time.split("-") 
        s_date_time = datetime.datetime(int(s_time_info[0]), int(s_time_info[1]), int(s_time_info[2]), 0, 0, 0)
        e_time_info = e_time.split("-")
        e_date_time = datetime.datetime(int(e_time_info[0]), int(e_time_info[1]), int(e_time_info[2]), 0, 0, 0)
        alltime = []
        temp_time = s_date_time
        alltime.append(str(s_date_time))
        onehour_timedelta = datetime.timedelta(0,3600)
        while True:
            temp_time = temp_time +  onehour_timedelta
            if temp_time > e_date_time:
                break
            else:
                alltime.append(str(temp_time))
        return alltime

    def get_music_station_time_list(self,s_time = '2014-11-11', e_time = '2014-11-12'):
        result = self.get_station_list()
        if not result:
            print 'No Have Data,Break!'
            return
        #all_time = self.get_alltime(s_time, e_time)
        list_time = self.get_alltime(s_time, e_time)
        for i in result:
            station = i.get('station','')
            code = i.get('code',0)
            code = int(code) if code else 0
            new_code = i.get('new_code',0)
            new_code = int(new_code) if new_code else 0
            self.check_dir(str(code),'url.txt')

            l_time = {}
            print station,code
            for k,j in enumerate(list_time[:-1]):
                start_time = j
                end_time = list_time[k+1]
            
                s_date,s_time = start_time.split(' ')
                s_Y,s_M,s_D = s_date[2:].split('-')
                s_h,s_m,s_s = s_time.split(':')
                e_date,e_time = end_time.split(' ')
                e_Y,e_M,e_D = e_date[2:].split('-')
                e_h,e_m,e_s = e_time.split(':')
                f_time = '%s%s%s%s%s%s_%s%s%s%s%s%s' % (s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
                day = '%s%s%s' % (s_Y,s_M,s_D)
                self.check_dir('%s/%s' % (code,day))
                file_name = u'%s/%s/%s/%s' % (self.dir,code,day,f_time)
                url = 'http://42.96.141.199/cache?id=%s&bitrate=24&start=%sM%sD%sh%sm%ss%s&end=%sM%sD%sh%sm%ss%s' % (new_code,s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
                l_time[file_name] = url
                with file('result/url.txt','a')as f:
                    # print file_name

                    # print type(str(datetime.datetime.now()).decode('utf8').split('.')[0])
                    # print type(url)
                    # print type(file_name)
                    # print type(file_name.encode('utf8').decode('utf8'))
                    con = '%s:%s > %s\n' % (str(datetime.datetime.now()).decode('utf8').split('.')[0], url, file_name)
                    f.write(con.encode('utf8'))
            i['url'] = l_time

        return result


    def get_station_list(self):
        l_station = []
        with file('station.txt','r')as f:
            r = f.read()
        if r:
            l = r.splitlines()
            for i in l:
                d_station = {}
                l_info = i.split('\t')
                if not l_info or len(l_info) < 3:
                    continue
                code = l_info[0]
                new_code = l_info[1]
                station = l_info[2]
                d_station['code']=code
                d_station['new_code']=new_code
                d_station['station']=station
                l_station.append(d_station)
        return l_station

def _main():
    Main = MainClass()
    Main.start(5,s_time = '2014-02-08', e_time = '2014-02-10')

if __name__ == '__main__':
     _main()
     try:
         _main()
         print 'End'
     except Exception,e:
         print e
         print 'End.'

