#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import subprocess
import time
import re 

TIME = 1
DEVICE = 'wlan0'
LOCAL = 'zh'
ALL = False

class NetSpeed():
    def __init__(self):
        self.device = DEVICE
        self.local = LOCAL

    def start(self, p_rint = True):
        if not ALL:
            all_net = [self.device]
        else:
            all_net = self.get_net_name()

        if not p_rint:
            for i in all_net:
                print i,self.get_rx_tx(device = i)
        else:
            result = []
            for j in all_net:
                res = self.get_rx_tx(device = j)
                result.append({'name':j,'rx':res[0],'tx':res[1]})
            return result

    def get_net_name(self):
        with file('/etc/udev/rules.d/70-persistent-net.rules','r')as f:
            r = f.read()
            return re.findall(r'NAME="(.+?)"',r)

    def ifconfig(self, device = 'wlan0', local = 'en'):
        output = subprocess.Popen(['ifconfig', device], stdout=subprocess.PIPE).communicate()[0]
        if local == 'zh':
            rx_bytes = re.findall('接收字节:([0-9]*) ', output)[0]
            tx_bytes = re.findall('发送字节:([0-9]*) ', output)[0]
        else:
            rx_bytes = re.findall('RX bytes:([0-9]*) ', output)[0]
            tx_bytes = re.findall('TX bytes:([0-9]*) ', output)[0]
        return float('%.3f' % (int(rx_bytes) / 1024)), float('%.3f' % (int(tx_bytes) / 1024))

    def get_rx_tx(self,device = 'wlan0',local = LOCAL):
        try:
            while True:
                rx_bytes, tx_bytes = self.ifconfig(device= device,local = self.local)
                time.sleep(TIME)
                rx_bytes_new,tx_bytes_new = self.ifconfig(device= device,local = self.local)

                col_rx = (rx_bytes_new - rx_bytes) / TIME
                col_tx = (tx_bytes_new - tx_bytes) / TIME

                col_rx = '%.3f' % col_rx
                col_tx = '%.3f' % col_tx
                return col_rx, col_tx
        except Exception, e:
            raise e

if __name__ == '__main__':
    Speedp = NetSpeed()
    print Speedp.get_rx_tx(device = 'wlan0', local = 'zh')
