#!/usr/bin/env python
# -*- coding: utf-8 -*-


import signal
import time
 
def sigint_handler(signum, frame):
  global is_sigint_up
  is_sigint_up = True
  print 'catched interrupt signal!'


def test0():
  while True:
    try:
      print 'test'
      time.sleep(1)
    except KeyboardInterrupt:
      exit('Keyboard End\n')
    except Exception, e:
      print e
      break

def test1():
  
  signal.signal(signal.SIGINT, sigint_handler)
  signal.signal(signal.SIGHUP, sigint_handler)
  signal.signal(signal.SIGTERM, sigint_handler)
  is_sigint_up = False
   
  while True:
    try:
      print 'test'
      time.sleep(1)
      if is_sigint_up:
       #中断时需要处理的代码
        print "Exit"
        break
    except Exception, e:
      print e
      break