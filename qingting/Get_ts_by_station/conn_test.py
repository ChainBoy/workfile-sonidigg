#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2
import time
import random

def test_conn(url = 'http://sonidigg.f3322.org:10000/'):
    closed = True
    while closed:
        try:
            u = urllib2.urlopen(url,timeout = 10)
            r = u.read()
            if r == 'OK':
                return True
            else:
                continue
        except Exception,e:
            t = random.randint(1,13)
            print 'Connection closed!Try again, wait %ss...[%s]' % (t,e)
            time.sleep(t)
            continue