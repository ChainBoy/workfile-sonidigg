#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import urllib2
PROXY_URL = 'http://sonidigg.xicp.net:8012/'


def set_proxy(enable_proxy = True, proxy_url = PROXY_URL):
    get_proxy_num = 0
    while True:
        try:
            proxy = urllib2.urlopen(proxy_url).read()
            proxy = re.findall(r'((\d{1,3}.){3}\d{1,3}:\d.+?$)',proxy)
            if proxy and len(proxy) >= 1 :
                if proxy[0] and len(proxy) >= 1:
                    proxy = proxy[0][0]
                    break
                else:
                    get_proxy_num += 1
            else:
                get_proxy_num += 1
            if get_proxy_num >= 5:
                enable_proxy = False
                break
        except Exception:
            get_proxy_num += 1
            if get_proxy_num > 5: 
                enable_proxy = False
                break
    print proxy
    proxy_handler = urllib2.ProxyHandler({"http" : proxy})
    null_proxy_handler = urllib2.ProxyHandler({})
    if enable_proxy:
        opener = urllib2.build_opener(proxy_handler)
    else:
        opener = urllib2.build_opener(null_proxy_handler)
    urllib2.install_opener(opener)
