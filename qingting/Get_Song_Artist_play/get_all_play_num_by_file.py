#!/usr/bin/python
#-*- coding:utf8 -*-
import MySQLdb as mysqldb
import urllib2
import re
import os
import datetime
import threading
import Queue

class MainClass(object):
    def __init__(self):
        super(MainClass, self).__init__()
        conn = mysqldb.connect(
                host = 'localhost',
                port = 3306,
                user = 'root',
                passwd = '654321',
                db = 'dataservice',
                charset = 'utf8'
            )
        self.conn = conn
    def start(self,save = 0):
        music_list = self.get_music_list_from_file()
        result = self.get_data(music_list)
        count = self.get_count(result)
        if save == 1:
            self.dir = 'result'
            self.check_dir()

            string = "Song\tArtist\tPlayCount"
            for i in result:
                string += "\n%s\t%s\t%s" % (i.get('song',' '),i.get('artist',' '),i.get('play_count',0))

            #string = "Song\tArtist\tTime\tStation"
            #for i in result:
            #    string += "\n%s\t%s\t%s\t%s" % (i.get('song',' '),i.get('artist',' '),i.get('time',''),i.get('name',))

            file_name = "%s [%s]" % (str(datetime.datetime.now()),count)
            file_name = file_name.replace(' ','_')
            with file('%s/%s'% (self.dir, file_name),'w')as f:
                f.write(string.encode('utf8'))
                print "Result Save OK. -->File Name: [%s/%s]" % (self.dir,file_name)
        return count

    def check_dir(self):
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)


    def make_sql_where(self,music_list):
        sql = ''
        if not music_list and not len(music_list) > 0:
            exit('File format error?Exit.')
            
        sql = " (name = '%s' and artist = '%s') " % (music_list[0].get('name',''),music_list[0].get('artist',''))
        for i in music_list[1:]:
            sql += " or (name = '%s' and artist = '%s') " % (i.get('name',''),i.get('artist',''))
        return sql

    def get_music_list_from_file(self):
        music_list = []
        with file('source.txt','r')as f:
            r = f.read()
            result = r.splitlines()
            n = 0
            for i in result:
                music = {}
                m_list = i.split('\t')
                if not m_list or not m_list[0] or len(m_list) <2:
                    continue
                name = m_list[1]
                artist = m_list[0]
                music['name'] = name.decode('utf8').strip()
                music['artist'] = artist.decode('utf8').strip()
                music_list.append(music)
        return music_list

    def get_count(self,song_play_list):
        if not song_play_list and not len(song_play_list) >0:
            exit('No Result !!!\nExit...')
        count = 0
        for i in song_play_list:
            count += int(i.get('play_count',0))
            #count += 1
        if not count:
            exit('All count is Zero !!! \nExit...')
        return count


    def get_data(self,music_list):
        sql_where = self.make_sql_where(music_list)
        if not sql_where:
            exit('Exit,not have data?')
        cur = self.conn.cursor(mysqldb.cursors.DictCursor)
        sql = "select tb2.*,count(*)as play_count from listen_log_once as tb1,(select name as song,artist,id as song_id,artist_id from song where %s )as tb2 where tb1.song=tb2.song_id group by tb2.song,tb2.artist order by play_count desc;" % sql_where
        #sql = "select tb2.song,artist,tb1.time,tb3.name from listen_log_once as tb1,(select name as song,artist,id as song_id,artist_id from song where %s )as tb2,station as tb3 where tb1.song=tb2.song_id and tb1.stationId=tb3.id; " % sql_where
        with file('sql.txt','w')as f:
            f.write(sql.encode('utf8'))
        a = cur.execute(sql)
        r = cur.fetchall()
        #print r
        cur.close()
        if a and r and len(r)>=1:return r
        else:return []

if __name__ == '__main__':
    Main = MainClass()
    exit("All Play Count:%s" % Main.start(1))
