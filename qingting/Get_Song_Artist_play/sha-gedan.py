#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as mysqldb
import os
import datetime

#files = "source_dashi.txt   source_hengda.txt   source_huayi.txt  source_niaoren.txt                source_taiyangciquban-xuchangde.txt  top-1000  source_haidie.txt  source_huanqiu.txt  source_lehua.txt  source_taiyangciquban-others.txt  source_yashen.txt"
#files = "source_dashi.txt\nsource_hengda.txt"
files = "yashen01.txt\tyashen02.txt"

class MainClass(object):
    def __init__(self):
        super(MainClass, self).__init__()
        conn = mysqldb.connect(
                host = 'localhost',
                port = 3306,
                user = 'root',
                passwd = '654321',
                db = 'shazam',
                charset = 'utf8'
            )
        self.conn = conn
    def start(self,save = 0, filename='source.txt'):
        music_list = self.get_music_list_from_file(filename)
        result = self.get_data(music_list)
        count = self.get_count(result)
        if save == 1:
            self.dir = 'shazam-result'
            self.check_dir()

            #string = "Song\tArtist\tPlayCount"
            #for i in result:
            #    string += "\n%s\t%s\t%s" % (i.get('song',' '),i.get('artist',' '),i.get('play_count',0))

            string = "Song\tArtist\tTime\tStation"
            for i in result:
                string += "\n%s\t%s\t%s\t%s" % (i.get('song',' '),i.get('artist',' '),i.get('time',''),i.get('name',))

            file_name = "%s_%s_%s.xlsx" % (filename.split('/')[-1].split('.')[0].replace('source_',''), str(datetime.datetime.now())[:-7].replace('-','').replace(':','').replace(' ','-'), count)
            fname = '%s/%s' % (self.dir,file_name)
            with file('%s/%s'% (self.dir, file_name),'w')as f:
                f.write(string.encode('utf8'))
                with file('result.txt','a')as fw:
                    fw.write(fname)
                print "Result Save OK. -->File Name: [%s]" % fname
        return count, fname

    def check_dir(self):
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)


    def make_sql_where(self,music_list):
        sql = ''
        if not music_list and not len(music_list) > 0:
            exit('File format error?Exit.')
            
        sql = " (name = '%s' and artist = '%s') " % (music_list[0].get('name',''),music_list[0].get('artist',''))
        for i in music_list[1:]:
            sql += " or (name = '%s' and artist = '%s') " % (i.get('name',''),i.get('artist',''))
        return sql

    def get_music_list_from_file(self,filename='source.txt'):
        music_list = []
        with file(filename,'r')as f:
            r = f.read()
            result = set(r.splitlines())
            n = 0
            for i in result:
                music = {}
                m_list = i.split('\t')
                if not m_list or not m_list[0] or len(m_list) <2:
                    continue
                name = m_list[1]
                artist = m_list[0]
                music['name'] = name.replace('"','').replace("'",'').decode('utf8').strip()
                music['artist'] = artist.replace('"','').replace("'",'').decode('utf8').strip()
                music_list.append(music)
        return music_list

    def get_count(self,song_play_list):
        if not song_play_list and not len(song_play_list) >0:
            exit('No Result !!!\nExit...')
        count = 0
        for i in song_play_list:
            #count += int(i.get('play_count',0))
            count += 1
        if not count:
            exit('All count is Zero !!! \nExit...')
        return count


    def get_data(self,music_list):
        l = len(music_list)
        result = []
        j = 0
        for i in music_list:
            j += 1
            cur = self.conn.cursor(mysqldb.cursors.DictCursor)
            #sql = "select tb2.*,count(*)as play_count from listen_log_once as tb1,(select name as song,artist,id as song_id,artist_id from song where %s )as tb2 where tb1.song=tb2.song_id group by tb2.song,tb2.artist order by play_count desc;" % sql_where
            sql = "select tb2.song,tb2.artist,tb1.time,tb3.name from onces as tb1,(select songs.title as song,artists.name as artist,songs.id as song_id,artist_id from songs,artists where songs.artist_id = artists.id and songs.title = '%s' and artists.name = '%s')as tb2,stations as tb3 where tb1.song_id=tb2.song_id and tb1.station_id=tb3.id;" % (i.get('name',''), i.get('artist',''))
            with file('sql.txt','w')as f:
                f.write(sql.encode('utf8'))
            a = cur.execute(sql)
            r = cur.fetchall()
            cur.close()
            print '%s/%s %.2f' % (j,l,(100/float(('%.2f' %(l/float('%s.0' % j)))))), a, len(r)
            if a and r and len(r)>=1:
                result.extend(list(r))
        return result

if __name__ == '__main__':
    with file('result.txt','w')as f:
        pass
    Main = MainClass()
    s = ''
    for i in [j.strip() for j in files.split('\t') if j]:
        print i
    for i in [j.strip() for j in files.split('\t') if j]:
        s += "Count:%s File:%s\n" % Main.start(1, 'source' + '/' + i.strip())
    print s
