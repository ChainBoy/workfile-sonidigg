#!/usr/bin/python
#-*- coding:utf8 -*-
import MySQLdb as mysqldb
import urllib2
import re
import os
import datetime
import threading
import Queue




class Get_Music(threading.Thread):
    def __init__(self,que):
        threading.Thread.__init__(self)
        self.que = que    
    def run(self):
        while True:
            if not self.que.empty():
                #print "%s:%s," % (self.name, self.que.get())#输出线程名字，取队列内容
                music_dic = self.que.get()
                file_name = music_dic.get('file_name')
                url = music_dic.get('url')
                url_list = self.get_ts_urls(url)
                ts = self.download_ts_by_urls_list(url_list)
                if ts:
                    self.save_ts(file_name + '.ts',ts)
                else:
                    self.save_ts('not_download_music.txt', (file_name + '\n').encode('utf8'),mode = 'a')
            else:
                break

    def download_ts_by_urls_list(self,urls):
        r = b''
        if len(urls) == 0:
            return r
        for url in urls:
            try:
                u = urllib2.urlopen(url,timeout = 20)
                r += u.read()
            except Exception,e:
                print 'Time Out.Continue.'
        return r
    def save_ts(self,file_name,ts,mode = 'w'):
        try:
            with file(file_name,mode)as f:
                f.write(ts)
            print '%s Save OK.' % file_name
        except Exception,e:
            print '%s Save ERROR.' % file_name

                
    def get_ts_urls(self,url):
        try:
            u = urllib2.urlopen(url)
            r = u.read()
            result = re.findall(r'http:.+?ts',r)
            return result
        except Exception,e:
            print e
            return []

class MainClass(object):
    def __init__(self):
        super(MainClass, self).__init__()
        self.dir = 'result'
        self.check_dir()
        conn = mysqldb.connect(
                host = 'localhost',
                port = 3306,
                user = 'root',
                passwd = '123',
                db = 'dataservice',
                charset = 'utf8'
            )
        self.conn = conn
    def check_dir(self):
        if not os.path.isdir(self.dir):
            os.mkdir(self.dir)
    def start(self,s_type,thread_num = 5,artist_list = ['韩红','Eminem'],s_time = 300, e_time = 300, top_num = 10, day_num = 1):
        music_list = self.get_music_station_time_list(s_type,artist_list,s_time,e_time,top_num,day_num)
        que = Queue.Queue()
        self.Que = self.que_put_list(que,music_list)
        self.start_thread(thread_num)

    def start_thread(self,thread_num):
        for i in xrange(1,thread_num + 1):
            n_thread = Get_Music(self.Que)
            n_thread.start()

    def que_put_list(self,Que,que_list_or_tuple):
        que_list = que_list_or_tuple
        for i in que_list:
            Que.put(i)
        return Que

    def get_music_list_from_file(self, s_time=300,e_time=300):
        """
        从文件读取信息，信息格式：
        RADIO GAGA\t佚名\t2014/1/31 12:39:35\t北京音乐广播
        """
        music_list = []
        with file('music.txt','r')as f:
            r = f.read()
            result = r.splitlines()
            for i in result:
                music = {}
                m_list = i.split('\t')
                #print m_list
                if not m_list or not m_list[0]:
                    break
                name = m_list[0]
                artist = m_list[1]
                time = datetime.datetime.strptime(m_list[2],'%Y/%m/%d %H:%M:%S')
                station = m_list[3]
                music['name'] = name.decode('utf8')
                music['artist'] = artist.decode('utf8')
                music['time'] = time
                music['station'] = station.decode('utf8')
                station_info = self.get_station_code_by_name(station)[0]
                music['code'] = station_info.get('code',0)
                music['new_code'] = station_info.get('new_code',0)
                music['start_time'] = time + datetime.timedelta(seconds = -s_time)
                music['end_time'] = time + datetime.timedelta(seconds = e_time)
                music_list.append(music)
        return music_list

    def get_station_code_by_name(self,station):
        """
        根据电台名字 获取电台信息
        """
        cur = self.conn.cursor(mysqldb.cursors.DictCursor)
        a = cur.execute('select * from station_code where name = "%s"' % station)
        r = cur.fetchall()
        if a and len(r) > 0:return list(r)
        else : return []

    def get_music_station_time_list(self,s_type,artist_list = ['韩红','Eminem','苏醒'],s_time = 300, e_time = 300, top_num = 10, day_num = 1):
        """
        获取歌曲排行榜的详情.
        s_type = 1,所有歌曲
        s_type = 2,英文歌曲
        s_type = 3,中文歌曲

        s_type = 4,独立音乐人歌曲
        s_type = 5,从文件读取


        artist_list,独立音乐人列表
        s_time ,前推多少分钟,默认5min 。
        e_time ,后推多少分钟,默认5min 。
        top_num,排行榜多少位，默认Top 10 。
        day_num,查询几天，默认1天 。
        """
        if s_type == 5:
            result = self.get_music_list_from_file()
        else:
            result = self.get_music_top_all(s_type,artist_list,s_time,e_time,top_num,day_num)
        if not result:
            print 'No Have Data,Break!'
            return
        for i in result:
        #name artist station station_id time start_time end_time
            song = i.get('name','')
            artist = i.get('artist','')
            station = i.get('station','')
            code = i.get('code',0)
            code = int(code) if code else 0
            new_code = i.get('new_code',0)
            new_code = int(new_code) if new_code else 0
            play_time = str(i.get('time'))
            start_time= str(i.get('start_time'))
            end_time = str(i.get('end_time'))
            
            s_date,s_time = start_time.split(' ')
            s_Y,s_M,s_D = s_date[2:].split('-')
            s_h,s_m,s_s = s_time.split(':')

            e_date,e_time = end_time.split(' ')
            e_Y,e_M,e_D = e_date[2:].split('-')
            e_h,e_m,e_s = e_time.split(':')
            print s_Y,s_M,s_D,s_h,s_m,s_s
            print e_Y,e_M,e_D,e_h,e_m,e_s
            print new_code
            if code:
                i['url'] = 'http://42.96.141.199/cache?id=%s&bitrate=24&start=%sM%sD%sh%sm%ss%s&end=%sM%sD%sh%sm%ss%s' % (new_code,s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
                i['url'] = 'http://110.76.47.134/cache?id=%s&bitrate=24&start=%sM%sD%sh%sm%ss%s&end=%sM%sD%sh%sm%ss%s' % (new_code,s_Y,s_M,s_D,s_h,s_m,s_s,e_Y,e_M,e_D,e_h,e_m,e_s)
            i['file_name'] = u'%s/%s %s 《%s》 %s' % (self.dir,station,play_time.replace('-','').replace(' ','').replace(':',''),song,artist)
            print i.get('url',0),i.get('file_name',0)
            with file('url.txt','a')as f:
                f.write('%s %s\n' % (i.get('url'),i.get('file_name').encode('utf8')))
        return result


    #http://42.96.141.199/cache?id=60266&bitrate=24&start=14M02D10h22m00s00&end=14M02D10h23m00s00
    def get_music_top_all(self,s_type,artist_list = ['韩红','Eminem'],s_time = 300, e_time = 300, top_num = 10, day_num = 1):
        """
        获取歌曲排行榜的详情.
        s_type = 1,所有歌曲
        s_type = 2,英文歌曲
        s_type = 3,中文歌曲

        s_type = 4,独立音乐人歌曲


        artist_list,独立音乐人列表
        s_time ,前推多少分钟,默认5min 。
        e_time ,后推多少分钟,默认5min 。
        top_num,排行榜多少位，默认Top 10 。
        day_num,查询几天，默认1天 。
        """
        cur = self.conn.cursor(mysqldb.cursors.DictCursor)
        if s_type == 1 :
            #a = cur.execute("select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where o.time between date_sub(date(now()),interval %s day) and date(now()) group by s.name,s.artist order by count desc limit %s;" % (day_num, top_num))
            a = cur.execute("select s1.name,s1.artist,d1.name as station,d1.code,d1.new_code, o1.time,date_sub(o1.time, interval %s second)as start_time,date_sub(o1.time,interval -%s second)as end_time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station_code d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where o.time between date_sub(date(now()),interval %s day) and date(now()) group by s.name,s.artist order by count desc limit %s)tb1 on s1.id = tb1.song_id where o1.time between date_sub(date(now()),interval %s day) and date(now()) and s1.artist_id=tb1.artist_id;" % (s_time,e_time,day_num, top_num, day_num))
        elif s_type == 2:
            #a = cur.execute("select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.name not regexp '[^ -~]' and s.artist not regexp '[^ -~]' and o.time between date_sub(date(now()),interval %s day) and date(now()) group by s.name,s.artist order by count desc limit %s;" % (day_num, top_num))
            a = cur.execute("select s1.name,s1.artist,d1.name as station,d1.code,d1.new_code, o1.time,date_sub(o1.time, interval %s second)as start_time,date_sub(o1.time,interval -%s second)as end_time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station_code d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id,count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.name not regexp '[^ -~]' and s.artist not regexp '[^ -~]' and o.time between date_sub(date(now()),interval %s day) and date(now()) group by s.name,s.artist order by count desc limit %s)tb1 on s1.id = tb1.song_id where o1.time between date_sub(date(now()),interval %s day) and date(now()) and s1.artist_id=tb1.artist_id;" % (s_time,e_time,day_num, top_num, day_num))
        elif s_type == 3:
            pass
        elif s_type == 4:
            artist_str = str(tuple(artist_list))
            a = cur.execute("select s1.name,s1.artist,d1.name as station,d1.code,d1.new_code, o1.time,date_sub(o1.time, interval %s second)as start_time,date_sub(o1.time,interval -%s second)as end_time from song s1 inner join listen_log_once o1 on s1.id=o1.song inner join station_code d1 on d1.id=o1.stationId inner join (select s.name,s.artist,s.id as song_id,s.artist_id, count(*) as count from song s inner join listen_log_once o on s.id=o.song where s.artist in %s and o.time between date_sub(date(now()),interval %s day) and date(now()) group by s.name,s.artist order by count desc limit %s)tb1 on s1.id = tb1.song_id where o1.time between date_sub(date(now()),interval %s day) and date(now()) and s1.artist_id=tb1.artist_id;" % (s_time,e_time,artist_str,day_num, top_num,day_num))

        else:
            return 0
        r = cur.fetchall()
        cur.close()
        if a and r and len(r)>=1:return r
        else:return 0



if __name__ == '__main__':
    Main = MainClass()
    #Main.get_music_station_time_list(1,day_num =5000)
    Main.start(5,day_num =5000)
