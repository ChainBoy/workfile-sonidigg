#!/usr/bin/python
#-*- coding:utf8 -*-
from get_ts_by_file import Get_Music

def start(url,file_name):
    G = Get_Music(None)
    url_list = G.get_ts_urls(url)
    ts = G.download_ts_by_urls_list(url_list)
    file_name = "%s.ts" % file_name
    G.save_ts(file_name, ts)

if __name__ == "__main__":
    file_name = "20140131020000-20140131040000 贵州音乐广播"
    url = "http://110.76.47.134/cache?id=23937&bitrate=24&start=14M01D31h02m00s00&end=14M01D31h02m00s20"
    start(url, file_name)
