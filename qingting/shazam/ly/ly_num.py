#!/usr/bin/python
#-*- coding: utf-8 -*-
import MySQLdb
import datetime

SPLIT = ['，',',','&',' ','、','\\','/',';','@','＠','：','<br']

class Main():
    def __init__(self, splits = '', file_name = 'songs.txt'):
        try:
            self.splits = splits if splits else SPLIT
            self.file_name = file_name
            self.conn=MySQLdb.connect(host='localhost',user='root',passwd='654321',port=3306,db='shazam',charset='utf8')
            #conn.set_character_set('utf8')
        except MySQLdb.Error,e:
            print e.args


    def main(self, ly = True, co = True, ar = True):
        all_info = self.read_file()
        print ly,co,ar
        self.work(all_info, ly, co, ar)
    
    def get_song_count(self,song,artist):
        artist = artist.replace('"','\\"').replace("'","\\'")
        song = song.replace('"','\\"').replace("'","\\'")
        cur = self.conn.cursor(MySQLdb.cursors.DictCursor)
        a = cur.execute("select t1.count from (select song_id,artist_id,count(*)as count from onces group by song_id)as t1,songs,artists where t1.song_id = songs.id and t1.artist_id = artists.id and artists.name='%s' and songs.title='%s';" % (artist, song))
        r = cur.fetchall()
        #print r ({'count': 46L},)
        if r and len(r) > 0:
            r = int(r[0].get('count',0))
        else:
            r = 0
        cur.close()
        return r
        #conn.close()

    def cut_name(self, name):
        split = self.splits
        if len(split) > 0:
            for i in split:
                name = name.replace(i,'\t')
            name = name.split('\t')
        return name

    def read_file(self):
        with file(self.file_name, 'r')as f:
            r = f.read()
            r = r.splitlines()[1:]
        return r

    def write_result(self, dicts, file_name):
        result = sorted(dicts.iteritems(), key=lambda d:d[1])[::-1]
        with file(file_name, 'a')as f:
            for i in result:
                f.write('%s\t%s\n' % (i[0], str(i[1])))
        print file_name, 'save ok.'

    def add_dict(self, dicts, key, value):
        key = key.strip().lower()
        dicts[key] = value if key not in (lambda dd:[i.lower() for i in dd.keys()])(dicts) else dicts[key] + value
        return dicts

    def work(self, all_info, ly = True, co = True, ar = True):
            lys = {}
            cos = {}
            ars = {}
            lyname = 'lyricist' + str(datetime.datetime.now())[:-7].replace(' ','_')
            coname = 'composer' + str(datetime.datetime.now())[:-7].replace(' ','_')
            arname = 'arranger' + str(datetime.datetime.now())[:-7].replace(' ','_')
            for i in all_info:
                rl = i.split('\t')
                song, artist, num, lyricist, composer, arranger = rl
                num = self.get_song_count(song, artist)
                print song,artist,num
                if ly:
                    for j in self.cut_name(lyricist):
                        lys = self.add_dict(lys, j, num) if j.strip() else lys
                        #lys[j] = num if j not in lys else lys[j] + num
                if co:
                    for j in self.cut_name(composer):
                        cos = self.add_dict(cos, j, num) if j.strip() else cos
                if ar:
                    for j in self.cut_name(arranger):
                        ars = self.add_dict(ars, j, num) if j.strip() else ars
            self.write_result(lys, lyname )
            self.write_result(cos, coname )
            self.write_result(ars, arname )

if __name__ == '__main__':
    #print Main().get_song_count('终于等到你','张靓颖')
    #print Main().cut_name('终于&等,到-你')
    print Main().main()
