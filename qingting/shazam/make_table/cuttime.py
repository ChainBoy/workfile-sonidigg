#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
def get_alltime(start_time = '2014-02-11', end_time = '2014-02-11 01:00:00', hours = [11, 12, 13], not_add_week = []):
    null_time = ' 00:00:00'
    result = (start_time + null_time).split(' ')
    s_date,s_time = result[0:2]
    s_Y,s_M,s_D = s_date.split('-')
    s_h,s_m,s_s = s_time.split(':')
    result = (end_time + null_time).split(' ')
    e_date,e_time = result[0:2]
    e_Y,e_M,e_D = e_date.split('-')
    e_h,e_m,e_s = e_time.split(':')

    print s_Y,s_M,s_D,s_h,s_m,s_s
    print e_Y,e_M,e_D,e_h,e_m,e_s
    s_date_time = datetime.datetime(int(s_Y),int(s_M),int(s_D),int(s_h),int(s_m),int(s_s))
    e_date_time = datetime.datetime(int(e_Y),int(e_M),int(e_D),int(e_h),int(e_m),int(e_s))
    alltime = []
    temp_time = s_date_time
    if int(s_h) in hours:
        if not s_date_time.weekday() in not_add_week:
            alltime.append(str(s_date_time))
    onehour_timedelta = datetime.timedelta(0,3600)
    while True:
        temp_time = temp_time +  onehour_timedelta
        if temp_time >= e_date_time:
            break
        else:
            if temp_time.weekday() in not_add_week:
                continue
            if temp_time.hour in hours:
                alltime.append(str(temp_time))
    return alltime

if __name__ == '__main__':
    print get_alltime(start_time = '2014-11-11', end_time = '2014-11-12 12:00:00', hours = [1, 2, 5], not_add_week = [1])
