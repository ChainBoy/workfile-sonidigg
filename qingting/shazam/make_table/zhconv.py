# coding: utf-8

class Zh2Hans(object):
    # 初始化
    def __init__(self):
        dic_CN = self.mdic()
        [length,dic_RU] = self.sdic()
        dic_RU.update(dic_CN)
        self.dic_CN = dic_RU
        self.length = length

    # 最大正向匹配
    def conv(self,string,dic):
        i = 0
        while i < len(string):
            for j in range(len(string) - i, 0, -1):
                if string[i:][:j] in dic:
                    t = dic[string[i:][:j]]
                    string = string[:i] + t + string[i:][j:]
                    i += len(t) - 1
                    break
            i += 1
        return string

    # 生成转换字典
    def mdic(self):
        table = open('ZhConversion.php','r').readlines()
        dic = dict()
        name = []
        for line in table:
            if line[0] == '$':
                #print line.split()[0][1:]
                name.append(dic)
                dic = dict()
            if line[0] == "'":
                word = line.split("'")
                dic[word[1]] = word[3]
        #name[3].update(name[1]) # 简繁通用转换规则(zh2Hant)加上台湾区域用法(zh2TW)
        #name[4].update(name[1]) # 简繁通用转换规则(zh2Hant)加上香港区域用法(zh2HK)
        name[5].update(name[2]) # 繁简通用转换规则(zh2Hans)加上大陆区域用法(zh2CN)
        #return name[3],name[4],name[5]
        return name[5]

    # 自定义规则song.txt生成转换字典
    def sdic(self):
        table = open('song.txt','r').readlines()
        dic = dict()
        for line in table:
            #print line.strip()
            song = line.strip().split('>>>')
            s0 = song[0].split('>>')
            s1 = song[1].split('>>')
            #print '\n'
            #print s0[0], '==>', s1[0]
            #print s0[1], '==>', s1[1]
            dic[s0[0]] = s1[0]
            dic[s0[1]] = s1[1]
        return len(table), dic

    def f2j(self,string):
        table = open('song.txt','r').readlines()
        num = len(table) - self.length
        if num > 0:
            _table = table[self.length-1:]
            dic = dict()
            for line in _table:
                song = line.strip().split('>>>')
                s0 = song[0].split('>>')
                s1 = song[1].split('>>')
                dic[s0[0]] = s1[0]
                dic[s0[1]] = s1[1]
            dic.update(self.dic_CN)
            self.dic_CN = dic
            self.length += num
        if isinstance(string,unicode):
            string = string.encode('utf-8')
        result = self.conv(string,self.dic_CN)
        return result

if __name__=="__main__":
    #a="头发发展萝卜卜卦秒表表达 "
    #b="大衛碧咸在寮國見到了布希"
    #c="大卫·贝克汉姆在老挝见到了布什"

    #[dic_TW,dic_HK,dic_CN] = mdic()
    #str_TW = conv(a,dic_TW)
    #str_HK = conv(c,dic_HK)
    #str_CN = conv(b,dic_CN)
    #print a, ' <-> ', str_TW, '\n', c, ' < -> ',str_HK,'\n', b,' < -> ',str_CN
    import time
    while True:
        zh = Zh2Hans()
        p = 'Song=Wo He Shang Guan Yan'
        p1 = 'Artist=Zhao Wei'
        p2 = 'Song=ni zhi dao wo zai deng ni ma'
        p3 = 'Artist=zhang hong liang'
        print p,'<->',zh.f2j(p)
        print p1,'<->',zh.f2j(p1)
        print p2,'<->',zh.f2j(p2)
        print p3,'<->',zh.f2j(p3)
        time.sleep(3)
