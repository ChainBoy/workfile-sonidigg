from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, BigInteger, Text
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, backref
Base = declarative_base()

class Station(Base):
    __tablename__ = 'stations'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    time = Column(DateTime)
    def __init__(self, name, time):
        self.time = time
        self.name = name

    def __repr__(self):
        return self.name


class Song(Base):
    __tablename__ = 'songs'
    id = Column(Integer, primary_key=True)
    title = Column(String(100))
    artist_id = Column(Integer, ForeignKey('artists.id'))
    def __init__(self, title, artist_id):
        self.title = title
        self.artist_id = artist_id

    def __repr__(self):
        return "<Song('%s')>" % (self.title)

class Artist(Base):
    __tablename__ = 'artists'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    songs = relationship('Song', backref = 'artists')
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<artist('%s')>" % (self.name)

class Once(Base):
    # get how times of playing on a specific station

    __tablename__ = 'onces'
    id = Column(Integer, primary_key=True)
    station_id = Column(Integer, ForeignKey('stations.id'))
    # can access artist_id by song_id
    artist_id = Column(Integer, ForeignKey('artists.id'))
    song_id = Column(Integer, ForeignKey('songs.id'))
    time = Column(DateTime)
    pad_id = Column(Integer)
    def __init__(self, song_id, artist_id, station_id, time, pad_id):
        self.song_id = song_id
        self.artist_id = artist_id
        self.station_id = station_id
        self.time = time
        self.pad_id = pad_id

class Times(Base):
    # get playing time 
    __tablename__ = 'times'
    id = Column(Integer, primary_key=True)
    station_id = Column(Integer, ForeignKey('stations.id'))
    # can access artist_id by song_id
    artist_id = Column(Integer, ForeignKey('artists.id'))
    song_id = Column(Integer, ForeignKey('songs.id'))
    once_id = Column(Integer, ForeignKey('onces.id'))
    time = Column(DateTime)

    def __init__(self, song_id, artist_id, station_id, once_id, time):
        self.song_id = song_id
        self.artist_id = artist_id
        self.station_id = station_id
        self.once_id = once_id
        self.time = time

def init_db(engine):
    Base.metadata.create_all(engine)


