#!/usr/bin/env python
# coding: utf-8
from models import *
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import func, and_, or_, not_
from sqlalchemy.sql import label
from optparse import OptionParser
import xlwt
from zhconv import Zh2Hans
import os
import cuttime
import random
import datetime

OPTIONS={}

class StationTop10(object):
    def __init__(self, host, is_top=True, names = [], port=3306):
        DATABASE = {'NAME': 'shazam',
                'USER': 'root',
                'PASSWORD': '654321',
                'HOST': host,
                'PORT': port,}
        db_path = 'mysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{NAME}?charset=utf8'.format(**DATABASE)
        engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
        self.db = scoped_session(sessionmaker(bind=engine))
        if is_top:
            #9000
            if not host == 'localhost':
                self.top10 = [[6, '北京通俗音乐', 1740],
                    [44, '四川城市之音', 1405],
                    [92, '四川岷江音乐频率', 1402],
                    [29, '江西音乐广播', 1317],
                    [84, '天津音乐广播', 1308],
                    [93, '江苏经典流行音乐', 1278],
                    [32, '陕西音乐广播', 1266],
                    [52, '山东城市之音', 1252],
                    [91, '江苏音乐魅力897', 1232],
                    [103, '河南音乐广播',1231]]

            #8000
            else:
                self._top10 = [[41, '北京通俗音乐', 1740],
                    [56, '四川城市之音', 1405],
                    [53, '四川岷江音乐频率', 1402],
                    [6, '江西音乐广播', 1317],
                    [47, '天津音乐广播', 1308],
                    [63, '江苏经典流行音乐', 1278],
                    [14, '陕西音乐广播', 1266],
                    [55, '山东城市之音', 1252],
                    [62, '江苏音乐魅力897', 1232],
                    [52, '河南音乐广播',1231]]
        else:
            if names and len(names)>0:
                if not host == 'localhost':
                    #9000
                    self.top10 = self.getlist(names)
                else:
                    #8000
                    self._top10 = self.getlist(names)

    def getlist(self, names):
        result = self.db.query(Once.station_id, Station.name, label('count', func.count('*'))).filter(Station.name.in_(names)).join(Station, Once.station_id==Station.id).group_by(Once.station_id).order_by('count desc').all()
        return result

    def countTop10(self):
        top10 = self.db.query(Once.station_id, Station.name, label('count', func.count('*'))).join(Station, Once.station_id==Station.id).group_by(Once.station_id).order_by('count desc').limit(10).all()
        #print top10
        #for top in top10:
        #   print top.station_id, top.name, top.count
        return top10

    def queryone(self, num, top, stime='2014-02-01', etime='2014-03-01'):
        station = {}
        station['top'] = num
        station['id'] = top[0]
        station['name'] = top[1]
        station['count'] = top[2]
        print 'Top %d %s %d start...' % (num, top[1], top[2])
        station_id = top[0]
        station_name = top[1]
        songs = self.db.query(Once.time, Song.title, Artist.name, Station.name).join(Song, Once.song_id==Song.id).join(Artist, Once.artist_id==Artist.id).join(Station, Once.station_id==Station.id).filter(Once.  station_id==station_id, Once.time>=stime, Once.time<etime).order_by(Once.time).all()
        station['songs'] = songs
        return station

    def querydata(self, top10, stime='2014-02-01', etime='2014-03-01'):
        data = []
        num = 0
        for top in top10:
            num += 1
            data.append(self.queryone(num, top, stime, etime))
            print 'Top %d %s %d end.' % (num, top[1], top[2])
        return data

    def writefile(self, songs, filename):
        songs = sorted(songs, key=lambda x: x[0])
        if not os.path.isdir('result'):
            os.mkdir('result')
        filename = 'result/' + filename + '.xls'
        #print filename
        zh = Zh2Hans()
        wbk = xlwt.Workbook()
        sheet = wbk.add_sheet('sheet1')
        style = xlwt.easyxf('font: bold 1')
        rows = 0
        for song in songs:
            #print song[0], song[1], song[2], song[3]
            #sheet.write(rows, 0, 'abcd')
            songname = zh.f2j('Song='+song[1]).replace('Song=', '')
            artistname = zh.f2j('Artist='+song[2]).replace('Artist=', '')
            sheet.write(rows, 0, songname.decode('utf-8'), style)
            sheet.write(rows, 1, artistname.decode('utf-8'), style)
            sheet.write(rows, 2, song[3], style)
            sheet.write(rows, 3, str(song[0]), style)
            rows += 1
        wbk.save(filename)

    def _start(self):
        top10 = self.countTop10()
        self.querydata(top10)


class Insert_Null(object):
    def __init__(self, host ='localhost', port=3306):
        DATABASE = {'NAME': 'shazam',
                'USER': 'root',
                'PASSWORD': '654321',
                'HOST':host, 
                'PORT': port,}
        db_path = 'mysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{NAME}?charset=utf8'.format(**DATABASE)
        engine = create_engine(db_path, convert_unicode=True, pool_recycle=7200)
        self.db = scoped_session(sessionmaker(bind=engine))
        self.song_count()

    def add_null(self,station_name, songs, stime, etime, snum=2, enum=10, times=[], not_add_weeks=[]):
        print times
        if self.count:
            if len(etime.split(' ')) == 1:
                Y,M,D = etime.split('-')
                etime = str(datetime.datetime(int(Y), int(M), int(D)) + datetime.timedelta(days=1))
            alltime = cuttime.get_alltime(stime, etime, times, not_add_weeks)
            alltime_dict = {}
            for i in alltime:
                if len(times) > 0 and int(i.split(':')[0][-2:]) in times:
                    alltime_dict[i.split(':')[0]] = 0
                    continue
            for i in songs:
                j = str(i[0]).split(':')[0]
                print j,
                if j in alltime_dict.keys():
                    print '',j
                    alltime_dict[j]= alltime_dict[j] + 1
            for i in alltime_dict:
                print i,alltime_dict[i]
                if alltime_dict[i] == 0:
                    songs.extend(self.add_data(station_name, i, snum, enum))
            with file('test.test','a')as f:
                for i in songs:
                    f.write(str(i) + '\n')
            return songs
        else:
            print 'Add null data failed,no song.'
            return songs

    def add_data(self, station_name, date, snum = 2, enum = 10):
        num = random.randint(snum,enum)
        time_list = self.make_time(date, num)
        song_list = self.get_songs(num)
        result = (lambda lx,ly,s : [(datetime.datetime.strptime(ly[i],'%Y-%m-%d %H:%M:%S'), ) + tuple(j) + (s, ) for i,j in enumerate(lx)])(song_list, time_list, station_name)
        for i in result:
            print 'Add null:',num , i
        return result

    def get_songs(self, num = 5):
        rlist = self.make_random(num, 1, self.count)
        result = self.db.query(Song.title, Artist.name).filter(Song.artist_id == Artist.id, Song.id.in_(rlist)).all()
        #songs = self.db.query(Once.time, Song.title, Artist.name, Station.name).join(Song, Once.song_id==Song.id).join(Artist, Once.artist_id==Artist.id).join(Station, Once.station_id==Station.id).filter(Once.station_id==station_id, Once.time>=stime, Once.time<etime).order_by(Once.time).all()
        return result
    def make_time(self, date, num = 5, snum = 1, enum = 59):
        result = []
        rlist = self.make_random(num, snum, enum)
        for i in rlist:
            t  = random.randint(0,50)
            result.append('%s:%.2d:%.2d' % (date, i, random.randint(0,50)))
        return result

    def make_random(self, num = 5, snum = 1, enum = 59):
        l = set([])
        while True:
            l.add(random.randint(snum,enum))
            if len(l) == num:
                return l

    def song_count(self):
        #count = self.db.query(label('count', func.count(Song.id)))
        self.count = self.db.query(func.count(Song.id)).scalar()

def nmain(stations_name,stime,etime,insert = 0, snum=2, enum=10, times=[], not_add_weeks=[]):
    is_top = True
    if stations_name:
        names = []
        for i in list(set(stations_name.split(','))):
            if i.strip():
                names.append(i.strip())
        if len(names) > 0 :
            is_top = False
    stt = StationTop10('localhost',is_top, names, 3306)
    local_data = stt.querydata(stt.top10, stime, etime)
    print 'query data %s len.' % len(local_data)
    for i in range(len(local_data)):
        station_name = local_data[i]['name']
        songs = local_data[i]['songs']
        if insert:
            songs = Insert_Null(host='localhost',port=3306).add_null(station_name, songs, stime, etime, snum, enum, times, not_add_weeks)
            print songs[0]
        if len(times) > 0:
            stt.writefile(songs, station_name + '_new')
        else:
            stt.writefile(songs, station_name)

def main(stations_name,stime,etime,insert = 0, snum=2, enum=10, times=[]):
    is_top = True
    if stations_name:
        names = []
        for i in list(set(stations_name.split(','))):
            if i.strip():
                names.append(i.strip())
        if len(names) > 0 :
            is_top = False
    stt = StationTop10('192.168.10.10',is_top, names)
    local_data = stt.querydata(stt.top10, stime,etime)
    stt1 = StationTop10('localhost',is_top, names)
    long_data = stt1.querydata(stt1._top10, stime,etime)
    for i in range(len(local_data)):
        station_name = local_data[i]['name']
        _station_name = long_data[i]['name']
        print station_name, _station_name
        songs = local_data[i]['songs']
        _songs = long_data[i]['songs']
        songs_dic = [song[0] for song in songs]
        for song in _songs:
            if song[0] not in songs_dic:
                print 'add', song[0], song[1]
                songs.append(song)
            else:
                print song[0], song[1], 'already exists!'
                songs = sorted(songs, key=lambda x: x[0])
        if insert:
            songs = Insert_Null().add_null(station_name, songs, stime, etime, snum, enum, times)
        stt.writefile(songs, station_name)

def check_option():
    parse = OptionParser()
    parse_list = ['-s','-e','-n','-i','-x','-y','-n','-t', 'w']
    parse.add_option('-s','--stime',dest='stime',help='input start time.',type='string', default= '', nargs = 1)
    parse.add_option('-e','--etime',dest='etime',help='input end time.',type='string',default='', nargs = 1)
    parse.add_option('-n','--name',dest='name',help='input some station name.split by space',type='string',default='')
    parse.add_option('-i','--insert',dest='insert',help='insert data to result.', type='int', default=0, nargs = 1)
    parse.add_option('-x','--snum',dest='snum',help='input start num.',type='int', default= 2, nargs = 1)
    parse.add_option('-y','--enum',dest='enum',help='input end num.',type='int',default=10, nargs = 1)
    parse.add_option('-t','--time',dest='time',help='continue some time(24 hour),ex:0 3 or 22-24',type='string', default= '')
    parse.add_option('-w','--notweek',dest='notweek',help='input not insert data for week 1-7,ex:2 5 6',type='string', default= '')
    (options, args) = parse.parse_args()
    if options.name in parse_list:
        parse.error('please input stations name.')
    if not options.stime and not options.etime:
        parse.error('please input start time and end time.')
    if options.insert == 1:
        if options.notweek in parse_list:
            parse.error('please input some not nead add data for weekday,ex:1 6 or 5')
        if len(options.notweek) > 0:
            try:
                options.notweek = [int(i.strip()) for i in options.notweek.split(' ') if 0 < int(i) < 8]
            except Exception:
                parse.error('please input some not nead add data for weekday,ex:1 6 or 5')
                options.notweek = []

        if options.time in parse_list:
            parse.error('please input some nead dump time,ex:1 4 or 0-6 or 22-24.')
        if options.time and len(options.time) > 0:
            times = options.time.strip().split(' ')
            ntime = []
            for i in times:
                try:
                    ntime.append(int(i))
                except Exception:
                    print 'Error'
                    r = i.split('-')
                    if r and len(r) == 2:
                        try:
                            r = [int(k) for k in r]
                            r.sort()
                            print r
                            for j in range(int(r[0]), int(r[1])):
                                ntime.append(int(j))
                        except Exception, e:
                            parse.error('please input some nead dump time,ex:1 4 or 0-6 or 22-24')
            options.time = list(set(ntime))
            print options.time
        else:
            options.time = []




    #return options.name,options.stime,options.etime,options.insert]
    return options


if __name__ == '__main__':
    #stt = StationTop10('localhost')
    #print stt.top10
    #stt.querydata(stt.top10)
    OPTIONS = check_option()
    print OPTIONS
    #main(OPTIONS.name, OPTIONS.stime, OPTIONS.etime, OPTIONS.insert, OPTIONS.snum, OPTIONS.enum ,OPTIONS.time)
    nmain(OPTIONS.name, OPTIONS.stime, OPTIONS.etime, OPTIONS.insert, OPTIONS.snum, OPTIONS.enum ,OPTIONS.time, OPTIONS.notweek)

