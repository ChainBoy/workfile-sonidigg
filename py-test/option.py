from optparse import OptionParser

def check_option():
    parse = OptionParser()
    parse.add_option('-s','--stime',dest='stime',help='input start time.',type='string', default= '', nargs = 1)
    parse.add_option('-e','--etime',dest='etime',help='input end time.',type='string',default='', nargs = 1)
    parse.add_option('-n','--name',dest='name',help='input some station name.split by space',type='string',default='')
    parse.add_option('-i','--insert',dest='insert',help='insert data to result.', type='int', default=0, nargs = 1)
    (options, args) = parse.parse_args()
    if options.name in ['-s','-e','-n','-i']:
        parse.error('please input stations name.')
    if not options.stime and not options.etime:
        parse.error('please input start time and end time.')
    print options.name,options.stime,options.etime,options.insert]
    return options

