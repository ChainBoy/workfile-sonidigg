#!coding=utf-8
import threading #线程库
import Queue #队列库
import string #字符串操作库


#继承Thread
class g(threading.Thread):
    def __init__(self, que):
        threading.Thread.__init__(self)
        self.que = que
    def run (self):#重写run方法
        while True:
            if not self.que.empty():
                print "%s:%s," % (self.name, self.que.get())#输出线程名字，取队列内容
            else:
                break
que = Queue.Queue() #实例一个queue队列
thread_num = 5 #要开启的线程数量
k = string.ascii_letters #+ string.digits #大小写字母，数字
for i in k:
    que.put(i) #将大小写字母填充至队列(貌似是生产者?)，也就是生成需要用到的资源，比如文件的url等
for i in range(1, thread_num + 1):
    h = g(que) #实例指定数量的线程
    h.start() #启动线程
