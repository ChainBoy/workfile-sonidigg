数据库初始化说明
1.area_inter.sql   --- 全球省市县联级表(国内三级,国外两级)
2.db.mwb  ---  数据库模型
3.db.sql  ---  DB初始化sql
4.db_tigger.sql ---  DB事务sql
5.db.png  ---  示例图
5.ReadMe.txt  ---  说明

aomen_taiwan.sql --> 补充澳门,台湾的sql
taiwan_area.py --> 生成补充台湾省的sql代码
area_inter_result.sql -->最终的国省州市县区划分表

用法说明：
1.运行db.sql --- 创建数据库
2.运行db_tigger.sql  ---  创建数据库事务
3.运行area_inter.sql ---  初始化地区联级表
4.Code表说明，很多的字段都指向Code表，例如：

ID    Name    PID
10000 歌曲风格  0
20000 歌曲节奏型 0
30000 歌曲调性 0
10001 摇滚 10000
10002 古典 10000
20001 2/4拍 20000
20002 4/4拍 20000
30001 A调 30000
30002 #F调 30000
...
调性有：G调 D调 A调 E调 B调 ＃F调 ＃C调 F调 bB调 bE调 bA调 bD调 bG调 bC调(http://zhidao.baidu.com/question/266649886.html)
节奏型：1/4,2/4,3/4,4/4,3/8,6/8,7/8,9/8,12/8（节拍?节奏型?。。。）
