SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `DB_Music` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `DB_Music` ;

-- -----------------------------------------------------
-- Table `DB_Music`.`Companys`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Companys` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '公司名称' ,
  `Time` DATE NULL DEFAULT NULL COMMENT '公司创建时间' ,
  `Summary` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '公司简介' ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '公司表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Code`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Code` (
  `ID` INT(11) NOT NULL DEFAULT '0' COMMENT '常量编号' ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '常量名' ,
  `PID` INT(11) NULL DEFAULT '0' COMMENT '常量父级编号,默认为0.' ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '常量标识表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Albums`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Albums` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '专辑名' ,
  `Time` DATE NULL DEFAULT NULL COMMENT '发行时间' ,
  `Company_ID` INT(11) NULL DEFAULT NULL COMMENT '专辑所属公司 ID  FK' ,
  `Summary` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '专辑简介' ,
  `Region_ID` INT(11) NULL DEFAULT NULL COMMENT '地区(Ex:港台，内地，欧美等，默认为其他   FK' ,
  `Lanuage_ID` INT(11) NULL DEFAULT NULL COMMENT '歌曲语言(Ex:港语,英语等，默认为其他  FK' ,
  `Pic` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '图片地址(Ex:url，file name)' ,
  `Producer` VARCHAR(300) NULL COMMENT '制作人' ,
  `Downmix` VARCHAR(300) NULL COMMENT '缩混人,以‘,’隔开即可' ,
  `Mastering` VARCHAR(300) NULL COMMENT '母带人,以‘,’隔开即可' ,
  `Record` VARCHAR(300) NULL COMMENT '录音乐手' ,
  `Price` FLOAT NULL DEFAULT 0 COMMENT '价格，RMB' ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '专辑表示列guid' ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `Key_UNIQUE` (`Key` ASC) ,
  INDEX `fk_Albums_Companys1` (`Company_ID` ASC) ,
  INDEX `fk_Albums_Code1` (`Region_ID` ASC) ,
  INDEX `fk_Albums_Code2` (`Lanuage_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '专辑表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Area`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Area` (
  `Area_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '地区ID' ,
  `Title` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL COMMENT '地区名称' ,
  `PID` INT(11) NOT NULL COMMENT '父级地区ID' ,
  `Sort` INT(11) NOT NULL COMMENT '排序值' ,
  `Content` VARCHAR(255) NOT NULL COMMENT '全地址' ,
  `Code` VARCHAR(10) NULL COMMENT '国际代码编号,比如 美国USA,' ,
  PRIMARY KEY (`Area_ID`) ,
  INDEX `PID` (`PID` ASC) )
ENGINE = MyISAM
AUTO_INCREMENT = 910011
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `DB_Music`.`Artists`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Artists` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT '佚名' COMMENT '歌手姓名' ,
  `A_Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌手别名' ,
  `Sex` INT(11) NULL DEFAULT '10101' COMMENT '性别' ,
  `Region_ID` INT(11) NULL DEFAULT NULL COMMENT '歌手地区(Ex:港台，内地，欧美等，默认为其他   FK' ,
  `Lanuage_ID` INT(11) NULL DEFAULT NULL COMMENT '歌手语言(Ex:港语,英语等，默认为其他  FK' ,
  `Nationlity_ID` INT(11) NULL COMMENT '歌手国籍 (Ex:中国,法国,韩国等 FK' ,
  `Address_ID` INT(11) NULL DEFAULT NULL COMMENT '出生地 ID' ,
  `Birthday` DATE NULL DEFAULT NULL COMMENT '生日' ,
  `Height` INT(11) NULL DEFAULT '0' COMMENT '身高 cm' ,
  `Weight` FLOAT NULL DEFAULT '0' COMMENT '体重 kg' ,
  `Summary` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌手简介' ,
  `Pic` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '图片 网址或者文件地址等' ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'guid 列' ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `Key_UNIQUE` (`Key` ASC) ,
  INDEX `fk_Artists_Area1` (`Address_ID` ASC) ,
  INDEX `fk_Artists_Area2` (`Nationlity_ID` ASC) ,
  INDEX `fk_Artists_Code1` (`Region_ID` ASC) ,
  INDEX `fk_Artists_Code2` (`Lanuage_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌手';


-- -----------------------------------------------------
-- Table `DB_Music`.`Groups`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Groups` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '乐队组合名字' ,
  `Time` DATE NULL DEFAULT NULL COMMENT '组合时间' ,
  `Company_ID` INT(11) NULL DEFAULT NULL COMMENT '所属公司' ,
  `Summary` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '乐队简介' ,
  `Region_ID` INT(11) NULL DEFAULT NULL COMMENT '地区(Ex:港台，内地，欧美等，默认为其他   FK' ,
  `Lanuage_ID` INT(11) NULL DEFAULT NULL COMMENT '地区(Ex:港台，内地，欧美等，默认为其他   FK' ,
  `Pic` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '图片地址(Ex:url，file name)' ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '组合标识列 guid' ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `Key_UNIQUE` (`Key` ASC) ,
  INDEX `fk_Groups_Companys1` (`Company_ID` ASC) ,
  INDEX `fk_Groups_Code1` (`Region_ID` ASC) ,
  INDEX `fk_Groups_Code2` (`Lanuage_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '乐队/组合表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Lyrics`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Lyrics` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '自增' ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌词标识列 guid,如果能对应上歌曲的key则为歌曲的key，对应不上则自动生成' ,
  `Title` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌词标题(歌名)' ,
  `MakeLyric` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '作词' ,
  `Content` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌词内容' ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `Key_UNIQUE` (`Key` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌词表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Songs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Songs` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '自增列' ,
  `Name` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌曲名' ,
  `Lyric_ID` INT(11) NULL DEFAULT NULL COMMENT '歌词   FK' ,
  `Time` DATE NULL DEFAULT NULL COMMENT '歌曲发布时间' ,
  `Region_ID` INT(11) NULL DEFAULT NULL COMMENT '地区(Ex:港台，内地，欧美等，默认为其他   FK' ,
  `Lanuage_ID` INT(11) NULL DEFAULT NULL COMMENT '歌曲语言(Ex:港语,英语等，默认为其他  FK' ,
  `From` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT 'other' COMMENT '来源(Ex:http://...mp3,百度音乐,QQ音乐等...' ,
  `Pic` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '图片地址(Ex:url，file name)' ,
  `IsGroup` INT(11) NULL DEFAULT '0' COMMENT '是否为乐队组合之类,默认为0非组合(歌手)。0-歌手，1-乐队组合' ,
  `Summary` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '歌曲简介' ,
  `Len` INT NULL DEFAULT 0 COMMENT '歌曲长度，秒为单位' ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'guid 列' ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `Key_UNIQUE` (`Key` ASC) ,
  INDEX `fk_Songs_Lyrics1` (`Lyric_ID` ASC) ,
  INDEX `fk_Songs_Code1` (`Region_ID` ASC) ,
  INDEX `fk_Songs_Code2` (`Lanuage_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌曲表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Song_Album`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Song_Album` (
  `Album_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '专辑GUID' ,
  `Song_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '歌曲GUID' ,
  INDEX `fk_Song_Album_Albums1` (`Album_Key` ASC) ,
  INDEX `fk_Song_Album_Songs1` (`Song_Key` ASC) ,
  CONSTRAINT `fk_Song_Album_Albums1`
    FOREIGN KEY (`Album_Key` )
    REFERENCES `DB_Music`.`Albums` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Song_Album_Songs1`
    FOREIGN KEY (`Song_Key` )
    REFERENCES `DB_Music`.`Songs` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '专辑，歌曲对应关系';


-- -----------------------------------------------------
-- Table `DB_Music`.`Song_Artist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Song_Artist` (
  `Artist_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '歌手GUID' ,
  `Song_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '歌曲GUID' ,
  INDEX `fk_Song_Artist_Artists1` (`Artist_Key` ASC) ,
  INDEX `fk_Song_Artist_Songs1` (`Song_Key` ASC) ,
  CONSTRAINT `fk_Song_Artist_Artists1`
    FOREIGN KEY (`Artist_Key` )
    REFERENCES `DB_Music`.`Artists` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Song_Artist_Songs1`
    FOREIGN KEY (`Song_Key` )
    REFERENCES `DB_Music`.`Songs` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌手，歌曲对应关系';


-- -----------------------------------------------------
-- Table `DB_Music`.`Song_Group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Song_Group` (
  `Group_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '乐队GUID' ,
  `Song_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '歌曲GUID' ,
  INDEX `fk_Song_Group_Groups1` (`Group_Key` ASC) ,
  INDEX `fk_Song_Group_Songs1` (`Song_Key` ASC) ,
  CONSTRAINT `fk_Song_Group_Groups1`
    FOREIGN KEY (`Group_Key` )
    REFERENCES `DB_Music`.`Groups` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Song_Group_Songs1`
    FOREIGN KEY (`Song_Key` )
    REFERENCES `DB_Music`.`Songs` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '乐队，歌曲对应关系';


-- -----------------------------------------------------
-- Table `DB_Music`.`Tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Tags` (
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '标签ID，对应song，group，artist，album的Key值。 形式FK!' ,
  `Code_ID` INT(11) NULL DEFAULT NULL COMMENT '标签名字 (Ex: 流行/DJ/校园/... FK --  Code_ID' ,
  PRIMARY KEY (`Key`) ,
  INDEX `fk_Tags_Code` (`Code_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '存放歌曲/歌手/乐队/专辑的标签记录';


-- -----------------------------------------------------
-- Table `DB_Music`.`Album_Artist`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Album_Artist` (
  `Artist_Key` CHAR(32) NOT NULL ,
  `Album_Key` CHAR(32) NOT NULL ,
  INDEX `fk_Album_Artist_Artists1` (`Artist_Key` ASC) ,
  INDEX `fk_Album_Artist_Albums1` (`Album_Key` ASC) ,
  CONSTRAINT `fk_Album_Artist_Artists1`
    FOREIGN KEY (`Artist_Key` )
    REFERENCES `DB_Music`.`Artists` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Album_Artist_Albums1`
    FOREIGN KEY (`Album_Key` )
    REFERENCES `DB_Music`.`Albums` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '专辑-歌手 对应表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Album_Group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Album_Group` (
  `Group_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '乐队key' ,
  `Album_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '专辑key' ,
  INDEX `fk_Album_Group_Groups1` (`Group_Key` ASC) ,
  INDEX `fk_Album_Group_Albums1` (`Album_Key` ASC) ,
  CONSTRAINT `fk_Album_Group_Groups1`
    FOREIGN KEY (`Group_Key` )
    REFERENCES `DB_Music`.`Groups` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Album_Group_Albums1`
    FOREIGN KEY (`Album_Key` )
    REFERENCES `DB_Music`.`Albums` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '专辑 乐队 关系表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Artist_Group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Artist_Group` (
  `Group_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '乐队key' ,
  `Artist_Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '歌手 key' ,
  INDEX `fk_Artist_Group_Groups1` (`Group_Key` ASC) ,
  INDEX `fk_Artist_Group_Artists1` (`Artist_Key` ASC) ,
  CONSTRAINT `fk_Artist_Group_Groups1`
    FOREIGN KEY (`Group_Key` )
    REFERENCES `DB_Music`.`Groups` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Artist_Group_Artists1`
    FOREIGN KEY (`Artist_Key` )
    REFERENCES `DB_Music`.`Artists` (`Key` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌手 乐队 对应表';


-- -----------------------------------------------------
-- Table `DB_Music`.`Melodys`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Melodys` (
  `ID` INT NOT NULL ,
  `Key` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL COMMENT 'key，如果能对应song Key，否则自动成生' ,
  `BPM` INT NULL DEFAULT 0 COMMENT '歌曲节拍速度' ,
  `Rhythm_ID` INT NULL COMMENT '节奏型\n1/4拍 2/4拍 3/4拍 4/4拍 3/8拍 6/8拍' ,
  `Tone_ID` INT NULL COMMENT '调型\nG调 D调 A调 E调 B调 ＃F调 ＃C调 F调 bB调 bE调 bA调 bD调 bG调 bC调' ,
  PRIMARY KEY (`ID`) ,
  INDEX `fk_Melody_Code1` (`Rhythm_ID` ASC) ,
  INDEX `fk_Melody_Code2` (`Tone_ID` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '歌曲特征表,作曲人,bpm,节奏型,调性';


-- -----------------------------------------------------
-- Table `DB_Music`.`Station_Code`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Station_Code` (
  `ID` INT NOT NULL ,
  `Name` VARCHAR(300) NULL ,
  `PID` INT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '电台常量表 ';


-- -----------------------------------------------------
-- Table `DB_Music`.`Station`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `DB_Music`.`Station` (
  `ID` INT NOT NULL COMMENT '电台编号' ,
  `Media_ID` INT NULL COMMENT '电台流编号' ,
  `Name` VARCHAR(300) NULL COMMENT '电台名' ,
  `Code_ID` INT NULL COMMENT '省份(分类)编号' ,
  `Frequency` FLOAT NULL DEFAULT 0 COMMENT '频率' ,
  `KeyWord` VARCHAR(300) NULL COMMENT '关键字' ,
  `Letter` VARCHAR(300) NULL COMMENT '拼音' ,
  `Summary` TEXT NULL COMMENT '电台简介' ,
  `Bitrate` INT NULL DEFAULT 24 COMMENT '比特率' ,
  `URL_M1` VARCHAR(300) NULL COMMENT 'm3u8网址\n阿里第一服务器：\nEx:http://42.96.141.199/live/332.m3u8?bitrate=24' ,
  `URL_M2` VARCHAR(300) NULL COMMENT 'm3u8网址\n阿里第二服务器\nEx:http://110.76.47.134/live/332.m3u8?bitrate=24' ,
  `URL_1` VARCHAR(300) NULL COMMENT 'mp3网址,Exhttp://122.49.28.30/fm974.mp3' ,
  `URL_2` VARCHAR(300) NULL COMMENT 'rtspt网址，Ex:rtspt://alive.rbc.cn/fm974' ,
  `URL_3` VARCHAR(300) NULL COMMENT '第三备用网址' ,
  `Pic` VARCHAR(300) NULL COMMENT '图片地址' ,
  PRIMARY KEY (`ID`) ,
  INDEX `fk_Station_Station_Code1` (`Code_ID` ASC) ,
  CONSTRAINT `fk_Station_Station_Code1`
    FOREIGN KEY (`Code_ID` )
    REFERENCES `DB_Music`.`Station_Code` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '电台表';



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
