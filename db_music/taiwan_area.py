#!-*- coding:utf8 -*-
#获取台湾省市县的行政划分
#
#补充 台中市 区行政划分
taizhong_q = """
insert into Area values(100710401,'中区',100710400,'台湾省 台中市 中区',1,NULL);
insert into Area values(100710402,'东区',100710400,'台湾省 台中市 中区',2,NULL);
insert into Area values(100710403,'西区',100710400,'台湾省 台中市 中区',3,NULL);
insert into Area values(100710404,'南区',100710400,'台湾省 台中市 中区',4,NULL);
insert into Area values(100710405,'北区',100710400,'台湾省 台中市 中区',5,NULL);
insert into Area values(100710406,'西屯区',100710400,'台湾省 台中市 中区',6,NULL);
insert into Area values(100710407,'南屯区',100710400,'台湾省 台中市 中区',7,NULL);
insert into Area values(100710408,'北屯区',100710400,'台湾省 台中市 中区',8,NULL);
"""
import urllib2
import re
s = """100710100  台北市
100710200  高雄市
100710300  基隆市
100710400  台中市
100710500  台南市
100710600  新竹市
100710700  嘉义市
100710800  台北县
100710900  宜兰县
100711000  新竹县
100711100  桃园县
100711200  苗栗县
100711300  台中县
100711400  彰化县
100711500  南投县
100711600  嘉义县
100711700  云林县
100711800  台南县
100711900  高雄县
100712000  屏东县
100712100  台东县
100712200  花莲县
100712300  澎湖县"""
l = s.splitlines()
for i in l:
    lr = i.split(' ')
    n_1 = lr[2]
    i_1 = int(lr[0])
    url = 'http://www.baike.com/wiki/' + n_1.decode('raw_unicode_escape').encode('unicode_escape').replace('\\','%').replace('x','').upper()
    #print url
    u = urllib2.urlopen(url)
    r = u.read()
    with file('result.html','w')as f:
        f.write(r)
    rl = re.findall(r'strong>下辖.+?span(.+?)span',r,re.S)
    if rl:rl=rl[0]
    else:continue
    rls = re.findall(r'href="(.+?)".+?>(.+?)</a>',rl,re.S)
    i_2 = 0
    for i in rls:
        i_2 += 1
        #url = 'http://www.baike.com/wiki/'+rls[0][1].decode('raw_unicode_escape').encode('unicode_escape').replace('\\','%').replace('x','').upper()
        #url = i[0]
        name = i[1]
        i_1 += 1
        #insert into Area values(100710800,'台北县',100710000,8,'台湾省 台北县',NULL);
        print "insert into Area values(%s,'%s',%s,'%s',%s,NULL);" % (i_1,name,lr[0],'台湾省 %s %s'%(n_1,name),i_2)
print taizhong_q
