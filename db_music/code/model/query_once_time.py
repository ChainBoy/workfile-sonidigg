#!/usr/bin/python
#-*- coding: utf-8 -*-
import MySQLdb
import datetime
import re

#SPLIT = ['，',',','&',' ','、','\\','/',';','@','＠','：','<br']

class QueryMain():
    def __init__(self, auto_commit = False):
        try:
            self.auto_commit = auto_commit
            #self.splits = splits if splits else SPLIT
            self.conn=MySQLdb.connect(host='localhost',user='root',passwd='123',port=3306,db='DB_Music',charset='utf8')
        except MySQLdb.Error,e:
            print e.args

    def main(self ):
        #sql = "CREATE TRIGGER before_insert_once201404   BEFORE INSERT ON once201404   FOR EACH ROW   SET NEW.`ID` = replace(uuid(),'-','');"
        result = self.sql('call sp_test();')
        for i in result:
            for j in i:
                r = i[j]
                if r:
                    self.sql(i[j])
        self.commit()

    def sql(self, sql = "select 'ok'"):
        cur = self.conn.cursor(MySQLdb.cursors.DictCursor)
        a = cur.execute(sql)
        r = cur.fetchall()
        cur.close()
        return list(r)

    def list_dict_to_obj(self, dicts, class_name = 'test'):
        result = []
        for i in dicts:
            exec(class_name + ' = model()')
            for j in i:
                r = i[j]
                exec("%s.%s = '%s'" % (class_name, str(j),str(i[j])))
            result.append(eval(class_name))
        return result

    def check_time(self,str_time = '2014-11-12 13:15:04'):
        try:
            r = re.findall(r'\d{4}-\d{2}-\d{2}\ \d{2}\:\d{2}\:\d{2}',str(str_time))
            if r and len(r) > 0:
                return True
            else:
                return False
        except Exception:
            return False

    def check_int(self,int_s = '0'):
        try:
            int(int_s)
            return True
        except Exception:
            return False

    def checktime(self,station_id, song_id, artist_id, time, once_id):
        result = self.check_time(time)
        if result == False:
            return False
        if not once_id or not song_id or not artist_id:
            return False
        return self.check_int(station_id)

    def add_once(self,station_id, song_id, artist_id, time, pad_id):
        self.main()
        if self.checktime(station_id, song_id, artist_id, time, pad_id):
            date = str(datetime.datetime.now())[:7].replace('-','')
            sql = "insert into once%s(station_id,song_id,artist_id,pad_id, time) values(%s, %s, %s, %s,'%s')" % (date,  station_id, song_id, artist_id, pad_id, time)
            self.sql(sql)
            if self.auto_commit:
                self.commit()
        return main.query_once(station_id, song_id)

    def add_time(self, station_id, song_id, artist_id, time, once_id):
        self.main()
        if self.checktime(station_id, song_id, artist_id, time, once_id):
            date = str(datetime.datetime.now())[:7].replace('-','')
            sql = "insert into time%s(station_id,song_id,artist_id,once_id, time) values(%s, %s, %s, '%s', '%s')" % (date, station_id, song_id, artist_id, once_id, time)
            print sql
            self.sql(sql)
            if self.auto_commit:
                self.commit()

    def query_once(self, station_id, song_id):
        self.main()
        sql = "select * from onces where station_id = %s and song_id = %s order by time desc limit 1;" % (station_id, song_id)
        que_result = self.sql(sql)
        que_result = self.list_dict_to_obj(que_result, class_name = 'onces')
        if que_result and len(que_result) > 0:
            return que_result[0]
        else :
            return False

    def commit(self):
        try:
            self.conn.commit()
            return True
        except Exception, e:
            print e
            return False

    def close(self):
        try:
            self.conn.close()
        except Exception,e:
            print e
            return False

class model():
    def __init__(self):
        return
    def var(self):
        return vars(self)

if __name__ == '__main__':
    station_id = 332
    song_id = 2
    artist_id = 1
    pad_id = 1
    ftime = '2014-11-12 13:14:15'
    main = QueryMain(True)
    oncedb =  main.query_once(station_id, song_id)
    if not oncedb:
        oncedb = main.add_once(station_id, song_id, artist_id, ftime, pad_id)
    print oncedb.id
    main.add_time(station_id, song_id, artist_id, ftime,oncedb.id)


