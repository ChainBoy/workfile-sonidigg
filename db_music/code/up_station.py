#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import urllib2
import MySQLdb as Mysqldb
import re
import simplejson
import urlChecked as urlcheck
import vlcCheck as vlccheck



class MainClass(object):
    """docstring for MainClass"""
    def __init__(self):
        super(MainClass, self).__init__()
        conn=Mysqldb.connect(
            host='localhost',
            user='root',
            passwd='123',
            port=3306,
            db='DB_Music',
            charset='utf8'
            )
        conn.set_character_set("utf8")
        self.conn = conn

    def check_all_station_state(self):
        result = self.get_all_station_code()
        url_true=[]
        url_false=[]
        for i in result:
            id = int(i.get('ID'))
            url_1 = i.get('URL_M1')
            url_2 = i.get('URL_M2')
            server_status_1,m3u8_status_1 = url_status(url_1)
            server_status_2,m3u8_status_2 = url_status(url_2)
            if (server_status_1 and m3u8_status_1) or (server_status_2 and m3u8_status_2):
                url_true.append(id)
            else:
                url_false.append(id)
        print '\nURL False:',url_false
        print '\nURL True:',url_true
        print 'update table[Station]:',
        print self.update_station_code_state(1,url_true),
        print self.update_station_code_state(0,url_false),' OK!'

    def get_all_station_code(self):
        print 'check all station state'
        cur=self.conn.cursor(Mysqldb.cursors.DictCursor)
        a = cur.execute("select ID, URL_M1, URL_M2 from Station")
        r = cur.fetchall()
        cur.close()
        if a and r:
            return r
        else:
            return 0

    def update_station_code_state(self,state,ids):
        #state 1/0. 1 is ok,0 is error.
        #ids List,Ex: [1, 2, 3, 5]
        ids = str(tuple(ids))
        cur=self.conn.cursor()
        a = cur.execute("update Station set State=%s where ID in %s" % (state, ids))
        self.conn.commit()
        cur.close()
        return a

    def get_station_id(self):
        u = urllib2.urlopen('http://qingting.fm/category/domestic/54')
        r = u.read()
        l = re.findall(r'/category/domestic/(\d{1,2})(..)(.+?)(</a)',r);
        d = {}
        for i in l:
            d[i[2].decode("utf-8")] = {'id':i[0]}
        print 'Get city code OK!'
        for i in d:
            print i,":",
            u = urllib2.urlopen('http://qingting.fm/category/domestic/%s' % d[i]['id'] )
            r = u.read()
            l = re.findall(r'/channels/(\d{1,10})(.+?)title=\'(.+?)\'',r)
            d_fre = {}
            for j in l:
                if j[0] and j[2]:
                    d_fre[j[2].decode("utf-8")] = {'id':j[0],'name':j[2].decode("utf-8")}
                print j[2].decode("utf-8"),
            print "\n"
            d[i]['data'] = d_fre
        print 'Get frequency OK.'
        return d
    def get_station_info(self):
        d = self.get_station_id()
        for i in d:
            dd = d[i]['data']
            city_id = d[i]['id']
            for j in dd:
                print j,
                n_city =  j
                i_city = dd[j]['id']
                r = ''
                timeout_num = 0
                while True:
                    try:
                        u= urllib2.urlopen('http://api.qingting.fm/api/qtradiov2/liveprograms?id=' + i_city + '&day=1,2,3,4,5,6,7&wt=json&_=' + str(time.time()).split('.')[0], timeout = 10)
                        r = u.read()
                        dr = simplejson.loads(r.decode("utf-8"))
                        dr = dr['data']
                        ID = dr['id']
                        Name = dr['name']
                        Frequency = dr['frequency']
                        Media_id = dr['mediainfo']['id']
                        Keyword = dr['keywords']
                        Letter = dr['letter']
                        Summary = dr['desc']
                        Bitrate = dr['mediainfo'].get('transbitrate',24)
                        Pic = dr['pic']
                        if Bitrate:Bitrate = Bitrate[0]
                        else:Bitrate = 24
                        URL_M1 = 'http://42.96.141.199/live/%s.m3u8?bitrate=%s' % (Media_id, Bitrate)
                        URL_M2 = 'http://110.76.47.134/live/%s.m3u8?bitrate=%s' % (Media_id, Bitrate)
                        self.up_station_code(ID,Media_id,Name,city_id,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1="",URL_2="",URL_3="")
                        break
                    except Exception, e:
                        timeout_num += 1
                        print e,',try again',
                        if timeout_num == 3:
                            print '<-Del.',
                            break
                # t = '%s:%s|%s|%s|%s\n' % (media_id, i, id, name, fre)
        print 'Get station code is OK,Exit!'
        return True


    def up_station_code(self,ID,Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1="",URL_2="",URL_3=""):
        print 'Start Of Update Table \'station_code\''
        all_station=[]
        if ID:
            server_status_1,protocal_status_1 = url_status(URL_M1)
            server_status_2,protocal_status_2 = url_status(URL_M2)
            u1 = False
            u2 = False
            if server_status_1 and protocal_status_1:
                u1 = True
            if server_status_2 and protocal_status_2:
                u2 = True

            if u2 and not u1:
                URL_M1,URL_M2 = URL_M2, URL_M1
            if not u1 and not u2:
                return
            station_ord = self.check_station_code(ID)
            if station_ord == 0:
                print 'insert.',ID
                all_station.append((ID,Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3))
            else:
                print 'Update:',ID
                if self.update_station_code(ID, Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3):
                    print 'True'
                else:
                    print 'False'
        else:
            return
        print 'Insere New station:',
        insert_re = self.insert_station_code(all_station)
        if insert_re:
            print insert_re,'True'
        else:
            print 'False'
        print 'End Of Update Table \'station_code\''

    def up_station_code(self,ID,Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1="",URL_2="",URL_3=""):
        print 'Start Of Update Table \'station_code\''
        all_station=[]
        if ID:
            station_ord = self.check_station_code(ID)
            if station_ord == 0:
                print 'insert.',ID
                all_station.append((ID,Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3))
            else:
                print 'Update:',ID,
                if self.update_station_code(ID, Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3):
                    print 'True'
                else:
                    print 'False'
        else:
            return
        print 'Insere New station:',
        insert_re = self.insert_station_code(all_station)
        if insert_re:
            print insert_re,'True'
        else:
            print 'False'
        print 'End Of Update Table \'station_code\''

#{'code': None, 'id': 2L, 'new_code': None, 'name': u'\u5317\u4eac\u6587\u827a\u5e7f\u64ad'}
    def check_station_code(self, ID):
        print 'check',
        cur=self.conn.cursor(Mysqldb.cursors.DictCursor)
        a = cur.execute("select * from Station where Media='%s'" % ID)
        r = cur.fetchall()
        cur.close()
        if a and r:
            return r[0]
        else:
            return 0

#Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3
    def update_station_code(self, ID, Media_id, Name, Code_ID, Frequency, Keyword, Letter, Summary, Bitrate, URL_M1, URL_M2, Pic, URL_1, URL_2, URL_3):
        cur=self.conn.cursor()
        a = cur.execute("update Station set Media_id='%s',Name='%s',Code_ID='%s',Frequency='%s',Keyword='%s',Letter='%s',Summary='%s',Bitrate='%s',URL_M1='%s',URL_M2='%s',Pic='%s',URL_1='%s',URL_2='%s',URL_3='%s' where Media='%s'" % (Media_id,Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3,ID))
        self.conn.commit()
        cur.close()
        return a
    def insert_station_code(self, all_station):
        cur=self.conn.cursor()
        a =  cur.executemany("insert into Station(Media, Media_id, Name,Code_ID,Frequency,Keyword,Letter,Summary,Bitrate,URL_M1,URL_M2,Pic,URL_1,URL_2,URL_3) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", all_station)
        self.conn.commit()
        cur.close()
        return a

def main():
    print 'End Update Table station.'
    start_check_update_DB()
    exit('END ALL!')

def url_status(url):
    try:
        uc = url.split(':')
        if len(uc) > 1 and not uc[0] == 'http':
            print '\n',url,'VLC!'
            return vlccheck.testts(url)
        uc = urlcheck.UrlChecker(url, check())
        print '\n',url
        server_status,protocal_status = uc.check_url()
        return server_status,protocal_status
    except Exception:
        return False,False

def start_check_update_DB():
    mainclass = MainClass()
    mainclass.check_all_station_state()
    exit('END ALL!')
        
class check(urlcheck.CheckObserver):
    def on_server_check_end(self, status):
        print 'server status is:' + str(status)
        return status
    def on_protocol_check_end(self, status):
        print 'protocol status is:' + str(status)
        return status


if __name__ == '__main__':
    main()
