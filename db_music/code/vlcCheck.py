#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import time
import os
import datetime

screen_name = 'vlc_test'

def now():
    return str(datetime.datetime.now()).split('.')[0]

def run_screen(screen_name, comm):
    l = 'screen -dmS %s %s' % (screen_name, comm)
    l = l.split(' ')
    print subprocess.Popen(l, stdout=subprocess.PIPE).communicate()

    return True

def kill_screen(screen_name):
    p = subprocess.Popen(['ps', 'a','u','x'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    l = []
    for line in out.splitlines():
        if screen_name in line:
            pid = int(line.split(None, 2)[1])
            l.append(pid)
        else:
            continue

    for pid in l:
        print now(),'will kill screen %s :'% screen_name, pid
        os.system('kill %s'% pid)

def savets(screen_name = 'screentest', comm = 'ls', sleeptime = 300):
    run_screen(screen_name, comm)
    time.sleep(sleeptime)
    kill_screen(screen_name)

def createcomm(url):
    if not url: return None,None
    ht = url.split(':')
    if not len(ht) > 1:return None,None
    ht = ht[0]
    if ht == 'mms':
        comm = "cvlc -vvv %s --sout #standard{access=file,mux=mp3,dst=/home/zhipeng/zhangzhipeng_ssh/db_music/code/mms.tmp}" % url
        tmp = 'mms.tmp'
    elif ht == 'rtsp':
        comm = "cvlc -vv --rtsp-tcp %s --sout #transcode{vcodec=none,acodec=mp3,ab=128,channels=1,samplerate=11025}:standard{access=file,mux=mp3,dst=rtsp.tmp}" % url
        tmp = 'rtsp.tmp'
    elif ht == 'rtmp':
        comm = "cvlc -vvv %s --sout #standard{access=file,mux=mp3,dst=/home/zhipeng/zhangzhipeng_ssh/db_music/code/rtmp.tmp}" % url
        tmp = 'rtmp.tmp'
    else:
        return None,None
    return comm,tmp

def checkfile(tmp):
    if os.path.isfile(tmp):
        if os.path.getsize(tmp) > 100:
            os.remove(tmp)
            return True,True
        else:return False, False
    else:return False, False

def testts(url):
    try:
        comm, tmp = createcomm(url)
        print comm, tmp
        screen_name = str(int(time.time()))
        savets(screen_name, comm, 30)
        return checkfile(tmp)
    except KeyboardInterrupt:
        print 'KeyboardInterrupt'
        kill_screen(screen_name)
        return checkfile(tmp)

if __name__ == '__main__':
    print testts('rtmp://audio.vojs.cn:1935/livepkgr/897')
    print testts('rtsp://v.liaozhai.tv/radio5')
    print testts('mms://radio.qingc.net/live')