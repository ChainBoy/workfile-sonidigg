#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Image as image
import threading
import subprocess

def main():
    sub = subprocess.Popen('find result/ -type f > .result', shell=True)
    sub.wait()
    alldic={}

    with file('.result', 'r')as f:
        while True:
            line = f.readline().strip()
            if not line:
                break
            img = image.open(line)
            #size = 'x'.join([str(i) for i in img.size])
            size = float('%s.%s' % img.size)
            alldic[size] = alldic.get(size, 0) + 1
    with file('count', 'w')as f:
        #f.write('\n'.join(['\t'.join([str(i[0]), str(i[1])]) for i in alldic.iteritems()]))
        text = '\n'.join([(lambda k: '\t'.join([str(j) for j in k]))(i) for i in sorted(alldic.items(), key=lambda x:x[0])])
        print text
        f.write(text)
    print 'End, save to file: count.'




if __name__ == '__main__':
    main()


