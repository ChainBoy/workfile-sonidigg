#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import time
import re
import subprocess
import threading
import Queue
import os

max_num = 0
def main(thread_num = 10):
    print '-'*50, 'Start Work.', '-'*50
    max_num = get_new_id()
    print 'New ID:', max_num
    que = Queue.Queue()
    threads = []
    try:
        for num in xrange(121725, max_num):
            que.put([get_img_by_num, num])
        for j in xrange(thread_num):
            threads.append(Down(que))
        for j in threads:
            j.setDaemon(True)
            j.start()
        print '=='*50
        print '  '*50, 'PUT END.'
        print '=='*50
        while True:
            time.sleep(1)
    except Exception, e:
        print e

def get_new_id(default_id=236775):
    #article_386644_1
    try:
        con = requests.get('http://www.ktvc8.com/cmslist/s_8_1.html', timeout=30).content.decode('gbk')
        return max([int(i)for i in re.findall('article/article_(\d.+?)_1.html',con, re.S)])
    except Exception, e:
        print e
        return default_id


class Down(threading.Thread):
    """docstring for Down"""
    def __init__(self, que):
        super(Down, self).__init__()
        self.que = que

    def run(self):
        while True:
            if not self.que.empty():
                work = self.que.get(block=False,timeout=1)
                if work[1] == max_num:
                    return
                work[0](work[1], self.name)
            else:
                return

def get_img_by_num(num, name=""):
    try:
        url = 'http://www.ktvc8.com/article/article_%s_1.html' % num
        print name, 'Start:', '='*70,'\b>', url
        con = requests.get(url, timeout=30).content.decode('gbk')
        location = re.sub(u' {0,}<.+?> {0,}→{0,} {0,}','/',re.findall(u'weizhi>.+? → (.+?)</DIV', con, re.S)[0]).replace('//','/').strip()
        #/声乐谱/歌曲简谱/她的眼睛会下雨
        singer = '未知'
        song = '未知'
        try:
            song = re.findall('cms1.+?cms2>(.+?)</H1', con, re.S)[0].strip()
        except IndexError:
            pass
        try:
            singer = re.findall(u'演唱（奏）：.+?=red>(.+?)</font>', con, re.S)[0]
        except IndexError, e:
            pass
        imgs = list(set(re.findall('<img.+?src=".+?(/uploadfiles/.+?)"', re.sub('<img.+?src="..(/uploadfiles/\d{8}/\d{8}\..+?)"','',con, re.S), re.S)))
        if len(imgs) > 0:
            path = 'result/%s' % location
            if os.path.isdir(path):
                for i in range(20):
                    tmp_path = '%s(%s)' % (path, i)
                    if not os.path.isdir(path):
                        path = tmp_path
                        break
            filename = '%s==>%s' % (song, singer)
            save_imgs(path, filename, imgs, name)
    except Exception, e:
        print name, 'Exception:', e

def save_imgs(path, filename, imgs, name=""):
    all_path = '%s/%s' % (path, filename)
    if len(imgs) == 1:
        url = 'http://www.ktvc8.com/%s' % imgs[0]
        filetype = url.split('.')[-1]
        all_path = '%s.%s' % (all_path, filetype)
        try:
            con = requests.get(url, timeout=30).content
            if not con:
                return True
            subprocess.Popen('mkdir -p "%s"' % path, shell=True)
            time.sleep(1)
            with file(all_path, 'w')as f:
                f.write(con)
            print name, '=' * 150 + '\b> 【', filename,'】OK!'
        except Exception, e:
            print name, e
    else:
        for j, i in enumerate(imgs):
            url = 'http://www.ktvc8.com/%s' % i
            filetype = i.split('.')[-1]
            try:
                con = requests.get(url, timeout=30).content
                if not con:
                    continue
                all_path = '%s(%s).%s' % (all_path, j, filetype)
                subprocess.Popen('mkdir -p "%s"' % path, shell=True)
                time.sleep(1)
                with file(all_path, 'w')as f:
                    f.write(con)
                print name, '=' * 150 + '\b> 【', filename,'】OK!'
            except Exception, e:
                print name, e

if __name__ == '__main__':
    main(thread_num=20)
