<?php
class IP{
    private function get_ip()
    {
         if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown'))
          {
                $ip = getenv('HTTP_CLIENT_IP');
                 }
          elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown'))
           {
                 $ip = getenv('HTTP_X_FORWARDED_FOR');
                  }
          elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown'))
           {
                 $ip = getenv('REMOTE_ADDR');
                  }
          elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown'))
           {
                 $ip = $_SERVER['REMOTE_ADDR'];
                  }
          return preg_match("/[\d\.]{7,15}/", $ip, $matches) ? $matches[0] : 'unknown';
    }
    public function ip()
    {
        $sip=$this->get_ip();

        //这样就能得到访问者的IP地址了，至于存文件，你可以这样写 H不分上午下午，G 24小时制度
        $time=date("Y-m-d G:i:s",strtotime('+8 hour'));
        $str=$time."  ".$sip;
        $l=fopen("ip.txt","a");
        fwrite($l,$str."\n");
        fclose($l);
    }
}
?>
