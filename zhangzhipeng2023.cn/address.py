#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2
import re

u = urllib2.urlopen('http://zhangzhipeng2023.cn/ip.txt')
r = u.readlines()

for i in r:
    ip = i.split('  ')[1]
    time = i.split('  ')[0]
    u = urllib2.urlopen('http://ip138.com/ips138.asp?ip=%s' % ip)
    rr = u.read().decode('gbk').encode('utf8')
    with file('result_ip_address.txt','a')as f:
        s = '%s\t%s\t%s' % (time,ip,' '.join(re.findall(r'参考数据.+?：(.+?)</li>',rr)))
        s = s.replace('\n','').replace('\r','') + '\n'
        f.write(s)
