#Songs-触发器——GUID
CREATE TRIGGER before_insert_Songs
  BEFORE INSERT ON Songs
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');

#Artists-触发器——GUID
CREATE TRIGGER before_insert_Artists
  BEFORE INSERT ON Artists
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');


#Albums-触发器——GUID
CREATE TRIGGER before_insert_Albums
  BEFORE INSERT ON Albums
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');


#Groups-触发器——GUID
CREATE TRIGGER before_insert_Groups
  BEFORE INSERT ON Groups
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');


#Lyrics-触发器——GUID
CREATE TRIGGER before_insert_Lyrics
  BEFORE INSERT ON Lyrics
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');

#Melodys-触发器——GUID
CREATE TRIGGER before_insert_Melodys
  BEFORE INSERT ON Melodys
  FOR EACH ROW
  SET NEW.`Key` = replace(uuid(),'-','');
