-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dataservice
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `add_yesDay`
--

DROP TABLE IF EXISTS `add_yesDay`;
/*!50001 DROP VIEW IF EXISTS `add_yesDay`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `add_yesDay` (
  `song_id` tinyint NOT NULL,
  `artist_id` tinyint NOT NULL,
  `play_num` tinyint NOT NULL,
  `play_time` tinyint NOT NULL,
  `time` tinyint NOT NULL,
  `hot` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `artist`
--

DROP TABLE IF EXISTS `artist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11062 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listen_log`
--

DROP TABLE IF EXISTS `listen_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listen_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stationId` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `fingurePrint` text COLLATE utf8_unicode_ci,
  `song` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=625807 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listen_log_null`
--

DROP TABLE IF EXISTS `listen_log_null`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listen_log_null` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `stationId` int(11) DEFAULT NULL,
  `fingerPrint` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5907801 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listen_log_once`
--

DROP TABLE IF EXISTS `listen_log_once`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listen_log_once` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `stationId` int(11) DEFAULT NULL,
  `song` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `song` (`song`),
  CONSTRAINT `listen_log_once_ibfk_1` FOREIGN KEY (`song`) REFERENCES `song` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168273 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listen_log_times`
--

DROP TABLE IF EXISTS `listen_log_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listen_log_times` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `stationId` int(11) DEFAULT NULL,
  `fingerPrint` text COLLATE utf8_unicode_ci,
  `song` int(11) DEFAULT NULL,
  `once_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `song` (`song`),
  CONSTRAINT `listen_log_times_ibfk_1` FOREIGN KEY (`song`) REFERENCES `song` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548727 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `song`
--

DROP TABLE IF EXISTS `song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artist` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47837 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `song_play_log`
--

DROP TABLE IF EXISTS `song_play_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_play_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `song_id` int(11) unsigned NOT NULL,
  `artist_id` int(11) unsigned NOT NULL,
  `play_num` int(11) unsigned NOT NULL,
  `play_time` int(11) unsigned NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hot` int(1) unsigned NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159995 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `stationId` int(11) DEFAULT NULL,
  `stateCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2891725 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `station`
--

DROP TABLE IF EXISTS `station`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `station` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned` int(11) DEFAULT NULL,
  `assignedTo` int(11) DEFAULT NULL,
  `failTime` int(11) DEFAULT NULL,
  `createdTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_lastDay_all`
--

DROP TABLE IF EXISTS `view_lastDay_all`;
/*!50001 DROP VIEW IF EXISTS `view_lastDay_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_lastDay_all` (
  `song_id` tinyint NOT NULL,
  `artist_id` tinyint NOT NULL,
  `play_num` tinyint NOT NULL,
  `time` tinyint NOT NULL,
  `play_time` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_song_play_num`
--

DROP TABLE IF EXISTS `view_song_play_num`;
/*!50001 DROP VIEW IF EXISTS `view_song_play_num`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_song_play_num` (
  `song` tinyint NOT NULL,
  `play_num` tinyint NOT NULL,
  `t` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_yesDay_all`
--

DROP TABLE IF EXISTS `view_yesDay_all`;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_yesDay_all` (
  `song_id` tinyint NOT NULL,
  `artist_id` tinyint NOT NULL,
  `play_num` tinyint NOT NULL,
  `time` tinyint NOT NULL,
  `play_time` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_yesDay_play`
--

DROP TABLE IF EXISTS `view_yesDay_play`;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_yesDay_play` (
  `song_id` tinyint NOT NULL,
  `artist_id` tinyint NOT NULL,
  `play_num` tinyint NOT NULL,
  `time` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_yesDay_play_time`
--

DROP TABLE IF EXISTS `view_yesDay_play_time`;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play_time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_yesDay_play_time` (
  `num` tinyint NOT NULL,
  `song` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_yesDay_play_time_all`
--

DROP TABLE IF EXISTS `view_yesDay_play_time_all`;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play_time_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_yesDay_play_time_all` (
  `play_time` tinyint NOT NULL,
  `song` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'dataservice'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetPage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_GetPage`(in pagename varchar(10000), columnname varchar(500), pageindex int, pagesize int,checkwhere varchar(10000) charset 'utf8')
begin

set @tbl =concat('select * from (select ntb.*,@rown := @rown + 1 AS Rank from ',pagename,'as ntb,(SELECT @rown := 0) r ',checkwhere,' order by ',columnname,') as newtable where Rank between ',pagesize,'*(',pageindex,'-1) and ',pageindex,'*',pagesize,';');



PREPARE stmt from @tbl;
EXECUTE stmt;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Get_day_all_listening_station_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Get_day_all_listening_station_count`()
begin
    
    SELECT tb1.time,hour(tb1.time) AS h,count(DISTINCT tb1.stationId) as station_count
    FROM listen_log_once as tb1
    WHERE tb1.time between date_sub(now(),interval 24 hour) and now()
    GROUP BY h order by tb1.time;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Get_day_all_song_play_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Get_day_all_song_play_count`()
begin
    
    SELECT hour(time) AS h,time,COUNT(*)as play_count
    FROM listen_log_once
    WHERE time between date_sub(now(),interval 24 hour) and now()
    GROUP BY h order by time;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Get_day_top_100_song_play_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Get_day_top_100_song_play_count`()
begin
    
    SELECT hour(tb1.time) AS h,tb1.time,COUNT(tb1.id)as play_count
    FROM
        listen_log_once as tb1,
        (select song,count(id)as play_count from listen_log_once where time between date_sub(now(),interval 1 day) and now() group by song order by play_count desc limit 0,100) as tb2
    WHERE tb1.time between date_sub(now(),interval 24 hour) and now() and tb1.song=tb2.song
    GROUP BY h order by tb1.time;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rank` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_rank`(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin

    

    

    declare time_space int;

    set time_space = DATEDIFF(time_end,time_start);

    if cate = 1 then

        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.song_id order by lo.play_num desc)as tb1 left join (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.song_id order by play_num)as tb2 on tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);

    elseif cate = 0 then

        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from (select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select so.name as artist,so.id as artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo,artist as so where lo.artist_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.artist_id order by lo.play_num desc)as tb1 left join (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.artist_id order by play_num)as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) as r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);

    end if;

    select @tql;

    PREPARE stmt from @tql;

    EXECUTE stmt;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rank_t` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_rank_t`(in cate int,num_start int,num_end int, time_start varchar(30),time_end varchar(30) charset 'utf8')
begin

    

    

    declare time_space int;

    set time_space = DATEDIFF(time_end,time_start);

    if cate = 1 then

        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from ( select tb1.song_id,tb1.song,tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select lo.song_id,so.song,so.artist,so.artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo, (select song.id,song.artist_id,song.name as song,artist.name as artist from song ,artist where song.artist_id=artist.id) as so where lo.song_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.song_id order by lo.play_num desc)as tb1 left join (select lo2.song_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.song_id order by play_num)as tb2 on tb1.song_id = tb2.song_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);

    elseif cate = 0 then

        set @tql=concat('select * from (select t.*,@rownum := @rownum + 1 AS rank from (select tb1.artist_id,tb1.artist,tb1.time,tb1.play_time,tb1.play_num,tb1.play_num-tb2.play_num as play_num_add from (select so.name as artist,so.id as artist_id,lo.time,sum(play_time)as play_time,sum(play_num) as play_num from song_play_log as lo,artist as so where lo.artist_id = so.id and lo.time between ','''',time_start,'''',' and ','''',time_end,'''',' group by lo.artist_id order by lo.play_num desc)as tb1 left join (select lo2.artist_id,sum(lo2.play_num)as play_num,sum(lo2.play_time)as play_time from song_play_log as lo2 where lo2.time between date_sub(','''',time_start,'''',',interval ',time_space,' day) and date_sub(','''',time_end,'''',',interval ',time_space,' day) group by lo2.artist_id order by play_num)as tb2 on tb1.artist_id=tb2.artist_id order by tb1.play_num desc) as t,(SELECT @rownum := 0) as r) as re_tb where re_tb.rank between ',num_start,' and ',num_end);

    end if;

    select @tql;

    PREPARE stmt from @tql;

    EXECUTE stmt;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_priview` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_priview`()
begin

select * from (select count(song.id)as song_count,(select count(artist.id)as artist_count from artist) as artist_count from song)as tb_s_a_count,(select sum(tb.play_time) as play_time from (select TIME_TO_SEC(TiMEDIFF(max(time),min(time))) as play_time from listen_log_times group by once_id) as tb) as tb_play_time_count,(select count(*)as play_num from listen_log_once) as tb_play_count;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `add_yesDay`
--

/*!50001 DROP TABLE IF EXISTS `add_yesDay`*/;
/*!50001 DROP VIEW IF EXISTS `add_yesDay`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `add_yesDay` AS select `b`.`song_id` AS `song_id`,`b`.`artist_id` AS `artist_id`,`b`.`play_num` AS `play_num`,`b`.`play_time` AS `play_time`,`b`.`time` AS `time`,(case when (`b`.`play_num` > `a`.`play_num`) then 0 when (`b`.`play_num` < `a`.`play_num`) then 1 else 2 end) AS `hot` from (`view_yesDay_all` `b` left join `view_lastDay_all` `a` on((`a`.`song_id` = `b`.`song_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_lastDay_all`
--

/*!50001 DROP TABLE IF EXISTS `view_lastDay_all`*/;
/*!50001 DROP VIEW IF EXISTS `view_lastDay_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_lastDay_all` AS select `lg`.`song_id` AS `song_id`,`lg`.`artist_id` AS `artist_id`,`lg`.`play_num` AS `play_num`,date_format(`lg`.`time`,'%Y-%m-%d') AS `time`,`lg`.`play_time` AS `play_time` from (`song_play_log` `lg` join `artist` `ar`) where ((date_format(`lg`.`time`,'%Y-%m-%d') = (date_format(now(),'%Y-%m-%d') - interval 2 day)) and (`ar`.`id` = `lg`.`artist_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_song_play_num`
--

/*!50001 DROP TABLE IF EXISTS `view_song_play_num`*/;
/*!50001 DROP VIEW IF EXISTS `view_song_play_num`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_song_play_num` AS select `tb`.`song` AS `song`,count(`tb`.`song`) AS `play_num`,date_format(`tb`.`time`,'%Y-%m-%d') AS `t` from `listen_log_once` `tb` where (date_format(`tb`.`time`,'%Y-%m-%d') = (date_format(now(),'%Y-%m-%d') - interval 1 day)) group by `tb`.`song` order by count(`tb`.`song`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_yesDay_all`
--

/*!50001 DROP TABLE IF EXISTS `view_yesDay_all`*/;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_yesDay_all` AS select `yes`.`song_id` AS `song_id`,`yes`.`artist_id` AS `artist_id`,`yes`.`play_num` AS `play_num`,`yes`.`time` AS `time`,`yl`.`play_time` AS `play_time` from (`view_yesDay_play` `yes` join `view_yesDay_play_time_all` `yl`) where (`yes`.`song_id` = `yl`.`song`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_yesDay_play`
--

/*!50001 DROP TABLE IF EXISTS `view_yesDay_play`*/;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_yesDay_play` AS select `song`.`id` AS `song_id`,`song`.`artist_id` AS `artist_id`,`pn`.`play_num` AS `play_num`,`pn`.`t` AS `time` from (`view_song_play_num` `pn` join `song`) where (`pn`.`song` = `song`.`id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_yesDay_play_time`
--

/*!50001 DROP TABLE IF EXISTS `view_yesDay_play_time`*/;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play_time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_yesDay_play_time` AS select time_to_sec(timediff(max(`listen_log_times`.`time`),min(`listen_log_times`.`time`))) AS `num`,`listen_log_times`.`song` AS `song` from `listen_log_times` where (date_format(`listen_log_times`.`time`,'%Y-%m-%d') = (date_format(now(),'%Y-%m-%d') - interval 1 day)) group by `listen_log_times`.`once_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_yesDay_play_time_all`
--

/*!50001 DROP TABLE IF EXISTS `view_yesDay_play_time_all`*/;
/*!50001 DROP VIEW IF EXISTS `view_yesDay_play_time_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_yesDay_play_time_all` AS select sum(`tb1`.`num`) AS `play_time`,`tb1`.`song` AS `song` from `view_yesDay_play_time` `tb1` group by `tb1`.`song` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-21 14:54:07
