/*
本sql 是针对dataservice更新song_play_log表，因播放时长存在0的情况进行修改表结构，添加事务，并且可以实时更新播放时长为0的数据时长修改为指定秒数
*/

/* 修改表结构*/
ALTER TABLE `dataservice`.`song_play_log` ADD COLUMN `state` INT(11) NULL DEFAULT '0' COMMENT '时间长度是否为零，如果时间为零，则状态为1；默认为0，正常状态，时间不为零 '  ;


/* 创建事务*/
DELIMITER $$
create trigger before_insert_song_play_log
before insert on song_play_log
for each row
begin
    if new.`play_time`=0 then
        set new.`state`=1, new.`play_time` = 20 * new.`play_num`;
    end if;
end$$



/*修改已存在的*/
update song_play_log set state=1, play_time = 20*play_num where play_time = 0;

/*实时修改时长为0的时长*/
update song_play_log set play_time = 20*play_num where play_time = 0 or state= 1;

/* 文件说明 */
