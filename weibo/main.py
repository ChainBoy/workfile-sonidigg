#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import datetime
from movie import Movie
from urllib import quote
from send_mail import send_mail
import time
import random
from weibo_tools import login, USER_NAME, PASSWORD, logins
import city
import requests
import threading
import Queue


END_ALL = False
SEARCH_SLEEP_TIME = 0

class Controller(object):
    """docstring for Controller"""
    def __init__(self, task_list):
        super(Controller, self).__init__()
        #self.session = login()
        self.relogin()
        print self.users
        self.task_list = task_list
        self.url = 'http://s.weibo.com/wb/'
        self.scode = 'http://sonidigg.xicp.net:9000/code'
        #self.scode = 'http://192.168.1.2:9999/code'
        self.page_count = 0
        self.get_province()
        self.que = Queue.Queue()
        self.is_start_thread = False
        self.time = threading.Timer(3600 * 2 , self.relogin)
        self.time.start()
        time.sleep(1)
        self.task = 0

    def cancel(self):
        print 'Main: Cancel Timer'
        self.time.cancel()
        for i in threading.enumerate():
            if type(i) == threading._Timer:
                i.cancel()

    def relogin(self):
        print 'Main:RE login !!!'
        if END_ALL:
            if self.time:
                self.time.cancel
            return
        print 'Main:RE login !!!'
        self.users = logins()
        self.user = self.users[0][0]
        self.passwd = self.users[0][1]
        self.session = self.users[0][2]
        self.time = threading.Timer(3600 * 3, self.relogin)
        self.time.start()

    def start(self):
        for index, i in enumerate(self.task_list):
            start_work_time = datetime.datetime.now()
            self.movie_name = i.get('movie_name')
            self.start_time = i.get('start_time')
            self.end_time = i.get('end_time')
            self.task_type = i.get('task_type')
            self.movie = Movie(self.movie_name, self.session)
            if self.task_type:
                self.spider_day_day()
            else:
                self.spider_day_hour()
            con = '"' + self.movie_name + '" ' + self.start_time + ' -- ' + self.end_time + ' Sina Weibo End.' + '\nStart word time:' + str(start_work_time) + '.\nEnd word time:'
            print '关键字：-> %s <- 检索完成！' % self.movie_name
            print con + str(datetime.datetime.now())
            try:
                l = [send_mail, 'Spider END.%s' % (index + 1), con, 0, int(i.get('line'))]
                self.que.put(l)
            except Exception, e:
                print e
        #End All.
        l.append(True)
        self.que.put(l)

    def spider_day_day(self):
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d')
        if start_time <= end_time:
            print 'Name:', self.movie_name
            print 'Time:', self.start_time, '--', self.end_time
            self.search_nomarl(start_time, end_time)
        else:
            print 'ERROR:起始时间必须比结束时间早!'

    def mail_code(self, url):
        send_mail('Sina weibo -- reboot!!机器人', ('user: ' + self.user + '\n pwd:' + self.passwd + '\n' + url))

    def search_nomarl(self, start_time, end_time):
        '''普通搜索,All时间'''
        print 'search_nomarl:::'
        url = self.url + quote(self.movie_name)
        s_time = datetime.datetime.strftime(start_time, '%Y-%m-%d')
        e_time = datetime.datetime.strftime(end_time, '%Y-%m-%d')
        while True:
            timescope = s_time + ':' + e_time
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            content = self.get_page_concent(url)
            count = self.get_page_count(content)

            if count == 0:
                code_num = 0
                if self.is_rebot(content):
                    code_num += 1
                    if code_num >= 10:
                        self.mail_code(url)
                    print '变机器人了,需要帮助'
                    #time.sleep(REBOT_SLEEP_TIME)
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
                    return self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
            elif count < 50:
                print '数据小于50页，直接爬取'
                return self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
            # 如果页数超过50
            elif count == 50:
                print '数据等于50页，分片爬取'
                self.search_by_day(s_time, e_time)
                return 'End'

    def search_by_day(self, start_time, end_time):
        '''根据天做分割，如果在普通搜索不能完成时间段所有信息的时候使用'''
        print 'search_by_day:::'
        start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d')
            timescope = timescope + ':' + timescope
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)
                code_num = 0
                if count == 0:
                    if self.is_rebot(content):
                        code_num += 1
                        if code_num >= 10:
                            self.mail_code(url)
                        print '变机器人了,需要帮助'
                        self.clear_code(timeout=10, url=url, content=content)
                        continue
                    else:
                        print '数据0页～～！'
                    self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
                    break
                # 如果页数低于50,直接爬取
                elif count < 50:
                    print '数据小于50页，直接爬取'
                    self.handle_pages(url, count, self.movie.id, end_time, end_time, content)
                    break
                # 如果页数超过50,分小时爬取
                elif count == 50:
                    print '数据等于50页，分片爬取'
                    self.search_by_hour(end_time)
                    break
            end_time = end_time - datetime.timedelta(days=1)
            if start_time > end_time:
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def spider_day_hour(self, search_province=False, search_city=False):
        '针对特定小时段落抓取'
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        start = True
        while True:
            if start:
                tmp_time = start_time
                start = False
            else:
                tmp_time = tmp_time + datetime.timedelta(hours=1)
            if tmp_time <= end_time:
                timescope = datetime.datetime.strftime(tmp_time, '%Y-%m-%d-%H')
                timescope = timescope + ':' + timescope
                url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
                print url
                while True:
                    content = self.get_page_concent(url)
                    count = self.get_page_count(content)
                    code_num = 0
                    if count == 0:
                        if self.is_rebot(content, url):
                            code_num += 1
                            if code_num >= 10:
                                self.mail_code(url)                            
                            print '变机器人了,需要帮助'
                            self.clear_code(timeout=10, url=url, content=content)
                            continue
                        else:
                            print '数据0页～～！'
                    # 如果页数低于50,直接爬取
                    elif count < 50:
                        print '数据小于50页，直接爬取'
                        self.handle_pages(url, count, self.movie.id, tmp_time, tmp_time, content)
                        break

                    # 如果页数超过50,分省抓取
                    elif count == 50:
                        if not search_province:
                            self.handle_pages(url, count, self.movie.id, tmp_time, tmp_time, content)
                            break
                        print '数据大于50页，分省爬取'
                        for province_code in self.provinces:
                            print '搜索:', self.citys[province_code]['name']
                            self.search_by_province(url, province_code, search_city)
                        break
                    self.handle_pages(url, count, self.movie.id, tmp_time, tmp_time, content)
                    break
                print 'while sleep %ss...' % SEARCH_SLEEP_TIME
                time.sleep(SEARCH_SLEEP_TIME)
            else:
                break

    def search_by_province(self, url, province_code, search_city=False):
        '分省搜索'
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:1000
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&region=custom:23:1000&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        url = url + '&region=custom:%s:' % province_code
        print url
        while True:
            content = self.get_page_concent(url)
            count = self.get_page_count(content)
            code_num = 0
            if count == 0:
                if self.is_rebot(content):
                    code_num += 1
                    if code_num >= 10:
                        self.mail_code(url)                    
                    print '变机器人了,需要帮助'
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
            # 如果页数低于50,直接爬取
            elif count < 50:
                print '数据小于50页，直接爬取'
                self.handle_pages(url, count, self.movie.id, end_time, end_time, content)
                break
            # 如果页数超过50,分市抓取
            elif count == 50:
                if not search_city:
                    self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
                    break
                print '数据等于50页，分市爬取'
                citys = self.citys[province_code]['city']
                for city_code in citys:
                    print '搜索:', self.citys[province_code]['name'], citys[city_code]
                    self.search_by_city(url, city_code)
                break
            self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
            break
        print 'while sleep %ss...' % SEARCH_SLEEP_TIME
        time.sleep(SEARCH_SLEEP_TIME)

    def search_by_city(self, url, city_code):
        '分市搜索'
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:1
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        url = '%s%s' % (url, city_code)
        print url
        while True:
            content = self.get_page_concent(url)
            count = self.get_page_count(content)
            code_num = 0
            if count == 0:
                if self.is_rebot(content):
                    code_num += 1
                    if code_num >= 10:
                        self.mail_code(url)                    
                    print '变机器人了,需要帮助'
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
            self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
            break
        print 'while sleep %ss...' % SEARCH_SLEEP_TIME
        time.sleep(SEARCH_SLEEP_TIME)

    def search_by_hour(self, day):
        '''针对一天按小时分割搜索'''
        print "search_by_hour:::"
        day = datetime.datetime.strftime(day, '%Y-%m-%d')
        l_hour = [i for i in range(0, 25)]
        # 1-24
        while True:
            i_day = l_hour.pop()
            print 'i_day', i_day
            if i_day == 24:
                end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')
                # end_time = end_time - datetime.timedelta(hours = 1)
            elif i_day == 0:
                break
            else:
                end_time = datetime.datetime.strptime(day + '-' + str(i_day), '%Y-%m-%d-%H')
                end_time = end_time - datetime.timedelta(hours=1)
            start_time = end_time

            timescope = datetime.datetime.strftime(start_time, '%Y-%m-%d-%H')
            timescope = timescope + ':'
            timescope = timescope + datetime.datetime.strftime(end_time, '%Y-%m-%d-%H')
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)
                code_num = 0
                if int(count) == 0 or count == '0':
                    if self.is_rebot(content):
                        code_num += 1
                        if code_num >= 10:
                            self.mail_code(url)                        
                        print '变机器人了,需要帮助'
                        self.clear_code(timeout=10, url=url, content=content)
                        continue
                    else:
                        print '数据0页～～！'
                self.handle_pages(url, count, self.movie.id, start_time, end_time, content)
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def get_page_concent(self, url, meth='GET', data={}, timeout=20, files={}, type=0):
        try_num = 0
        while True:
            try:
                if files:
                    #print files
                    #content = self.session.post(url,files=files).content
                    #return content
                    r = requests.post(url,files=files, timeout=timeout)
                    #print r.request.headers
                    return r.content
                if type == 1:
                    return self.session.post(url, timeout=timeout,data=data, files=files)
                content = self.session.request(meth, url, timeout=timeout,data=data, files=files).content
                return content
            except Exception,e:
                print e
                try_num += 1
                if try_num > 5:
                    continue
                else:
                    return ""

    def get_province(self):
        self.citys = city.start()
        self.provinces = city.province_list

    def get_page_count(self, content):
        max_count = 0
        with file('content.html', 'w')as f:
            f.write(content)
        result = re.findall(r'&page=(\d+)', content)
        result.append(0)
        print result
        max_count = max([int(i) for i in result])
        print 'All Page:', result, 'Max Page:-->', max_count
        return max_count

    def clear_code(self, timeout=30, url='http://s.weibo.com/wb/5&timescope=custom:2014-05-17%3A2014-05-17', content=''):
        'timeout --> 处理验证码的等待时间.'
        self.session.headers.update({'Referer':url})
        self.session.headers.update({'Host':'s.weibo.com'})
        self.session.headers.update({'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'})
        self.session.headers.update({'Accept-Encoding':'gzip,deflate,sdch'})
        self.session.headers.update({'Accept-Language':'zh,zh-CN;q=0.8,zh-TW;q=0.6'})
        print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        iurl = 'http://s.weibo.com/' + re.findall(r'img.+?=\\"(.+?)\\".+?yzm_img',content)[0].replace('\\','')
        itime = 0
        while itime <= SEARCH_SLEEP_TIME:
            code = self.get_code(iurl, timeout=timeout)
            print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            res_code = self.post_code(code, url=url)
            print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            print 'Code reboot result:', res_code,
            if res_code:
                print 'Successfull!'
                return True
            else:
                print 'Failed.'
                time.sleep(1)
                itime += 1

    def is_rebot(self, content, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            return True
        else:
            return False

    def get_code(self, url, timeout=30):
        print 'get code img'
        self.session.headers.update({'Cache-Control':'max-age=0'})
        content = self.get_page_concent(url)
        try:
            del self.session.headers['Cache-Control']
        except KeyError, e:
            print e
        print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        files={'code.png':content}
        code = self.get_page_concent(self.scode, 'GET', data={'img':content}, timeout=timeout, files=files)
        #code = raw_input('\ncode:')
        if len(code) == 4:
            with file('code/' + code + '.png','wb')as f:
                f.write(content)
            return code

    def post_code(self, code, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        if code and len(code) == 4:
            curl = 'http://s.weibo.com/ajax/pincode/verified?__rnd=%d' % (time.time() * 1000)
            data = { "secode":code, "type":"sass", "pageid":"wb", "_t":"0" }
            self.session.headers.update({'Content-Type':'application/x-www-form-urlencoded'})
            self.session.headers.update({'X-Requested-With':'XMLHttpRequest'})
            self.session.headers.update({'Origin':'http://s.weibo.com'})
            clen = self.get_page_concent(curl, meth='POST', data=data, timeout=30, type=1)
            try:
                print '-'*20, 'response content:',clen.content
                res_len  = clen.headers.get('Content-Length','36')
                if res_len == "80":
                    return True
                else:
                    return False
            except Exception,e:
                print e

    def handle_pages(self, url_head, page_count, movie_id, start_time, end_time, content):
        if page_count == 0 or page_count == 1:
            self.page_count += 1
            print 'Main Start Page', '-' * 90, '->:%02d' % 1, '/', '%02d .' % page_count, 'All Data Page:', self.page_count
            return self.movie.decode_content(content, movie_id, start_time, end_time, 'Main-->')
        for i in xrange(1, page_count + 1):
            self.page_count += 1
            url = url_head + "&page=%s" % i
            # print 'Start Page', '-' * 90, '->:%02d' % i, '/', '%02d .' % page_count, 'All Data Page:', self.page_count
            # print url
            self.que.put((url, self.page_count, [i, page_count]))
            self.task += 1
        if self.task < (len(self.users) * 2):
            print '任务过少, 累积任务.'
            return
        #n = random.choice([1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10])
        # print 'End  Page', '-' * 90, '->:%02d' % i, '/', '%02d' % page_count, 'while sleep %ss...\n' % n
        try:
            if not self.is_start_thread:
                threads = []
                for index, i in enumerate(self.users):
                    threads.append(ThreadMain(self.movie_name, start_time, end_time,movie_id, i[2], i[0], i[1], self.que, self.movie, index + 1))
                for i in threads:
                    #i.setDaemon(True)
                    i.start()
                self.is_start_thread = True
                time.sleep(2)
           #threadnum = [int(i.getName().split('-')[-1]) for i in threading.enumerate() if str(type(i)) == str("<class '__main__.ThreadMain'>")]
           #if len(threadnum) < len(self.users):
           #    l1 = range(1, len(self.users) + 1)
           #    [l1.remove(i) for i in threadnum if i in l1]
           #    for index, i in enumerate(self.users):
           #        threads.append(ThreadMain(self.movie_name, start_time, end_time,movie_id, i[2], i[0], i[1], self.que, self.movie))
           #    for i in threads:
           #        #i.setDaemon(True)
           #        i.start()
           #    self.is_start_thread = True
           #    print threading.current_thread()
        except KeyboardInterrupt:
            word = 'Will Exit?\tY/N\n'
            key = raw_input(word)
            if key.lower() == 'y':
                word += 'Will Exit,Please waiting 3s!'
                print word
                time.sleep(3)
                word +=  '\nEnd All.'
                print word
                exit(9)
            else:pass
        return

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

class ThreadMain(threading.Thread):
    """docstring for ThreadMain"""
    def __init__(self, movie_name, start_time, end_time, movie_id, session, user, passwd, que, movie, name = ""):
        super(ThreadMain, self).__init__()
        if name:
            self.name = 'Thread-%s' % name
        self.session = session
        self.movie = movie
        self.movie_name = movie_name
        self.movie_id = movie_id
        self.start_time = start_time
        self.end_time = end_time
        self.user = user
        self.passwd = passwd
        self.scode = 'http://sonidigg.xicp.net:9000/code'
        #self.scode = 'http://192.168.1.2:9999/code'
        self.que = que
        self.time = threading.Timer(3600 * 2, self.relogin)
        self.time.start()
        time.sleep(1)

    def time_cancel(self):
        print '%s: Cancel Timer' % self.name
        self.time.cancel()
        for i in threading.enumerate():
            if type(i) == threading._Timer:
                i.cancel()

    def run(self):
        global END_ALL
        while True:
            if END_ALL:
                self.time_cancel()
                return
            if not self.que.empty():
                url = self.que.get(block=False,timeout=1)
                if type(url) == tuple:
                    url, self.page_count, self.page = url
                if type(url) == list:
                    print url[1]
                    url[2] += str(datetime.datetime.now())
                    print url[2]
                    url[0](url[1], url[2], url[3])
                    self.write_tasks(url[4])
                    if len(url) == 6 and url[-1] == True:
                        print '='*150
                        print ' '*50, 'End All!'
                        END_ALL = True
                        self.time_cancel()
                        return
                    time.sleep(1)
                    continue
                self.handle_pages(url)
                self.que.task_done()
            else:
                time.sleep(1)
                print self.name, 'not have task, wait..'

    def relogin(self):
        print '=' * 160, '\b>', self.name, 'RE Login ... '
        if END_ALL:
            return
        self.session = login(self.user, self.passwd)
        self.time = threading.Timer(3600 * 5, self.relogin)
        self.time.start()

    def write_tasks(self, line):
        with file('task','r')as f:
            r = f.read()
            r = r.splitlines()
            r[line] += '\t-------OK'
        with file('task','w')as f:
            f.write('\n'.join(r))

    def get_page_concent(self, url, meth='GET', data={}, timeout=20, files={}, type=0):
        try_num = 0
        while True:
            try:
                if files:
                    #print files
                    #content = self.session.post(url,files=files).content
                    #return content
                    r = requests.post(url,files=files)
                    #print self.name, r.request.headers
                    return r.content
                if type == 1:
                    return self.session.post(url, timeout=timeout,data=data, files=files)
                content = self.session.request(meth, url, timeout=timeout,data=data, files=files).content
                return content
            except Exception,e:
                print self.name, e
                try_num += 1
                if try_num > 5:
                    continue
                else:
                    return ""

    def clear_code(self, timeout=30, url='http://s.weibo.com/wb/5&timescope=custom:2014-05-17%3A2014-05-17', content=''):
        'timeout --> 处理验证码的等待时间.'
        self.session.headers.update({'Referer':url})
        self.session.headers.update({'Host':'s.weibo.com'})
        self.session.headers.update({'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'})
        self.session.headers.update({'Accept-Encoding':'gzip,deflate,sdch'})
        self.session.headers.update({'Accept-Language':'zh,zh-CN;q=0.8,zh-TW;q=0.6'})
        print self.name, 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        iurl = 'http://s.weibo.com/' + re.findall(r'img.+?=\\"(.+?)\\".+?yzm_img',content)[0].replace('\\','')
        itime = 0
        while itime <= SEARCH_SLEEP_TIME:
            code = self.get_code(iurl, timeout=timeout)
            print self.name, 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            res_code = self.post_code(code, url=url)
            print self.name, 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            print self.name, 'Code reboot result:', res_code,
            if res_code:
                print self.name, 'Successfull!'
                return True
            else:
                print self.name, 'Failed.'
                time.sleep(1)
                itime += 1

    def is_rebot(self, content, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            return True
        else:
            return False

    def mail_code(self, url):
        send_mail('Sina weibo -- reboot!!机器人', ('user: ' + self.user + '\n pwd:' + self.passwd + '\n' + url))

    def get_code(self, url, timeout=30):
        print self.name, 'get code img'
        self.session.headers.update({'Cache-Control':'max-age=0'})
        content = self.get_page_concent(url)
        del self.session.headers['Cache-Control']
        print self.name, 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        with file('code/code.png','wb')as f:
            f.write(content)
            f.flush()
        files={'code.png':content}
        code = self.get_page_concent(self.scode, 'GET', data={'img':content}, timeout=timeout, files=files)
        #code = raw_input('\ncode:')
        if len(code) == 4:
            with file('code/' + code + '.png','wb')as f:
                f.write(content)
            return code

    def post_code(self, code, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        if code and len(code) == 4:
            curl = 'http://s.weibo.com/ajax/pincode/verified?__rnd=%d' % (time.time() * 1000)
            data = { "secode":code, "type":"sass", "pageid":"wb", "_t":"0" }
            self.session.headers.update({'Content-Type':'application/x-www-form-urlencoded'})
            self.session.headers.update({'X-Requested-With':'XMLHttpRequest'})
            self.session.headers.update({'Origin':'http://s.weibo.com'})
            clen = self.get_page_concent(curl, meth='POST', data=data, timeout=30, type=1)
            try:
                print self.name, '-'*20, 'response content:',clen.content
                res_len  = clen.headers.get('Content-Length','36')
                if res_len == "80":
                    return True
                else:
                    return False
            except Exception,e:
                print self.name, e

    def handle_pages(self, url):
        earliest_time = False
        print self.name, 'Start Page', '-' * 90, '->:%02d' % self.page[0], '/', '%02d .' % self.page[1], 'All Data Page:', self.page_count
        print
        print '-' * 120
        print self.name, url
        print '-' * 120
        print
        error_num = 0
        while True:
            content = self.get_page_concent(url)
            with file('contents.html', 'w')as f:
                f.write(content)
            code_num = 0
            if self.is_rebot(content):
                code_num += 1
                if code_num >= 10:
                    self.mail_code(url)  
                print self.name, '变机器人了,需要帮助'
                self.clear_code(timeout=10, url=url, content=content)
                continue
            earliest_time = self.movie.decode_content(content, self.movie_id, self.start_time, self.end_time, self.name)
            if not earliest_time:
                break
            if earliest_time == 'page_error':
                time.sleep(1)
                error_num += 1
                if error_num <= 1:
                    continue
                else:
                    break
            n = random.choice([1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10])
            n = 0
            # print 'End  Page', '-' * 90, '->:%02d' % i, '/', '%02d' % page_count, 'while sleep %ss...\n' % n
            time.sleep(n)
            break
        time.sleep(2)
        return earliest_time

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

def kill(main):
    while True:
        if END_ALL == True:
            main.cancel()
            return True
        try:
            time.sleep(1)
        except (KeyboardInterrupt, SystemExit):
            word = 'Exit?\ty/n\n'
            key = raw_input(word)
            if key.lower() == 'y':
                word += 'Will Exit,Please waiting 3s!'
                print word
                time.sleep(3)
                word +=  '\nEnd All.'
                print word
                exit(9)
            else:pass

def get_tasks_list():
    tasks=[]
    with file('task','r')as f:
        r = f.read().strip()
        r = r.splitlines()
        for line, i in enumerate(r):
            if i.find('#') >= 0:
                print i, 'Has been writed notes"#"!'
                continue
            task = i.strip().split('\t')
            if len(task) == 3:
                task_type = True
                if task[1].count('-') >=3 and task[2].count('-') >= 3:
                    task_type = False
                tasks.append({'movie_name':task[0],'start_time':task[1],'end_time':task[2], 'task_type':task_type, 'line': line})
                print '\t'.join(task), 'Add new task!'
                continue
            elif len(task) < 3:
                print '\t'.join(task), 'SETTING ERROR!'
                continue
            elif len(task) > 3 and i.find('----OK'):
                print '\t'.join(task), 'Has been completed!'
                continue
    return tasks

def main():
    task_list = get_tasks_list()
    if not task_list:
        exit('Not have task.')
    main_class = Controller(task_list)
    main_class.start()
    kill(main_class)

if __name__ == '__main__':
    #hour_down(['的','啊'], '2014-05-11-00', '2014-05-11-08',pro=False,city=False)

    #big_down(['不将就'], '2014-04-01', '2014-05-20')
    main()
