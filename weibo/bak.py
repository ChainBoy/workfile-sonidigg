#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import datetime
from movie import Movie
from urllib import quote
from send_mail import send_mail
import time
import random
from weibo_tools import login, USER_NAME, PASSWORD
import city
import requests


REBOT_SLEEP_TIME = 120
SEARCH_SLEEP_TIME = 3

class Controller(object):
    """docstring for Controller"""
    def __init__(self, movie_name, start_time, end_time):
        super(Controller, self).__init__()
        self.session = login()
        # self.session = None
        self.movie_name = movie_name
        self.start_time = start_time
        self.end_time = end_time
        self.movie = Movie(self.movie_name, self.session)
        self.url = 'http://s.weibo.com/wb/'
        self.scode = 'http://sonidigg.xicp.net:9000/code'
        #self.scode = 'http://192.168.1.2:9999/code'
        self.page_count = 0
        self.get_province()

    def start(self):
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d')
        if start_time <= end_time:
            print 'Name:', self.movie_name
            print 'Time:', self.start_time, '--', self.end_time
            self.search_nomarl(start_time, end_time)
        else:
            print 'ERROR:起始时间必须比结束时间早!'

    def search_nomarl(self, start_time, end_time):
        '''普通搜索,All时间'''
        print 'search_nomarl:::'
        url = self.url + quote(self.movie_name)
        s_time = datetime.datetime.strftime(start_time, '%Y-%m-%d')
        e_time = datetime.datetime.strftime(end_time, '%Y-%m-%d')
        while True:
            timescope = s_time + ':' + e_time
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            content = self.get_page_concent(url)
            count = self.get_page_count(content)

            if count == 0:
                if self.is_rebot(content):
                    print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                    #time.sleep(REBOT_SLEEP_TIME)
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
                    return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            elif count < 50:
                print '数据小于50页，直接爬取'
                return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            # 如果页数超过50
            elif count == 50:
                print '数据等于50页，分片爬取'
                self.search_by_day(s_time, e_time)
                return 'End'

    def search_by_day(self, start_time, end_time):
        '''根据天做分割，如果在普通搜索不能完成时间段所有信息的时候使用'''
        print 'search_by_day:::'
        start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d')
            timescope = timescope + ':' + timescope
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)

                if count == 0:
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                        #time.sleep(REBOT_SLEEP_TIME)
                        self.clear_code(timeout=10, url=url, content=content)
                        continue
                    else:
                        print '数据0页～～！'
                    self.handle_pages(url, count, self.movie.id, start_time, end_time)
                    break
                # 如果页数低于50,直接爬取
                elif count < 50:
                    print '数据小于50页，直接爬取'
                    self.handle_pages(url, count, self.movie.id, end_time, end_time)
                    break
                # 如果页数超过50,分小时爬取
                elif count == 50:
                    print '数据等于50页，分片爬取'
                    self.search_by_hour(end_time)
                    break
            end_time = end_time - datetime.timedelta(days=1)
            if start_time > end_time:
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def spider_day_hour(self, search_province=False, search_city=False):
        '针对特定小时段落抓取'
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        start = True
        while True:
            if start:
                tmp_time = start_time
                start = False
            else:
                tmp_time = tmp_time + datetime.timedelta(hours=1)
            if tmp_time <= end_time:
                timescope = datetime.datetime.strftime(tmp_time, '%Y-%m-%d-%H')
                timescope = timescope + ':' + timescope
                url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
                print url
                while True:
                    content = self.get_page_concent(url)
                    count = self.get_page_count(content)
                    if count == 0:
                        if self.is_rebot(content, url):
                            print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME
                            #time.sleep(REBOT_SLEEP_TIME)
                            self.clear_code(timeout=10, url=url, content=content)
                            continue
                        else:
                            print '数据0页～～！'
                    # 如果页数低于50,直接爬取
                    elif count < 50:
                        print '数据小于50页，直接爬取'
                        self.handle_pages(url, count, self.movie.id, start_time, end_time)
                        break

                    # 如果页数超过50,分省抓取
                    elif count == 50:
                        if not search_province:
                            self.handle_pages(url, count, self.movie.id, start_time, end_time)
                            break
                        print '数据大于50页，分省爬取'
                        for province_code in self.provinces:
                            print '搜索:', self.citys[province_code]['name']
                            self.search_by_province(url, province_code, search_city)
                        break
                    self.handle_pages(url, count, self.movie.id, start_time, end_time)
                    break
                print 'while sleep %ss...' % SEARCH_SLEEP_TIME
                time.sleep(SEARCH_SLEEP_TIME)
            else:
                break

    def search_by_province(self, url, province_code, search_city=False):
        '分省搜索'
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:1000
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&region=custom:23:1000&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        url = url + '&region=custom:%s:' % province_code
        print url
        while True:
            content = self.get_page_concent(url)
            count = self.get_page_count(content)
            if count == 0:
                if self.is_rebot(content):
                    print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME
                    #time.sleep(REBOT_SLEEP_TIME)
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
            # 如果页数低于50,直接爬取
            elif count < 50:
                print '数据小于50页，直接爬取'
                self.handle_pages(url, count, self.movie.id, end_time, end_time)
                break
            # 如果页数超过50,分市抓取
            elif count == 50:
                if not search_city:
                    self.handle_pages(url, count, self.movie.id, start_time, end_time)
                    break
                print '数据等于50页，分市爬取'
                citys = self.citys[province_code]['city']
                for city_code in citys:
                    print '搜索:', self.citys[province_code]['name'], citys[city_code]
                    self.search_by_city(url, city_code)
                break
            self.handle_pages(url, count, self.movie.id, start_time, end_time)
            break
        print 'while sleep %ss...' % SEARCH_SLEEP_TIME
        time.sleep(SEARCH_SLEEP_TIME)

    def search_by_city(self, url, city_code):
        '分市搜索'
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:
        # http://s.weibo.com/wb/%25E4%25BA%25BA&xsort=time&timescope=custom:2014-05-06-0:2014-05-06-0&Refer=g&region=custom:23:1
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        url = '%s%s' % (url, city_code)
        print url
        while True:
            content = self.get_page_concent(url)
            count = self.get_page_count(content)
            if count == 0:
                if self.is_rebot(content):
                    print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME
                    #time.sleep(REBOT_SLEEP_TIME)
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                else:
                    print '数据0页～～！'
            self.handle_pages(url, count, self.movie.id, start_time, end_time)
            break
        print 'while sleep %ss...' % SEARCH_SLEEP_TIME
        time.sleep(SEARCH_SLEEP_TIME)

    def search_by_hour(self, day):
        '''针对一天按小时分割搜索'''
        print "search_by_hour:::"
        day = datetime.datetime.strftime(day, '%Y-%m-%d')
        l_hour = [i for i in range(0, 25)]
        # 1-24
        while True:
            i_day = l_hour.pop()
            print 'i_day', i_day
            if i_day == 24:
                end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')
                # end_time = end_time - datetime.timedelta(hours = 1)
            elif i_day == 0:
                break
            else:
                end_time = datetime.datetime.strptime(day + '-' + str(i_day), '%Y-%m-%d-%H')
                end_time = end_time - datetime.timedelta(hours=1)
            start_time = end_time

            timescope = datetime.datetime.strftime(start_time, '%Y-%m-%d-%H')
            timescope = timescope + ':'
            timescope = timescope + datetime.datetime.strftime(end_time, '%Y-%m-%d-%H')
            url = self.url + quote(self.movie_name) + '&timescope=custom:' + quote(timescope) + '&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)

                if int(count) == 0 or count == '0':
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME

                        #time.sleep(REBOT_SLEEP_TIME)
                        self.clear_code(timeout=10, url=url, content=content)
                        continue
                    else:
                        print '数据0页～～！'
                self.handle_pages(url, count, self.movie.id, start_time, end_time)
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def get_page_concent(self, url, meth='GET', data={}, timeout=20, files={}, type=0):
        try_num = 0
        while True:
            try:
                if files:
                    #print files
                    #content = self.session.post(url,files=files).content
                    #return content
                    r = requests.post(url,files=files)
                    print r.request.headers
                    return r.content
                if type == 1:
                    return self.session.post(url, timeout=timeout,data=data, files=files)
                content = self.session.request(meth, url, timeout=timeout,data=data, files=files).content
                return content
            except Exception,e:
                print e
                try_num += 1
                if try_num > 5:
                    continue
                else:
                    return ""

    def get_province(self):
        self.citys = city.start()
        self.provinces = city.province_list

    def get_page_count(self, content):
        max_count = 0
        with file('aaaaaaaaaaaaaa.html', 'w')as f:
            f.write(content)
        result = re.findall(r'&page=(\d+)', content)
        result.append(0)
        print result
        max_count = max([int(i) for i in result])
        print 'All Page:', result, 'Max Page:-->', max_count
        return max_count

    def clear_code(self, timeout=30, url='http://s.weibo.com/wb/5&timescope=custom:2014-05-17%3A2014-05-17', content=''):
        'timeout --> 处理验证码的等待时间.'
        self.session.headers.update({'Referer':url})
        self.session.headers.update({'Host':'s.weibo.com'})
        self.session.headers.update({'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'})
        self.session.headers.update({'Accept-Encoding':'gzip,deflate,sdch'})
        self.session.headers.update({'Accept-Language':'zh,zh-CN;q=0.8,zh-TW;q=0.6'})
        print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        iurl = 'http://s.weibo.com/' + re.findall(r'img.+?=\\"(.+?)\\".+?yzm_img',content)[0].replace('\\','')
        itime = 0
        while itime <= SEARCH_SLEEP_TIME:
            code = self.get_code(iurl, timeout=timeout)
            print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            res_code = self.post_code(code, url=url)
            print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
            print 'Code reboot result:', res_code,
            if res_code:
                print 'Successfull!'
                return True
            else:
                print 'Failed.'
                time.sleep(1)
                itime += 1

    def is_rebot(self, content, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            send_mail('机器人', ('user: ' + USER_NAME + '\n pwd:' + PASSWORD + '\n' + url))
            return True
        else:
            return False

    def get_code(self, url, timeout=30):
        print 'get code img'
        self.session.headers.update({'Cache-Control':'max-age=0'})
        content = self.get_page_concent(url)
        del self.session.headers['Cache-Control']
        print 'ULOGIN_IMG', self.session.cookies.get('ULOGIN_IMG')
        with file('code/code.png','wb')as f:
            f.write(content)
            f.flush()
        files={'code.png':content}
        code = self.get_page_concent(self.scode, 'GET', data={'img':content}, timeout=timeout, files=files)
        #code = raw_input('\ncode:')
        if len(code) == 4:
            with file('code/' + code + '.png','wb')as f:
                f.write(content)
            return code

    def post_code(self, code, url='http://s.weibo.com/wb/568&timescope=custom:2014-05-17%3A2014-05-17&xsort=time&nodup=1'):
        if code and len(code) == 4:
            curl = 'http://s.weibo.com/ajax/pincode/verified?__rnd=%d' % (time.time() * 1000)
            data = { "secode":code, "type":"sass", "pageid":"wb", "_t":"0" }
            self.session.headers.update({'Content-Type':'application/x-www-form-urlencoded'})
            self.session.headers.update({'X-Requested-With':'XMLHttpRequest'})
            self.session.headers.update({'Origin':'http://s.weibo.com'})
            clen = self.get_page_concent(curl, meth='POST', data=data, timeout=30, type=1)
            try:
                print '-'*20, 'response content:',clen.content
                res_len  = clen.headers.get('Content-Length','36')
                if res_len == "80":
                    return True
                else:
                    return False
            except Exception,e:
                print e
            #content = self.get_page_concent(url)
            #isreboot = self.is_rebot(content)
            #if not isreboot:
            #    self.session = login()
            #    return True
            #else:
            #    print '还是机器人?'
            #    return False

    def handle_pages(self, url_head, page_count, movie_id, start_time, end_time):
        earliest_time = False
        if page_count == 0: page_count = 1
        for i in xrange(1, page_count + 1):
            self.page_count += 1
            url = url_head + "&page=%s" % i
            print 'Start Page', '-' * 90, '->:%02d' % i, '/', '%02d .' % page_count, 'All Data Page:', self.page_count
            print url
            isreboot = False
            isear = False
            error_num = 0
            while True:
                content = self.get_page_concent(url)
                isreboot = self.is_rebot(content)
                if isreboot:
                    print '变机器人了,需要帮助,while sleep %ss...' % REBOT_SLEEP_TIME
                    #time.sleep(REBOT_SLEEP_TIME)
                    self.clear_code(timeout=10, url=url, content=content)
                    continue
                earliest_time = self.movie.decode_content(content, movie_id, start_time, end_time)
                if not earliest_time:
                    isear = True
                    break
                if earliest_time == 'page_error':
                    time.sleep(1)
                    error_num += 1
                    if error_num <= 1:
                        continue
                    else:
                        break
                n = random.choice([1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10])
                n = 0
                print 'End  Page', '-' * 90, '->:%02d' % i, '/', '%02d' % page_count, 'while sleep %ss...\n' % n
                time.sleep(n)
                break
            if isear or error_num >= 3:
                break
            time.sleep(2)
        return earliest_time

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

def big_down(movies=['的'], start_time='2014-03-01', end_time='2014-03-11'):
    index = 0
    for movie in movies:
        index += 1
        start_work_time = datetime.datetime.now()
        Controller(movie, start_time, end_time).start()
        con = '"' + movie + '" ' + start_time + ' -- ' + end_time + ' 数据已经爬取完成。' + '\nStart word time:' + str(start_work_time) + '.\nEnd word time:' + str(datetime.datetime.now())
        send_mail('Spider END.%s' % index, con)

def hour_down(movies=['的'], start_time='2014-03-01-04', end_time='2014-03-11-07', pro=True,city=True):
    index = 0
    for movie in movies:
        print movie
        index += 1
        start_work_time = datetime.datetime.now()
        Controller(movie, start_time, end_time).spider_day_hour(pro, city)
        con = '"' + movie + '" ' + start_time + ' -- ' + end_time + ' 数据已经爬取完成。' + '\nStart word time:' + str(start_work_time) + '.\nEnd word time:' + str(datetime.datetime.now())
        send_mail('Spider END.%s' % index, con)


if __name__ == '__main__':
    #s = "赵传,叶世荣,周柏豪,许艺娜,朱克,温兆伦,EXO-M,全球中文音乐榜上榜"
    #hour_down(l, '2014-04-15', '2014-05-15', pro=False, city=False)
    l = '的'.split(',')
    hour_down(l, '2014-05-11-00', '2014-05-11-08',pro=False,city=False)

    l = 'DJ'.split(',')
    big_down(l, '2014-04-15', '2014-05-10')

    l = 'DJ推荐'.split(',')
    big_down(l, '2014-04-15', '2014-05-15')

    l = '喜马拉雅听,蜻蜓FM'.split(',')
    big_down(l, '2014-05-10', '2014-05-16')

    l = '龙卷风,酷狗FM,考拉FM,优听radio,荔枝FM,多听FM'.split(',')
    big_down(l, '2014-04-15', '2014-05-15')

