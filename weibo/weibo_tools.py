#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import re
import base64
import urllib
import rsa, binascii

USER_NAME = "b1n2h3"
PASSWORD = "nihao1234"

URL_PRELOGIN = 'http://login.sina.com.cn/sso/prelogin.php?entry=weibo&callback=sinaSSOController.preloginCallBack&su=&rsakt=mod&client=ssologin.js(v1.4.5)&_=1364875106625'
URL_LOGIN = 'http://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.4.5)'

def login(name="b1n2h3", password="nihao1234"):
    session = requests.Session()
    session.headers.update({'User-Agent':'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'})
    ret = session.get(URL_PRELOGIN)
    data = json.loads(re.findall('({.*?})', ret.content)[0])

    servertime = data['servertime']
    nonce = data['nonce']
    pubkey = data['pubkey']
    rsakv = data['rsakv']

    su = base64.b64encode(urllib.quote(name))
    rsaPublickey = int(pubkey, 16)
    key = rsa.PublicKey(rsaPublickey, 65537)
    message = str(servertime) + '\t' + str(nonce) + '\n' + str(password)
    sp = binascii.b2a_hex(rsa.encrypt(message, key))

    postdata = {
                    'entry': 'weibo',
                    'gateway': '1',
                    'from': '',
                    'savestate': '7',
                    'userticket': '1',
                    'ssosimplelogin': '1',
                    'vsnf': '1',
                    'vsnval': '',
                    'su': su,
                    'service': 'miniblog',
                    'servertime': servertime,
                    'nonce': nonce,
                    'pwencode': 'rsa2',
                    'sp': sp,
                    'encoding': 'UTF-8',
                    'url': 'http://weibo.com/ajaxlogin.php?framelogin=1&callback=parent.sinaSSOController.feedBackUrlCallBack',
                    'returntype': 'META',
                    'rsakv' : rsakv,
                    }

    r = session.post(URL_LOGIN, data=postdata)
    login_url = re.findall("replace\('(.*)'\)", r.content)
    if login_url and len(login_url) > 0:
        r = session.get(login_url[0])
        with file('login.html','w')as f:
            f.write(r.content.decode('gbk').encode('utf8'))
        print 'login.html save ok'
    login_re = re.findall(r"result*\"\:true",r.content)
    if login_re:
        print name,'Login OK.'
        return session
    else:
        print name,'Login ERROR.'
        if len(re.findall(r"result*\"\:false",r.content))>0:
            return False

def logins():
    accounts = []
    with file('accounts','r')as f:
        accounts = f.read().splitlines()
        if not accounts:
            return login()
        for i, j in enumerate(accounts):
            user, passwd = j.split('\t')
            session = login(user, passwd)
            accounts[i] = [user, passwd, session]
    return accounts

if __name__ == '__main__':
	login()
