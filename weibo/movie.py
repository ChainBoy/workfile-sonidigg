#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#STEP2第2步,规模化抓取50页用户名，地区分布，性别
#sinaweibo登陆，非官方API实现方式，采用python语言强制性访问新浪链接地址，强制传输数据
1. 新浪搜索限制是10s一个请求，单IP可以10个用户同时搜索，最多返回50页，超过50页的请求从第一页重新开始
2. 新浪设置了防爬虫设置，网页加载采用javascript动态加载的方式，阻断了网络爬虫爬静态页面
3. 数据加载采用ajax动态加载，只有用户点击或者悬停在上之后才触发进一步请求
4. 前几天还允许不能路进行察看页面，现在只有登陆才能察看评论用户ajax数据
5. 会判定程序为机器人，新浪服务器会阻断连接，需要重新人工输入验证码
6. 网页编码进行了特殊设置，需要进行编码处理，不然无法获取网页信息
"""
import re, time
import simplejson
import jieba
import jieba.analyse
import string
import codecs
from urllib import quote
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import model
from keyword_handle import Keyword_Handler
import datetime

class Movie(object):
    """docstring for Movie"""
    def __init__(self, name, session):
        super(Movie, self).__init__()
        engine = create_engine(model.db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine))
        self.name = name
        self.session = session
        self.id = self.init_movie()

    def init_movie(self):
        movie = self.db.query(model.Movie).filter(model.Movie.name == self.name).first()
        if not movie:
            print "DB Not have the movie name, Add new movie:",movie
            movie = model.Movie(name=self.name)
            self.db.add(movie)
            self.db.commit()
        return movie.id

    def decode_content(self, content, movieid,  start_time, end_time, threadname=""):
        #result = re.findall('<script>STK && STK.pageletM && STK.pageletM.view\(({"pid":"pl_weibo_direct",.*?)\)<\/script>', content)
        result = re.findall('<script>STK && STK.pageletM && STK.pageletM.view\(({"pid":"pl_wb_feedlist",.*?)\)<\/script>', content)
        earliest_time = False
        if result:
            result = result[0]
            r = result.encode("utf-8").decode('unicode_escape').encode("utf-8")
            record_line = r.replace("\/", "/")
            if self.check_page_really(record_line):
                return 'page_error'
            items = re.findall(r'<p node-type="feed_list_content">(.*?)</p>.*?<p class="info W_linkb W_textb">(.*?)</p>', record_line, re.S)
            # end_time = end_time - datetime.timedelta(days=-1)
            for i,item in enumerate(items):
                detail, strf = self.resolver(item)
                earliest_time = datetime.datetime.strptime(detail[10].encode("utf-8"), '%Y-%m-%d %H:%M')
                print threadname, '%02d:' % (i+1), 'The Date Time:', earliest_time,'Now Time Line:', start_time, '～', end_time,' --> ',detail[5]
                if earliest_time > end_time - datetime.timedelta(days=-1):
                    if start_time != end_time:
                        print threadname, '%02d' % (i+1),' :信息时间超过了限制最晚时间，忽略该信息!'
                        continue
                if earliest_time < start_time:
                    if start_time != end_time:
                        print threadname, start_time,end_time,earliest_time
                        print threadname, '%02d' % (i+1),' :信息时间比限制时间早，爬取结束!'
                        earliest_time = False
                        break
                #self.keywording(strf, movieid)
                self.db.add(model.Detail(movie=movieid, address=detail[0],gender=int(detail[1]), focus_num=int(detail[2]),fans_num=int(detail[3]), wb_num=int(detail[4]),nick_name=detail[5],daren=int(detail[6]),vip=int(detail[7]),face=detail[8],follows=detail[9],time=detail[10],ping_num=int(detail[11]),zan_num=int(detail[12]),zhuan_num=int(detail[13]),content=detail[14]))
            self.db.commit()
        else:
            print threadname, '当前页面无内容.'
            return 'page_error'
        return earliest_time

    def resolver(self, contuple):
        assert isinstance(contuple, tuple), "resolver wrong"
        nick_name = re.findall(r'nick-name="(.*?)"',contuple[0]) + [""]
        daren = re.findall(r'node-type="daren"',contuple[0]) + [""]
        face = re.findall(r'alt="\[(.*?)\]"\stype="face"', contuple[0]) + [""]
        #uid = re.findall(r'weibo_feed_.*?:(\d+)"', contuple[0])
        uid = re.findall(r'usercard="id=(\d+)&usercardkey=', contuple[0])

        follows = re.findall(r'rel="nofollow">(.*?)<', contuple[1]) + [""]
        time = re.findall(r'title="(.*?)"\sdate="[0-9]*?"',contuple[1]) + [""]
        ping = re.findall(r'评论\((\d+)\)', contuple[1]) + ["0"]
        zan = re.findall(r'>赞</em>\((\d+)\)</a>', contuple[1]) + ["0"]
        zhuan = re.findall(r'转发\((\d+)\)', contuple[1]) + ["0"]
        content = re.findall(r'<em>(.*?)</em>', contuple[0]) + [""]
        vip = re.findall(r'class="approve_co"', contuple[0]) + [""]
        new_content = re.sub("<.*?>", "", content[0])
        detail = []
        vip = u'1' if vip[0] else u'0'
        daren = u'1' if daren[0] else u'0'
        try:
            detail = self.ajaxres(uid[0])
        except:
            detail = [u""]+[u'0']*4
        detail.extend([nick_name[0].decode("utf-8"), daren, vip, face[0].decode("utf-8"), follows[0].decode("utf-8"), time[0].decode("utf-8"), ping[0].decode("utf-8"), zan[0].decode("utf-8"), zhuan[0].decode("utf-8"), new_content.decode("utf-8")])
        return detail, new_content

    def check_page_really(self, content):
        error_info = re.findall(r'noresult_tit',content)#noresult_tit,您可以尝试更换关键词，再次搜索
        if len(error_info) > 0:
            print '新浪微博页面错误，当前页面没有结果!'
            return True
        else:
            return False

    def keywording(self, strf, movie):
        keywords =  jieba.analyse.extract_tags(strf, topK = 10)
        for keyword in keywords:
            if keyword:
                handler = Keyword_Handler(model.db_path)
                handler.handle(keyword.encode('utf-8'), movie)

    def ajaxres(self, uid):
        detail = []
        url = "http://s.weibo.com/ajax/user/card?id=" + uid + "&usercardkey=weibo_mp" + "&_t=0&__rnd=1384487536112"
        ajaxresponse = self.session.get(url)
        jsonloads = simplejson.loads(ajaxresponse.content)['data']
        address = re.findall(r'<p class="address">(.*?)<', jsonloads) + [""]
        gender = re.findall(r'<span class="(.*?) m_icon"', jsonloads) + [""]
        fans = re.findall(r'\s(\d+)', jsonloads) + [""]
        gender = u'1' if gender[0] =='male' else u'0'
        try:
            detail = [address[0], gender[0], fans[0], fans[1], fans[2]]
        except:
            return [address[0],gender[0], u"0",u"0", u"0"]
        return detail
