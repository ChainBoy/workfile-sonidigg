#!/bin/python
# -*- coding: utf-8 -*- 

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import jieba.analyse
import model #import Movie_Keyword, Movie, Detail
from movie import Movie
import sys, time


class New_Keyword(object):
    """movie_names is a List; Ex:['movie1', 'movie2', 'movie3']
        addresss is a List; Ex:['Beijing', 'BeiJing HaiDian', 'GuangDong', 'ShanDong TaiAn']
        start_time,end_time is string time; Ex: '2013-11-12' or '2013-11-12 16:20:30"""
    def __init__(self, movie_names, start_time, end_time, addresss):
        super(New_Keyword, self).__init__()
        engine = create_engine(model.db_path, convert_unicode=True, pool_recycle=7200)
        engine.connect()
        self.db = scoped_session(sessionmaker(bind=engine))
        self.names = movie_names
        self.movie_ids = self.init_movie()
        self.addresss = addresss
        self.start_time = start_time
        self.end_time = end_time
        self.kv = {}

    def init_movie(self):
        '''
        get movie ids
        '''
        movie_ids = []
        for name in self.names:
            movie_id = self.db.query(model.Movie).filter(model.Movie.name == name).first().id
            if not movie_id:
                print "数据库电影表中没有这部电影:",name
            movie_ids.append(movie_id)
        return movie_ids

    def cut_keyword(self, strf):
        keywords =  jieba.analyse.extract_tags(strf, topK = 10)
        for i,k in enumerate(keywords):
            if k:
                self.kv[k] = 1 if self.kv.get(k, None) == None else self.kv[k] + 1
            s = str('%06.2f' % (i+1 / float(len(keywords)) * 100))
            if i+1 == len(keywords) : s = '100.00'
            sys.stdout.write('\rCut keyword: ' + s + '%')
            sys.stdout.flush()
            time.sleep(0.002)
            #'^[a-zA-Z0-9]{1,2}$|^[a-zA-Z0-9]{6,7}$|##|http|cn|^/./.$|^/…$'
    def clear_word(self,rex = '^[a-zA-Z0-9]{1,2}$|^[a-zA-Z0-9]{6,7}$|##|http|cn|^/./.$|^/…$'):
        res_dict = {}
        for i in self.kv:
            r = re.findall('^[a-zA-Z0-9]{1,2}$|^[a-zA-Z0-9]{6,7}$|##|http|cn|^/./.$|^/…$',i)
            if not r:
                res_dict[i] = self.kv[i]
        self.kv = res_dict
        del res_dict
    
    def get_wb(self):
        #wbs = self.db.query(model.Detail).filter(model.Detail.time.between(self.start_time,self.end_time),model.Detail.movie.in_(self.movie_ids),model.Detail.address.op('regexp')('|'.join(self.addresss)))
        wbs = self.db.query(model.Detail).filter(model.Detail.time.between(self.start_time,self.end_time),model.Detail.movie.in_(self.movie_ids))
        for k,wb in enumerate(wbs):
            s = str('%06.2f' % (k / float(wbs.count()) * 100))
            if k+1 == wbs.count() : s = '100.00'
            sys.stdout.write('\r                               All: ' + s + '%')
            self.cut_keyword(wb.content)
            sys.stdout.flush()
        print '\n'


if __name__ == '__main__':
    #movie_names, start_time, end_time, addresss
    #movie_names = ['怒放2013', '2013怒放']
    movie_names = ['庄心妍']
    start_time = '2014-05-10 19:00:00'
    end_time = '2014-05-15 19:59:59'
    addresss = ['',]# '重庆']
    Keyword = New_Keyword(movie_names, start_time, end_time, addresss)
    Keyword.get_wb()
#    with file(','.join(movie_names) + ';' + ','.join(addresss) + ';' + start_time + '~' + end_time + '.xls', 'w') as f:
#        f.writelines('keyword'+','+'num\n')
#        for k in Keyword.kv:
#            f.writelines(k.encode('utf-8') +','+ str(Keyword.kv[k]) + '\n')
#
    with file(','.join(movie_names) + '-' + ','.join(addresss) + '-' + start_time + '~' + end_time + '.xls', 'w') as f:
        f.writelines('keyword'+','+'num\n')
        r = sorted(Keyword.kv.iteritems(), key=lambda d:d[1])
        for k in r:
            f.writelines(k[0].encode('utf-8') +'\t'+ str(k[1]) + '\n')
