#!/bin/python
# -*- coding: utf-8 -*-

import urllib, urllib2, re, time
import datetime
import requests
from urllib  import quote
from movie import Movie
import codecs
import datetime
import time
import random
from weibo_tools import login


REBOT_SLEEP_TIME = 120
SEARCH_SLEEP_TIME = 30

class Controller(object):
    """docstring for Controller"""
    def __init__(self, movie_name, start_time,end_time):
        super(Controller, self).__init__()
        self.session = login()
        #self.session = None
        self.movie_name = movie_name
        self.start_time = start_time
        self.end_time = end_time
        self.movie = Movie(self.movie_name, self.session)
        #self.url = 'http://s.weibo.com/weibo/'
        self.url = 'http://s.weibo.com/wb/'
        self.page_count = 0

    def start(self):
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d')
        if start_time <= end_time:
            print 'Name:', self.movie_name
            print 'Time:', START_DAY,'--',END_DAY
            self.search_nomarl(start_time, end_time)
        else:
            print 'ERROR:起始时间必须比结束时间早!'

    def search_nomarl(self, start_time, end_time):
        '''普通搜索,All时间'''
        print 'search_nomarl:::'
        url = self.url+ quote(self.movie_name)
        s_time = datetime.datetime.strftime(start_time, '%Y-%m-%d')
        e_time = datetime.datetime.strftime(end_time, '%Y-%m-%d')
        while True:
            timescope = s_time +':' + e_time
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)+ '&xsort=time&nodup=1'
            print url
            content = self.get_page_concent(url)
            count = self.get_page_count(content)

            if count == 0:
                if self.is_rebot(page_content):
                    print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                    time.sleep(REBOT_SLEEP_TIME)
                    continue
                else:
                    print '数据0页～～！'
                    return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            elif count < 50:
                print '数据小于50页，直接爬取'
                return self.handle_pages(url, count, self.movie.id, start_time, end_time)
            #如果页数超过50
            elif count == 50:
                print '数据等于50页，分片爬取'
                self.search_by_day(s_time, e_time)
                return 'End'

    def search_by_day(self, start_time, end_time):
        '''根据天做分割，如果在普通搜索不能完成时间段所有信息的时候使用'''
        print 'search_by_day:::'
        start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        while True:
            timescope = datetime.datetime.strftime(end_time, '%Y-%m-%d') 
            timescope = timescope +':' + timescope
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope)+ '&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)

                if count == 0:
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助, sleep %ss' % REBOT_SLEEP_TIME
                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        print '数据0页～～！'
                    self.handle_pages(url, count, self.movie.id, start_time, end_time)
                    break
                #如果页数低于50,直接爬取
                elif count < 50:
                    print '数据小于50页，直接爬取'
                    self.handle_pages(url, count, self.movie.id, end_time, end_time)
                    break
                #如果页数超过50,分小时爬取
                elif count == 50:
                    print '数据等于50页，分片爬取'
                    self.search_by_hour(end_time)
                    break
            end_time = end_time - datetime.timedelta(days=1)
            if start_time > end_time:
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def spider_day_hour(self):
        start_time = datetime.datetime.strptime(self.start_time, '%Y-%m-%d-%H')
        end_time = datetime.datetime.strptime(self.end_time, '%Y-%m-%d-%H')
        start = True
        while True:
            if start:
                tmp_time = start_time
                start = False
            else:
                tmp_time = tmp_time + datetime.timedelta(hours = 1)
            if tmp_time <= end_time:
                timescope = datetime.datetime.strftime(tmp_time, '%Y-%m-%d-%H')
                timescope = timescope + ':' + timescope
                url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope) +'&xsort=time&nodup=1'
                print url
                while True:
                    content = self.get_page_concent(url)
                    count = self.get_page_count(content)

                    if int(count) == 0 or count == '0':
                        if self.is_rebot(content):
                            print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME

                            time.sleep(REBOT_SLEEP_TIME)
                            continue
                        else:
                            print '数据0页～～！'
                    self.handle_pages(url, count, self.movie.id, start_time, end_time)
                    break
                print 'while sleep %ss...' % SEARCH_SLEEP_TIME
                time.sleep(SEARCH_SLEEP_TIME)
            else:
                break

    def search_by_hour(self, day):
        '''针对一天按小时分割搜索'''
        print "search_by_hour:::"
        day = datetime.datetime.strftime(day, '%Y-%m-%d')
        l_hour = [i for i in range(0 ,25)] #1-24
        while True:
            i_day = l_hour.pop()
            print 'i_day',i_day
            if i_day == 24:
                end_time = datetime.datetime.strptime(day + '-23', '%Y-%m-%d-%H')
                # end_time = end_time - datetime.timedelta(hours = 1)
            elif i_day == 0:
                break
            else :
                end_time = datetime.datetime.strptime(day + '-' + str(i_day), '%Y-%m-%d-%H')
                end_time = end_time - datetime.timedelta(hours = 1)
            start_time = end_time #- datetime.timedelta(hours=1)

            timescope = datetime.datetime.strftime(start_time, '%Y-%m-%d-%H') 
            timescope = timescope +':' 
            timescope = timescope + datetime.datetime.strftime(end_time, '%Y-%m-%d-%H') 
            url = self.url+ quote(self.movie_name) +'&timescope=custom:'+ quote(timescope) +'&xsort=time&nodup=1'
            print url
            while True:
                content = self.get_page_concent(url)
                count = self.get_page_count(content)

                if int(count) == 0 or count == '0':
                    if self.is_rebot(content):
                        print '变机器人了,需要帮助,while sleep %ss...' % SEARCH_SLEEP_TIME

                        time.sleep(REBOT_SLEEP_TIME)
                        continue
                    else:
                        print '数据0页～～！'
                self.handle_pages(url, count, self.movie.id, start_time, end_time)
                break
            print 'while sleep %ss...' % SEARCH_SLEEP_TIME
            time.sleep(SEARCH_SLEEP_TIME)

    def get_page_concent(self, url):
        try_num = 0
        while True:
            try:
                content = self.session.request('GET',url,timeout = 20).content
                return content
            except Exception,e:
                try_num += 1
                if try_num > 5:
                    continue
                else:
                    return ""


    def get_page_count(self, content):
        max_count = 0
        with file('aaaaaaaaaaaaaa.html','w')as f:
            f.write(content)
        result = re.findall(r'&page=(\d+)', content)
        result.append(0)
        print result
        max_count = max([int(i) for i in result])
        print 'All Page:', result, 'Max Page:-->', max_count
        return max_count

    def is_rebot(self, content):
        content = self.format_content(content)
        result = re.findall(r'我真滴不是机器人', content)
        if len(result) > 0:
            from send_mail import send_mail
            send_mail('机器人', '快填验证码！')
            return True
        else:
            return False

    def handle_pages(self, url_head, page_count, movie_id, start_time, end_time):
        earliest_time = False
        if page_count == 0: page_count = 1
        for i in xrange(1, page_count + 1):
            self.page_count += 1
            url = url_head +"&page=%s" % i
            print 'Start Page','-'*90, '->:%02d' % i,'/', '%02d .' % page_count, 'All Data Page:',self.page_count
            print url
            isreboot = False
            isear = False
            error_num = 0
            while True:
                content = self.get_page_concent(url)
                isreboot = self.is_rebot(content)
                if isreboot:
                    print '变机器人了,需要帮助,while sleep %ss...' % REBOT_SLEEP_TIME
                    time.sleep(REBOT_SLEEP_TIME)
                    continue
                earliest_time = self.movie.decode_content(content, movie_id, start_time, end_time)
                if not earliest_time:
                    isear = True
                    break
                if earliest_time == 'page_error' :
                    time.sleep(1)
                    error_num +=1
                    if error_num <= 1:
                        continue
                    else :
                        break
                n = random.choice([1,2,3,3,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,10])
                print 'End  Page','-'*90, '->:%02d' % i,'/', '%02d' % page_count, 'while sleep %ss...\n' % n
                time.sleep(n)
                break
            if isear or error_num >= 3:
                break
            time.sleep(2)
        return earliest_time

    def format_content(self, content):
        r = content.decode('unicode_escape').encode("utf-8")
        return r.replace("\/", "/")

def big_down(movies = ['的'], start_time = '2014-03-01', end_time = '2014-03-11'):
    index = 0
    for movie in movies:
        index += 1
        start_work_time = datetime.datetime.now()
        Controller(movie, start_time, end_time).start()
        from send_mail import send_mail
        con = '"' + movie +'" '+ start_time + ' -- '+ end_time +' 数据已经爬取完成。'+'\nStart word time:' + str(start_work_time) + '.\nEnd word time:' + str(datetime.datetime.now())
        send_mail('Spider END.%s' % index, con)

def hour_down(movies = ['的'], start_time = '2014-03-01-04', end_time = '2014-03-11-07'):
    index = 0
    for movie in movies:
        index += 1
        start_work_time = datetime.datetime.now()
        Controller(movie, start_time, end_time).spider_day_hour()
        from send_mail import send_mail
        con = '"' + movie +'" '+ start_time + ' -- '+ end_time +' 数据已经爬取完成。'+'\nStart word time:' + str(start_work_time) + '.\nEnd word time:' + str(datetime.datetime.now())
        send_mail('Spider END.%s' % index, con)


if __name__ == '__main__':
    #big_down(['全球中文音乐榜上榜'], '2014-03-01', '2014-03-11')
    hour_down(['的'], '2014-03-01-01', '2014-03-01-05')
